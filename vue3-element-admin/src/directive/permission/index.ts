import type { Directive, DirectiveBinding } from "vue";
import { useUserStoreHook } from "@/store/modules/user";
/**
 * 按钮权限
 */
export const permission: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    const { userInfo, permCodes: roleCodes } = useUserStoreHook().userLoginInfo;
    //99 为超级管理员
    if (userInfo?.userType === 99) {
      return true;
    }
    const { value } = binding;
    if (value) {
      const hasPerm = roleCodes?.some((perm) => {
        return value.includes(perm);
      });
      if (!hasPerm) {
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        el.parentNode && el.parentNode.removeChild(el);
      }
    } else {
      throw new Error('需要设置 权限! Like v-permission="DssPerKey.DssBasedata_Operate"');
    }
  },
};

/**
 * 角色权限指令

export const hasRole: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    const requiredRoles = binding.value;

    // 校验传入的角色值是否合法
    if (!requiredRoles || (typeof requiredRoles !== "string" && !Array.isArray(requiredRoles))) {
      throw new Error(
        "需要提供角色标识！例如：v-has-role=\"'ADMIN'\" 或 v-has-role=\"['ADMIN', 'TEST']\""
      );
    }

    const roleCodes = useUserStoreHook().userLoginInfo.roleCodes;

    // 检查是否有对应角色权限
    const hasAuth = Array.isArray(requiredRoles)
      ? requiredRoles.some((role) => roleCodes.includes(role))
      : roles.includes(requiredRoles);

    // 如果没有权限，移除元素
    if (!hasAuth && el.parentNode) {
      el.parentNode.removeChild(el);
    }
  },
};
 */
