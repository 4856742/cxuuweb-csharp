import request from "@/utils/request";

const ContentAPI = {
  /**
   * 列表
   *
   * @param queryParams
   */
  getList(queryParams?: QueryParams) {
    return request({ url: "/api/Content/index", method: "get", params: queryParams });
  },

  /**
   * 详情
   *
   * @param id
   */
  getEdit(id?: number) {
    return request({ url: "/api/Content/AddOrEdit", method: "get", params: { id: id } });
  },

  /**
   * 新增
   *
   * @param data
   */
  postAdd(data: CmsContent) {
    return request({ url: "/api/Content/Create", method: "post", data: data });
  },

  /**
   *  修改
   *
   * @param id
   * @param data
   */
  putUpdate(data: CmsContent) {
    return request({ url: "/api/Content/edit", method: "put", data: data });
  },

  /**
   * 删除
   *
   * @param ids
   */
  deleteOne(id: number) {
    return request({ url: "/api/Content/Delete", method: "delete", params: { id: id } });
  },

  //编辑内容状态
  putSwitchValue(data: ValChangeModel) {
    return request({ url: "/api/content/SwitchValue", method: "put", data: data });
  },

  /**
   * 批量转移
   */
  putMultiTransfer(ids?: string, targetCid?: number) {
    return request({
      url: "/api/Content/MultiTransfer",
      method: "put",
      data: { ids: ids, targetCid: targetCid },
    });
  },
  /**
   * 批量删除
   */
  putMultiDelete(ids?: string) {
    return request({ url: "/api/Content/MultiDelete", method: "put", data: { ids: ids } });
  },
};
export default ContentAPI;

/**
 * 查询参数
 */
export interface QueryParams extends PageQuery {
  title?: string;
  status?: boolean;
  cid?: number;
  att?: number;
  startDate?: Date;
  endDate?: Date;
}

export interface CmsContent {
  id?: number;
  type?: number;
  cid?: number;
  title?: string;
  examine?: string;
  abstract?: string;
  coverUrl?: string;
  coverCount?: number;
  attachments?: string;
  createTime?: string;
  attA?: boolean;
  attB?: boolean;
  attC?: boolean;
  status?: boolean;

  content?: string;

  videoUrl?: string;
}
