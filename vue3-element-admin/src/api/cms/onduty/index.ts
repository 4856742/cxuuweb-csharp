import request from "@/utils/request";

const OndutyAPI = {
  getPage(queryParams: QueryParams) {
    return request({
      url: "/api/CmsOnduty/index",
      method: "get",
      params: queryParams,
    });
  },
  getOneEdit(dateTime: Date) {
    return request({
      url: "/api/CmsOnduty/GetOneEdit",
      method: "get",
      params: { dateTime: dateTime },
    });
  },

  putValChange(data: CmsOnduty) {
    return request({
      url: "/api/CmsOnduty/Edit",
      method: "put",
      data: data,
    });
  },
  getExportList(queryParams: QueryParams) {
    return request({
      url: "/api/CmsOnduty/index",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },

  getGetConfig() {
    return request({
      url: "/api/CmsOnduty/GetConfig",
      method: "get",
    });
  },

  putEditConfig(data_a: CmsConfig[]) {
    return request({
      url: "/api/CmsOnduty/EditConfig",
      method: "put",
      data: data_a,
    });
  },
};
export default OndutyAPI;

export interface QueryParams extends PageQuery {
  keywords?: string;
  dateTime?: Date;
  fields?: string;
}

export interface CmsOnduty {
  id?: number;
  uid?: number;
  did?: number;
  dateTime?: Date;
  dateTimeStr?: string;
  weekDate?: number;
  dateInMonth?: number;
  leader?: string;
  leaderPhone?: string;
  dutyer?: string;
  dutyerPhone?: string;
  insertTime?: Date;
  deptName?: string;
  nickName?: string;
  //isAuditor?: boolean
}

export interface CmsConfig {
  id?: number;
  label?: string | Date;
  valueData?: string;
}

export interface PresetDutyerOndutyCommand {
  name?: string;
  duty?: string;
}

// export interface OndutyCommandValChangeModel {
//   title?: string;
//   key?: string;
//   dateTime?: Date;
//   val?: string;
// }
