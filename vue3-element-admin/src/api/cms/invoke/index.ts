import request from "@/utils/request";
import { type AxiosPromise } from "axios";

const InvokeAPI = {
  /***       --------------------------      分类     --------------------------    */

  getCatList(queryParams?: QueryParams) {
    return request({
      url: "/api/CmsInvokeCat/index",
      method: "get",
      params: queryParams,
    });
  },

  /**
   * 下拉列表
   */
  getCatOptions(): AxiosPromise<OptionType[]> {
    return request({
      url: "/api/CmsInvokeCat/OptionTypes",
      method: "get",
    });
  },

  getCatEdit(id: number) {
    return request({
      url: "/api/CmsInvokeCat/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },

  postCatAdd(data: CmsInvoke) {
    return request({
      url: "/api/CmsInvokeCat/Create",
      method: "post",
      data: data,
    });
  },

  putCatUpdate(data: CmsInvoke) {
    return request({
      url: "/api/CmsInvokeCat/edit",
      method: "put",
      data: data,
    });
  },

  deleteCatOne(id: number) {
    return request({
      url: "/api/CmsInvokeCat/Delete",
      method: "delete",
      params: { id: id },
    });
  },

  /***     --------------------------  调用内容   --------------------------   */

  getList(queryParams?: QueryParams) {
    return request({
      url: "/api/CmsInvoke/index",
      method: "get",
      params: queryParams,
    });
  },

  getEdit(id: number) {
    return request({
      url: "/api/CmsInvoke/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },

  postAdd(data: CmsInvoke) {
    return request({
      url: "/api/CmsInvoke/Create",
      method: "post",
      data: data,
    });
  },

  putUpdate(data: CmsInvoke) {
    return request({
      url: "/api/CmsInvoke/edit",
      method: "put",
      data: data,
    });
  },

  deleteOne(id: number) {
    return request({
      url: "/api/CmsInvoke/Delete",
      method: "delete",
      params: { id: id },
    });
  },
  putSwitchValue(data: ValChangeModel) {
    return request<any, ResponseData>({
      url: "/api/CmsInvoke/SwitchValue",
      method: "put",
      data: data,
    });
  },
};
export default InvokeAPI;

/**
 * 查询参数
 */
export interface QueryParams extends PageQuery {
  keywords?: string;
  status?: number;
  cid?: number;
}
export interface CmsInvoke {
  id?: number;
  cid?: number;
  name?: string;
  ico?: string;
  url?: string;
  content?: string;
  urlInt?: number;
  sort?: number;
  status?: boolean;
  img?: string;
}

export interface CmsInvokeCat {
  id?: number;
  name?: string;
  remark?: string;
  url?: string;
  pid?: number;
  sort?: number;
  type?: number;
  invokeNum?: number;
}
