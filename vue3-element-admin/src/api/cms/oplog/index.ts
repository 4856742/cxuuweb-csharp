import request from "@/utils/request";

const CmsLogAPI = {
  getLogList(queryParams?: LogQueryParams) {
    return request({
      url: "/api/CmsLog/LogList",
      method: "get",
      params: queryParams,
    });
  },
  getExportList(queryParams: LogQueryParams) {
    return request({
      url: "/api/CmsLog/LogList",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },
};
export default CmsLogAPI;

/**
 * 查询参数
 */
export interface LogQueryParams extends PageQuery {
  startDate?: Date;
  endDate?: Date;
  method?: string;
  selectDid?: number;
  selectUids?: string;
  status?: boolean;
  fields?: string;
}
/** 系统数据库操作日志*/
export interface LogList {
  id?: number;
  uid: number;
  time: Date;
  ip: string;
  contrAct: string;
  method: string;
  doThing: string;
  nickName: string;
  deptName: string;
}
