import request from "@/utils/request";
import { type AxiosPromise } from "axios";

const CategoryAPI = {
  /**
   * 列表
   *
   * @param queryParams
   */
  getList(queryParams?: QueryParams) {
    return request({ url: "/api/CmsCategory/index", method: "get", params: queryParams });
  },

  /**
   * 下拉列表
   */
  getCmsCatOptions(): AxiosPromise<OptionType[]> {
    return request({ url: "/api/CmsCategory/OptionTypes", method: "get" });
  },

  /**
   * 详情
   *
   * @param id
   */
  getEdit(id?: number) {
    return request({ url: "/api/CmsCategory/AddOrEdit", method: "get", params: { id: id } });
  },

  /**
   * 新增
   *
   * @param data
   */
  postAdd(data: CmsCategory) {
    return request({ url: "/api/CmsCategory/Create", method: "post", data: data });
  },

  /**
   *  修改
   *
   * @param id
   * @param data
   */
  putUpdate(data: CmsCategory) {
    return request({ url: "/api/CmsCategory/edit", method: "put", data: data });
  },

  /**
   * 删除
   *
   * @param ids
   */
  deleteOne(id: number) {
    return request({ url: "/api/CmsCategory/Delete", method: "delete", params: { id: id } });
  },

  //编辑状态
  putSwitchValue(data: ValChangeModel) {
    return request({ url: "/api/CmsCategory/SwitchValue", method: "put", data: data });
  },

  getEditScopeDept(id: number) {
    return request({ url: "/api/CmsCategory/GetEditScopeDept", method: "get", params: { id: id } });
  },
  /**
   *  修改
   *
   * @param id
   * @param data
   */
  putScopeDeptUpdate(data: CmsCategoryDept) {
    return request({ url: "/api/CmsCategory/EditScopeDept", method: "put", data: data });
  },

  /**
   * 部门授权
   *
   * @param  id 部门ID
   */
  getDeptRole(id: number) {
    return request({ url: "/api/CmsCategory/GetDeptRole", method: "get", params: { id: id } });
  },
  putDeptRole(data: any) {
    return request({ url: "/api/CmsCategory/EditDeptRole", method: "put", data: data });
  },
};
export default CategoryAPI;

/**
 * 查询参数
 */
export interface QueryParams {
  keywords?: string;
  status?: number;
}
export interface CmsCategory {
  id?: number;
  pid?: number;
  name?: string;
  theme?: string;
  ctheme?: string;
  ico?: string;
  type?: number;
  num?: number;
  sort?: number;
  status?: boolean;
  bannerHeader?: string;
  bannerAd?: string;
  bannerLit?: string;
}
export interface CmsCategoryDept {
  id?: number;
  submitDept?: string;
}
