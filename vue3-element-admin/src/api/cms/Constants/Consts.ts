export const CmsPerKey = {
  //调用管理
  Cms_Invoke: "Cms_Invoke",
  Cms_Onduty: "Cms_Onduty",
  //内容管理
  Content_Cate: "Content_Cate",
  Content_AddOrEdit: "Content_AddOrEdit",
  Content_Delete: "Content_Delete",
  Content_Att: "Content_Att",
  Content_Status: "Content_Status",
};

/**
 * CMS内容模型
 */
export const contentTypes = [
  { value: 1, label: "专题频道" },
  { value: 2, label: "文章" },
  { value: 3, label: "视频" },
  // { value: 3, label: "图集" },
  // { value: 5, label: "音频" },
];

export interface CmsConfig {
  id?: number;
  label?: string | Date;
  valueData?: string;
}

export interface PresetConfigCmsOnduty {
  name?: string;
  phone?: string;
}
