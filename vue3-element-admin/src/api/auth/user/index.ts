import request from "@/utils/request";
import type { AxiosPromise } from "axios";
import type { TenantStorageConfig, SysInfo } from "@/api/sys/sysconfig/index";

const AuthAPI = {
  // 查询组织架构树
  getOrgTree(param: QueryUserParams) {
    return request({
      url: "oa/org/tree",
      method: "get",
      params: param,
    });
  },

  // 查询系统角色
  getRole() {
    return request({
      url: "oa/org/tree",
      method: "get",
    });
  },

  // 搜索人员
  getUserByName(param: QueryUserParams) {
    return request({
      url: "oa/org/tree/user/search",
      method: "get",
      params: param,
    });
  },

  // 搜索人员
  getUserDepts(userId: number) {
    return request({
      url: `oa/org/user/${userId}/dept`,
      method: "get",
    });
  },

  /** 登录接口*/
  login(data: LoginFormData) {
    return request<LoginResult>({
      url: "/api/login/login",
      method: "post",
      data: data,
    });
  },
  /** 刷新 token 接口*/
  refreshToken(refreshToken: RefreshTokenIn) {
    return request<LoginResult>({
      url: "/api/login/refreshToken",
      method: "post",
      params: refreshToken,
      headers: {
        Authorization: "no-auth",
      },
    });
  },
  /**
   * 注销API
   */
  loginOut() {
    return request({
      url: "/api/login/LoginOut",
      method: "delete",
    });
  },
  getQueryUserOptions(queryParams: QueryUserParams) {
    return request({
      url: "/api/user/QueryUserOptions",
      method: "get",
      params: queryParams,
    });
  },
  /**
   * 登录成功后获取用户信息（昵称、头像、权限集合和角色集合）
   */
  getUserInfoApi(): AxiosPromise<UserLoginInfo> {
    return request({
      url: "/api/user/GetUserInfo",
      method: "get",
    });
  },
  getUserIndexNoAuth(queryParams: QueryParams) {
    return request({
      url: "/api/user/IndexNoAuth",
      method: "get",
      params: queryParams,
    });
  },

  /**
   * 获取验证码
   */
  getCaptchaApi(): AxiosPromise<CaptchaResult> {
    return request({
      url: "/api/login/CaptchaImage",
      method: "get",
    });
  },

  /**
   * 获取用户分页列表
   *
   * @param queryParams
   */
  getQueryUserList(queryParams: QueryParams) {
    return request({
      url: "/api/user/QueryUserList",
      method: "get",
      params: queryParams,
    });
  },

  getUserPage(queryParams: QueryParams) {
    return request({
      url: "/api/user/index",
      method: "get",
      params: queryParams,
    });
  },
  getExport(queryParams: QueryParams) {
    return request({
      url: "/api/user/UserExportExcel",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },

  /**
   * 当前用户获取自己的信息
   */
  getUserChange(): AxiosPromise<UserForm> {
    return request({
      url: "/api/user/UserChange",
      method: "get",
    });
  },

  putUserChange(data: UserForm) {
    return request({
      url: "/api/user/UserChangePost",
      method: "put",
      data: data,
    });
  },

  /**
   * 获取用户表单详情
   */
  getUserForm(id?: number) {
    return request({
      url: "/api/user/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },
  /**
   * 添加用户
   *
   * @param data
   */
  addUser(data: any) {
    return request({
      url: "/api/user/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改用户
   *
   * @param id
   * @param data
   */
  updateUser(data: UserForm) {
    return request({
      url: "/api/user/Edit",
      method: "put",
      data: data,
    });
  },

  /**
   * 修改用户密码
   *
   * @param id
   * @param password
   */
  putChangeStatus(id: number, status: boolean) {
    return request({
      url: "/api/user/ChStatus",
      method: "put",
      params: { id: id, status: status },
    });
  },
  /**
   * 修改用户密码
   *
   * @param id
   * @param password
   */
  updateUserPassWord(id: number, password: string) {
    return request({
      url: "/api/user/UpdateUserPassWord",
      method: "put",
      params: { id: id, password: password },
    });
  },
  /**
   * 删除用户
   *
   * @param id
   */
  deleteUsers(id: number) {
    return request({
      url: "/api/user/Delete",
      method: "delete",
      params: { id: id },
    });
  },

  //在线用户状态
  getOnlineUserList(queryParams: QueryParams) {
    return request({
      url: "/api/user/OnlineUserList",
      method: "get",
      params: queryParams,
    });
  },
  forceOffline(id: number) {
    return request({
      url: "/api/user/ForceOffline",
      method: "delete",
      params: { id: id },
    });
  },
  /**
   * 下载用户导入模板
   *
   * @returns
   */
  downloadTemplateApi() {
    return request({
      url: "/api/v1/users/template",
      method: "get",
      responseType: "arraybuffer",
    });
  },

  /**
   * 导出用户
   *
   * @param queryParams
   * @returns
   */
  exportUser(queryParams: QueryParams) {
    return request({
      url: "/api/v1/users/_export",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },

  /**
   * 导入用户
   *
   * @param file
   */
  importUser(deptId: number, file: File) {
    const formData = new FormData();
    formData.append("file", file);
    return request({
      url: "/api/v1/users/_import",
      method: "post",
      params: { deptId: deptId },
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
};

export default AuthAPI;

/**
 * 登录用户信息
 */
export interface UserLoginInfo {
  userInfo?: UserInfo;
  permCodes?: string[]; //按钮权限标识
  tenantStorageConfigs?: TenantStorageConfig[];
  sysInfo?: SysInfo;
  statistics?: Statistics;
}
export interface Statistics {
  noticeNoRead?: number;
}
/**
 * 登录请求参数
 */
export interface LoginFormData {
  account: string;
  password: string;
  remember: boolean;
  /**
   * 验证码缓存key
   */
  uuid?: string;
  /**
   * 验证码
   */
  validCode?: string;
}
/**
 * 用户查询对象类型
 */
export interface QueryUserParams extends PageQuery {
  keywords?: string;
  status?: boolean;
  type?: number;
  deptId?: number;
  excludOwn?: boolean;
  fields?: string;
}

export const UserTypeEnum = [
  { label: "普通用户", value: 1 },
  { label: "超级管理员", value: 99 },
];

/**
 * 登录用户信息
 */
export interface UserInfo {
  userId?: number;
  nickName?: string;
  avatar?: string;
  userType?: number;
  mobile?: string;
}

export interface UserOptions {
  userId: number;
  nickName?: string;
  deptName?: string;
}
/**
 * 用户查询对象类型
 */
export interface QueryParams extends PageQuery {
  keywords?: string;
  status?: boolean;
  deptId?: number;
}
/**在线用户列表 */
export interface UserOnlineView {
  connectionId?: string;
  userId: number;
  userName?: string;
  nickName?: string;
  connectTime?: Date;
  ipAddress?: string;
  deptName?: string;
  properties?: UserOnlineProperties;
}
interface UserOnlineProperties {
  userAgent?: string;
}
/**
 * 用户表单类型
 */
export interface UserForm {
  avatar?: string;
  deptName?: string;
  deptId?: number;
  email?: string;
  id?: number;
  type?: number;
  mobile?: string;
  idNumber?: string;
  nickName?: string;
  roles?: string;
  status?: boolean;
  userName?: string;
  passWord?: string;
  oldPassword?: string;
  createTime?: Date;
}

export interface UserRole {
  id?: number;
  name?: string;
}

/**
 * 登录响应
 */
export interface LoginResult {
  accessToken?: string;
  refreshToken?: string;
  newAccessToken?: string;
  newRefreshToken?: string;
}
/**
 * 登录响应
 */
export interface RefreshTokenIn {
  accessToken: string;
  refreshToken: string;
}
/**
 * 验证码响应
 */
export interface CaptchaResult {
  /**
   * 验证码缓存key
   */
  uuid: string;
  /**
   * 验证码图片Base64字符串
   */
  img: string;
  isOnValidCode: boolean;
}
