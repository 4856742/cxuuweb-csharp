import request from "@/utils/request";
import { type AxiosPromise } from "axios";

const DeptAPI = {
  listDepts(queryParams?: DeptQuery) {
    return request({
      url: "/api/userDept/index",
      method: "get",
      params: queryParams,
    });
  },
  /**
   * 全部部门下拉列表
   */
  getDeptOptions(): AxiosPromise<OptionType[]> {
    return request({
      url: "/api/userDept/OptionTypes",
      method: "get",
    });
  },
  /**
   * 当前用户权限内的部门下拉列表
   */
  getOptionTypesAuth(): AxiosPromise<OptionType[]> {
    return request({
      url: "/api/userDept/OptionTypesAuth",
      method: "get",
    });
  },
  /**
   * 获取部门详情
   *
   * @param id
   */
  getDeptForm(id: number) {
    return request({
      url: "/api/userDept/Get",
      method: "get",
      params: { id: id },
    });
  },

  /**
   * 新增部门
   *
   * @param data
   */
  addDept(data: DeptForm) {
    return request({
      url: "/api/userDept/Create",
      method: "post",
      data: data,
    });
  },

  /**
   *  修改部门
   *
   * @param id
   * @param data
   */
  updateDept(data: DeptForm) {
    return request({
      url: "/api/userDept/edit",
      method: "put",
      data: data,
    });
  },

  /**
   * 删除部门
   *
   * @param ids
   */
  deleteDept(id: number) {
    return request({
      url: "/api/userDept/Delete",
      method: "delete",
      params: { id: id },
    });
  },
};
export default DeptAPI;
/**
 * 查询参数
 */
export interface DeptQuery extends PageQuery {
  keywords?: string;
  status?: number;
  excludOwn?: boolean;
}
/** 部门管理*/
export interface DeptForm {
  id?: number;
  pid: number;
  type: number;
  name: string;
  sort?: number;
  remark?: string;
  status?: boolean;
}
/**
 * 部门类型
 */
export interface DeptVO {
  /**
   * 子部门
   */
  children?: DeptVO[];
  haveChild?: boolean;
  /**
   * 创建时间
   */
  createTime?: Date;
  /**
   * 部门ID
   */
  id?: number;
  /**
   * 部门名称
   */
  name?: string;
  /**
   * 父部门ID
   */
  pid?: number;
  /**
   * 排序
   */
  sort?: number;
  /**
   * 状态(1:启用；0:禁用)
   */
  status?: boolean;
  /**
   * 修改时间
   */
  updateTime?: Date;
}
