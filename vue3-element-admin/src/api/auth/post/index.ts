import request from "@/utils/request";

const UserPostAPI = {
  /**
   * 分页列表
   */
  getPage() {
    return request({
      url: "/api/UserPost/index",
      method: "get",
    });
  },

  /**
   * 获取表单数据
   *
   * @param id
   */
  getFormData(id: number) {
    return request({
      url: "/api/UserPost/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },

  /**
   * 新增
   *
   * @param data
   */
  postAdd(data: UserPost) {
    return request({
      url: "/api/UserPost/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: UserPost) {
    return request({
      url: "/api/UserPost/Edit",
      method: "put",
      data: data,
    });
  },
  putSaveSort(data: OptionType[]) {
    return request({
      url: "/api/UserPost/ChangeSort",
      method: "put",
      data: data,
    });
  },
  /**
   * 删除
   *
   * @param id ID
   */
  delOne(id: number) {
    return request({
      url: "/api/UserPost/Delete/",
      method: "delete",
      params: { id: id },
    });
  },
};

export default UserPostAPI;

/**岗位管理 */
export type UserPost = {
  id?: number;
  postName?: string;
  postCode?: number;
  authDeptIds?: string;
  postDataEdit?: boolean;
  ico?: string;
  sort?: number;
  remark?: string;
};
