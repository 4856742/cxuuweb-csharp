import request from "@/utils/request";

const UserRoleAPI = {
  /**
   * 分页列表
   */
  getPage() {
    return request({
      url: "/api/UserRole/index",
      method: "get",
    });
  },

  /**
   * 获取表单数据
   *
   * @param id
   */
  getFormData(id?: number) {
    return request({
      url: "/api/UserRole/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },

  /**
   * 新增
   *
   * @param data
   */
  postAdd(data: UserRole) {
    return request({
      url: "/api/UserRole/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: UserRole) {
    return request({
      url: "/api/UserRole/Edit",
      method: "put",
      data: data,
    });
  },
  putSaveSort(data: OptionType[]) {
    return request({
      url: "/api/UserRole/ChangeSort",
      method: "put",
      data: data,
    });
  },
  /**
   * 删除
   *
   * @param id ID
   */
  delOne(id: number) {
    return request({
      url: "/api/UserRole/Delete/",
      method: "delete",
      params: { id: id },
    });
  },
};
export default UserRoleAPI;

/**角色管理 */
export type UserRole = {
  id?: number;
  name?: string;
  isKeep?: boolean;
  isEditOthersData: boolean;
  menuRole?: string;
  authDeptIds?: string;
  remark?: string;
  accessLevel?: number;
  sort?: number;
};
