import request from "@/utils/request";

const WorkflowStepAPI = {
  /**
   * 分页列表
   */
  getPage(queryParams: QueryParams) {
    return request({
      url: "/api/WorkflowStep/index",
      method: "get",
      params: queryParams,
    });
  },

  /**
   * 获取表单数据
   *
   * @param id
   */
  getListByWorkflowId(wflowId: number) {
    return request({
      url: "/api/WorkflowStep/ListByWorkflowId",
      method: "get",
      params: { wflowId: wflowId },
    });
  },

  /**
   * 获取用户数据
   *
   */
  getEdit(id: number) {
    return request({
      url: "/api/WorkflowStep/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },
  /**
   * 回复或新写信
   *
   * @param data
   */
  postAdd(data: WorkflowStep) {
    return request({
      url: "/api/WorkflowStep/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: { data: string; workflowId: number }) {
    return request({
      url: "/api/WorkflowStep/Edit",
      method: "put",
      data: data,
    });
  },

  /**
   * 删除
   *
   * @param id ID
   */
  delOne(id: number) {
    return request({
      url: "/api/WorkflowStep/Delete/",
      method: "delete",
      params: { id: id },
    });
  },
};
export default WorkflowStepAPI;

/**
 * 角色查询参数
 */
export interface QueryParams extends PageQuery {
  name?: string;
  workflowId?: number;
}

export interface WorkflowStep {
  id?: number;
  pid?: number;
  type: WorkflowStepType;
  name?: string;
  remark?: string;
  workflowId?: number;
  props: WorkflowStepProps;
  branch?: any[][];
  stepOrder?: number;
}
export interface WorkflowStepProps {
  type?: any;
  branch?: any[];
  logic?: boolean;
  groups?: any[];
  approvalMode?: ApprovalMode;
  mode?: ApprovalMode;
  ruleType?: ApprovalRuleType;
  taskMode?: { type: TaskModeType; percentage: 100 };
  needSign?: boolean;
  assignUser?: [];
  selectMultiple?: boolean;
  leader?: { level: 1; emptySkip: false };
  leaderTop?: { level: 0; toEnd: false; emptySkip: false };
  assignDept?: { dept: []; type: ApprovalRuleType };
  assignDeptTop?: { dept: []; type: ApprovalRuleType };
  assignRole?: [];
  noUserHandler?: { type: NoUserHandlerType; assigned: [] };
  sameRoot?: { type: SameRootType; assigned: [] };
  timeout?: { enable: false; time: 1; timeUnit: TimeoutTimeUnit; type: TimeoutType };
}
export enum WorkflowApproverType {
  None = 1,
  Dept = 2,
  Post = 3,
  Group = 4,
  User = 5,
}
//必须字符串
export enum WorkflowStepType {
  Gateway = "Gateway",
  Exclusive = "Exclusive",
  Parallel = "Parallel",
  Approval = "Approval",
  Cc = "Cc",
  Start = "Start",
}
export enum TaskModeType {
  AND,
  OR,
  NEXT,
  CUSTOM,
}
export enum SameRootType {
  TO_SELF,
  TO_LEADER,
  TO_SKIP,
}
export enum ApprovalMode {
  AUTO_REFUSE,
  AUTO_PASS,
  USER,
}
export enum NoUserHandlerType {
  TO_NEXT,
  TO_REFUSE,
  TO_ADMIN,
  TO_USER,
}
export enum TimeoutType {
  TO_PASS,
  TO_REFUSE,
  NOTIFY,
}
export enum TimeoutTimeUnit {
  M,
  H,
  D,
}
export enum ApprovalRuleType {
  ASSIGN_USER,
  ROOT_SELECT,
  ROOT_SELF,
  LEADER,
  LEADER_TOP,
  ASSIGN_DEPT,
  ASSIGN_ROLE,
}
export enum OrgPickerType {
  user,
  dept,
  role,
  org,
}
export interface WorkflowStepActiveNode {
  name: string;
  id: number;
  type: string;
  pid: number;
}
export const WorkflowStepTypeMap = [
  { value: WorkflowStepType.Gateway, label: "网关节点" },
  { value: WorkflowStepType.Exclusive, label: "互斥条件" },
  { value: WorkflowStepType.Parallel, label: "并行分支" },
  { value: WorkflowStepType.Approval, label: "审批人" },
  { value: WorkflowStepType.Cc, label: "抄送人" },
  { value: WorkflowStepType.Start, label: "开始节点" },
];
