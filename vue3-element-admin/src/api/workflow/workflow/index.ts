import request from "@/utils/request";

const WorkflowAPI = {
  /**
   * 分页列表
   */
  getPage(queryParams: QueryParams) {
    return request({
      url: "/api/Workflow/index",
      method: "get",
      params: queryParams,
    });
  },

  /**
   * 获取表单数据
   *
   * @param id
   */
  getOne(id: number) {
    return request({
      url: "/api/Workflow/Get",
      method: "get",
      params: { id: id },
    });
  },

  /**
   * 获取用户数据
   *
   */
  getEdit(id?: number) {
    return request({
      url: "/api/Workflow/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },
  /**
   * 回复或新写信
   *
   * @param data
   */
  postAdd(data: Workflow) {
    return request({
      url: "/api/Workflow/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: Workflow) {
    return request({
      url: "/api/Workflow/Edit",
      method: "put",
      data: data,
    });
  },

  /**
   * 删除
   *
   * @param id ID
   */
  delOne(id: number) {
    return request({
      url: "/api/Workflow/Delete/",
      method: "delete",
      params: { id: id },
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putCatUpdate(data: OptionType[]) {
    return request({
      url: "/api/Workflow/EditCat",
      method: "put",
      data: data,
    });
  },
};
export default WorkflowAPI;

/**
 * 角色查询参数
 */
export interface QueryParams extends PageQuery {
  nickName?: string;
  name?: string;
  cid?: number;
  startDate?: string;
  endDate?: string;
}

export interface Workflow {
  id?: number;
  cid?: number;
  name?: string;
  catName?: string;
  nickName?: string;
  deptName?: string;
  remark?: string;
  uid?: number;
  did?: number;
  createdTime?: Date;
  isActive?: boolean;
}
