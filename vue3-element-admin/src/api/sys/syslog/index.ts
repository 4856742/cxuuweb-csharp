import request from "@/utils/request";

const SysLogAPI = {
  clearLogException() {
    return request({
      url: "/api/SysLog/ClearLogException",
      method: "get",
    });
  },
  getLogException(id: number) {
    return request({
      url: "/api/SysLog/LogException",
      method: "get",
      params: { id: id },
    });
  },
  getLogExceptionPage(queryParams?: LogQueryParams) {
    return request({
      url: "/api/SysLog/LogExceptionPage",
      method: "get",
      params: queryParams,
    });
  },
  getExportLogException(queryParams: LogQueryParams) {
    return request({
      url: "/api/SysLog/LogExceptionExportExcel",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },
  getSqlLogList(queryParams?: LogQueryParams) {
    return request({
      url: "/api/SysLog/SqlLogList",
      method: "get",
      params: queryParams,
    });
  },
  getExportSqlLogList(queryParams: LogQueryParams) {
    return request({
      url: "/api/SysLog/SqlLogListExportExcel",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },

  getLoginLogList(queryParams?: LogQueryParams) {
    return request({
      url: "/api/SysLog/LoginLogList",
      method: "get",
      params: queryParams,
    });
  },
  getExport(queryParams: LogQueryParams) {
    return request({
      url: "/api/SysLog/LoginLogExportExcel",
      method: "get",
      params: queryParams,
      responseType: "arraybuffer",
    });
  },
};
export default SysLogAPI;

/**
 * 查询参数
 */
export interface LogQueryParams extends PageQuery {
  startDate?: Date;
  endDate?: Date;
  method?: string;
  selectDid?: number;
  selectUids?: string;
  status?: boolean;
  fields?: string;
}
/** 系统数据库操作日志*/
export interface SqlLog {
  id?: number;
  uid: number;
  time: Date;
  ip: string;
  contrAct: string;
  method: string;
  doThing: string;
  nickName: string;
  deptName: string;
}
export interface LogException {
  id?: number;
  uid: number;
  time: Date;
  ip: string;
  contrAct: string;
  method: string;
  stackTrace: string;
  message: string;
  nickName: string;
  deptName: string;
}
