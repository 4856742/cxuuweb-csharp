import request from "@/utils/request";

const SysConfigAPI = {
  /**
   * 获取表单数据
   *
   * @param id
   */
  getData() {
    return request({
      url: "/api/AppConfig/Get",
      method: "get",
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: SysInfo) {
    return request({
      url: "/api/AppConfig/Edit",
      method: "put",
      data: data,
    });
  },
};
export default SysConfigAPI;

export interface SysInfo {
  appName?: string;
  maxConnections?: number;
  isEnabled?: boolean;
  unitOfUse?: string;
  appUrl?: string;
  attUrlPre?: string;
  description?: string;
  copyright?: string;
  beianWeb?: string;
  beianApp?: string;
  userDefaultPass?: string;
  isOnValidCode?: boolean;
  validCodeType?: number;
}

export interface TenantStorageConfig {
  tenantType: string;
  /// <summary>
  /// 桶名称，用于存储该类型租户的文件
  /// </summary>
  bucketName: string;
  /// <summary>
  /// 最大文件大小（字节），0表示无限制
  /// </summary>
  maxFileSize: number;
  /// <summary>
  /// 允许的扩展名列表，例如 [".pdf", ".jpg"]
  /// </summary>
  allowedExtensions: string[];
}
