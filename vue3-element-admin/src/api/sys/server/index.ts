import request from "@/utils/request";
const ServerAPI = {
  getServerInfo() {
    return request({
      url: "/api/Server/Index",
      method: "get",
    });
  },
};
export default ServerAPI;
