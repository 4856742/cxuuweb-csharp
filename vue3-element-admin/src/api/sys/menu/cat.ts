import request from "@/utils/request";
import type { AxiosPromise } from "axios";
const MenuCatApi = {
  getCatOptions(): AxiosPromise<OptionType[]> {
    return request({
      url: "/api/MenuCat/OptionTypes",
      method: "get",
    });
  },
  getIndentedList(): AxiosPromise<MenuCat[]> {
    return request({
      url: "/api/MenuCat/IndentedList",
      method: "get",
    });
  },
  getList() {
    return request({
      url: "/api/MenuCat/Index",
      method: "get",
    });
  },
  getEdit(id: number) {
    return request({
      url: "/api/MenuCat/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },
  postAdd(data: MenuCat) {
    return request({
      url: "/api/MenuCat/Create",
      method: "post",
      data: data,
    });
  },
  putUpdate(data: MenuCat) {
    return request({
      url: "/api/MenuCat/edit",
      method: "put",
      data: data,
    });
  },
  deleteOne(id: number) {
    return request({
      url: "/api/MenuCat/Delete",
      method: "delete",
      params: { id: id },
    });
  },
};
export default MenuCatApi;

export interface MenuCat {
  id?: number;
  pid?: number;
  name?: string;
  icon?: string;
  clientId?: string;
  clientUrl?: string;
  clientSecret?: string;
  type?: number;
  sort?: number;
  remark?: string;
}

export interface MenuOptions {
  value?: number;
  cid?: number;
  label?: string;
  type?: number;
}
