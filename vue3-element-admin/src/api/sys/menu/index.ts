import request from "@/utils/request";
import type { AxiosPromise } from "axios";

const MenuApi = {
  /**
   * 获取路由列表
   */
  listRoutes() {
    return request({
      url: "/api/menu/GetRoute",
      method: "get",
      params: { cids: [3, 4] },
    });
  },

  /**
   * 获取菜单树形列表
   *
   * @param queryParams
   */
  getList(id: number) {
    return request({
      url: "/api/menu/index",
      method: "get",
      params: { id: id },
    });
  },
  getEdit(id: number) {
    return request({
      url: "/api/Menu/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },
  getMenuOptions(cid: number): AxiosPromise<OptionType[]> {
    return request({
      url: "/api/menu/OptionTypes",
      method: "get",
      params: { cid: cid },
    });
  },
  addMenu(data: MenuForm) {
    return request({
      url: "/api/menu/Create",
      method: "post",
      data: data,
    });
  },
  updateMenu(id: string, data: MenuForm) {
    data.id = id;
    return request({
      url: "/api/menu/edit",
      method: "put",
      data: data,
    });
  },
  deleteMenu(id: number) {
    return request({
      url: "/api/menu/Delete",
      method: "delete",
      params: { id: id },
    });
  },
};
export default MenuApi;

export interface RouteVO {
  /** 子路由列表 */
  children: RouteVO[];
  /** 组件路径 */
  component?: string;
  /** 路由属性 */
  meta?: Meta;
  /** 路由名称 */
  name?: string;
  /** 路由路径 */
  path?: string;
  /** 跳转链接 */
  redirect?: string;
}

export interface Meta {
  /** 【目录】只有一个子路由是否始终显示 */
  alwaysShow?: boolean;
  /** 是否隐藏(true-是 false-否) */
  hidden?: boolean;
  /** ICON */
  icon?: string;
  /** 【菜单】是否开启页面缓存 */
  keepAlive?: boolean;
  /** 路由title */
  title?: string;
}

export interface Menu {
  id: number;
  pid: number;
  name: string;
  icon: string;
  type: number;
  path?: string;
  component?: string;
  permCode?: string;
  sort?: number;
  status?: boolean;
  redirect?: string;
  hide?: boolean;
}

/**
 * 菜单视图对象类型
 */
export interface MenuVO {
  /**
   * 子菜单
   */
  children?: MenuVO[];
  /**
   * 组件路径
   */
  component?: string;
  /**
   * ICON
   */
  icon?: string;
  /**
   * 菜单ID
   */
  menuId?: number;
  /**
   * 菜单名称
   */
  name?: string;
  /**
   * 父菜单ID
   */
  pid?: number;
  /**
   * 按钮权限标识
   */
  permCode?: string;
  /**
   * 跳转路径
   */
  redirect?: string;
  /**
   * 路由名称
   */
  routeName?: string;
  /**
   * 路由相对路径
   */
  path?: string;
  /**
   * 菜单排序(数字越小排名越靠前)
   */
  sort?: number;
  /**
   * 菜单类型
   */
  type?: number;
  /**
   * 菜单是否可见(1:显示;0:隐藏)
   */
  hide?: number;
}

/**
 * 菜单表单对象类型
 */
export interface MenuForm {
  /**
   * 菜单ID
   */
  id?: string;
  cid?: number;
  /**
   * 父菜单ID
   */
  pid?: number;
  /**
   * 菜单名称
   */
  name?: string;
  /**
   * 菜单是否可见(1:是;0:否;)
   */
  hide: boolean;
  icon?: string;
  /**
   * 排序
   */
  sort: number;
  /**
   * 组件路径
   */
  component?: string;
  /**
   * 路由路径
   */
  path?: string;
  /**
   * 跳转路由路径
   */
  redirect?: string;

  /**
   * 菜单类型
   */
  type: number;

  /**
   * 权限标识
   */
  permCode?: string;
  /**
   * 【菜单】是否开启页面缓存
   */
  keepAlive?: boolean;

  /**
   * 【目录】只有一个子路由是否始终显示
   */
  alwaysShow?: boolean;
}
