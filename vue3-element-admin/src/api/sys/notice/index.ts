import request from "@/utils/request";

const NoticeAPI = {
  getNoReadCount() {
    return request({
      url: "/api/Notice/NoReadCount",
      method: "get",
    });
  },
  /**
   * 分页列表
   */
  getPage(queryParams: QueryParams) {
    return request({
      url: "/api/Notice/index",
      method: "get",
      params: queryParams,
    });
  },

  /**
   * 获取表单数据
   *
   * @param id
   */
  getRead(id: number) {
    return request({
      url: "/api/Notice/Read",
      method: "get",
      params: { id: id },
    });
  },
  /**
   * 获取表单数据
   *
   * @param uid
   */
  getReply(uid: number) {
    return request({
      url: "/api/Notice/RePly",
      method: "get",
      params: { uid: uid },
    });
  },

  /**
   * 回复或新写信
   *
   * @param data
   */
  postAdd(data: SysNotice) {
    return request({
      url: "/api/Notice/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: SysNotice) {
    return request({
      url: "/api/Notice/Edit",
      method: "put",
      data: data,
    });
  },

  /**
   * 删除
   *
   * @param id ID
   */
  delOne(id: number) {
    return request({
      url: "/api/Notice/Delete/",
      method: "delete",
      params: { id: id },
    });
  },
};
export default NoticeAPI;

/**
 * 角色查询参数
 */
export interface QueryParams extends PageQuery {
  keywords?: string;
  status?: boolean;
  listType: number; //1为收到消息列表,空或2是已发消息列表
}

export interface SysNotice {
  title?: string;
  content?: string;
  attments?: string;
  type?: number;
  postTime?: Date;
  getUserAvatar?: string;
  getNickname?: string;
  postUid?: number;
  postName?: string;
  getUid?: string;
  url?: string;
  status?: boolean;
  img?: string;
}

export interface SysNoticeRead {
  id?: number;
  title?: string;
  content?: string;
  attments?: string;
  getUid?: number;
  postTime?: Date;
  postAvatar?: string;
  postNickName?: string;
}

export interface SysNoticePost {
  id?: number;
  title?: string;
  content?: string;
  attments?: string;
  type?: number;
  getUid?: number;
  postTime?: Date;
  getUserAvatar?: string;
  getNickname?: string;
}
