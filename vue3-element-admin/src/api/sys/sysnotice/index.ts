import request from "@/utils/request";

const SysNoticeAPI = {
  /**
   * 分页列表
   */
  getPage(queryParams: QueryParams) {
    return request({
      url: "/api/SysNotice/index",
      method: "get",
      params: queryParams,
    });
  },

  /**
   * 获取表单数据
   *
   * @param id
   */
  getOne(id: number) {
    return request({
      url: "/api/SysNotice/Get",
      method: "get",
      params: { id: id },
    });
  },

  /**
   * 获取用户数据
   *
   */
  getEdit(id: number) {
    return request({
      url: "/api/SysNotice/AddOrEdit",
      method: "get",
      params: { id: id },
    });
  },
  /**
   * 回复或新写信
   *
   * @param data
   */
  postAdd(data: SysNotice) {
    return request({
      url: "/api/SysNotice/Create",
      method: "post",
      data: data,
    });
  },

  /**
   * 修改
   *
   * @param data
   */
  putUpdate(data: SysNotice) {
    return request({
      url: "/api/SysNotice/Edit",
      method: "put",
      data: data,
    });
  },

  /**
   * 删除
   *
   * @param id ID
   */
  delOne(id: number) {
    return request({
      url: "/api/SysNotice/Delete/",
      method: "delete",
      params: { id: id },
    });
  },
};
export default SysNoticeAPI;

/**
 * 角色查询参数
 */
export interface QueryParams extends PageQuery {
  keywords?: string;
  status?: boolean;
}

export interface SysNotice {
  id?: number;
  title?: string;
  content?: string;
  attments?: string;
  status?: boolean;
  uid?: number;
  greatTime?: Date;
  nickName?: string;
}
