import request from "@/utils/request";

const AttachmentAPI = {
  getPageAttachmentUser(queryParams?: QueryParams) {
    return request({
      url: "/api/Attachment/PageAttachmentUser",
      method: "get",
      params: queryParams,
    });
  },

  delOne(attachmentId: string) {
    return request<any, ResponseData>({
      url: "/api/Attachment/Delete",
      method: "delete",
      params: { attachmentId: attachmentId },
    });
  },
};

export default AttachmentAPI;

export interface QueryParams extends PageQuery {
  startDate?: Date;
  endDate?: Date;
  friendlyName?: string;
}
export const MediaType = [
  { laebl: "Image", value: 0, name: "图片" },
  { laebl: "Document", value: 1, name: "文档" },
  { laebl: "Archive", value: 2, name: "压缩文件" },
  { laebl: "Video", value: 3, name: "视频文件" },
  { laebl: "Unknown", value: 4, name: "未知类型" },
];
export interface Attachment {
  id?: number;
  attachmentId?: string;
  tenantType?: string;
  tenantId?: string;
  fileName?: string;
  bucketName?: string;
  objectName?: string;
  friendlyName?: string;
  mediaType?: number;
  mimeType?: string;
  fileSize?: number;
  uploaderIp?: string;
  description?: string;
  isDeleted?: boolean;
  deleteToken?: string;
  isTemporary?: boolean;
  createTime?: Date;
}
