import request from "@/utils/request";

// className租户类型  ProcessType 表示文件后台对应处理类型   images_source 原图
export const uploadConfig = {
  // 系统公告图片
  SysNoticeImg: { className: "sys_notice_img" },
  // 消息通知图片
  NoticeImg: { className: "notice_img" },

  // 系统附件直接发布,租户是是当前用户
  SysUserAttachments: { className: "sys_user", IsPublish: "is_publish" },
  // 系统用户头像附件，并进行压缩
  SysUserAvatar: { className: "sys_user_avatar", ProcessType: "is_compress" },

  // CMS
  CmsImg: { className: "cms_img" },
  CmsVideo: { className: "cms_video" },
  CmsCat: { className: "cms_cat" },
  CmsInvoke: { className: "cms_invoke" },
};

const FileAPI = {
  /**
   * 请求文件
   */
  getAttachment(attachmentId: string) {
    return request({
      url: `/api/file/${attachmentId}`,
      method: "get",
      responseType: "arraybuffer",
    });
  },
  /**
   * 上传文件为临时文件
   */
  uploadFile(data: UpFileType) {
    return request<any, ResponseData<FileInfo>>({
      url: "/api/file/upload-temporary",
      method: "post",
      data: data,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },

  uploadFileBytes(data: UpFileType) {
    return request<any, ResponseData<FileInfo>>({
      url: "/api/file/upload-temporary-byte",
      method: "post",
      data: data.bytes, // 直接发送字节数组
      headers: {
        "Content-Type": "application/octet-stream",
        FileName: data.data.fileName || "",
        ClassName: data.data.className || "",
        ProcessType: data.data.processType || "",
      },
    });
  },

  /**
   * 分片上传文件
   */
  uploadChunkApi(file: UpFileType) {
    return request<any, ResponseData<FileInfo>>({
      url: "/api/file/upload-chunk",
      method: "post",
      data: file,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  /**
   * 删除文件
   *
   * @param filePath 文件完整路径
   */
  delete(attachmentId: string) {
    return request({
      url: "/api/file/del-attid",
      method: "delete",
      params: { attachmentId: attachmentId },
    });
  },
  /**
   * 常规上传文件，主要用于不定义租户的场景，直接保存在公开目录中
   */
  upload(data: UpFileType) {
    return request<any, ResponseData<FileInfo>>({
      url: "/api/file/upload",
      method: "post",
      data: data,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
};

export default FileAPI;
/**
 * 文件API类型声明
 */
export interface FileInfo {
  name: string;
  url: string;
  attachmentId: string;
  fileName: string;
}

export interface UpFileType {
  data: {
    className?: string;
    chunkIndex?: string;
    totalChunks?: string;
    totalSize?: string;
    originalFileName?: string;
    fileName?: string;
    processType?: string;
    isPublish?: string;
  };
  headers: string;
  file: File | Blob;
  bytes: Uint8Array<ArrayBufferLike>;
}

/* FileType */
export type ImageMimeType =
  | "image/apng"
  | "image/bmp"
  | "image/gif"
  | "image/jpeg"
  | "image/pjpeg"
  | "image/png"
  | "image/svg+xml"
  | "image/tiff"
  | "image/webp"
  | "image/x-icon";

export type ExcelMimeType = "application/vnd.ms-excel" | "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
