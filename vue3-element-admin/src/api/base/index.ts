import request from "@/utils/request";

const SysBaseAPI = {
  getWorkspace(noticeNum: number) {
    return request({
      url: "/api/stat/workspace",
      method: "get",
      params: { noticeNum: noticeNum },
    });
  },
  getCmsCount() {
    return request({
      url: "/Api/stat/cmscount",
      method: "get",
    });
  },
};
export default SysBaseAPI;
