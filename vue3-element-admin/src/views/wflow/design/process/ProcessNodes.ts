import { defineAsyncComponent, AsyncComponentLoader } from "vue";
import { WorkflowStep, WorkflowStepType } from "@/api/workflow/step/index";
import CommonUtil from "@/library/Utils/CommonUtil";
import { ApprovalRuleType, ApprovalMode, TimeoutType, NoUserHandlerType, SameRootType, TaskModeType, TimeoutTimeUnit } from "@/api/workflow/step/index";
// 节点组件类型定义
type NodeComponent = ReturnType<typeof defineAsyncComponent>;
type NodeComponentsMap = Record<string, NodeComponent>;

// 节点组件导出
export const NodeComponents: NodeComponentsMap = Object.entries(import.meta.glob("./node/*.vue")).reduce((acc, [key, component]) => {
  const name = key.match(/([^/]+)\.vue$/)?.[1] || "";
  if (name) acc[name] = defineAsyncComponent(component as AsyncComponentLoader);
  return acc;
}, {} as NodeComponentsMap);
// 节点配置组件导出
export const NodeComponentConfigs: NodeComponentsMap = Object.entries(import.meta.glob("./config/*.vue")).reduce((acc, [key, component]) => {
  const name = key.match(/([^/]+)\.vue$/)?.[1] || "";
  if (name) acc[name] = defineAsyncComponent(component as AsyncComponentLoader);
  return acc;
}, {} as NodeComponentsMap);

// 网关节点创建函数
//  多个分支节点统一生成
const createGateway = (type: keyof typeof branchNode): WorkflowStep => ({
  id: CommonUtil.GenerateRandNumberId(),
  type: WorkflowStepType.Gateway,
  name: "网关节点",
  pid: 0,
  props: {
    type,
    branch: [branchNode[type].createSelf(1), branchNode[type].createSelf()],
  },
  branch: [[], []],
});

// 分支节点配置
const branchNode = {
  Exclusive: {
    name: "互斥条件",
    icon: "Share",
    color: "#1BB782",
    createSelf(i?: number): WorkflowStep {
      return {
        id: CommonUtil.GenerateRandNumberId(),
        type: WorkflowStepType.Exclusive,
        name: i ? `条件${i}` : "默认条件",
        pid: 0,
        props: {
          logic: true,
          groups: [{ logic: true, conditions: [] }],
        },
      };
    },
    create: () => createGateway(WorkflowStepType.Exclusive),
  },
  Parallel: {
    name: "并行分支",
    icon: "Operation",
    color: "#718dff",
    createSelf(i?: number): WorkflowStep {
      return {
        id: CommonUtil.GenerateRandNumberId(),
        type: WorkflowStepType.Parallel,
        name: `并行路径${i ?? 2}`,
        pid: 0,
        props: {},
      };
    },
    create: () => createGateway(WorkflowStepType.Parallel),
  },
} as const;

// 节点创建器
const createNode = <T extends WorkflowStep>(config: T) => config;

// 节点配置
export default {
  Approval: {
    name: "审批",
    icon: "Stamp",
    color: "#EF994F",
    create: () =>
      createNode({
        id: CommonUtil.GenerateRandNumberId(),
        type: WorkflowStepType.Approval,
        name: "审批",
        pid: 0,
        //序列化道具包，直接存入数据库可能会更合理
        props: {
          //审批模式，用户/角色
          mode: ApprovalMode.USER,
          //审批规则类型，指定用户/指定角色/部门领导/部门领导(包含上级)/上级领导/上级领导(包含上级)
          ruleType: ApprovalRuleType.ASSIGN_USER,
          //任务模式，会签/或签 且签/会签百分比
          taskMode: { type: TaskModeType.AND, percentage: 100 },
          //审批同意时是否需要签字
          needSign: false,
          //指定的审批人
          assignUser: [],
          //自选一个人或自选多个人
          selectMultiple: false,
          //上级领导  level指定主管级别  emptySkip对应部门主管未设置时
          leader: { level: 1, emptySkip: false },
          // level部门层级终点   emptySkip对应部门主管未设置时
          leaderTop: { level: 0, toEnd: false, emptySkip: false },
          //部门领导
          assignDept: { dept: [], type: ApprovalRuleType.LEADER },
          //部门领导(包含上级)
          assignDeptTop: { dept: [], type: ApprovalRuleType.LEADER_TOP },
          //指定角色
          assignRole: [],
          //无审批人处理
          noUserHandler: { type: NoUserHandlerType.TO_NEXT, assigned: [] },
          //当审批人与提交人为同一人时
          sameRoot: { type: SameRootType.TO_SELF, assigned: [] },
          //超时处理
          timeout: { enable: false, time: 1, timeUnit: TimeoutTimeUnit.M, type: TimeoutType.TO_PASS },
        },
      }),
  },
  Cc: {
    name: "抄送",
    icon: "Promotion",
    color: "#5994F3",
    create: () =>
      createNode({
        id: CommonUtil.GenerateRandNumberId(),
        type: WorkflowStepType.Cc,
        name: "抄送",
        pid: 0,
        props: {
          ruleType: ApprovalRuleType.ASSIGN_USER,
          assignUser: [],
          rootSelect: { multiple: false },
          assignDept: { dept: [], type: ApprovalRuleType.LEADER },
          assignRole: [],
        },
      }),
  },
  Start: {
    create: () =>
      createNode({
        id: CommonUtil.GenerateRandNumberId(),
        type: WorkflowStepType.Start,
        name: "发起人",
        pid: 0,
        props: {},
      }),
  },
  //分支节点
  ...branchNode,
};
