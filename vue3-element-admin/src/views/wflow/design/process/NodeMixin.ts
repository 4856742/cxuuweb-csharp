import { WorkflowStep, ApprovalMode } from "@/api/workflow/step/index";
//节点复用逻辑
export default {
  props: {
    modelValue: {
      type: Object as () => WorkflowStep,
      default: () => ({}),
    },
    approvalMode: {
      type: Number as () => ApprovalMode,
      default: ApprovalMode.USER,
    },
    //formItems: Object, //表单字段列表
    readonly: {
      type: Boolean,
      default: undefined,
    },
    index: {
      type: Number,
      default: 0,
    }, //处在当前支路位置的索引
    branch: {
      type: Array,
      default: () => [],
    }, //当前整个支路
  },
  computed: {
    _value: function (props: { modelValue: WorkflowStep }, emit: (arg0: string, arg1: any) => void) {
      return {
        get() {
          return props.modelValue;
        },
        set(val: WorkflowStep) {
          emit("update:modelValue", val);
        },
      };
    },
  },
  emits: ["update:modelValue", "delete", "insertNode", "select"],
};
