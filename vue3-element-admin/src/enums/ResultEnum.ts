/**
 * 响应码枚举
 */
export const enum ResultEnum {
  /**
   * 成功
   */
  SUCCESS = "SUCCESS",
  /**
   * 错误
   */
  Fail = "FAIL",
  /**
   * 未授权
   */
  Status403Forbidden = "FORBIDDEN",
  /**
   * 未登录
   */
  Status401Unauthorized = "UNAUTHENTICATED",
  /**
   * 未知错误
   */
  Status500InternalServerError = "UNKNOWN_ERROR",
  /**
   * 业务错误
   */
  BUSINESS_ERROR = "BUSINESS_ERROR",
  /**
   * 验证错误
   */
  VALIDATION_ERROR = "VALIDATION_ERROR",
}
