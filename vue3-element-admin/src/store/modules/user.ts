import { store } from "@/store";
import { usePermissionStoreHook } from "@/store/modules/permission";
import AuthAPI, { type Statistics, type UserLoginInfo, type LoginFormData, type UserInfo } from "@/api/auth/user/index";
import type { TenantStorageConfig, SysInfo } from "@/api/sys/sysconfig/index";
import type { RefreshTokenIn } from "@/api/auth/user/index";

import { setToken, clearToken, setRefreshToken, getRefreshToken, getToken } from "@/utils/auth";

export const useUserStore = defineStore("user", () => {
  const userLoginInfo: UserLoginInfo = {
    permCodes: <string[]>[],
    userInfo: <UserInfo>{},
    tenantStorageConfigs: <TenantStorageConfig[]>[],
    sysInfo: <SysInfo>{},
    statistics: <Statistics>{},
  };
  /**
   * 登录
   *
   * @param {LoginFormData}
   * @returns
   */
  function login(LoginFormData: LoginFormData) {
    return new Promise<void>((resolve, reject) => {
      AuthAPI.login(LoginFormData)
        .then((res) => {
          const accessToken = res.data;
          setToken(accessToken.accessToken ?? ""); // Bearer eyJhbGciOiJIUzI1NiJ9.xxx.xxx
          setRefreshToken(accessToken.refreshToken ?? ""); // Bearer eyJhbGciOiJIUzI1NiJ9.xxx.xxx
          resolve();
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * 获取用户信息
   *
   * @returns {UserInfo} 用户信息
   */
  function getUserInfo() {
    return new Promise<UserLoginInfo>((resolve, reject) => {
      AuthAPI.getUserInfoApi()
        .then((res) => {
          if (!res.data) {
            reject("Verification failed, please Login again.");
            return;
          }
          Object.assign(userLoginInfo, { ...res.data });
          resolve(res.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * 登出
   */
  function logout() {
    return new Promise<void>((resolve, reject) => {
      AuthAPI.loginOut()
        .then(() => {
          clearUserData();
          resolve();
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * 刷新 token
   */
  function refreshToken() {
    const refreshToken: RefreshTokenIn = {
      refreshToken: getRefreshToken(),
      accessToken: getToken(),
    };
    return new Promise<void>((resolve, reject) => {
      AuthAPI.refreshToken(refreshToken)
        .then((res) => {
          const accessToken = res.data;
          setToken(accessToken.newAccessToken ?? "");
          setRefreshToken(accessToken.newRefreshToken ?? "");
          resolve();
        })
        .catch((error) => {
          console.log(" refreshToken  刷新失败", error);
          reject(error);
        });
    });
  }

  /**
   * 清理用户数据
   *
   * @returns
   */
  function clearUserData() {
    return new Promise<void>((resolve) => {
      clearToken();
      usePermissionStoreHook().resetRouter();
      //useDictStoreHook().clearDictionaryCache();
      resolve();
    });
  }

  return {
    userLoginInfo,
    getUserInfo,
    login,
    logout,
    clearUserData,
    refreshToken,
  };
});

/**
 * 用于在组件外部（如在Pinia Store 中）使用 Pinia 提供的 store 实例。
 * 官方文档解释了如何在组件外部使用 Pinia Store：
 * https://pinia.vuejs.org/core-concepts/outside-component-usage.html#using-a-store-outside-of-a-component
 */
export function useUserStoreHook() {
  return useUserStore(store);
}
