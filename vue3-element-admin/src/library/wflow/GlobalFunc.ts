import CommonUtil from "@/library/Utils/CommonUtil";

// 生成指定长度的随机字符串
export function generateStr(len: number): string {
  let result = "";
  const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < len; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// 深拷贝对象
export function deepCopy<T>(obj: T): T {
  return JSON.parse(JSON.stringify(obj));
}

// 删除数组中指定索引的元素
export function delField(cols: any[], i: number): void {
  cols.splice(i, 1);
}

// 复制数组中指定索引的元素并添加到数组末尾
export function copyField(cols: any[], i: number): void {
  const col = deepCopy(cols[i]);
  col.id = CommonUtil.GenerateRandNumberId();
  col.key = col.type + "_" + generateStr(8);
  cols.push(col);
}

export default {
  deepCopy,
};
