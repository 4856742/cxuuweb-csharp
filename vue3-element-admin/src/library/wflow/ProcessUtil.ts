import CommonUtil from "@/library/Utils/CommonUtil";
import { WorkflowStepType } from "@/api/workflow/step/index";
// 重载所有的节点id
export function reloadNodeId(nodes: any[] | { id: number; type: number; props?: { branch: any[] }; branch?: any[] }): void {
  if (Array.isArray(nodes)) {
    nodes.forEach((node) => {
      if (node.type === WorkflowStepType.Gateway) {
        // 递归网关，网关id加上一个后缀
        node.id = CommonUtil.GenerateRandNumberId();
        // 分支头部节点
        if (node.props && node.props.branch) {
          reloadNodeId(node.props.branch);
        }
        // 分支
        if (node.branch) {
          reloadNodeId(node.branch);
        }
      } else {
        node.id = CommonUtil.GenerateRandNumberId();
      }
    });
  } else {
    nodes.id = CommonUtil.GenerateRandNumberId();
  }
}
