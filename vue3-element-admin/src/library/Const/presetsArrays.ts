//此处预设均与后台枚举或常量对应
export const DeptTypes = [
  { value: 1, label: "国家级" },
  { value: 2, label: "省部级" },
  { value: 3, label: "地市级" },
  { value: 4, label: "市局直属" },
  { value: 5, label: "所队室" },
  { value: 6, label: "其他" },
];

/**
 * 用户岗位数据权限
 */
export const dataPermissions = [
  { key: 0, value: "本用户" },
  { key: 1, value: "全部权限" },
  { key: 2, value: "本部门及以下" },
  { key: 3, value: "本部门" },
  { key: -1, value: "指定部门" },
];

/**
 * 上传附件类型
 */
export const attmentTypes = [
  { key: 1, value: "图像" },
  { key: 2, value: "文件" },
  { key: 3, value: "视频" },
  { key: 4, value: "音频" },
];

/**
 * 资产管理模型
 */
export const asssetTypes = [
  { key: 1, value: "目录" },
  { key: 2, value: "固定资产" },
  { key: 3, value: "耗材" },
  { key: 4, value: "车辆" },
  { key: 5, value: "装备" },
  { key: 6, value: "周转房" },
];

/**
 * 通用分类模型
 */
export const ModelTypes = [
  { value: 2, label: "内容" },
  { value: 1, label: "目录" },
];

/**
 * 公文模型
 */
export const OasWorkTypes = [
  { key: 2, value: "内容" },
  { key: 1, value: "目录" },
];
/**
 * 公文发文文号类型
 */
export const docNumTypes = [
  { key: 2, value: "自动生成" },
  { key: 1, value: "人工填写" },
];

/**
 * 公文发文填写模型
 */
export const docPostFillInTypes = [
  //{ key: 1, value: "人工文号", fieldName: "docNumber" },
  //{ key: 2, value: "自动文号", fieldName: "docNumberString" },
  //{ key: 3, value: "发文标题", fieldName: "title" },
  { key: 4, value: "密级", fieldName: "secretLevel" },
  { key: 5, value: "发往部门", fieldName: "department" },
  { key: 6, value: "报送领导", fieldName: "leader" },
  //{ key: 7, value: "拟稿", fieldName: "auther" },
  { key: 8, value: "关联帐号", fieldName: "associated" },
  { key: 9, value: "关键词", fieldName: "keywords" },
  //{ key: 10, value: "终审", fieldName: "examine" },
  { key: 11, value: "内容", fieldName: "content" },
  { key: 12, value: "附件", fieldName: "attments" },
  { key: 13, value: "备注", fieldName: "remarks" },
];
/**
 * 性别
 */
export const GenderTypes = [
  { value: 3, label: "未知" },
  { value: 2, label: "女" },
  { value: 1, label: "男" },
];

export const BoolTypes = [
  { value: true, label: "是" },
  { value: false, label: "否" },
];
