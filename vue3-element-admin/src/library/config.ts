/**
 * 系统配置
 */
const hostURL = import.meta.env.VITE_APP_API_URL;
//singalR 响应地址
const msgHub = hostURL + "/chatHub";

export const systemConfig = {
  hostURL: hostURL,
  msgHub: msgHub,
};
