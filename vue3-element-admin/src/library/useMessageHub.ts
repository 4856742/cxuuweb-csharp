import * as signalR from "@microsoft/signalr";
import { systemConfig } from "./config";
import { getToken } from "@/utils/auth";

export interface MessageVo {
  postUid: number;
  postName?: string;
  getUid?: string;
  title: string;
  content?: string;
  url?: string;
  status?: boolean;
  img?: string;
}

const connection = new signalR.HubConnectionBuilder()
  .withUrl(systemConfig.msgHub, {
    accessTokenFactory: () => {
      const token = getToken();
      // 移除可能存在的Bearer前缀
      //return token.replace(/^Bearer\s+/i, "");
      return token;
    },
    withCredentials: true,
  })
  .withAutomaticReconnect({
    nextRetryDelayInMilliseconds: () => {
      return 5000; // 每5秒重连一次
    },
  }) //自动重新连接
  .configureLogging(signalR.LogLevel.Warning)
  .build();

connection.keepAliveIntervalInMilliseconds = 15 * 1000; // 心跳检测15s
connection.serverTimeoutInMilliseconds = 30 * 60 * 1000; // 超时时间30m

// 启动连接
connection.start().then(() => {
  //console.log(connection);
});

//手动断开连接
// const disconnect = async () => {
//   await connection.stop();
// };

// 断开连接
connection.onclose(async () => {
  ElMessage.error("服务器已断线");
});
// 重连中
connection.onreconnecting(() => {
  ElMessage.success("服务器重新连线中...");
});
// 重连成功
connection.onreconnected(() => {
  ElMessage.success("重连成功");
});

export { connection as signalR };
