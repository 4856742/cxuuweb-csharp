export class StringUtil {
  /**
   * 根据身份证号算出当前年龄岁
   * @param idCard
   * @returns
   */
  calculateAgeFromIdCard(idCard: string) {
    // 验证是否是18位身份证号码
    // if (!/^\d{17}[\dXx]$/.test(idCard)) {
    //   throw new Error("Invalid ID card number");
    // }
    // 提取出生日期部分（第7到第14位）
    const birthDateString = idCard.slice(6, 14);

    // 将出生日期字符串转换为日期对象
    const year = parseInt(birthDateString.slice(0, 4), 10);
    const month = parseInt(birthDateString.slice(4, 6), 10) - 1; // 月份从0开始
    const day = parseInt(birthDateString.slice(6, 8), 10);
    const birthDate = new Date(year, month, day);

    // 获取当前日期
    const currentDate = new Date();

    // 计算年龄
    let age = currentDate.getFullYear() - birthDate.getFullYear();
    const monthDiff = currentDate.getMonth() - birthDate.getMonth();
    const dayDiff = currentDate.getDate() - birthDate.getDate();

    if (monthDiff < 0 || (monthDiff === 0 && dayDiff < 0)) {
      age--;
    }

    return age;
  }

  /**
   * 根据身份证号算出 人员性别
   * @param idCard
   * @returns  1为男性 0为女性
   */
  determineGenderFromIdCard(idCard: string) {
    // 验证是否是18位身份证号码
    // if (!/^\d{17}[\dXx]$/.test(idCard)) {
    //   throw new Error("Invalid ID card number");
    // }
    // 提取第17位数字（性别码）
    const genderCode = parseInt(idCard.charAt(16), 10);
    // 判断性别
    const gender = genderCode % 2 === 0 ? 0 : 1;
    return gender;
  }
}
export default new StringUtil();
