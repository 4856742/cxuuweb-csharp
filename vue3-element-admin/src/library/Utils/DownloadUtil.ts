import FileAPI from "@/api/sys/file/file";
import { systemConfig } from "../config";
/**
 * 工具类
 */
export class DownloadUtil {
  /**
   * 文件下载URL
   */
  downloadFileForUrl(url: string, fileName: string) {
    const down = document.createElement("a");
    down.download = fileName;
    down.style.display = "none"; //隐藏,没必要展示出来
    down.href = systemConfig.hostURL + url;
    document.body.appendChild(down);
    down.click();
    URL.revokeObjectURL(down.href); // 释放URL 对象
    document.body.removeChild(down); //下载完成移除
  }
  /**
   * Excel文件下载 流
   */
  downloadExcelFile(response: any) {
    const matchesStar = response.headers["content-disposition"].match(/filename\*=([^;\n]*)/);
    let utf8Filename = "";
    if (matchesStar && matchesStar[1]) {
      utf8Filename = decodeURIComponent(matchesStar[1].replace(/^UTF-8''/, "")); // 提取并反编码 filename*
    } else {
      utf8Filename = "未命名文件";
    }
    //console.log(response.headers["content-type"])
    const blob = new Blob([response.data], { type: response.headers["content-type"] });
    const downloadUrl = window.URL.createObjectURL(blob);
    const downloadLink = document.createElement("a");
    downloadLink.href = downloadUrl;
    downloadLink.download = utf8Filename;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
    window.URL.revokeObjectURL(downloadUrl);
  }

  /**
   * 通用文件下载方法
   * @param response - 响应对象
   * @param defaultName - 默认文件名（可选）
   */
  downloadFile(response: any, defaultName = "未命名文件") {
    let filename = defaultName;
    const contentDisposition = response.headers["content-disposition"];
    if (contentDisposition) {
      // 优先使用 filename* 编码
      const matchesStar = contentDisposition.match(/filename\*=([^;\n]*)/);
      if (matchesStar && matchesStar[1]) {
        filename = decodeURIComponent(matchesStar[1].replace(/^UTF-8''/, ""));
      } else {
        // 使用普通 filename
        const matches = contentDisposition.match(/filename="([^"]+)"/);
        if (matches && matches[1]) {
          filename = matches[1];
        }
      }
    }

    // 创建 Blob 对象
    const blob = new Blob([response.data], {
      type: response.headers["content-type"] || "application/octet-stream",
    });

    // 创建下载链接
    const downloadUrl = window.URL.createObjectURL(blob);
    const downloadLink = document.createElement("a");
    downloadLink.href = downloadUrl;
    downloadLink.download = filename;
    document.body.appendChild(downloadLink);
    downloadLink.click();

    // 清理
    document.body.removeChild(downloadLink);
    window.URL.revokeObjectURL(downloadUrl);
  }

  //下载文件
  async downloadAttachmentFile(attachmentId?: string, defaultName?: string) {
    try {
      if (!attachmentId) return;
      let res = await FileAPI.getAttachment(attachmentId);
      this.downloadFile(res, defaultName);
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * 将 arraybuffer 转换为图片 URL
   * @param arrayBuffer - 图片的 arraybuffer 数据
   * @param contentType - 图片的 MIME 类型
   * @returns 图片的 URL
   */
  arrayBufferToImageUrl(arrayBuffer: ArrayBuffer, contentType: string): string {
    const blob = new Blob([arrayBuffer], { type: contentType });
    return URL.createObjectURL(blob);
  }
}

export default new DownloadUtil();
