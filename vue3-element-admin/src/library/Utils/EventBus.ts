// 定义事件名称的枚举
export enum EventTypeName {
  DialogIsOkRefreshData, // 对话框关闭后刷新指定数据
  OPEN_REPORT, // 打开报告
  Read_Notice, // 打开报告
}

// 定义事件总线对象
const EventBus = {
  list: {} as { [key in EventTypeName]: Array<(data?: any) => void> },

  // 订阅
  listen(name: EventTypeName, fn: (data?: any) => void) {
    if (!this.list[name]) {
      this.list[name] = [];
    }
    this.list[name].push(fn);
  },

  // 发布
  notify(name: EventTypeName, data?: any) {
    if (this.list[name]) {
      this.list[name].forEach((fn) => {
        fn(data);
      });
    }
  },

  // 取消订阅
  unListen(name: EventTypeName) {
    if (this.list[name]) {
      Reflect.deleteProperty(this.list, name); // 删除指定事件的订阅列表
    }
  },
};

export default EventBus;

// 使用示例
// 订阅事件
// EventBus.listen(EventTypeName.OPEN_REPORT, (data: any) => {
//   console.log("data :>> ", data);
//   const lti48Obj = {
//     1: "inspection", // 检验
//     2: "microorganism", // 微生物
//     99: "check", // 检查
//   };
//   // 假设 state 是某种全局状态或组件状态
//   const state = {
//     reportId: "",
//     defaultValue: "",
//   };
//   state.reportId = data.reportId; // 报告id
//   state.defaultValue = lti48Obj[data.lti48];
// });

// // 发布事件
// const row = { reportId: "123", lti48: 1 };
// EventBus.notify(EventTypeName.OPEN_REPORT, row);

// 取消订阅事件（通常在组件卸载时调用）
// 假设 onUnmounted 是 Vue 3 的生命周期钩子
// import { onUnmounted } from 'vue';
// onUnmounted(() => {
//   EventBus.unListen(EventTypeName.OPEN_REPORT);
// });
