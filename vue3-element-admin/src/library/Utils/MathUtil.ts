export class MathUtil {
  /**
   * 平均值，取小数点后两位
   * @param list
   * @param field
   * @returns
   */
  meanNum(list: any[], field: string) {
    let count = 0;
    list.forEach((item) => {
      count += Number(item[field]);
    });
    let res = count / list.length;
    return res > 0 ? res.toFixed(2) : "";
  }

  /**
   * 合值
   * @param list
   * @param field
   * @returns
   */
  sumNum(list: any[], field: string) {
    let count = 0;
    list.forEach((item) => {
      count += Number(item[field]);
    });
    return count > 0 ? count : "";
  }

  /**
   * 求百分比
   * @param num
   * @param total
   * @returns
   */
  getPercent(num: number, total: number) {
    if (isNaN(num) || isNaN(total)) {
      return "-";
    }
    return total <= 0 ? "0" : Math.round((num / total) * 10000) / 100.0;
  }
}
export default new MathUtil();
