import { systemConfig } from "../config";
import { greetings } from "../text";
/**
 * 工具类
 */
export class CommonUtil {
  /**
   * 生成唯一数字ID 使用当前时间戳的毫秒部分加上随机数
   * @returns  // 8765432123
   */
  GenerateRandNumberId(): number {
    // 获取时间戳的毫秒部分，加上4位随机数
    const timestamp = Date.now() % 1000000; // 取后6位
    const random = Math.floor(Math.random() * 10000);
    return timestamp * 10000 + random; // 将时间戳左移4位，加上随机数
  }

  /**
   * 生成唯一ID
   * @returns  // 12c96135-d4b6-488f-ba1d-449b27851e0b
   */
  GenerateUuid(style = "xxxxxx-xxxx-yxxx-xxxxxxxxxxxx") {
    return style.replace(/[xy]/g, function (c) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  /**
   * @description 生成唯一 uuid
   * @returns {String}
   */
  GenerateUUID(): string {
    let uuid = "";
    for (let i = 0; i < 32; i++) {
      let random = (Math.random() * 16) | 0;
      if (i === 8 || i === 12 || i === 16 || i === 20) uuid += "-";
      uuid += (i === 12 ? 4 : i === 16 ? (random & 3) | 8 : random).toString(16);
    }
    return uuid;
  }

  /**
   * 生成一个不重复的ID
   */
  GenNonDuplicateID(randomLength = 6) {
    return Number(Math.random().toString().substring(3, randomLength) + Date.now()).toString(36);
  }

  /**
   * 截取字符串
   * subStrs(str,0,20);
   * @param str（字符串）
   * @param start（截取开始）
   * @param end （截取结束）
   */
  subStrs(str: string, start = 0, end = 20) {
    if (str === null || str === undefined) return;
    if (str.length > end) {
      return str.substring(start, end) + " ...";
    } else {
      return str.substring(start, end);
    }
  }

  /**
   * 获取字符串中所有图片
   * subStrs(str);
   * @param str（字符串）
   */

  getAllImagesFromString(input: string): string[] {
    // 正则表达式匹配 <img> 标签中的 src 属性
    const imgRegex = /<img[^>]+src="([^">]+)"/g;
    const images: string[] = [];
    let match: RegExpExecArray | null;
    // 循环匹配所有图片 URL
    while ((match = imgRegex.exec(input)) !== null) {
      images.push(match[1]); // 将匹配到的图片 URL 添加到数组中
    }
    return images; // 返回所有图片 URL 的数组
  }

  // 排除非本服务器图片地址
  excludeGeghttp(src: string) {
    var x = src.match(/[^.]/);
    return x?.[0] === "/" ? true : false;
  }

  /**
   * 获取字符串中第某张图片
   * subStrs(str);
   * @param str（字符串）
   * @param n（第几张）
   */
  getNthImageFromString(input: string, n: number): string | null {
    // 正则表达式匹配 <img> 标签中的 src 属性
    const imgRegex = /<img[^>]+src="([^">]+)"/g;
    let match: RegExpExecArray | null;
    let count = 0;
    while ((match = imgRegex.exec(input)) !== null) {
      count++;
      if (count === n) {
        return match[1]; // 返回第 n 张图片的 URL
      }
    }
    return null; // 如果没有找到第 n 张图片，返回 null
  }

  /**
   * 随机显示问候语
   */
  randomGreetings() {
    let arr = [...greetings];
    return arr[Math.floor(Math.random() * arr.length)];
  }

  /**
   * 包含兼容性处理（useClipboard 有兼容性问题）
   */
  copyText(text: string) {
    if (navigator.clipboard && navigator.clipboard.writeText) {
      // 使用 Clipboard API
      navigator.clipboard
        .writeText(text)
        .then(() => {
          ElMessage.success("已复制到剪贴板");
        })
        .catch(() => {
          ElMessage.warning("复制文本失败，请手动复制");
        });
    } else {
      const input = document.createElement("input");
      input.style.position = "absolute";
      input.style.left = "-9999px";
      input.setAttribute("value", text);
      document.body.appendChild(input);
      input.select();
      try {
        const successful = document.execCommand("copy");
        if (successful) {
          ElMessage.success("已复制到剪贴板");
        } else {
          ElMessage.warning("复制文本失败，请手动复制");
        }
      } catch {
        ElMessage.error("复制文本失败，请手动复制");
      } finally {
        document.body.removeChild(input);
      }
    }
  }
}

export default new CommonUtil();
