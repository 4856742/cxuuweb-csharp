export class TimeUtil {
  queryTime(type: number, queryTime: string) {
    if (!queryTime) {
      return "";
    }
    let subTime = new Date(queryTime);
    if (type == 1 || type == 2) {
      return subTime.getFullYear() + "年" + (subTime.getMonth() + 1) + "月" + subTime.getDate() + "日";
    }
    if (type == 3) {
      return subTime.getFullYear() + "年 第" + this.getYearWeek(subTime) + "周";
    }
    if (type == 4) {
      return subTime.getFullYear() + "年" + (subTime.getMonth() + 1) + "月";
    }
    if (type == 5) {
      return subTime.getFullYear() + "年 第" + this.getYearSeason(subTime) + "季度";
    }
    if (type == 6) {
      return subTime.getFullYear() + "年";
    }
  }
  /**
   * 检查每种类型填报状态  对应  dssTableTypes
   * @param type
   * @param submitTime
   */
  submitStats(type: number, submitTime: string, systemTime: string, isEdit = false) {
    if (isEdit && !submitTime) return true;
    if (!submitTime || submitTime == "0001-01-01 00:00:00") {
      return false;
    }
    let subTime = new Date(submitTime);
    let subTime_Year = subTime.getFullYear();
    let subTime_Month = subTime.getMonth() + 1;
    let subTime_Date = subTime.getDate();
    let subTime_Week = this.getYearWeek(subTime);
    let subTime_Season = this.getYearSeason(subTime);

    let sysTime = new Date(systemTime);
    let sysTime_Year = sysTime.getFullYear();
    let sysTime_Month = sysTime.getMonth() + 1;
    let sysTime_Date = sysTime.getDate();
    let sysTime_Week = this.getYearWeek(sysTime);
    let sysTime_Season = this.getYearSeason(sysTime);
    //console.log(subTime_Week, sysTime_Week)
    if (type == 1) {
      return true;
    }
    if (type == 2) {
      return subTime_Year == sysTime_Year && subTime_Month == sysTime_Month && subTime_Date == sysTime_Date;
    }
    if (type == 3) {
      return subTime_Year == sysTime_Year && subTime_Week == sysTime_Week;
    }
    if (type == 4) {
      return subTime_Year == sysTime_Year && subTime_Month == sysTime_Month;
    }
    if (type == 5) {
      return subTime_Year == sysTime_Year && subTime_Season == sysTime_Season;
    }
    if (type == 6) {
      return subTime_Year == sysTime_Year;
    }
  }

  //计算本年第几季度
  getYearSeason(dataTime: Date) {
    var month = dataTime.getMonth() + 1;
    if (month >= 1 && month <= 3) {
      return 1;
    } else if (month >= 4 && month <= 6) {
      return 2;
    } else if (month >= 7 && month <= 9) {
      return 3;
    } else {
      return 4;
    }
  }

  //计算本年的周数
  getYearWeek(dataTime: Date) {
    //本年的第一天
    var beginDate = new Date(dataTime.getFullYear(), 0, 1);
    //星期从0-6,0代表星期天，6代表星期六
    var endWeek = dataTime.getDay();
    if (endWeek == 0) endWeek = 7;
    var beginWeek = beginDate.getDay();
    if (beginWeek == 0) beginWeek = 7;
    //计算两个日期的天数差
    var millisDiff = dataTime.getTime() - beginDate.getTime();
    var dayDiff = Math.floor((millisDiff + (beginWeek - endWeek) * (24 * 60 * 60 * 1000)) / 86400000);
    return Math.ceil(dayDiff / 7) + 1;
  }

  /**
   * 格式化时间
   * 调用formatDate(strDate, 'yyyy-MM-dd HH:MM:ss');
   * @param strDate（中国标准时间、时间戳等）
   * @param strFormat（返回格式）
   */
  formatDate(strDate: any, strFormat?: any) {
    if (!strDate) {
      return;
    }
    if (!strFormat) {
      strFormat = "yyyy-MM-dd";
    }
    switch (typeof strDate) {
      case "string":
        strDate = new Date(strDate.replace(/-/, "/"));
        break;
      case "number":
        strDate = new Date(strDate);
        break;
    }
    if (strDate instanceof Date) {
      const dict: any = {
        yyyy: strDate.getFullYear(),
        yy: strDate.getFullYear() % 100,
        M: strDate.getMonth() + 1,
        d: strDate.getDate(),
        H: strDate.getHours(),
        m: strDate.getMinutes(),
        s: strDate.getSeconds(),
        MM: ("" + (strDate.getMonth() + 101)).substring(1),
        dd: ("" + (strDate.getDate() + 100)).substring(1),
        HH: ("" + (strDate.getHours() + 100)).substring(1),
        mm: ("" + (strDate.getMinutes() + 100)).substring(1),
        ss: ("" + (strDate.getSeconds() + 100)).substring(1),
      };
      return strFormat.replace(/(yyyy|yy?|MM?|dd?|HH?|mm?|ss?)/g, function () {
        return dict[arguments[0]];
      });
    }
  }

  //  比较时间的大小
  compareTime(date1 = "0001-01-01 00:00:00", date2 = "1970-01-01 00:00:00") {
    // 因为苹果的时间格式是2020/10/19/ 00:00:00这样的格式；进行处理下
    let date_initial = date1.replace(/-/g, "/");
    let data_finsh = date2.replace(/-/g, "/");

    var oDate1 = new Date(date_initial);
    var oDate2 = new Date(data_finsh);
    if (oDate1.getTime() > oDate2.getTime()) {
      return 1;
    } else if (oDate1.getTime() < oDate2.getTime()) {
      return 2;
    } else if (oDate1.getTime() == oDate2.getTime()) {
      return 0;
    }
  }

  isSafeTime(timeStr: string | number | Date) {
    const time = new Date(timeStr);
    // 判断时间是否在有效范围内
    // 检查日期是否有效（例如，不是Invalid Date）
    if (!isNaN(time.getTime())) {
      let year = time.getFullYear();
      return year >= 2002 && year <= 2999;
    } else {
      return false;
    }
  }
}
export default new TimeUtil();
