export class ArrayUtil {
  /**
   * 自定数组常量，获取数组对象中的一条     *
   * @param arr 数组
   * @param key  数字
   */

  find(arr: any, key: number) {
    if (arr != null) {
      let ar = arr.find((item: { key: number }) => item.key === key) ?? null;
      return ar != null ? ar.value : null;
    } else {
      return null;
    }
  }
  // dictFind(arr: any, value: number) {
  //   if (arr != null) {
  //     let ar =
  //       arr.find((item: { value: number }) => item.value === value) ?? null;
  //     return ar != null ? ar.label : null;
  //   } else {
  //     return null;
  //   }
  // }
  /**
   * 获取数据差异
   *
   * @param arr1 数组
   * @param arr2 数组
   */

  diff(arr1: any[], arr2: any[]) {
    arr1 = Array.from(new Set(arr1));
    arr2 = Array.from(new Set(arr2));
    var mergeArr = arr1.concat(arr2);
    return mergeArr.filter((x) => !(arr1.includes(x) && arr2.includes(x)));
  }
}
export default new ArrayUtil();
