<div align="center">
  <img alt="vue3-element-admin" width="90" height="90" src="./vue3-element-admin/src/assets/logo.png">
  <h1>琪耀Admin NEXT 2025</h1>
</div>

### 技术栈

- 后端：.NET9 .NET8 .NET6 开发框架基于： [PmSoft 通用 Web 框架](https://gitee.com/pingmac/pmsoft)
- 前端： Vue3 Element Vite TypeScript 开发框架基于：[vue3-element-admin](https://gitee.com/youlaiorg/vue3-element-admin)
- 集成数据库： MYSQL mariadb，可根据实际情况自行更换和增加其它数据库支持
- 缓存： Redis + Memcached

### 系统特点

- 简单易用的后台管理系统
- 支持多场景使用，可作为文件传输、站内邮件、CMS 等使用
- 初学者可快速上手，简单易用
- 可自由扩展，可自由定制

- layui VUE 版本请看 V1 V2 分支

### 基本功能

- 完善强大的权限管理：部门、角色、用户控制系统权限及数据权限
- 灵活的平台菜单管理：对系统后台菜单显示、角色权限、按钮权限进行控制
- 独立的分表日志管理：用户的操作业务的日志的查看和管理
- 易用的公告管理演示：系统的公告的管理
- 实时在线和站内通知：自由的站内通知功能，可作为文件传输、站内邮件功能使用
- 统一强大的附件管理：常规网盘式文件的上传下载查看等操作
- 通用全面的系统配置：系统运行的参数的维护，参数的配置
- 独立的模块参数配置：对系统常用的变量参数进行灵活设置，支持多种类型数据
- 无藕的项目模块开发：方便独立集成的项目模块开发，可自由扩展
- 集成成熟的 CMS 模块：针对机关事业单位内网和官方网站定制 CMS 管理模块，可定制的专题管理、模块调用等功能
- 更多功能和精彩等待您探索

### 环境要求

NET9
MYSQL mariaDB
VS 2022

后台用户名及密码：admin 123456

后端项目启动入口：QyProject\Web.Entry

前端目录：vue3-element-admin

### 演示站址

后台演示：[后台演示](https://cxuu.top/sys/) admin 123456

### 系统截图

![后台照片](demoimg/01.png)

![后台照片](demoimg/02.png)

![后台照片](demoimg/03.png)

![后台照片](demoimg/04.png)

![后台照片](demoimg/05.png)

![后台照片](demoimg/06.png)

![后台照片](demoimg/07.png)

![后台照片](demoimg/08.png)
