﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Entities;
using Qy.Base.Entities.Auth;
using Qy.Base.Utilities;

namespace Qy.Base.Services.SysNotices;

[Inject(ServiceLifetime.Scoped)]
public class SysNoticeRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, SysNotice, int>(dbContext, applicationContext)
{

    public async Task<IPagedList<SysNotice>> GetPagingAsync(SysNoticeQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QySysNotice);
        if (!string.IsNullOrEmpty(query.KeyWords))
            sql.Where("title like @0", "%" + StringUtility.StripSQLInjection(query.KeyWords) + "%");
        if (query.Status != null)
            sql.Where("status = @0", query.Status);
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }

    public async Task<IEnumerable<SysNotice>> GetListAsync(int getLimit)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QySysNotice);
        sql.Where("status = 1");
        sql.Append("limit @0", getLimit);
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }
}
