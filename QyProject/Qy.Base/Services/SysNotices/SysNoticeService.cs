﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Entities;
using Qy.Base.Entities.Auth;
using Qy.Base.Services.Attachment;

namespace Qy.Base.Services.SysNotices;

[Inject(ServiceLifetime.Scoped)]
public class SysNoticeService(
    SysNoticeRepository noticeRepository,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    IAttachmentService attmentService,
    AttachmentHelper attachmentHelper,
    IApplicationContext applicationContext)
{

    public async Task<IEnumerable<SysNotice>> GetListAsync(int getLimit)
    {
        return await noticeRepository.GetListAsync(getLimit);
    }
    public async Task<IPagedList<ViewSysNotice>> GetPageDAsync(SysNoticeQuery query)
    {
        var pagedList = await noticeRepository.GetPagingAsync(query);
        return await pagedList.ToPagedDto(m =>
        {
            return m.AsView();
        })
        .WithRelatedAsync(
            foreignKeySelector: attment => attment.Uid,
            loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
            attachAction: (v, user) => v.NickName = user.NickName
        );
    }
    public async Task<object> InsertAsync(SysNotice notice)
    {
        notice.Uid = applicationContext.RequiredCurrentUser.UserId;
        notice.GreatTime = DateTime.Now;
        return await noticeRepository.InsertAsync(notice);
    }
    public async Task<int> UpdateAsync(SysNotice notice)
    {
        return await noticeRepository.UpdateAsync(notice);
    }
    public async Task<SysNotice?> GetAsync(int id)
    {
        return await noticeRepository.GetAsync(id);
    }
    public async Task<IEnumerable<SysNotice>> GetNoticesAsync(IEnumerable<int> ids)
    {
        return await noticeRepository.GetEntitiesByIdsAsync(ids);
    }
    public async Task<int> DeleteAsync(int id)
    {
        var get = await GetAsync(id);
        if (get != null)
        {
            var attIds = await attachmentHelper.ExtractAttachmentIds(get.Content);
            await attmentService.DeleteBatchAsync(attIds);
            return await noticeRepository.DeleteAsync(get);
        }
        return 0;
    }

    /// <summary>
    /// 批量发布内容图片
    /// </summary>
    /// <param name="content"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public async Task PublishAttachments(string content, object tenantId)
    {
        //var attachments =
            await attachmentHelper.PublishAttachmentsContentAsync(content, tenantId);
    }
}
