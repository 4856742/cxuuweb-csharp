﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;

namespace Qy.Base.Services.Auth.QyUserDept;

[Inject(ServiceLifetime.Scoped)]
public class UserDeptService(
    UserDeptRepository userDeptRepository,
    UserRepository userRepository)
{

    public async Task<IEnumerable<UserDept>> GetAllDeptAsync()
    {
        return await userDeptRepository.GetAllDeptAsync();
    }

    public async Task<UserDept?> GetAsync(int id)
    {
        IEnumerable<UserDept> userDepts = await GetAllDeptAsync();
        return userDepts.FirstOrDefault(a => a.Id == id);
    }
    public async Task<IEnumerable<UserDept>> GetDeptsAsync(IEnumerable<int> ids)
    {
        return await userDeptRepository.GetEntitiesByIdsAsync(ids);
    }
    /// <summary>
    /// 某部门最上级部门的id
    /// </summary>
    /// <param name="userDepts"></param>
    /// <param name="did"></param>
    /// <returns></returns>
    private static int FindTopMostPid(List<UserDept> userDepts, int did)
    {
        UserDept? currentDept = userDepts.FirstOrDefault(d => d.Id == did);
        if (currentDept == null) return -1;
        if (currentDept.Pid == 0) return currentDept.Id;
        else
        {
            UserDept? parentDept = userDepts.FirstOrDefault(d => d.Id == currentDept.Pid);
            if (parentDept == null) return -1;
            return FindTopMostPid(userDepts, currentDept.Pid);
        }
    }

    /// <summary>
    /// 当前用户所在部门的权限部门信息
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public async Task<IEnumerable<OptionItemDto>> IndentedOptionAuthAsync(AuthUser user)
    {
        IEnumerable<UserDept> list = await GetAllDeptAsync();
        IList<OptionItemDto> optionTypes = [];
        //如果为超级管理员或有全部权限的
        if (user.IsSuperAdminAndAllAuthData())
        {
            optionTypes= list.AsIndentedOptionType();
            return optionTypes;
        }
        if (user.Dids == null)
        {
            return [];
        }
        var depts = list.Where(x => user.Dids.Contains(x.Id)).ToList();
        //如果指定部门权限的
        if (user.AccessLevel == AccessLevelType.SpecifyDepartment)
        {
            List<OptionItemDto> commonOptionTypes = [];
            foreach (IHierarchicalEntity item in depts)
            {
                OptionItemDto treeModel = new()
                {
                    Value = item.Id,
                    Label = item.Name,
                    Type = item.Type,
                };
                commonOptionTypes.Add(treeModel);
            }
            return commonOptionTypes;
        }

        if (depts.Count != 0)
        {
            int pid = FindTopMostPid(list.ToList(), user.DeptId);
            optionTypes = depts.AsIndentedOptionType(pid);
        }
        return optionTypes;
    }
    public async Task<IEnumerable<ViewUserDeptOptions>> GetPagingOptionsAsync(UserDeptQuery query)
    {

        IEnumerable<UserDept> userDepts = await userDeptRepository.GetPagingListAsNameAsync(query);
        return userDepts.Select(x => new ViewUserDeptOptions()
        {
            Value = x.Id,
            Label = x.Name,
        });
    }
    /// <summary>
    /// 所有部门
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<OptionItemDto>> IndentedOptionAsync()
    {
        IEnumerable<UserDept> list = await GetAllDeptAsync();
        return list.AsIndentedOptionType();
    }


    public async Task<bool> CheckHaveSonAsync(int id)
    {
        IEnumerable<UserDept> userDepts = await GetAllDeptAsync();
        return userDepts.Any(a => a.Pid == id);
    }

    #region 获取所有下级

    /// <summary>  
    /// 获取当前部门及子部门ID值字符串  
    /// </summary>  
    /// <param name="Pid"></param>  
    /// <returns></returns>  
    public async Task<IEnumerable<int>> GetDeptAndSonIdsAsync(int Pid)
    {
        var userDepts = await GetAllDeptAsync();
        // 获取当前部门  
        var currentDept = userDepts.Where(p => p.Id == Pid);
        // 获取所有子部门  
        var sonDepts = GetSonList(userDepts, Pid);
        // 合并并返回 ID 列表  
        return currentDept.Select(o => o.Id).Concat(sonDepts.Select(o => o.Id)).Distinct();
    }

    public static IEnumerable<UserDept> GetSonList(IEnumerable<UserDept> list, int Pid)
    {
        // 使用递归获取所有子部门  
        var children = list.Where(p => p.Pid == Pid).ToList();
        foreach (var child in children)
        {
            children.AddRange(GetSonList(list, child.Id));
        }
        return children;
    }
    #endregion

    /// <summary>
    /// 获取所有上级
    /// </summary>
    /// <param name="list"></param>
    /// <param name="Id"></param>
    /// <returns></returns>
    public static IEnumerable<UserDept> GetFatherList(IEnumerable<UserDept> list, int Id)
    {
        IEnumerable<UserDept> query = list.Where(p => p.Id == Id);
        return query.Concat(query.SelectMany(t => GetFatherList(list, t.Pid)));
    }

    public async Task<object> InsertAsync(EditUserDept editUserDept)
    {
        UserDept userDept = new()
        {
            Pid = editUserDept.Pid,
            Name = editUserDept.Name,
            Type = editUserDept.Type,
            Sort = editUserDept.Sort,
            Remark = editUserDept.Remark,
            Status = editUserDept.Status,
            GreatTime = DateTime.Now
        };
        return await userDeptRepository.InsertAsync(userDept);
    }

    public async Task<int> UpdateAsync(UserDept userDept)
    {
        return await userDeptRepository.UpdateAsync(userDept);
    }
    public async Task<int> ValChangeAsync(ValChangeModel valChangeModel)
    {
        var get = await userDeptRepository.GetAsync(valChangeModel.Id);
        if (get == null || valChangeModel.Val == null)
            return 0;
        switch (valChangeModel.Key)
        {
            case "Name":
                get.Name = valChangeModel.Val.ParseToString();
                break;
            case "Sort":
                get.Sort = valChangeModel.Val.ParseToInt();
                break;
            case "Remark":
                get.Remark = valChangeModel.Val.ParseToString();
                break;
        }
        return await userDeptRepository.UpdateAsync(get);
    }

    public async Task<int> DeleteByEntityIdAsync(int id)
    {
        bool isNull = await userRepository.ExistsByDeptIdAsync(id);
        bool isHaveSon = await CheckHaveSonAsync(id);
        if (isNull || isHaveSon)
            return -1;
        return await userDeptRepository.DeleteByEntityIdAsync(id);
    }

    public async Task<IEnumerable<ViewUserDept>> GetUserDeptsAsync()
    {
        IEnumerable<UserDept> cates = await GetAllDeptAsync();
        return await AsUserDeptsAsync(0, cates.ToList());
    }

    public static async Task<List<ViewUserDept>> AsUserDeptsAsync(int pid, List<UserDept> depts)
    {
        if (depts.Count < 1)
            return [];
        var parent = depts.Where(P => P.Pid == pid);
        List<ViewUserDept> lists = [];
        foreach (var item in parent)
        {
            ViewUserDept Childer = new()
            {
                Id = item.Id,
                Pid = item.Pid,
                Name = item.Name,
                Type = item.Type,
                Sort = item.Sort,
                Remark = item.Remark,
                Status = item.Status,
            };
            Childer.Children = await GetSon(Childer, depts);
            if (Childer.Children != null)
            {
                Childer.HaveChild = true;
                Childer.Open = true;
            }
            lists.Add(Childer);
        }
        return lists.OrderBy(o => o.Sort).ThenBy(o => o.Id).ToList();
    }

    private static async Task<List<ViewUserDept>> GetSon(ViewUserDept cates, List<UserDept> sonCates)
    {
        if (sonCates.Exists(x => x.Pid == cates.Id))
            return await AsUserDeptsAsync(cates.Id, sonCates);
        return [];
    }


}
