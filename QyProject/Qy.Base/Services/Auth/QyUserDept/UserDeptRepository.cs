﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Auth.QyUserDept;

[Inject(ServiceLifetime.Scoped)]

public class UserDeptRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, UserDept, int>(dbContext, applicationContext)
{

    /// <summary>
    /// 当部门太多时，可以按地域分类处理
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<UserDept>> GetAllDeptAsync()
    {
        var globalVer = await CacheVersionService.GetGlobalVersionAsync();
        return await GetTopEntitiesWithCacheAsync(1000, CachingExpirationType.RelativelyStable, () =>
        {
            return $"AllUserDept:{globalVer}";
        }, () =>
        {
            Sql sql = Sql.Builder;
            sql.Select("id").From(DbTableName.QyUserDept).OrderBy("sort asc");
            return sql;
        });
    }
    public async Task<IPagedList<UserDept>> GetPostsAsync(UserDeptQuery query)
    {
        Sql sql = Sql.Builder.Select("id").From(DbTableName.QyUserDept);
        return await GetPagedEntitiesAsync(sql, query.PageSize, query.PageIndex);
    }
    public async Task<IEnumerable<UserDept>> GetPagingListAsNameAsync(UserDeptQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyUserDept);
        if (!string.IsNullOrEmpty( query.KeyWords))
        {
            string keyWords = StringUtility.StripSQLInjection(query.KeyWords);
            sql.Where("name like @0", "%" + keyWords + "%");
        }
        else
            sql.Where("id = 0");
        sql.Where("status = 1");
        sql.OrderBy("id desc LIMIT 50");
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);

    }
}
