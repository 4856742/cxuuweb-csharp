﻿using Qy.Base.Utilities.EPPlusExcel;
namespace Qy.Base.Services.Auth.QyUser;

public static class UserConsts
{
    /// <summary>
    /// 超级管理员用户组Id标识
    /// </summary>
    public const int SuperAdmin = 1;
    public const string DefaultUserPass = "123456";
    public static List<GreatExcelField> UserFields
    {
        get
        {
            return [
                new () {Value = 1, FieldName = "Id", Label = "用户Id", Width = 8,Type =GreatExcelDataType.Int32 },
                new () {Value = 2, FieldName = "UserName", Label = "用户名", Width =30,Type =GreatExcelDataType.String },
                new () {Value = 3, FieldName = "IdNumber", Label = "身份证号", Width =30,Type =GreatExcelDataType.String },
                new () {Value = 4, FieldName = "DeptName", Label = "部门", Width = 30,Type =GreatExcelDataType.String },
                new () {Value = 5, FieldName = "Mobile", Label = "电话", Width = 20,Type =GreatExcelDataType.String },
                new () {Value = 6, FieldName = "NickName", Label = "姓名", Width = 10 ,Type =GreatExcelDataType.String},
                new () {Value = 8, FieldName = "GroupName", Label = "角色", Width = 10,Type =GreatExcelDataType.String },
            ];
        }
    }
}
