﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Core.Cryptos;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Auth;
using Qy.Base.Dtos.Auth.UserDto;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Auth.QyUser;

[Inject(ServiceLifetime.Scoped)]

public class UserRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, User, int>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;
    public async Task<User?> GetOneByIdNumberAsync(string userName)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyUser);
        sql.Where("id_number = @0 ", userName);
        //sql.OrderBy("id desc");
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }
    public async Task<IPagedList<User>> GetPagingAsync(UserQuery query)
    {
        var user = applicationContext.GetRequiredCurrentUser();
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyUser);
        if (query.DeptId != 0)
            sql.Where("dept_id = @0", query.DeptId);

        #region 根据用户权限判断数据权限
        if (user.IsNotSuperAdminAndAllAuthData())
        {
            if (user.AccessLevel == (int)AccessLevelType.ThisUser)
            {
                sql.Where($"id=@0", user.Id);
            }
            else
            {
                int[] dids = [.. user.Dids];
                if (dids == null)
                    sql.Where($"dept_id=@0", user.DeptId); //只能通过部门计算
                else
                    sql.Where($"dept_id IN (@dids)", new { dids });
            }
        }
        #endregion

        if (!string.IsNullOrEmpty(query.KeyWords))
            sql.Where("nick_name like @0 or py_name like @0 or pinyin_name like @0", "%" + StringUtility.StripSQLInjection(query.KeyWords) + "%");
        if (query.Status != null)
            sql.Where("status = @0", query.Status);
        if (query.Type > 0)
            sql.Where("type = @0", query.Type);
        //排除技术支持帐号
        sql.Where("id <> 1");
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    public async Task<IEnumerable<User>> GetListAsNickNameAsync(UserQuery query)
    {
        AuthUser user = applicationContext.GetRequiredCurrentUser();
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyUser);
        #region 根据用户权限判断数据权限
        if (query.IsLimitDept == true && user.IsNotSuperAdminAndAllAuthData())
        {
            if (user.AccessLevel == (int)AccessLevelType.ThisUser)
            {
                sql.Where($"id=@0", user.UserId);
            }
            else
            {
                int[] dids = user.Dids.ToArray();
                if (dids == null) sql.Where($"dept_id=@0", user.DeptId);
                else sql.Where($"dept_id IN (@dids)", new { dids });
            }
        }
        #endregion
        if (!string.IsNullOrEmpty(query.KeyWords))
        {
            string keyWords = StringUtility.StripSQLInjection(query.KeyWords);
            //姓名及拼音第一字母或全拼
            sql.Where("nick_name like @0 or py_name like @0 or pinyin_name like @0", "%" + keyWords + "%");
        }
        else sql.Where("id = 0");
        sql.Where("status = 1");
        //排除技术支持帐号
        sql.Where("id <> 1");
        sql.OrderBy("id desc LIMIT 50");
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }
    public async Task<User?> GetLoginUserAsync(string account)
    {
        Sql sql = Sql.Builder;
        sql.Select("id");
        sql.From(DbTableName.QyUser);
        sql.Where("id_number =@0  or mobile =@0  or user_name =@0 AND status = 1", StringUtility.StripSQLInjection(account));
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0)
            return null;
        return await GetAsync(id);
    }


    public async Task<int> UpdateUserPassWordAsync(int userId, string passWord)
    {
        Sql sql = Sql.Builder;
        sql.Append("SET pass_word = @0", passWord).Where("id =@0", userId);
        return await DbContext.UpdateAsync<User>(sql);
    }

    public async Task<IEnumerable<User>> GetForDeptAsync(int deptId)
    {
        int areaVersion = await CacheVersionService.GetAreaVersionAsync(m => m.DeptId, deptId);
        return await GetTopEntitiesWithCacheAsync(1000,
            CachingExpirationType.ObjectCollection,
            () => $"Users:{deptId}:{areaVersion}",
            () =>
            {
                Sql sql = Sql.Builder.Select("Id").From(DbTableName.QyUser).Where("dept_id = @0", deptId);
                return sql;
            });
    }
    public async Task<bool> ExistsByGroupIdAsync(int groupId)
    {
        return await DbContext.ExistsAsync<User>("group_id =@0", groupId);
    }

    public async Task<bool> ExistsByDeptIdAsync(int deptId)
    {
        return await DbContext.ExistsAsync<User>("dept_id =@0", deptId);
    }
    public async Task<bool> ExistsByIdNumberAsync(string idNumber)
    {
        return await DbContext.ExistsAsync<User>("id_number =@0", idNumber);
    }
    public async Task<bool> ExistsByUserNameAsync(string userName)
    {
        return await DbContext.ExistsAsync<User>("user_name =@0", userName);// and is_deleted = 0
    }
    public async Task<bool> ExistsByMobileAsync(string mobile)
    {
        return await DbContext.ExistsAsync<User>("mobile =@0", mobile);
    }
}
