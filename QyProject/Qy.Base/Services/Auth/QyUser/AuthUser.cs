﻿using PmSoft.Core.Domain.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.InterFace.User;

namespace Qy.Base.Services.Auth.QyUser;

/// <summary>
/// 认证用户
/// </summary>
public class AuthUser : IAuthUser
{
    public int Id { get; set; }
    public string? TenantType { get; set; }
    public int UserId { get; set; }
    public int DeptId { get; set; }
    //public UserTypeEnum Type { get; set; }
    public UserTypeEnum UserType { get; set; }
    /// <summary>
    /// 当前用户拥有的角色集合
    /// </summary>
    public IEnumerable<IRole> Roles { get; set; } = [];
    public string Name { get; set; } = string.Empty;
    public List<int> Dids { get; set; } = [];
    public string UserName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string DeptName { get; set; } = string.Empty;
    public AccessLevelType AccessLevel { get; set; }
    public bool Status { get; set; }
    public bool IsEditOthersData { get; set; }
    public bool IsSuperAdmin()
    {
        return UserType == UserTypeEnum.SuperAdministrator;
    }
    public string Avatar { get; set; } = string.Empty;
    public string Mobile { get; set; } = string.Empty;
    UserTypeEnum IAuthUser.Type { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

    string IAuthedUser.UserType => UserType.ToString();
}
