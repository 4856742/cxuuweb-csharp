﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions.Attachment;
using PmSoft.Web.Abstractions.RealTime;
using Qy.Base.AppBase;
using Qy.Base.AppBase.RealTime;
using Qy.Base.Dtos.Auth.UserDto;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Services.Auth.QyUserRole;
using Qy.Base.Services.SysConfigs;
using Qy.Base.Utilities;
using System.Diagnostics;

namespace Qy.Base.Services.Auth.QyUser;
[Inject(ServiceLifetime.Scoped)]
public class UserService(
    UserDeptService userDeptService,
    UserRepository userRepository,
    UserRoleService userRoleService,
    UserRelRoleRepository userRelRoleRepository,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    IOnlineClientManager onlineClientManager,
    AttachmentManagerService attachmentManagerService,
    IApplicationContext applicationContext,
    AppConfigService appConfigService
    )
{

    public async Task<PagedList<OnlineSysUsers>> OnlineUserListAsync(UserQuery query)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();
        var onlineUsers = await onlineClientManager.GetAllClientsAsync();
        IEnumerable<User>? users = await GetUsersAsync(onlineUsers.Select(x => x.UserId ?? 0));
        var deptAll = await userDeptService.GetAllDeptAsync();
        IEnumerable<OnlineSysUsers> userList = onlineUsers.Select(online => online.AsView(users, deptAll));
        IEnumerable<OnlineSysUsers> item = string.IsNullOrEmpty(query.KeyWords)
            ? userList.OrderByDescending(f => f.ConnectTime)
                      .Skip((query.PageIndex - 1) * query.PageSize)
                      .Take(query.PageSize)
            : userList.Where(x => x.NickName != null && x.NickName.Contains(query.KeyWords))
                      .OrderByDescending(f => f.ConnectTime)
                      .Skip((query.PageIndex - 1) * query.PageSize)
                      .Take(query.PageSize);

        stopwatch.Stop();
        TimeSpan timespan = stopwatch.Elapsed;
        int total = onlineUsers.Count;
        var pageResult = new PagedList<OnlineSysUsers>(total, query.PageSize, item)
        {
            QueryDuration = timespan.TotalMilliseconds
        };
        return pageResult;
    }

    public async Task<User?> GetOneByIdNumberAsync(string userName)
    {
        return await userRepository.GetOneByIdNumberAsync(userName);
    }
    public async Task<User?> GetAsync(int id)
    {
        return await userRepository.GetAsync(id);
    }
    public async Task<ViewUser?> GetToEditAsync(int id)
    {
        var user = await GetAsync(id);
        if (user == null)
            return null;
        return new ViewUser()
        {
            Id = id,
            DeptId = user.DeptId,
            UserName = user.UserName,
            NickName = user.NickName,
            IdNumber = user.IdNumber,
            Status = user.Status,
            Mobile = user.Mobile,
            Avatar = user.Avatar
        };
    }

    public async Task<ViewUserOptions?> GetOneOptionAsync(int id)
    {
        var user = await GetAsync(id);
        if (user == null)
            return null;
        IEnumerable<UserDept> depts = await userDeptService.GetAllDeptAsync();
        return ViewUserOptions.AsOne(user, depts);

    }
    public async Task<LoginUserBaseInfo?> GetLoginUserBaseInfoAsync()
    {
        if (applicationContext.CurrentUser == null || applicationContext.CurrentUser.UserId < 1)
            return null;
        User? user = await userRepository.GetAsync(applicationContext.CurrentUser.UserId);
        if (user == null) return null;
        return new LoginUserBaseInfo
        {
            UserId = user.Id,
            Avatar = user.Avatar,
            NickName = user.NickName,
            Mobile = user.Mobile,
            UserType = user.Type
        };
    }

    public async Task<ViewUser> GetUnionOneAsync(int id)
    {
        User? user = await userRepository.GetAsync(id);
        if (user == null)
            return new ViewUser();
        var depts = await userDeptService.GetAllDeptAsync();
        IEnumerable<UserRole> userRoles = await userRoleService.GetUserRolesAsync(user.Id);
        var userView = user.ToEditView(userRoles);
        userView.DeptName = depts.GetDepartmentHierarchyPath(user.DeptId, 0, "/");
        return userView;
    }

    public async Task<IEnumerable<User>> GetUsersAsync(IEnumerable<int> userIds)
    {
        return await userRepository.GetEntitiesByIdsAsync(userIds);
    }

    public async Task<IEnumerable<ViewUser?>> GetUserListAsync(List<int> userIds)
    {
        var users = await GetUsersAsync(userIds);
        var depts = await userDeptService.GetAllDeptAsync();
        return users.Select(u => u.AsWithDept(depts));
    }

    public async Task<IEnumerable<ViewUserOptions>> QueryUserOptionsWhitIdAsync(UserQuery query)
    {
        if (!string.IsNullOrEmpty(query.Ids))
            return [];
        IEnumerable<int> userIds = query.Ids.Split(',').Select(id => int.Parse(id.Trim()));
        if (!userIds.Any()) return [];
        IEnumerable<User> users = await GetUsersAsync(userIds);
        IEnumerable<UserDept> detps = await userDeptService.GetDeptsAsync(users.Select(u => u.DeptId));
        var pageData = users.Select(u => ViewUserOptions.AsNewList(u, detps));
        return pageData;
    }

    public async Task<IEnumerable<ViewUserOptions>> GetUserOptionsAsync(UserQuery query)
    {
        //var user = await  GetCurrentUserAsync();
        IEnumerable<User> users = await userRepository.GetListAsNickNameAsync(query);
        IEnumerable<UserDept> detps = await userDeptService.GetDeptsAsync(users.Select(u => u.DeptId));
        var data = users.Select(u => ViewUserOptions.AsNewList(u, detps));
        //排除自己
        //return pageData.Where(x => x.Id != user.Id);
        return data;
    }


    public async ValueTask<IPagedList<ViewUser>> GetPageListAsync(UserQuery query)
    {
        var pagedUsers = await userRepository.GetPagingAsync(query);
        return await pagedUsers
            .ToPagedDtoAsync(async user =>
            {
                var v = user.AsViewUser();
                IEnumerable<UserRole> userRoles = await userRoleService.GetUserRolesAsync(user.Id);
                v.Roles = userRoles.Select(x => new UserRoleDto { Id = x.Id, Name = x.Name });
                return v;
            })
            .WithRelatedAsync(
                foreignKeySelector: user => user.DeptId,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<UserDept, int>,
                attachAction: (user, dept) => user.DeptName = dept?.Name
            );
    }

    public async Task<bool> ExistsByIdNumberAsync(string idNumber)
    {
        return await userRepository.ExistsByIdNumberAsync(idNumber);
    }
    public async Task<bool> ExistsByUserNameAsync(string userName)
    {
        return await userRepository.ExistsByUserNameAsync(userName);
    }
    public async Task<bool> ExistsByMobileAsync(string mobile)
    {
        return await userRepository.ExistsByMobileAsync(mobile);
    }
    public async Task<int> UpdateAndPublishAttachmentAsync(User user, string editAvatar)
    {
        if (user.Avatar != editAvatar)
        {
            var atts = await attachmentManagerService.PublishAttachmentAsync(editAvatar, applicationContext.RequiredCurrentUser.UserId.ToString());
            user.Avatar = atts.AttachmentId;
        }
        return await userRepository.UpdateAsync(user);
    }
    /// <summary>
    /// 修改用户昵称拼音
    /// </summary>
    /// <param name="user"></param>
    /// <param name="newNickName"></param>
    /// <returns></returns>
    private static void NickNameAsPinyinAsync(User user, string newNickName)
    {
        if (string.IsNullOrEmpty(user.NickName) || user.NickName != newNickName)
        {
            user.NickName = newNickName;
            user.PyName = StringUtility.GetPinyinInitials(newNickName) ?? "";
            user.PinyinName = StringUtility.GetPinyin(newNickName) ?? "";
        }
    }

    public async Task<int> InsertAsync(EditUser editUser)
    {
        User user = new()
        {
            DeptId = editUser.DeptId,
            UserName = editUser.UserName,
            Status = editUser.Status,
            Mobile = editUser.Mobile,
            IdNumber = editUser.IdNumber,
            Avatar = editUser.Avatar,
            Type = editUser.Type,
            CreateTime = DateTime.Now,
        };
        NickNameAsPinyinAsync(user, editUser.NickName);
        if (!string.IsNullOrEmpty(editUser.PassWord))
        {
            var configDefaultPS = await appConfigService.GetAsync();
            //先读取配置的默认密码，如果为空，则使用程序内部设置默认密码
            var getParmPassword = !string.IsNullOrWhiteSpace(configDefaultPS.UserDefaultPass) ? configDefaultPS.UserDefaultPass : UserConsts.DefaultUserPass;
            user.PassWord = UserPasswordHelper.EncodePassword(getParmPassword, UserPasswordFormat.MD5);
        }
        else
            user.PassWord = UserPasswordHelper.EncodePassword(editUser.PassWord, UserPasswordFormat.MD5);
        var insert = await userRepository.InsertAsync(user);
        if (insert != null)
        {
            await UpdateRelRolesAsync(editUser);
            return 1;
        }
        return 0;
    }
    public async Task<int> UpdateAsync(EditUser editUser)
    {
        var user = await userRepository.GetAsync(editUser.Id);
        if (user == null)
            return -1;
        if (user.Id == 1)
            return 0;
        if (editUser.UserName != user.UserName && await ExistsByUserNameAsync(editUser.UserName))
            return -2;
        if (!string.IsNullOrEmpty(editUser.Mobile) && editUser.Mobile != user.Mobile && await ExistsByMobileAsync(editUser.Mobile))
            return -2;
        if (!string.IsNullOrEmpty(editUser.IdNumber) && editUser.IdNumber != user.IdNumber && await ExistsByIdNumberAsync(editUser.IdNumber))
            return -2;
        user.DeptId = editUser.DeptId;
        user.UserName = editUser.UserName;
        user.IdNumber = editUser.IdNumber;
        user.Status = editUser.Status;
        user.Mobile = editUser.Mobile;
        user.Type = editUser.Type;
        NickNameAsPinyinAsync(user, editUser.NickName);
        if (!string.IsNullOrEmpty(editUser.PassWord))
            user.PassWord = UserPasswordHelper.EncodePassword(editUser.PassWord, UserPasswordFormat.MD5);
        await UpdateRelRolesAsync(editUser);
        return await UpdateAndPublishAttachmentAsync(user, editUser.Avatar);
    }
    /// <summary>
    /// 更新用户关联角色表
    /// </summary>
    /// <param name="edit"></param>
    /// <returns></returns>
    public async Task<bool> UpdateRelRolesAsync(EditUser edit)
    {
        try
        {
            List<int> menuIds = [];
            menuIds = Json.Parse<List<int>>(edit.Roles ?? "[]") ?? [];
            if (menuIds.Count > 0)
            {
                IEnumerable<UserRelRole> list = menuIds.Select(m => new UserRelRole
                {
                    RoleId = m,
                    UserId = edit.Id
                });
                return await userRelRoleRepository.ComparerCreateOrUpdateAsync(edit.Id, list);
            }
            else
            {
                var de = await userRelRoleRepository.BatchDeleteAsync(edit.Id);
                return de > 0;
            }
        }
        catch
        {
            return false;
        }
    }

    public async Task<int> UpdateUserPassWordAsync(int id, string? passWord)
    {
        var user = await GetAsync(id);
        if (user == null || user.Id == 1)
            return 0;
        var configDefaultPS = await appConfigService.GetAsync();
        var getParmPassword = passWord ?? (!string.IsNullOrWhiteSpace(configDefaultPS.UserDefaultPass) ? configDefaultPS.UserDefaultPass : UserConsts.DefaultUserPass);
        user.PassWord = UserPasswordHelper.EncodePassword(getParmPassword, UserPasswordFormat.MD5);
        return await userRepository.UpdateUserPassWordAsync(user.Id, user.PassWord);
    }
    public async Task<int> ChStatusUpdateAsync(int id, bool status)
    {
        if (id == 1) return 0;
        var user = await GetAsync(id);
        if (user == null) return 0;
        user.Status = status;
        return await userRepository.UpdateAsync(user);
    }

    public async Task<int> EditUserProfileAsync(EditUser editUser)
    {
        User? user = await userRepository.GetAsync(editUser.Id);
        if (user == null)
            return -1;
        user.UserName = editUser.UserName;
        user.IdNumber = editUser.IdNumber;
        user.Mobile = editUser.Mobile;
        NickNameAsPinyinAsync(user, editUser.NickName);
        return await UpdateAndPublishAttachmentAsync(user, editUser.Avatar);
    }

    public async Task<int> EditUserInfoAsync(EditUser editUser, int userId)
    {
        var user = await GetAsync(userId) ?? throw new PmSoftException("用户不存在");
        if (editUser.UserName != user.UserName && await ExistsByUserNameAsync(editUser.UserName))
        {
            return -2;
        }
        if (editUser.Mobile != user.Mobile && await ExistsByMobileAsync(editUser.Mobile))
        {
            return -2;
        }
        user.Mobile = editUser.Mobile;
        user.UserName = editUser.UserName;
        NickNameAsPinyinAsync(user, editUser.NickName);

        if (!string.IsNullOrEmpty(editUser.PassWord))
        {
            string oldPassWordHash = UserPasswordHelper.EncodePassword(editUser.OldPassword, UserPasswordFormat.MD5);
            if (user.PassWord != oldPassWordHash)
                return -1;
            user.PassWord = UserPasswordHelper.EncodePassword(editUser.PassWord, UserPasswordFormat.MD5);
            return await UpdateAndPublishAttachmentAsync(user, editUser.Avatar) + 100;
        }
        return await UpdateAndPublishAttachmentAsync(user, editUser.Avatar);
    }

    public async Task<int> EditPasswordAsync(UserPasswordChangeForm editUser, int userId)
    {
        User? user = await GetAsync(userId);
        if (!string.IsNullOrEmpty(editUser.NewPassword) && user != null)
        {
            string oldPassWord = UserPasswordHelper.EncodePassword(editUser.OldPassword, UserPasswordFormat.MD5);
            if (user.PassWord != oldPassWord)
                return -1;
            user.PassWord = UserPasswordHelper.EncodePassword(editUser.NewPassword, UserPasswordFormat.MD5);
            await userRepository.UpdateAsync(user);
            return 101;
        }
        return 0;
    }
    public async Task<int> Delete(int id)
    {
        return await userRepository.DeleteByEntityIdAsync(id);
    }
}

