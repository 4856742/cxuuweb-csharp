﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PmSoft.Core;
using PmSoft.Core.Domain.Auth;
using PmSoft.Web.Abstractions.Authorization;
using Qy.Base.Dtos.Auth;
using Qy.Base.Dtos.Auth.UserDto;
using Qy.Base.Dtos.Menu;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.QyMenu;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Services.Auth.QyUserRole;
using Qy.Base.Services.Loging.Login;
using Qy.Base.Services.Menus;

namespace Qy.Base.Services.Auth.QyUser;

/// <summary>
/// 用户认证服务
/// </summary>
[Inject(ServiceLifetime.Scoped, typeof(IUserAuthenticationService))]
public class UserAuthenticationService(
    UserRepository userRepository,
    UserDeptRepository userDeptRepository,
    UserRoleRepository userRoleRepository,
    UserRelRoleRepository userRelRoleRepository,
    LogLoginService logLoginService,
    MenuRepository menuRepository,
    UserRoleRelMenuRepository roleRelMenuRepository
    ) : IUserAuthenticationService
{

    /// <summary>
    /// 验证用户的密码是否正确，并返回认证的用户信息
    /// </summary>
    /// <param name="user">待认证的用户对象</param>
    /// <param name="password">用户输入的密码</param>
    /// <returns>认证后的用户信息</returns>
    private async Task<IAuthedUser> AuthenticateUserAsync(User? user, string password)
    {
        if (user == null)
            throw new PmSoftException("用户或密码错误");
        // 校验密码是否正确
        if (!UserPasswordHelper.CheckPassword(password, user.PassWord, UserPasswordFormat.MD5))
            throw new PmSoftException("用户或密码错误");
        // 获取用户的认证信息
        return await GetAuthUserAsync(user.Id, "unknow");
    }

    public async Task<IAuthedUser?> CheckLoginAsync(LoginDto login)
    {
        var user = await userRepository.GetLoginUserAsync(login.Account);
        var authUser = await AuthenticateUserAsync(user, login.PassWord);
        //登录日志单独写入
        await logLoginService.InsertLogLoginAsync(authUser);
        return authUser;
    }
    /// <summary>
    /// 根据用户ID获取认证后的用户信息
    /// </summary>
    /// <param name="userId">用户ID</param>
    /// <param name="tenantType">租户类型</param>
    /// <returns>认证后的用户信息</returns>
    public async Task<IAuthedUser> GetAuthUserAsync(int userId, string? tenantType)
    {
        var user = await userRepository.GetAsync(userId) ?? throw new PmSoftException("用户不存在");
        var dept = await userDeptRepository.GetAsync(user.DeptId) ?? throw new PmSoftException("部门不存在");
        IEnumerable<UserRelRole>? userRelRoles = [];
        IEnumerable<UserRole>? roles = [];
        var authUser = new AuthUser
        {
            Id = user.Id,
            UserName = user.UserName,
            Avatar = user.Avatar,
            NickName = user.NickName,
            Mobile = user.Mobile,
            Status = user.Status,
            DeptName = dept.Name,
            DeptId = dept.Id,
            UserId = user.Id,
            UserType = user.Type,
            IsEditOthersData = true,
            TenantType  = "SysUser" //区分如：企业用户、系统用户等用户类型定义租户类型
        };
        if (user.Type == UserTypeEnum.User)
        {
            userRelRoles = await userRelRoleRepository.GetUserRolesAsync(user.Id);
            roles = await userRoleRepository.GetEntitiesByIdsAsync(userRelRoles.Select(m => m.RoleId));
            authUser.Roles = roles.Select(x => new UserRoleDto
            {
                Id = x.Id,
                Name = x.Name,
            });
            authUser.IsEditOthersData = roles.Any(x => x.IsEditOthersData == true);
            authUser.AccessLevel = roles.Max(x => x.AccessLevel);
            if (authUser.AccessLevel != AccessLevelType.FullPermissions)
            {
                authUser.Dids = await CheckDeptAuthAsync(authUser, roles);
            }
        }
        return authUser;
    }

    public async Task<IEnumerable<IMenu>> GeUserMenusAsync(IAuthedUser user)
    {
        var roleMenus = new List<UserRoleRelMenu>();
        IEnumerable<Menu> menus = [];
        // 超级管理员获取所有菜单
        if (user is AuthUser currentUser && currentUser.IsSuperAdmin())
        {
            menus = await menuRepository.GetAllMenuAsync();
        }
        else
        {
            foreach (var role in user.Roles)
            {
                roleMenus.AddRange(await roleRelMenuRepository.GetRoleMenusAsync(role.Id));
            }
            menus = await menuRepository.GetEntitiesByIdsAsync(roleMenus.Select(m => m.MenuId));
        }
        return menus.Select(x =>
        {
            return new ViewMenu
            {
                Id = x.Id,
                Cid = x.Cid,
                Pid = x.Pid,
                Type = x.Type,
                Name = x.Name,
                Sort = x.Sort,
                Path = x.Path,
                Component = x.Component,
                PermCode = x.PermCode,
                Icon = x.Icon,
                Status = x.Status,
                AlwaysShow = x.AlwaysShow,
                KeepAlive = x.KeepAlive,
            };
        });
    }

    /// <summary>
    /// 检查当前用户拥有的部门ID
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    private async Task<List<int>> CheckDeptAuthAsync(AuthUser user, IEnumerable<UserRole> userRoles)
    {
        switch (user.AccessLevel)
        {
            case AccessLevelType.ThisUser:
                // 当前用户，不配置部门权限，返回null  
                return [];
            case AccessLevelType.ThisDepartmentAndBelow:
                // 获取当前部门及其子部门的ID  
                return (await GetDeptAndSonIdsAsync(user.DeptId)).ToList();
            case AccessLevelType.ThisDepartment:
                // 返回当前部门ID  
                return [user.DeptId];
            case AccessLevelType.SpecifyDepartment:
                //指定部门
                try
                {
                    List<int> deptIds = [];
                    var strings = userRoles.Select(x => x.AuthDeptIds).Where(x => !string.IsNullOrEmpty(x)).ToList();
                    foreach (var item in strings)
                    {
                        var ids = JsonConvert.DeserializeObject<List<int>>(item ?? "[]") ?? [];
                        deptIds.AddRange(ids);
                    }
                    // 添加当前用户所在部门并去重  
                    deptIds.Add(user.DeptId);
                    return [.. deptIds.Distinct()];
                }
                catch
                {
                    return [];
                }
            default:
                return [];
        }
    }

    /// <summary>  
    /// 获取当前部门及子部门ID值字符串  
    /// </summary>  
    /// <param name="Pid"></param>  
    /// <returns></returns>  
    public async Task<IEnumerable<int>> GetDeptAndSonIdsAsync(int Pid)
    {
        var userDepts = await userDeptRepository.GetAllDeptAsync();
        // 获取当前部门  
        var currentDept = userDepts.Where(p => p.Id == Pid);
        // 获取所有子部门  
        var sonDepts = GetSonList(userDepts, Pid);
        // 合并并返回 ID 列表  
        return currentDept.Select(o => o.Id).Concat(sonDepts.Select(o => o.Id)).Distinct();
    }

    public static IEnumerable<UserDept> GetSonList(IEnumerable<UserDept> list, int Pid)
    {
        // 使用递归获取所有子部门  
        var children = list.Where(p => p.Pid == Pid).ToList();
        foreach (var child in children)
        {
            children.AddRange(GetSonList(list, child.Id));
        }
        return children;
    }


}
