﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Dtos.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;

namespace Qy.Base.Services.Auth.QyUserRole;

[Inject(ServiceLifetime.Scoped)]
public class UserRoleService(
    UserRoleRepository userRoleRepository,
    UserRoleRelMenuRepository userRoleRelMenuRepository,
    UserRelRoleRepository userRelRoleRepository,
    UserRepository userRepository)
{

    public async Task<IEnumerable<UserRole>> GetAllAsync()
    {
        return await userRoleRepository.GetAllAsync();
    }

    public async Task<IEnumerable<UserRole>> GetUserRolesAsync(int userId)
    {
        var userRelRoles = await userRelRoleRepository.GetUserRolesAsync(userId);
        return await userRoleRepository.GetEntitiesByIdsAsync(userRelRoles.Select(m => m.RoleId));
    }

    public async Task<IEnumerable<OptionItemDto>> IndentedOptionAsync()
    {
        var list = await GetAllAsync();
        return list.Select(x => new OptionItemDto { Value = x.Id, Label = x.Name, Sort = x.Sort });
    }
    public async Task<UserRole?> GetAsync(int id)
    {
        return await userRoleRepository.GetAsync(id);
    }
    public async Task<ViewUserRole?> GetViewUserRoleAsync(int id)
    {
        var data = await userRoleRepository.GetAsync(id);
        if (data != null)
        {
            var userRoleRelMenus = await userRoleRelMenuRepository.GetRoleMenusAsync(data.Id);
            return data.ToViewUserRole(userRoleRelMenus);
        }
        else
        {
            return null;
        }
    }
    public async Task<bool> ExistsByNameAsync(string name)
    {
        return await userRoleRepository.ExistsByNameAsync(name);
    }

    public async Task<bool> ChangeSortAsync(OptionItemDto[] optionType)
    {
        if (optionType.Length == 0)
            return false;
        return await userRoleRepository.ChangeSortAsync(optionType);
    }

    public async Task<bool> Delete(int id)
    {
        if (await userRepository.ExistsByGroupIdAsync(id))
            return false;
        return await userRoleRepository.DeleteByEntityIdAsync(id) > 0;
    }

    public async Task<bool> GreatOrUpdateAsync(EditUserRole edit)
    {
        try
        {
            if (edit.Id > 0)
            {
                var up = await userRoleRepository.UpdateAsync(edit.EditToUserRole());
                if (up < 1) return false;
            }
            else
            {
                edit.CreateTime = DateTime.Now;
                var res = await userRoleRepository.InsertAsync(edit.EditToUserRole());
                if (res == null) return false;
                edit.Id = res.ParseToInt();
            }
            List<int> menuIds = [];
            menuIds = Json.Parse<List<int>>(edit.MenuRole ?? "[]") ?? [];
            if (menuIds.Count > 0)
            {
                IEnumerable<UserRoleRelMenu> list = menuIds.Select(m => new UserRoleRelMenu
                {
                    RoleId = edit.Id,
                    MenuId = m
                });
                return await userRoleRelMenuRepository.ComparerCreateOrUpdateAsync(edit.Id, list);
            }
            else
            {
                var de = await userRoleRelMenuRepository.BatchDeleteAsync(edit.Id);
                return de > 0;
            }
        }
        catch
        {
            return false;
        }
    }
}
