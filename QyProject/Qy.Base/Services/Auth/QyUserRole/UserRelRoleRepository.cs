﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities.Auth;

namespace Qy.Base.Services.Auth.QyUserRole;


[Inject(ServiceLifetime.Scoped)]

public class UserRelRoleRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, UserRelRole, int>(dbContext, applicationContext)
{

	public async Task<IEnumerable<UserRelRole>> GetUserRolesAsync(int userId)
	{
		int areaVersion = await CacheVersionService.GetAreaVersionAsync(m => m.UserId, userId);
		return await GetTopEntitiesWithCacheAsync(1000,
			CachingExpirationType.UsualObjectCollection,
			() => $"SysUserRoles:{userId}:{areaVersion}",
			() =>
		{
			Sql sql = Sql.Builder.Select("id").From(DbTableName.QyUserRelRole).Where("user_id=@0", userId);
			return sql;
		});
	}

    /// <summary>
    /// 对比增改删
    /// </summary>
    public async Task<bool> ComparerCreateOrUpdateAsync(int roleId, IEnumerable<UserRelRole> newRelMenus)
    {
        await DbContext.BeginTransactionAsync();
        try
        {
            var relMenus = await GetUserRolesAsync(roleId);
            await BulkSyncAsync(relMenus, newRelMenus, m => (m.RoleId, m.UserId));
            await DbContext.CompleteTransactionAsync();
            return true;
        }
        catch (Exception)
        {
            await DbContext.AbortTransactionAsync();
            return false;
        }
    }
    public async Task<int> BatchDeleteAsync(int roleId)
    {
        await DbContext.OpenSharedConnectionAsync();
        try
        {
            int conut = 0;
            var relMenus = await GetUserRolesAsync(roleId);
            foreach (var item in relMenus)
            {
                int del = await DeleteAsync(item);
                if (del > 0)
                    conut++;
            }
            return conut;
        }
        finally
        {
            DbContext.CloseSharedConnection();
        }
    }
}
