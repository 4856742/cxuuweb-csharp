﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;

namespace Qy.Base.Services.Auth.QyUserRole;

[Inject(ServiceLifetime.Scoped)]
public class UserRoleRepository : CacheRepository<AppDbContext, UserRole, int>
{
    public UserRoleRepository(AppDbContext dbContext, IApplicationContext applicationContext) : base(dbContext, applicationContext)
    {
    }
    public async Task<IEnumerable<UserRole>> GetAllAsync()
    {
        var globalVer = await CacheVersionService.GetGlobalVersionAsync();
        return await GetTopEntitiesWithCacheAsync(1000, CachingExpirationType.Stable,
            () => { return $"UserRoleAll:{globalVer}"; },
            () =>
                {
                    Sql sql = Sql.Builder;
                    sql.Select("id").From(DbTableName.QyUserRole).OrderBy("sort asc");
                    return sql;
                });
    }
    public async Task<bool> ExistsByNameAsync(string name)
    {
        return await DbContext.ExistsAsync<UserRole>("name =@0", name);
    }
    public async Task<bool> ChangeSortAsync(OptionItemDto[] optionType)
    {
        await DbContext.BeginTransactionAsync();
        try
        {
            foreach (var item in optionType)
            {
                var getData = await GetAsync(item.Value);
                if (getData != null && item.Sort != getData.Sort && getData.Id != 1)
                {
                    getData.Sort = item.Sort;
                    await UpdateAsync(getData);
                }
            }
            await DbContext.CompleteTransactionAsync();
            return true;
        }
        catch
        {
            await DbContext.AbortTransactionAsync();
            return false;
        }
    }
}
