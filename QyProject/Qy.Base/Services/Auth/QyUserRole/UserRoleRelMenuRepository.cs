﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;

namespace Qy.Base.Services.Auth.QyUserRole;

[Inject(ServiceLifetime.Scoped)]
public class UserRoleRelMenuRepository : CacheRepository<AppDbContext, UserRoleRelMenu, int>
{
    public UserRoleRelMenuRepository(AppDbContext dbContext, IApplicationContext applicationContext) : base(dbContext, applicationContext)
    {
    }

    public async Task<IEnumerable<UserRoleRelMenu>> GetRoleMenusAsync(int roleId)
    {
        int areaVersion = await CacheVersionService.GetAreaVersionAsync(m => m.RoleId, roleId);
        return await GetTopEntitiesWithCacheAsync(1000,
            CachingExpirationType.UsualObjectCollection,
            () => $"UserRoleRelMenus:{roleId}:{areaVersion}",
            () =>
        {
            Sql sql = Sql.Builder.Select("id").From(DbTableName.QyUserRoleRelMenu).Where("role_id=@0", roleId);
            return sql;
        });
    }

    /// <summary>
    /// 对比增改删
    /// </summary>
    public async Task<bool> ComparerCreateOrUpdateAsync(int roleId, IEnumerable<UserRoleRelMenu> newRelMenus)
    {
        await DbContext.BeginTransactionAsync();
        try
        {
            var relMenus = await GetRoleMenusAsync(roleId);
            await BulkSyncAsync(relMenus,newRelMenus, m => (m.RoleId, m.MenuId));
            await DbContext.CompleteTransactionAsync();
            return true;
        }
        catch (Exception)
        {
            await DbContext.AbortTransactionAsync();
            return false;
        }
    }
    ///// <summary>
    ///// 对比增改删
    ///// </summary>
    //public async Task<bool> ComparerCreateOrUpdateAsync(int roleId, IEnumerable<UserRoleRelMenu> newRelMenus)
    //{
    //    await DbContext.BeginTransactionAsync();
    //    try
    //    {
    //        var relMenus = await GetRoleMenusAsync(roleId);
    //        //找不同的删除 差集：在 list1 中但不在 list2 中的元素
    //        var difference = relMenus.ExceptByKeys(newRelMenus, p => (p.RoleId, p.MenuId));
    //        foreach (var item in difference)
    //        {
    //            await DeleteAsync(item);
    //        }
    //        //找不同的的新增：在 list2 中但不在 list1 中的元素
    //        var differenceB = newRelMenus.ExceptByKeys(relMenus, p => (p.RoleId, p.MenuId));
    //        foreach (var item in differenceB)
    //        {
    //            await InsertAsync(item);
    //        }
    //        await DbContext.CompleteTransactionAsync();
    //        return true;
    //    }
    //    catch (Exception)
    //    {
    //        await DbContext.AbortTransactionAsync();
    //        return false;
    //    }
    //}
    /// <summary>
    /// 批量删除
    /// </summary>
    public async Task<int> BatchDeleteAsync(int roleId)
    {
        await DbContext.OpenSharedConnectionAsync();
        try
        {
            int conut = 0;
            var relMenus = await GetRoleMenusAsync(roleId);
            foreach (var item in relMenus)
            {
                int del = await DeleteAsync(item);
                if (del > 0)
                    conut++;
            }
            return conut;
        }
        finally
        {
            DbContext.CloseSharedConnection();
        }
    }
}
