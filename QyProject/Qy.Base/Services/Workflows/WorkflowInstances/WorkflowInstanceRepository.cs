﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;

namespace Qy.Base.Services.Workflows.WorkflowInstances;

[Inject(ServiceLifetime.Scoped)]

public class WorkflowInstanceRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, WorkflowInstance, int>(dbContext, applicationContext)
{

    public async Task<IPagedList<WorkflowInstance>> GetPagingAsync(QueryWorkflowInstance query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyWorkflowInstance);

        if (query.ModuleType > 0)
          sql.Where("module_type = @0", query.ModuleType);
        if (query.StartTime != null && query.EndTime != null)
            sql.Where("start_time >= @0 AND end_time <= @1", query.StartTime, query.EndTime);
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }

    public async Task<bool> ExistsByWidAsync(int wid)
    {
        return await DbContext.ExistsAsync<WorkflowInstance>("workflow_id =@0", wid);
    }

}
