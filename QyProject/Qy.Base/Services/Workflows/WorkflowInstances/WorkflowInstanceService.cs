﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;
using Qy.Base.Services.Workflows.WorkflowRecords;

namespace Qy.Base.Services.Workflows.WorkflowInstances;

[Inject(ServiceLifetime.Scoped)]
public class WorkflowInstanceService(
    WorkflowRecordRepository recordRepository,
    WorkflowInstanceRepository instanceRepository
    )
{

    public async Task<WorkflowInstance?> GetAsync(int id)
    {
        return await instanceRepository.GetAsync(id);
    }

    public async Task<IPagedList<WorkflowInstance>> GetPagingAsync(QueryWorkflowInstance query)
    {
        return await instanceRepository.GetPagingAsync(query);
    }
    public async Task<IEnumerable<WorkflowInstance>> GetListAsync(IEnumerable<int> ids)
    {
        return await instanceRepository.GetEntitiesByIdsAsync(ids);
    }

    public async Task<object> InsertAsync(WorkflowInstance userPost)
    {
        return await instanceRepository.InsertAsync(userPost);
    }

    public async Task<int> UpdateAsync(WorkflowInstance userPost)
    {
        return await instanceRepository.UpdateAsync(userPost);
    }
    public async Task<int> DeleteByEntityIdAsync(int id)
    {
        if (await recordRepository.ExistsByIidAsync(id))
            return -1;
        return await instanceRepository.DeleteByEntityIdAsync(id);
    }


    // 通知审批人（示例：预留接口）
    //private async Task NotifyApprovers(WorkflowInstance instance, WorkflowStep step)
    //{
    //    // 根据 step.ApproverType 和 ApproverRefIds 获取目标用户列表
    //    List<int> userIds = await ResolveApprovers(step);
    //    foreach (var userId in userIds)
    //    {
    //        // 调用你的通知服务（邮件、短信、站内信）
    //        await _notificationService.SendAsync(userId, $"您有新的审批任务：{instance.BusinessKey}");
    //    }
    //}
}
