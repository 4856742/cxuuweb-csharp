﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;

namespace Qy.Base.Services.Workflows.WorkflowRecords;

[Inject(ServiceLifetime.Scoped)]
public class WorkflowRecordService(
    WorkflowRecordRepository recordRepository
    )
{

    public async Task<WorkflowRecord?> GetAsync(int id)
    {
        return await recordRepository.GetAsync(id);
    }

    public async Task<IPagedList<WorkflowRecord>> GetPagingAsync(QueryWorkflowRecord query)
    {
        return await recordRepository.GetPagingAsync(query);
    }
    public async Task<IEnumerable<WorkflowRecord>> GetListAsync(IEnumerable<int> ids)
    {
        return await recordRepository.GetEntitiesByIdsAsync(ids);
    }

    public async Task<object> InsertAsync(WorkflowRecord userPost)
    {
        return await recordRepository.InsertAsync(userPost);
    }

    public async Task<int> UpdateAsync(WorkflowRecord userPost)
    {
        return await recordRepository.UpdateAsync(userPost);
    }
    public async Task<int> DeleteByEntityIdAsync(int id)
    {
        return await recordRepository.DeleteByEntityIdAsync(id);
    }


    // 通知审批人（示例：预留接口）
    //private async Task NotifyApprovers(WorkflowInstance instance, WorkflowStep step)
    //{
    //    // 根据 step.ApproverType 和 ApproverRefIds 获取目标用户列表
    //    List<int> userIds = await ResolveApprovers(step);
    //    foreach (var userId in userIds)
    //    {
    //        // 调用你的通知服务（邮件、短信、站内信）
    //        await _notificationService.SendAsync(userId, $"您有新的审批任务：{instance.BusinessKey}");
    //    }
    //}
}
