﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;

namespace Qy.Base.Services.Workflows.WorkflowRecords;

[Inject(ServiceLifetime.Scoped)]

public class WorkflowRecordRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, WorkflowRecord, int>(dbContext, applicationContext)
{


    public async Task<IPagedList<WorkflowRecord>> GetPagingAsync(QueryWorkflowRecord query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyWorkflowRecord);
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    public async Task<bool> ExistsByIidAsync(int instanceId)
    {
        return await DbContext.ExistsAsync<WorkflowRecord>("instance_id =@0", instanceId);
    }
}
