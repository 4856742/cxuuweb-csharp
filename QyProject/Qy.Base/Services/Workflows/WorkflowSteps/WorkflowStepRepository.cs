﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Workflows.WorkflowSteps;

[Inject(ServiceLifetime.Scoped)]

public class WorkflowStepRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, WorkflowStep, int>(dbContext, applicationContext)
{

    public async Task<WorkflowStep?> GetOneIdAsync(int id ,int workflowId)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyWorkflowStep).Where("id = @0 and workflow_id = @1", id, workflowId);
        int entId = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (entId <= 0)
            return null;
        return await GetAsync(entId);
    }
    public async Task<IPagedList<WorkflowStep>> GetPagingAsync(QueryWorkflowStep query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyWorkflowStep);
        if (!string.IsNullOrEmpty(query.Name))
          sql.Where("name like @0", "%" + StringUtility.StripSQLInjection(query.Name) + "%");
        sql.Where("workflow_id = @0", query.WorkflowId);
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }


    public async Task<IEnumerable<WorkflowStep>> GetByWorkflowIdAsync(int workflowId)
    {
        int areaVersion = await CacheVersionService.GetAreaVersionAsync(m => m.WorkflowId, workflowId);
        return await GetTopEntitiesWithCacheAsync(1000,
            CachingExpirationType.ObjectCollection,
            () => $"WorkflowStep:{workflowId}:{areaVersion}",
            () =>
            {
                Sql sql = Sql.Builder;
                sql.Select("Id").From(DbTableName.QyWorkflowStep).Where("workflow_id = @0", workflowId);
                sql.OrderBy("step_order asc");
                return sql;
            });
    }

    public async Task<bool> ExistsByWidAsync(int wid)
    {
        return await DbContext.ExistsAsync<WorkflowStep>("workflow_id =@0", wid);
    }
}
