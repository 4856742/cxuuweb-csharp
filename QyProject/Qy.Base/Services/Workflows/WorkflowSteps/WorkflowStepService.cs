﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Workflows;

namespace Qy.Base.Services.Workflows.WorkflowSteps;

[Inject(ServiceLifetime.Scoped)]
public class WorkflowStepService(
    WorkflowStepRepository stepRepository,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader
    )
{

    public async Task<WorkflowStep?> GetAsync(int id)
    {
        return await stepRepository.GetAsync(id);
    }

    public async Task<IPagedList<ViewWorkflowStep>> GetPagingAsync(QueryWorkflowStep query)
    {
        var pagedList = await stepRepository.GetPagingAsync(query);
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            return v;
        })
        .WithRelatedAsync(
            foreignKeySelector: v => v.Uid,
            loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<Workflow, int>,
            attachAction: (v, m) => v.WorkflowName = m.Name
        );
    }
    public async Task<IEnumerable<WorkflowStep>> GetListAsync(IEnumerable<int> ids)
    {
        return await stepRepository.GetEntitiesByIdsAsync(ids);
    }
    public async Task<IEnumerable<WorkflowStep>> GetByWorkflowIdAsync(int workflowId)
    {
        return await stepRepository.GetByWorkflowIdAsync(workflowId);
    }
    public async Task<object> InsertAsync(WorkflowStep userPost)
    {
        return await stepRepository.InsertAsync(userPost);
    }

    public async Task<int> UpdateAsync(string data, Workflow workflow)
    {
        if (string.IsNullOrEmpty(data))
            return 0;
        //await applicationContext.Transaction.BeginAsync();
        try
        {
            IEnumerable<WorkflowStep> workflowSteps = Json.Parse<IEnumerable<WorkflowStep>>(data) ?? [];
            int result = 0;
            if (workflowSteps != null)
                foreach (var workflowStep in workflowSteps)
                {
                    var get = await stepRepository.GetOneIdAsync(workflowStep.Id, workflow.Id);
                    if (get == null)
                    {
                        await stepRepository.InsertAsync(workflowStep);
                    }
                    else
                    {
                        await stepRepository.UpdateAsync(workflowStep);
                    }
                    result++;
                }
            //await applicationContext.Transaction.CompleteAsync();
            if (result > 0)
                return 1;
            return 0;
        }
        catch
        {
           // await applicationContext.Transaction.AbortAsync(); // 回滚事务  
            return 0;
        }
    }
    public async Task<int> DeleteByEntityIdAsync(int id)
    {
        return await stepRepository.DeleteByEntityIdAsync(id);
    }

}
