﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;
using Qy.Base.Extensions;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Workflows.Workflows;

[Inject(ServiceLifetime.Scoped)]

public class WorkflowRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, Workflow, int>(dbContext, applicationContext)
{

    private readonly IApplicationContext applicationContext = applicationContext;

    public async Task<IPagedList<Workflow>> GetPagingAsync(QueryWorkflow query)
    {
        Sql sql = Sql.Builder;
        sql.Select("workflow.id").From(DbTableName.QyWorkflow + " as  workflow")
        .LeftJoin(DbTableName.QyUser + " as user").On("workflow.uid = user.id");

        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser();
        user.GenQueryPermissionsSql(sql, "workflow.uid", "workflow.did");
        #endregion

        if (query.Cid > 0)
              sql.Where("workflow.cid = @0", query.Cid);
        if (!string.IsNullOrEmpty(query.Name))
          sql.Where("workflow.name like @0", "%" + StringUtility.StripSQLInjection(query.Name) + "%");
        if (!string.IsNullOrEmpty(query.NickName))
            sql.Where("user.nick_name like @0", "%" + StringUtility.StripSQLInjection(query.NickName) + "%");
        if (query.StartDate != null && query.EndDate != null)
            sql.Where("workflow.created_time between @0 AND @1", query.StartDate, query.EndDate);
        sql.OrderBy("workflow.id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }

}
