﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Workflows;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Services.Workflows.Config;
using Qy.Base.Services.Workflows.WorkflowInstances;
using Qy.Base.Services.Workflows.WorkflowSteps;

namespace Qy.Base.Services.Workflows.Workflows;

[Inject(ServiceLifetime.Scoped)]
public class WorkflowService(
    WorkflowRepository workflowRepository,
    WorkflowInstanceRepository instanceRepository,
    WorkflowStepRepository stepRepository,
    IApplicationContext applicationContext,
    WorkflowCategoryConfigService configService,
    UserDeptService userDeptService,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader
    )
{

    public async Task<Workflow?> GetAsync(int id)
    {
        return await workflowRepository.GetAsync(id);
    }
    public async Task<IEnumerable<OptionTypeDto>> GetCatAsync()
    {
        return await configService.GetAsync();
    }
    public async Task<IPagedList<ViewWorkflow>> GetPagingAsync(QueryWorkflow query)
    {
        var pagedList = await workflowRepository.GetPagingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        var cats = await GetCatAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            v.CatName = cats.FirstOrDefault(m => m.Value == v.Cid)?.Label ?? string.Empty;
            return v;
        })
        .WithRelatedAsync(
            foreignKeySelector: v => v.Uid,
            loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
            attachAction: (v, user) => v.NickName = user.NickName
        );

    }
    public async Task<IEnumerable<Workflow>> GetListAsync(IEnumerable<int> ids)
    {
        return await workflowRepository.GetEntitiesByIdsAsync(ids);
    }

    public async Task<object> InsertAsync(Workflow userPost)
    {
        userPost.CreatedTime = DateTime.Now;
        userPost.Uid = applicationContext.RequiredCurrentUser.UserId;
        userPost.Did = applicationContext.GetRequiredCurrentUser().DeptId;
        return await workflowRepository.InsertAsync(userPost);
    }

    public async Task<int> UpdateAsync(Workflow userPost)
    {
        var get = await workflowRepository.GetAsync(userPost.Id);
        if (get != null)
        {
            get.Cid = userPost.Cid;
            get.Name = userPost.Name;
            get.Remark = userPost.Remark;
            get.IsActive = userPost.IsActive;
        }
        return await workflowRepository.UpdateAsync(get);
    }
    public async Task<int> DeleteByEntityIdAsync(int id)
    {
        if (await instanceRepository.ExistsByWidAsync(id))
            return -1;
        if (await stepRepository.ExistsByWidAsync(id))
            return -1;
        return await workflowRepository.DeleteByEntityIdAsync(id);
    }


    // 通知审批人（示例：预留接口）
    //private async Task NotifyApprovers(WorkflowInstance instance, WorkflowStep step)
    //{
    //    // 根据 step.ApproverType 和 ApproverRefIds 获取目标用户列表
    //    List<int> userIds = await ResolveApprovers(step);
    //    foreach (var userId in userIds)
    //    {
    //        // 调用你的通知服务（邮件、短信、站内信）
    //        await _notificationService.SendAsync(userId, $"您有新的审批任务：{instance.BusinessKey}");
    //    }
    //}
}
