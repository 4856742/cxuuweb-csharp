﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Dtos;
using Qy.Base.Services.SysConfigs;

namespace Qy.Base.Services.Workflows.Config;



[Inject(ServiceLifetime.Scoped)]
public class WorkflowCategoryConfigService(SysConfigRepository repository) : SysConfigService<List<OptionTypeDto>>(repository)
{
    protected override string ConfigKey => "WorkflowCategoryConfigs";

    /// <summary>
    /// 初始化应用程序配置的实现
    /// </summary>
    /// <param name="config">待初始化的配置对象</param>
    /// <returns>已完成的任务</returns>
    /// <exception cref="ArgumentNullException">当配置对象为 null 时抛出</exception>
    protected override Task InitializeAsync(List<OptionTypeDto> configs)
    {
        configs.Add(new OptionTypeDto { Value = 1, Label = "门户网站" });
        configs.Add(new OptionTypeDto { Value = 2, Label = "维权中心" });
        configs.Add(new OptionTypeDto { Value = 3, Label = "办公业务" });
        return Task.CompletedTask;
    }

}