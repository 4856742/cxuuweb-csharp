﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Services.Workflows.Config;

namespace Qy.Base.Services.SysConfigs;

/// <summary>
/// 应用程序配置服务类，继承自通用配置服务
/// </summary>
/// <remarks>
/// 构造函数，注入配置仓储
/// </remarks>
/// <param name="repository">应用程序配置仓储</param>
/// <exception cref="ArgumentNullException">当仓储参数为 null 时抛出</exception>
[Inject(ServiceLifetime.Scoped)]
public class AppConfigService(SysConfigRepository repository) : SysConfigService<AppConfig>(repository)
{

    /// <summary>
    /// 初始化应用程序配置的实现
    /// </summary>
    /// <param name="config">待初始化的配置对象</param>
    /// <returns>已完成的任务</returns>
    /// <exception cref="ArgumentNullException">当配置对象为 null 时抛出</exception>
    protected override Task InitializeAsync(AppConfig config)
	{
		// 设置默认配置值
		config.AppName = "琪耀管理系统";
		config.MaxConnections = 100;
		config.IsEnabled = true;
        config.UnitOfUse = "琪耀管理系统";
        config.AppUrl = "http://localhost:8080/";
        config.AttUrlPre = "api/common/att/";
        config.Description = "琪耀管理系统";
        config.Copyright = "© 2025 琪耀管理系统";
        config.BeianWeb = "渝ICP备20004602号-1";
        config.BeianApp = "渝ICP备20004602号-1";
        config.IsOnValidCode = true;
        config.ValidCodeType = 1;
        config.UserDefaultPass = "123456";


		return Task.CompletedTask;
	}
}