﻿namespace Qy.Base.Services.SysConfigs;

/// <summary>
/// 应用程序配置实体类
/// </summary>
public class AppConfig
{
	/// <summary>
	/// 应用程序名称
	/// </summary>
	public string AppName { get; set; } = string.Empty;
	/// <summary>
	/// 最大连接数
	/// </summary>
	public int MaxConnections { get; set; }
	/// <summary>
	/// 是否启用
	/// </summary>
	public bool IsEnabled { get; set; }
    public string UnitOfUse { get; set; } = string.Empty;
    public string AppUrl { get; set; } = string.Empty;
    public string AttUrlPre { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Copyright { get; set; } = string.Empty;
    public string BeianWeb { get; set; } = string.Empty;
    public string BeianApp { get; set; } = string.Empty;
    public string UserDefaultPass { get; set; } = string.Empty;
    public bool IsOnValidCode { get; set; }
    public int ValidCodeType { get; set; }


}