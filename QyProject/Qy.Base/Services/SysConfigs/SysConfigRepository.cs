﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities;

namespace Qy.Base.Services.SysConfigs;

/// <summary>
/// 通用配置仓储类，负责配置的获取和保存操作
/// </summary>
/// <remarks>
/// 构造函数，初始化仓储上下文
/// </remarks>
/// <param name="dbContext">数据库上下文</param>
/// <param name="applicationContext">应用上下文</param>
[Inject(ServiceLifetime.Scoped)]
public class SysConfigRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, SysConfig, string>(dbContext, applicationContext)
{

    /// <summary>
    /// 异步获取配置
    /// </summary>
    public async Task<TConfig?> GetAsync<TConfig>(string configKey)
        where TConfig : class, new()
    {
        var sysConfig = await base.GetAsync(configKey);
        return sysConfig?.ConfigValue != null
            ? Json.Parse<TConfig>(sysConfig.ConfigValue)
            : null;
    }
    /// <summary>
    /// 异步保存配置
    /// </summary>
    public async Task SaveAsync<TConfig>(string configKey, TConfig config)
        where TConfig : class, new()
    {
        var sysConfig = await base.GetAsync(configKey);
        var configValue = Json.Stringify(config);
        if (sysConfig == null)
        {
            await base.InsertAsync(new SysConfig
            {
                Id = configKey,
                ConfigValue = configValue
            });
        }
        else
        {
            sysConfig.ConfigValue = configValue;
            await base.UpdateAsync(sysConfig);
        }
    }

}