﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Entities.Attachment;
using Qy.Base.Extensions;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Attachment;

[Inject(ServiceLifetime.Scoped)]

public class AttachmentRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, SysAttachment, string>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;
    public async Task<IPagedList<SysAttachment>> GetPagingAsync(AttachmentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("attment.id").From(DbTableName.QyAttachment + " as attment");
        //.LeftJoin(DbTableName.QyUser + " as user").On("attment.uid = user.id");

        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser();
        user.GenQueryPermissionsSql(sql, "attment.uid", "attment.did");
        #endregion

        //if (query.NickName.IsNotEmpty())
          //  sql.Where("user.nick_name like @0", "%" + StringUtility.StripSQLInjection(query.NickName) + "%");
        if (!string.IsNullOrEmpty( query.Ext))
            sql.Where("attment.ext like @0", "%" + StringUtility.StripSQLInjection(query.Ext) + "%");
        if (query.StartDate != null && query.EndDate != null)
            sql.Where("attment.create_time between @0 AND @1", query.StartDate, query.EndDate);
        sql.OrderBy("attment.create_time desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }

    public async Task<IEnumerable<SysAttachment>> GetListAsync(string tenantType, string tenantId)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyAttachment);
        sql.Where("tenant_type = @0 and tenant_id = @1", tenantType, tenantId);
        return await DbContext.FetchAsync<SysAttachment>(sql);
    }
    /// <summary>
    /// 获得取要删除的附件列表，不缓存
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<SysAttachment>> GetCleanupListAsync()
    {
        Sql sql = Sql.Builder;
        sql.Select("*").From(DbTableName.QyAttachment);
        sql.Where("is_temporary = @0 or is_deleted = @0", true);
        sql.Where("create_time < @0", DateTime.Now.AddDays(-1));
        sql.Append("limit 0,500");
        return await DbContext.FetchAsync<SysAttachment>(sql);
    }

    /// <summary>
    /// 直接删除数据实体
    /// </summary>
    /// <param name="ent"></param>
    /// <returns></returns>
    public async Task<int> DelAsync(SysAttachment ent)
    {
        return await DbContext.DeleteAsync(ent);
    }
}
