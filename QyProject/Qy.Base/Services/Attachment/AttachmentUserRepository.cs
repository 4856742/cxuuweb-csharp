﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Entities.Attachment;
using Qy.Base.Extensions;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Attachment;

[Inject(ServiceLifetime.Scoped)]

public class AttachmentUserRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, AttachmentUser, int>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;
    public async Task<IPagedList<AttachmentUser>> GetPagingAsync(AttachmentUserQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("attuser.id").From(DbTableName.QyAttachmentUser + " as attuser")
         .LeftJoin(DbTableName.QyAttachment + " as att").On("attuser.attachment_id = att.id");

        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser();
        user.GenQueryPermissionsSql(sql, "attuser.uid", "attuser.did");
        #endregion

        if (!string.IsNullOrEmpty(query.FriendlyName))
            sql.Where("att.friendly_name like @0", "%" + StringUtility.StripSQLInjection(query.FriendlyName) + "%");
        if (query.StartDate != null && query.EndDate != null)
            sql.Where("att.create_time between @0 AND @1", query.StartDate, query.EndDate);
        sql.OrderBy("attuser.id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }

    public async Task<AttachmentUser?> GetWithAttachmentIdAsync(string attachmentId)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(DbTableName.QyAttachmentUser);
        sql.Where("attachment_id = @0", attachmentId);
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }
}
