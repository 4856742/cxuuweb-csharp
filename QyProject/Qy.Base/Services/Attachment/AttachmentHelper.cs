﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.Services.SysConfigs;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Qy.Base.Services.Attachment;

[Inject(ServiceLifetime.Scoped)]
public class AttachmentHelper(
    AttachmentService attachmentService,
     AppConfigService appConfigService,
    AttachmentManagerService attachmentManagerService)
{



    /// <summary>
    /// 从htm提取附件集合
    /// </summary>
    /// <param name="htmlContent"></param>
    /// <returns></returns>
    public async Task<List<string>> ExtractAttachmentIds(string htmlContent)
    {
        if (string.IsNullOrEmpty(htmlContent))
            return [];
        AppConfig appConfig = await appConfigService.GetAsync();
        // 正则表达式匹配 /api/file/ 后面的 attachmentId
        string pattern = @"([^/?\s""]+)";
        var regex = new Regex(appConfig.AttUrlPre + pattern);

        // 使用 HashSet 去重
        var attachmentIds = new HashSet<string>();

        // 获取所有匹配项
        MatchCollection matches = regex.Matches(htmlContent);
        foreach (Match match in matches)
        {
            if (match.Success && match.Groups.Count > 1)
            {
                attachmentIds.Add(match.Groups[1].Value);
            }
        }
        return attachmentIds.ToList();
    }

    /// <summary>
    /// 对内容中的图片或文件附件进行发布 
    /// </summary>
    /// <param name="content"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public async Task<List<IAttachment>> PublishAttachmentsContentAsync(string content, object tenantId)
    {
        var attIds = await ExtractAttachmentIds(content);
        List<PublishAttachmentArgs> publishAttachmentArgs = [.. attIds.Select(m =>
        {
            return new PublishAttachmentArgs
            {
                AttachmentId = m,
                TenantId = tenantId?.ToString() ?? string.Empty
            };
        })];
        return await attachmentManagerService.PublishAttachmentsAsync(publishAttachmentArgs);
    }

    /// <summary>
    /// 根据附件ID 批量发布附件
    /// </summary>
    /// <param name="attachments"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public async Task<List<IAttachment>> PublishAttachmentsAsync(List< string> attachments, object tenantId)
    {
        List<PublishAttachmentArgs> publishAttachmentArgs = [.. attachments.Select(m =>
        {
            return new PublishAttachmentArgs
            {
                AttachmentId = m,
                TenantId = tenantId?.ToString() ?? string.Empty
            };
        })];
        return await attachmentManagerService.PublishAttachmentsAsync(publishAttachmentArgs);
    }


    /// <summary>
    /// 发布单个附件
    /// </summary>
    /// <param name="attachments"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public async Task PublishAttachmentAsync(string attId, object tenantId)
    {
        if (string.IsNullOrWhiteSpace(attId))
            return;
            var attachment = await attachmentService.GetAsync(attId);
        if (attachment == null || !attachment.IsTemporary)
           return;
        await attachmentManagerService.PublishAttachmentAsync(attId, tenantId?.ToString() ?? string.Empty);
    }
}
