﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Entities.Attachment;

namespace Qy.Base.Services.Attachment;

[Inject(ServiceLifetime.Scoped, typeof(IAttachmentService))]
public class AttachmentService(
    AttachmentRepository attachmentRepository,
    List<TenantStorageConfig> _tenantConfigs
    ) : IAttachmentService
{

    /// <summary>
    /// 删除附件
    /// </summary>
    /// <param name="attachmentId"></param>
    /// <returns></returns>
    public async Task<int> DeleteAsync(string attachmentId)
    {
        return await attachmentRepository.DeleteByEntityIdAsync(attachmentId);
    }
    /// <summary>
    /// 实现删除附件接口
    /// </summary>
    /// <param name="attachmentId"></param>
    /// <returns></returns>
    Task IAttachmentService.DeleteAsync(string attachmentId)
    {
        return DeleteAsync(attachmentId);
    }
    /// <summary>
    /// 删除附件集合
    /// </summary>
    /// <param name="attachmentIds"></param>
    /// <returns></returns>
    public async Task DeleteBatchAsync(List<string> attachmentIds)
    {
        foreach (var attachmentId in attachmentIds)
            await DeleteAsync(attachmentId);
    }
    public async Task<IEnumerable<SysAttachment>> GetListAsync(string tenantType, string tenantId)
    {
        return await attachmentRepository.GetListAsync(tenantType, tenantId);
    }

    public async Task DeleteBatchAsync(string tenantType, object tenantId)
    {
        var tenId = tenantId.ToString();
        if (string.IsNullOrEmpty(tenantType) || string.IsNullOrEmpty(tenId))
        {
            throw new ArgumentNullException(nameof(tenantId), "租户及其ID不能为空");
        }
        var sysAttachments = await attachmentRepository.GetListAsync(tenantType, tenId);
        if (sysAttachments.Any())
        {
            await DeleteBatchAsync([.. sysAttachments.Select(x => x.Id)]);
        }
    }

    public async Task CreateAsync(IAttachment attachment)
    {
        SysAttachment attment = new()
        {
            Id = attachment.AttachmentId,
            TenantType = attachment.TenantType,
            FileName = attachment.FileName ?? "",
            MimeType = attachment.MimeType ?? "",
            FileSize = attachment.FileSize,
            CreateTime = DateTime.Now,
            UploaderIp = attachment.UploaderIp,
            MediaType = attachment.MediaType,
            FriendlyName = attachment.FriendlyName ?? "",
            ObjectName = attachment.ObjectName ?? "",
            BucketName = attachment.BucketName ?? "",
            Description = attachment.Description ?? "",
            IsTemporary = attachment.IsTemporary,
        };
        await attachmentRepository.InsertAsync(attment);
    }

    /// <summary>
    /// 异步获取附件信息。
    /// </summary>
    /// <param name="attachmentId">附件ID。</param>
    /// <returns>附件信息。</returns>
    /// <exception cref="ArgumentNullException">当附件不存在时抛出。</exception>
    public async Task<IAttachment> GetAsync(string attachmentId)
    {
        var attachment = await attachmentRepository.GetAsync(attachmentId);
        return attachment == null ? throw new PmSoftException("附件不存在") : (IAttachment)attachment.AsViewAttment();
    }

    public async Task PublishAsync(IAttachment attachment)
    {
        // 从数据库获取附件信息
        var sysAttachment = await attachmentRepository.GetAsync(attachment.AttachmentId) ?? throw new PmSoftException("附件不存在");
        // 更新附件信息
        sysAttachment.TenantId = attachment.TenantId;
        sysAttachment.FriendlyName = attachment.FriendlyName;
        sysAttachment.ObjectName = attachment.ObjectName;
        sysAttachment.IsTemporary = false;
        // 更新附件到数据库
        await attachmentRepository.UpdateAsync(sysAttachment);
    }


    public async Task<IEnumerable<IAttachment>> GetBatchAsync(List<string> attachmentIds)
    {
        var sysAttachments = await attachmentRepository.GetEntitiesByIdsAsync(attachmentIds);
        return sysAttachments.Select(m => m.AsViewAttment());
    }

    public async Task PublishBatchAsync(List<IAttachment> updatedAttachments)
    {
        foreach (var attachment in updatedAttachments)
        {
            await PublishAsync(attachment);
        }
    }


    /// <summary>
    /// 获取租户配置，如果未找到则使用默认配置
    /// </summary>
    public TenantStorageConfig GetTenantConfig(string tenantType)
    {
        return _tenantConfigs.FirstOrDefault(t => t.TenantType == tenantType)
            ?? _tenantConfigs.First(t => t.TenantType == "default");
    }
    /// <summary>
    /// 验证文件是否符合租户配置限制
    /// </summary>
    public static void ValidateFile(IFormFile file, TenantStorageConfig config, string? fileName = null)
    {
        if (file == null || file.Length == 0)
            throw new PmSoftException("未提供有效的文件。", "INVALID_FILE");

        if (config.MaxFileSize > 0 && file.Length > config.MaxFileSize)
            throw new PmSoftException($"文件大小超过限制 ({config.MaxFileSize / 1024 / 1024}MB)。", "FILE_SIZE_EXCEEDED");

        var ext = Path.GetExtension(string.IsNullOrEmpty(fileName) ? file.FileName : fileName).ToLower();
        if (config.AllowedExtensions.Count != 0 && !config.AllowedExtensions.Contains(ext))
            throw new PmSoftException($"不支持的文件扩展名: {ext}。允许的扩展名: {string.Join(", ", config.AllowedExtensions)}", "INVALID_EXTENSION");
    }
    /// <summary>
    /// 异步保存文件到本地文件系统
    /// </summary>
    /// <param name="args">保存文件参数</param>
    /// <returns>表示异步操作的任务</returns>
    public async Task<string> SaveFileAsync(IFormFile file)
    {
        var config = GetTenantConfig("general");
        ValidateFile(file, config);
        using var stream = new MemoryStream();
        await file.CopyToAsync(stream);
        var normalizedPath = NormalizePath(Path.Combine("uploads", "general", DateTime.Now.ToString("yyyyMM")));
        string _rootPath = Path.Combine(Environment.CurrentDirectory, "wwwroot");
        var directoryPath = Path.Combine(_rootPath, normalizedPath); // 组合目录路径
        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath); // 确保目录存在

        string fileExt = Path.GetExtension(file.FileName).ToLower();
        string newName = $"{GenerateFileName()}{fileExt}";
        var filePath = Path.Combine(directoryPath, newName); // 组合完整文件路径
        await File.WriteAllBytesAsync(filePath, stream.ToArray()); // 写入文件
        return Path.Combine("/", normalizedPath, $"{newName}").Replace("\\", "/");
    }

    // <summary>
    /// 将路径中的 "/" 转换为平台特定的分隔符
    /// </summary>
    /// <param name="path">原始路径，可能包含 "/"</param>
    /// <returns>规范化后的路径</returns>
    private static string NormalizePath(string? path)
    {
        if (string.IsNullOrEmpty(path))
            return string.Empty;
        // 将 "/" 替换为当前平台的路径分隔符（Windows 为 "\", Linux 为 "/"）
        return path.Replace("/", Path.DirectorySeparatorChar.ToString());
    }

    /// <summary>
    /// 生成唯一的文件名
    /// </summary>
    private static string GenerateFileName()
    {
        string guid = Guid.NewGuid().ToString("N"); // 生成 GUID 并移除连字符
        return guid[..10]; // 截取前 10 个字符  唯一性：2^40 ≈ 1.1 万亿种可能。 如果生成 100,000 个 GUID，碰撞概率约为 0.0000005%。
    }
}
