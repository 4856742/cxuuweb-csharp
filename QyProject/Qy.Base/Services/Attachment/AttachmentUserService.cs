﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Entities.Attachment;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;

namespace Qy.Base.Services.Attachment;
/// <summary>
/// 用户个人网盘
/// </summary>
/// <param name="attachmentUserRepository"></param>
/// <param name="cachedEntityLoader"></param>
/// <param name="applicationContext"></param>
/// <param name="userDeptService"></param>
[Inject(ServiceLifetime.Scoped)]
public class AttachmentUserService(
    AttachmentUserRepository attachmentUserRepository,
    AttachmentRepository attachmentRepository,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    IApplicationContext applicationContext,
    UserDeptService userDeptService)
{

    /// <summary>
    /// 获取用户附件集合
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task<IEnumerable<ViewAttachmentUser>> GetListAsync(string ids)
    {
        try
        {
            if (string.IsNullOrEmpty(ids))
                return [];
            var asIds = JsonConvert.DeserializeObject<List<int>>(ids ?? "") ?? [];
            if (asIds.Count > 0)
            {
                var userAttachments = await attachmentUserRepository.GetEntitiesByIdsAsync(asIds);
                var sysAttachments = await attachmentRepository.GetEntitiesByIdsAsync(userAttachments.Select(m => m.AttachmentId));
                return userAttachments.Select(x => x.AsViewAttachmentUser(sysAttachments));
            }
            return [];
        }
        catch
        {
            return [];
        }
    }

    public async ValueTask<IPagedList<ViewAttachmentUser>> GetPageListAsync(AttachmentUserQuery query)
    {
        var pagedList = await attachmentUserRepository.GetPagingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsViewAttachmentUser();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            return v;
        })
            .WithRelatedAsync(
                foreignKeySelector: v => v.Uid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
                attachAction: (v, user) => v.NickName = user.NickName
            )
            .WithRelatedByStrAsync(
                foreignKeySelector: v => v.AttachmentId,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<SysAttachment, string>,
                attachAction: (v, m) =>
                {
                    v.TenantType = m.TenantType;
                    v.TenantId = m.TenantId;
                    v.FileName = m.FileName;
                    v.BucketName = m.BucketName;
                    v.ObjectName = m.ObjectName;
                    v.FriendlyName = m.FriendlyName;
                    v.MediaType = m.MediaType;
                    v.MimeType = m.MimeType;
                    v.FileSize = m.FileSize;
                    v.UploaderIp = m.UploaderIp;
                    v.Description = m.Description;
                    v.IsDeleted = m.IsDeleted;
                    v.DeleteToken = m.DeleteToken;
                    v.IsTemporary = m.IsTemporary;
                    v.CreateTime = m.CreateTime;
                }
            );
    }
    public async Task<object> InsertAttachmentUserAsync(IAttachment attachment)
    {
        AttachmentUser attment = new()
        {
            AttachmentId = attachment.AttachmentId,
            Uid = applicationContext.RequiredCurrentUser.UserId,
            Did = applicationContext.GetRequiredCurrentUser().DeptId,
        };
        return await attachmentUserRepository.InsertAsync(attment);
    }
    public async Task<AttachmentUser?> GetWithAttachmentIdAsync(string id)
    {
        return await attachmentUserRepository.GetWithAttachmentIdAsync(id);
    }
    public async Task<int> DeleteAsync(string attachmentId)
    {
        var get = await attachmentUserRepository.GetWithAttachmentIdAsync(attachmentId);
        if (get != null)
        {
            await attachmentRepository.DeleteByEntityIdAsync(get.AttachmentId);
            return await attachmentUserRepository.DeleteByEntityIdAsync(get.Id);
        }
        return 0;
    }
}
