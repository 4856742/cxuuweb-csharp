﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Loging;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUserDept;

namespace Qy.Base.Services.Loging.Sys;

[Inject(ServiceLifetime.Scoped)]
public class LogSysService(LogSysRepository repository,
    IApplicationContext applicationContext,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    UserDeptService userDeptService)
{
    public async Task<IPagedList<ViewLogSys>> GetPagingAsync(LogingQuery query)
    {
        var pagedList = await repository.GetPagingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            return v;
        })
            .WithRelatedAsync(
                foreignKeySelector: v => v.Uid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
                attachAction: (v, user) => v.NickName = user?.NickName
            );
    }
    public void InsertLog(string doThing, string url)
    {
        try
        {
            var logEntry = new LogSys()
            {
                Did = applicationContext.GetRequiredCurrentUser().DeptId,
                Method = applicationContext.HttpContext.GetMethod(),
                ContrAct = url,
                Ip = applicationContext.ClientInfo.ClientIpAddress,
                Time = DateTime.Now,
                DoThing = doThing,
                Uid = applicationContext.RequiredCurrentUser.UserId,
            };
            repository.Insert(logEntry);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
