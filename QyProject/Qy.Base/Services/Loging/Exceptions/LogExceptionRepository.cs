﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Entities.Loging;
using Qy.Base.Extensions;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Loging.Exceptions;

[Inject(ServiceLifetime.Scoped)]
public class LogExceptionRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext) : CacheRepository<AppDbContext, LogException, int>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;
    public async Task<IPagedList<LogException>> GetPagingAsync(LogingQuery query)
    {
        Sql sql = Sql.Builder.Select("syslog.id").From(DbTableName.QyLogException + " as syslog");

        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser(); 
        if (user.IsNotSuperAdminAndAllAuthData())
            sql.Where("syslog.uid = @0", user.Id);
        #endregion

        if (!string.IsNullOrEmpty(query.SelectUids))
        {
            int[] uids = JsonConvert.DeserializeObject<int[]>(query.SelectUids) ?? [];
            if (uids != null && uids.Length > 0)
                sql.Where("syslog.uid IN (@uids)", new { uids });
        }
        if (query.SelectDid != null && query.SelectDid > 0)
            sql.Where("syslog.did = @0", query.SelectDid);
        if (!string.IsNullOrEmpty(query.Method))
            sql.Where("syslog.method = @0", StringUtility.StripSQLInjection(query.Method));
        if (query.StartDate != null && query.EndDate != null)
            sql.Where("syslog.time between @0 AND @1", query.StartDate, query.EndDate);
        if (query.Status != null)
            sql.Where("syslog.Status = @0", query.Status);
        sql.OrderBy("syslog.id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    /// <summary>
    /// 记录日志不需要返回值，采用同步方式
    /// </summary>
    /// <param name="logSql"></param>
    /// <returns></returns>
    public override object Insert(LogException logSql)
    {
        return DbContext.Insert(logSql);
    }

    public async Task<int> ClearAsync()
    {
        return await DbContext.ExecuteAsync("TRUNCATE " + DbTableName.QyLogException);
    }
    /// <summary>
    /// 未使用
    /// </summary>
    /// <returns></returns>
    public async Task AutoDelAsync(int num)
    {
        int conNum = await DbContext.ExecuteScalarAsync<int>($"select COUNT(Id) from {DbTableName.QyLogException}");
        if (conNum > num)
            await DbContext.ExecuteAsync($"delete from {DbTableName.QyLogException} order by id asc limit 1");
    }
}
