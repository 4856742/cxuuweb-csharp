﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Loging;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUserDept;

namespace Qy.Base.Services.Loging.Exceptions;

[Inject(ServiceLifetime.Scoped)]
public class LogExceptionService(LogExceptionRepository repository,
    IApplicationContext applicationContext,
    UserDeptService userDeptService,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader)
{
    public async Task<int> ClearAsync()
    {
        return await repository.ClearAsync();
    }
    public async Task<LogException?> GetAsync(int id)
    {
        return await repository.GetAsync(id);
    }
    public async Task<IPagedList<ViewLogException>> GetPagingAsync(LogingQuery query)
    {
        var pagedList = await repository.GetPagingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            return v;
        })
        .WithRelatedAsync(
            foreignKeySelector: attment => attment.Uid,
            loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
            attachAction: (v, user) => v.NickName = user?.NickName
        );
    }
    public void InsertLog(LogException jsonResult)
    {
        try
        {
            var logEntry = new LogException()
            {
                Did = applicationContext.GetRequiredCurrentUser().DeptId,
                Method = applicationContext.HttpContext.GetMethod(),
                ContrAct = $"{applicationContext.HttpContext.GetController()}/{applicationContext.HttpContext.GetAction()}",
                Ip = applicationContext.ClientInfo.ClientIpAddress,
                Time = DateTime.Now,
                StackTrace = jsonResult.StackTrace,
                Message = jsonResult.Message,
                Uid = applicationContext.RequiredCurrentUser.UserId,
            };
            repository.Insert(logEntry);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
