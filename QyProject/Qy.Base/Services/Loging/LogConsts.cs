﻿using System.Collections.Generic;
using Qy.Base.Utilities.EPPlusExcel;
namespace Qy.Base.Services.Loging;

public static class LogConsts
{
    public static List<GreatExcelField> ViewLogExceptionFields
    {
        get
        {
            return [
                   new() { Value = 1, FieldName = "Id", Label = "Id", Width = 10, Type = GreatExcelDataType.Int32 },
                   new() { Value = 2, FieldName = "DeptName", Label = "部门", Width = 16, Type = GreatExcelDataType.String },
                   new() { Value = 3, FieldName = "NickName", Label = "姓名", Width = 13, Type = GreatExcelDataType.String },
                   new() { Value = 4, FieldName = "Ip", Label = "Ip", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 5, FieldName = "ContrAct", Label = "控制器", Width = 30, Type = GreatExcelDataType.String },
                   new() { Value = 7, FieldName = "Method", Label = "请求方法", Width = 30, Type = GreatExcelDataType.String },
                   new() { Value = 8, FieldName = "Message", Label = "提示信息", Width = 40, Type = GreatExcelDataType.String },
                   //new() { Value = 9, FieldName = "StackTrace", Label = "堆栈信息", Width = 60, Md5 = GreatExcelDataType.String },
                   new() { Value = 10, FieldName = "Time", Label = "操作时间", Width = 30, Type = GreatExcelDataType.DateTime },
            ];
        }
    }
    public static List<GreatExcelField> ViewLogSysFields
    {
        get
        {
            return [
                   new() { Value = 1, FieldName = "Id", Label = "Id", Width = 10, Type = GreatExcelDataType.Int32 },
                   new() { Value = 2, FieldName = "DeptName", Label = "部门", Width = 16, Type = GreatExcelDataType.String },
                   new() { Value = 3, FieldName = "NickName", Label = "姓名", Width = 13, Type = GreatExcelDataType.String },
                   new() { Value = 4, FieldName = "Ip", Label = "Ip", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 5, FieldName = "ContrAct", Label = "控制器", Width = 30, Type = GreatExcelDataType.String },
                   new() { Value = 7, FieldName = "Method", Label = "请求方法", Width = 30, Type = GreatExcelDataType.String },
                   new() { Value = 8, FieldName = "DoThing", Label = "内容", Width = 40, Type = GreatExcelDataType.String },
                   new() { Value = 9, FieldName = "Time", Label = "操作时间", Width = 30, Type = GreatExcelDataType.DateTime },
            ];
        }
    }
    public static List<GreatExcelField> LogLoginViewFields
    {
        get
        {
            return [
                   new() { Value = 1, FieldName = "Id", Label = "Id", Width = 8, Type = GreatExcelDataType.Int32 },
                   new() { Value = 2, FieldName = "DeptName", Label = "部门", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 3, FieldName = "NickName", Label = "姓名", Width = 10, Type = GreatExcelDataType.String },
                   new() { Value = 4, FieldName = "Ip", Label = "Ip", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 5, FieldName = "GetUserAgent", Label = "客户端", Width = 50, Type = GreatExcelDataType.String },
                   new() { Value = 6, FieldName = "Time", Label = "操作时间", Width = 30, Type = GreatExcelDataType.DateTime },
            ];
        }
    }
}
