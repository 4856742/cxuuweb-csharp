﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Core.Domain.Auth;
using PmSoft.Data.Abstractions;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Loging;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;

namespace Qy.Base.Services.Loging.Login;

[Inject(ServiceLifetime.Scoped)]
public class LogLoginService(
    LogLoginRepository logLoginRepository,
    IApplicationContext applicationContext,
    UserDeptService userDeptService,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader
)
{
    public async Task<IPagedList<ViewLogLogin>> GetPagingAsync(LogingQuery query)
    {
        var pagedList = await logLoginRepository.GetPagingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            return v;
        })
            .WithRelatedAsync(
                foreignKeySelector: attment => attment.Uid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
                attachAction: (v, user) => v.NickName = user?.NickName
            );
    }

    public async Task<object> InsertLogLoginAsync(IAuthedUser user)
    {
        if(user is not AuthUser authUser)
            throw new PmSoftException("用户信息错误");
        LogLogin logSql = new()
        {
            Uid = user.UserId,
            Did = authUser.DeptId,
            Ip = applicationContext.ClientInfo.ClientIpAddress,
            UserAgent = applicationContext.HttpContext.GetUserAgent() ?? "N/A",
            Time = DateTime.Now
        };
        return await logLoginRepository.InsertAsync(logSql);
    }
}
