﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Entities.Loging;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;

namespace Qy.Base.Services.Loging.Login;

[Inject(ServiceLifetime.Scoped)]
public class LogLoginRepository(AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, LogLogin, int>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;
    public async Task<IPagedList<LogLogin>> GetPagingAsync(LogingQuery query)
    {
        Sql sql = Sql.Builder.Select("log.id").From(DbTableName.QyLogLogin + " as log");
            //.LeftJoin(DbTableName.QyUser + " as user").On("log.uid = user.id");
        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser(); 
        user.GenQueryPermissionsSql(sql, "log.uid", "log.did");
        #endregion
        if (!string.IsNullOrEmpty( query.SelectUids))
        {
            int[] uids = JsonConvert.DeserializeObject<int[]>(query.SelectUids) ?? [];
            if (uids.Length > 0)
                sql.Where("log.uid IN (@uids)", new { uids });
        }
        if (query.SelectDid != null && query.SelectDid > 0)
            sql.Where("log.did = @0", query.SelectDid);
        if (query.StartDate != null && query.EndDate != null)
            sql.Where("log.time between @0 AND @1", query.StartDate, query.EndDate);
        sql.OrderBy("log.id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }

}
