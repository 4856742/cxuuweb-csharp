﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities.Notice;

namespace Qy.Base.Services.Notices;

[Inject(ServiceLifetime.Scoped)]

public class NoticeRepository(AppDbContext dbContext, IApplicationContext appContext) : CacheRepository<AppDbContext, Notice, int>(dbContext, appContext)
{
}
