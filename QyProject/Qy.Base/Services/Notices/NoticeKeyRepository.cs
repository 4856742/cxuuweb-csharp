﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Entities.Notice;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Notices;

[Inject(ServiceLifetime.Scoped)]
public class NoticeKeyRepository(
    AppDbContext dbContext,
    IApplicationContext appContext
    ) : CacheRepository<AppDbContext, NoticeKey, int>(dbContext, appContext)
{
    public async Task<IPagedList<NoticeKey>> GetPagedAsync(NoticeQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("noticeKey.Id")
            .From($"{DbTableName.QyNoticeKey} as noticeKey")
            .LeftJoin($"{DbTableName.QyNotice} as data").On("noticeKey.nid = data.id");

        if (!string.IsNullOrEmpty(query.KeyWords))
            sql.Where("data.title like @0", "%" + StringUtility.StripSQLInjection(query.KeyWords) + "%");
        if (query.Status == false)
            sql.Where("noticeKey.status = @0", false);
        if (query.Status == true)
            sql.Where("noticeKey.status = @0", true);
        if (query.ListType == 2)//空或1为收到消息列表,2是已发消息列表
            sql.Where("data.post_uid =@0", query.Uid);
        else
            sql.Where("noticeKey.get_uid = @0", query.Uid);
        sql.OrderBy("noticeKey.status asc,noticeKey.id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    public async Task<long> GetNoticeCountAsync(int getUid)
    {
        Sql sql = Sql.Builder;
        sql.Select("COUNT(1) AS Count").From(DbTableName.QyNoticeKey).Where("status = 0 and get_uid = @0", getUid);
        return await DbContext.FirstOrDefaultAsync<int>(sql);
    }
}
