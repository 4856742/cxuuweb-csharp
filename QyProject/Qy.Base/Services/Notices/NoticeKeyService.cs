﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PmSoft.Core;
using PmSoft.Core.Extensions;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Notice;
using Qy.Base.Extensions;
using Qy.Base.Services.Attachment;
using Qy.Base.Services.Auth.QyUser;

namespace Qy.Base.Services.Notices;

[Inject(ServiceLifetime.Scoped)]
public class NoticeKeyService(
    NoticeService noticeService,
    NoticeKeyRepository noticeKeyRepository,
    IApplicationContext applicationContext,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    AttachmentHelper attachmentHelper,
    PetaPocoUnitOfWork<AppDbContext> unitOfWork
    )
{
    public async Task<ViewNoticeKey?> GetAndUserAsync(int id)
    {
        var notice = await noticeService.GetOneAsync(id);
        if (notice == null)
            return null;
        var user = await applicationContext.GetRequiredService<UserService>().GetAsync(notice.PostUid);
        if (user == null)
            return null;
        return new ViewNoticeKey
        {
            Title = notice.Title,
            Type = notice.Type,
            Content = notice.Content,
            PostTime = notice.PostTime,
            PostUid = notice.PostUid,
            Attments = notice.Attments,
            NoticeId = notice.Id,
            PostNickName = user.NickName,
            PostAvatar = user.Avatar,
        };
    }

    /// <summary>
    /// 读取一条
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userInfo"></param>
    /// <returns></returns>
    public async Task<ViewNoticeKey?> ReadAsync(int id, int uid)
    {
        var noticeKey = await noticeKeyRepository.GetAsync(id);
        if (noticeKey == null)
            return null;
        var notice = await GetAndUserAsync(noticeKey.Nid);
        if (notice == null)
            return null;
        if (uid != notice.PostUid && noticeKey.Status != true)
        {
            noticeKey.Status = true;
            noticeKey.ReadTime = DateTime.Now;
            await noticeKeyRepository.UpdateAsync(noticeKey);
        }
        return notice;
    }
    /// <summary>
    /// 发送消息
    /// </summary>
    /// <param name="noticeId"></param>
    /// <returns></returns>
    public async Task<ViewNoticeKey?> ReadOneAsync(int noticeId)
    {
        return await GetAndUserAsync(noticeId);
    }

    public async Task<long> GetNoticeCountAsync(int uid)
    {
        return await noticeKeyRepository.GetNoticeCountAsync(uid);
    }

    public async Task<IPagedList<ViewNoticeKey>> GetPageListAsync(NoticeQuery query)
    {
        var pagedList = await noticeKeyRepository.GetPagedAsync(query);
        return await pagedList.ToPagedDto(m => m.AsView())
            .WithRelatedAsync(
                foreignKeySelector: v => v.NoticeId,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<Notice, int>,
                attachAction: (v, m) =>
                {
                    v.Title = m.Title;
                    v.Type = m.Type;
                    v.PostTime = m.PostTime;
                    v.PostUid = m.PostUid;
                }
            ).WithRelatedAsync(
                foreignKeySelector: v => v.GetUid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
                attachAction: (v, m) =>
                {
                    v.GetNickName = m.NickName;
                    v.GetUserAvatar = m.Avatar;
                }
            ).WithRelatedAsync(
                foreignKeySelector: v => v.PostUid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
                attachAction: (v, m) =>
                {
                    v.PostNickName = m.NickName;
                    v.PostAvatar = m.Avatar;
                }
            );
    }

    public async Task<int> DeleteAsync(int id)
    {
        //预留删除附件，暂未实现。思路是，当删除当前notice表中最后一个通知时，删除当前notice表对应数据和关联的附件
        return await noticeKeyRepository.DeleteByEntityIdAsync(id);
    }

    /// <summary>
    /// 批量写入通知 
    /// </summary>
    public async Task<bool> InsertAsync(EditNotice editNotice, int postUid)
    {
        Notice notice = new()
        {
            Title = editNotice.Title,
            PostTime = DateTime.Now,
            PostUid = postUid,
            Type = editNotice.Type,
            Content = editNotice.Content,
            Attments = editNotice.Attments,
        };
        var res = await noticeService.InsertAsync(notice);
        //发布附件
        await PublishAttachments(editNotice.Content, res);
        int noticeId = res.ParseToInt();
        try
        {
            int count = 0;
            int[] uids = JsonConvert.DeserializeObject<int[]>(editNotice.GetUid) ?? [];
            if (uids.Length < 1 || noticeId < 1)
                return false;
            //执行事务
            await unitOfWork.ExecuteInTransactionAsync(async () =>
            {
                for (int i = 0; i < uids.Length; i++)
                {
                    NoticeKey noticeKey = new()
                    {
                        Nid = noticeId,
                        GetUid = uids[i],
                        PostUid = notice.PostUid,
                        Status = false,
                    };
                    var insert = await noticeKeyRepository.InsertAsync(noticeKey);
                    if (insert != null) count++;
                }
            });
            return count > 0;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            return false;
        }
    }

    /// <summary>
    /// 批量发布内容图片
    /// </summary>
    /// <param name="content"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public async Task PublishAttachments(string content, object tenantId)
    {
        //var attachments =
        await attachmentHelper.PublishAttachmentsContentAsync(content, tenantId);
    }
}
