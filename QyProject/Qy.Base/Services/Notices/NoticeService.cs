﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Entities.Notice;

namespace Qy.Base.Services.Notices;

[Inject(ServiceLifetime.Scoped)]
public class NoticeService(NoticeRepository noticeRepository)
{
    public async Task<object> InsertAsync(Notice notice)
    {
        return await noticeRepository.InsertAsync(notice);
    }
    public async Task<Notice?> GetOneAsync(int id)
    {
        return await noticeRepository.GetAsync(id);
    }
    public async Task<IEnumerable<Notice>> GetNoticesAsync(IEnumerable<int> ids)
    {
        return await noticeRepository.GetEntitiesByIdsAsync(ids);
    }
}
