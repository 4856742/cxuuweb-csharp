﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities.QyMenu;

namespace Qy.Base.Services.Menus;

[Inject(ServiceLifetime.Scoped)]
public class MenuRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, Menu, int>(dbContext, applicationContext)
{
    public async Task<IEnumerable<Menu>> GetAllMenuAsync()
    {
        return await GetTopEntitiesWithCacheAsync(2000,
           CachingExpirationType.RelativelyStable,
           () => "AllMenu",
           () =>
           {
               Sql sql = Sql.Builder;
               return sql;
           });
    }

    public async Task<IEnumerable<Menu>> GetListForCidAsync(int cid)
    {
        int areaVersion = await CacheVersionService.GetAreaVersionAsync(m => m.Cid, cid);
        return await GetTopEntitiesWithCacheAsync(1000,
            CachingExpirationType.ObjectCollection,
            () => $"menus:{cid}:{areaVersion}",
            () =>
            {
                Sql sql = Sql.Builder;
                sql.Select("id").From(DbTableName.QyMenu);
                if (cid > 0)
                    sql.Where("cid = @0", cid);
                sql.OrderBy("sort asc");
                return sql;
            });
    }

    public async Task<IEnumerable<Menu>> GetListForTypeAsync(int type)
    {
        int areaVersion = await CacheVersionService.GetAreaVersionAsync(m => m.Type, type);
        return await GetTopEntitiesWithCacheAsync(1000,
            CachingExpirationType.ObjectCollection,
            () => $"menus:{type}:{areaVersion}",
            () =>
            {
                Sql sql = Sql.Builder;
                sql.Select("id").From(DbTableName.QyMenu);
                sql.Where("type = @0", type);
                sql.OrderBy("sort asc");
                return sql;
            });
    }

    public async Task<bool> ExistsByCidAsync(int cid)
    {
        return await DbContext.ExistsAsync<Menu>("cid =@0", cid);
    }
}
