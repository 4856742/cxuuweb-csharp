﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Auth;
using Qy.Base.Entities.QyMenu;
using Qy.Base.Utilities;

namespace Qy.Base.Services.Menus.MenuCats;

[Inject(ServiceLifetime.Scoped)]
public class MenuCatRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, MenuCat, int>(dbContext, applicationContext)
{

    public async Task<IEnumerable<MenuCat>> GetAllMenuCatAsync()
    {
        var globalVer = await CacheVersionService.GetGlobalVersionAsync();
        return await GetTopEntitiesWithCacheAsync(1000, CachingExpirationType.RelativelyStable, () =>
        {
            return $"AllMenuCat:{globalVer}";
        }, () =>
        {
            Sql sql = Sql.Builder;
            sql.Select("id").From(DbTableName.QyMenuCat).OrderBy("sort asc");
            return sql;
        });
    }
    public async Task<MenuCat?> GetWithAppIdAsync(LoginDto login)
    {
        Sql sql = Sql.Builder;
        sql.Select("id");
        sql.From(DbTableName.QyMenuCat);
        sql.Where("client_id =@0", StringUtility.StripSQLInjection(login.ClientId));
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0)
            return null;
        return await GetAsync(id);
    }
}
