﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Dtos.Auth;
using Qy.Base.Dtos.Menu;
using Qy.Base.Entities.QyMenu;
using Qy.Base.Extensions;

namespace Qy.Base.Services.Menus.MenuCats;

[Inject(ServiceLifetime.Scoped)]
public class MenuCatService(MenuCatRepository qyMenuCatRepository, MenuRepository menuRepository)
{
    private readonly MenuCatRepository qyMenuCatRepository = qyMenuCatRepository;
    private readonly MenuRepository menuRepository = menuRepository;

    public async Task<IEnumerable<MenuCat>> GetAllAsync()
    {
        return await qyMenuCatRepository.GetAllMenuCatAsync();
    }
    public async Task<MenuCat?> GetWithAppIdAsync(LoginDto login)
    {
        var all = await GetAllAsync();
        return all.FirstOrDefault(x => x.ClientId == login.ClientId);
    }
    /// <summary>
    /// 输出不含客户端密钥等敏感信息的列表
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<MenuCatOptionType>> GetByAsOptionsAsync()
    {
        var all = await GetAllAsync();
        var whereList = all?.Where(x => x.Type == (int)QyMenuCatType.Content);
        if (whereList == null)
            return [];
        return whereList.Select(x => new MenuCatOptionType
        {
            Name = x.Name,
            Type = x.Type,
            Id = x.Id,
            Pid = x.Pid,
        });
    }

    public async Task<IEnumerable<OptionItemDto>> IndentedOptionAsync()
    {
        var cates = await GetAllAsync();
        return cates.AsIndentedOptionType(); 
    }

    public async Task<IEnumerable<ViewQyMenuCat>> IndentedListAsync()
    {
        var cates = await GetAllAsync();
        List<ViewQyMenuCat> optionTypes = [];
        IndentedOptionType(cates, optionTypes);
        return optionTypes;
    }
    public static void IndentedOptionType(IEnumerable<MenuCat> workCats, IList<ViewQyMenuCat> viewWorkCats, int parentId = 0, int depth = 0)
    {
        depth++;
        IEnumerable<MenuCat> cates = workCats.Where(a => a.Pid == parentId).ToList();
        foreach (MenuCat item in cates)
        {
            ViewQyMenuCat treeModel = new()
            {
                Id = item.Id,
                Name = item.Name,
                Type = item.Type,
                Icon = item.Icon,
            };
            viewWorkCats.Add(treeModel);
            IndentedOptionType(workCats, treeModel.Children, treeModel.Id, depth);
        }
    }

    public async Task<IEnumerable<MenuCat>> GetAppLinkCatesAsync(IEnumerable<int> ids)
    {
        return await qyMenuCatRepository.GetEntitiesByIdsAsync(ids);
    }
    public async Task<MenuCat?> GetAsync(int id)
    {
        return await qyMenuCatRepository.GetAsync(id);
    }
    public async Task<object> InsertAsync(MenuCat appLinkCate)
    {
        return await qyMenuCatRepository.InsertAsync(appLinkCate);
    }

    public async Task<int> UpdateAsync(MenuCat appLinkCate)
    {
        return await qyMenuCatRepository.UpdateAsync(appLinkCate);
    }

    public async Task<int> DeleteAsync(int id)
    {
        if (await menuRepository.ExistsByCidAsync(id))
            return 0;
        return await qyMenuCatRepository.DeleteByEntityIdAsync(id);
    }

    public async Task<IEnumerable<ViewQyMenuCat>> GetCatsAsync()
    {
        IEnumerable<MenuCat> cates = await GetAllAsync();
        return await AsCatsAsync(0, cates.ToList());
    }

    /// <summary>
    /// 递归栏目树
    /// </summary>
    /// <returns></returns>
    private static async Task<List<ViewQyMenuCat>> AsCatsAsync(int pid, List<MenuCat> depts)
    {
        // 获取所有父分类  
        var parent = depts.Where(P => P.Pid == pid).ToList();
        var lists = new List<ViewQyMenuCat>();

        foreach (var item in parent)
        {
            var child = new ViewQyMenuCat
            {
                Id = item.Id,
                Name = item.Name,
                Sort = item.Sort,
                Icon = item.Icon,
                Type = item.Type,
                Remark = item.Remark,
                ClientId = item.ClientId,
                ClientUrl = item.ClientUrl,
                ClientSecret = item.ClientSecret,
                Children = await GetSonsAsync(item.Id, depts)
            };

            // 如果有子分类，则设置相关属性  
            if (child.Children != null && child.Children.Count > 0)
            {
                child.HaveChild = true;
                child.Spread = true;
            }

            lists.Add(child);
        }

        // 返回根据 Sort 和 Id 排序的列表  
        return lists.OrderBy(o => o.Sort).ThenBy(o => o.Id).ToList();
    }

    private static async Task<List<ViewQyMenuCat>> GetSonsAsync(int parentId, List<MenuCat> depts)
    {
        // 检查是否有子分类  
        if (!depts.Any(x => x.Pid == parentId))
        {
            return [];
        }
        // 递归调用 AsCatsAsync 获取子分类  
        return await AsCatsAsync(parentId, depts);
    }
}
