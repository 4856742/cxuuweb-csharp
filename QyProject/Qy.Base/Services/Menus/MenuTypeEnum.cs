﻿namespace Qy.Base.Services.Menus;
public enum QyMenuCatType
{
    /// <summary>
    /// 目录
    /// </summary>
    Catalog = 1,
    /// <summary>
    /// 内容
    /// </summary>
    Content = 2,

}
public enum QyMenuType
{
    /// <summary>
    /// 目录
    /// </summary>
    Catalog = 0,
    /// <summary>
    /// 菜单
    /// </summary>
    Menu = 1,
    /// <summary>
    /// 按钮
    /// </summary>
    Botton = 2,
    /// <summary>
    /// 外链地址
    /// </summary>
    Link = 3,

}
