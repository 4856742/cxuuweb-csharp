﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Dtos.Menu;
using Qy.Base.Entities.QyMenu;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;

namespace Qy.Base.Services.Menus;

[Inject(ServiceLifetime.Scoped)]
public class MenuService(
    MenuRepository menuRepository,
    UserAuthenticationService userAuthenticationService,
    IApplicationContext applicationContext)
{
    /// <summary>
    /// 根据角色权限输出按钮权限集
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<string>> GetUserBottonRolesAsync()
    {
        var currentUser = applicationContext.GetRequiredCurrentUser();
        var cates = await GetListWithBottonAsync();
        HashSet<int> idsToFind = [.. currentUser.Roles.Select(x => x.Id)];
        return cates.Where(c => idsToFind.Contains(c.Id)).Select(x => x.PermCode);
    }

    public async Task<IEnumerable<Menu>> GetListWithBottonAsync()
    {
        return await menuRepository.GetListForTypeAsync((int)QyMenuType.Botton);
    }
    /// <summary>
    /// 目录与菜单组合
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<Menu>> GetListWithMenuAndCatalogAsync()
    {
        var atalogs = await menuRepository.GetListForTypeAsync((int)QyMenuType.Catalog);
        var menus = await menuRepository.GetListForTypeAsync((int)QyMenuType.Menu);
        var list = atalogs.Concat(menus) ?? [];
        return list;
    }

    public async Task<object> InsertAsync(Menu menu)
    {
        return await menuRepository.InsertAsync(menu);
    }
    public async Task<int> UpdateAsync(Menu menu)
    {
        return await menuRepository.UpdateAsync(menu);
    }
    public async Task<int> DeleteByEntityIdAsync(int id)
    {
        var menus = await GetListWithMenuAndCatalogAsync();
        if (menus.Any(a => a.Pid == id))
            return 0;
        return await menuRepository.DeleteByEntityIdAsync(id);
    }
    public async Task<Menu?> GetAsync(int id)
    {
        return await menuRepository.GetAsync(id);
    }

    public async Task<IEnumerable<Menu>> GetListForCidAsync(int cid)
    {
        return await menuRepository.GetListForCidAsync(cid);
    }
    /// <summary>
    /// 角色管理编辑功能权限使用
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<MenuOptionType>> IndentedOptionAsync()
    {
        var menus = await GetListWithMenuAndCatalogAsync();
        var bottons = await GetListWithBottonAsync();
        var list = menus.Concat(bottons);
        List<MenuOptionType> menuOptionTypes = [];
        IndentedOptionType(list, menuOptionTypes);
        return menuOptionTypes;
    }

    public static void IndentedOptionType(IEnumerable<Menu> list, IList<MenuOptionType> commonOptionTypes, int parentId = 0)
    {
        if (list == null || !list.Any()) return;
        var cates = list.Where(a => a.Pid == parentId).ToList();
        foreach (var item in cates)
        {
            var treeModel = new MenuOptionType
            {
                Value = item.Id,
                Cid = item.Cid,
                Label = item.Name,
                Type = item.Type,
            };
            commonOptionTypes.Add(treeModel);
            IndentedOptionType(list, treeModel.Children, treeModel.Value);
        }
    }
    /// <summary>
    /// 编辑菜单时调用
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<OptionItemDto>> IndentedMenuOptionAsync(int cid)
    {
        var list = await GetListForCidAsync(cid);
        list = list.Where(list => list.Type != (int)QyMenuType.Botton);
        return list.AsIndentedOptionType();
    }

    /// <summary>
    /// 管理列表 全部
    /// </summary>
    /// <returns></returns>
    public async Task<List<ViewMenu>> GetListAsync(int cid)
    {
        var menus = await GetListForCidAsync(cid);
        return await GetCatesAsync(0, menus.ToList());
    }
    /// <summary>
    /// 递归树
    /// </summary>
    /// <param name="pid">父级Id</param>
    /// <param name="cates">数据源</param>
    /// <returns></returns>
    private static async Task<List<ViewMenu>> GetCatesAsync(int pid, List<Menu> cates)
    {
        var parent = cates.Where(P => P.Pid == pid);
        List<ViewMenu> lists = [];
        foreach (var item in parent)
        {
            ViewMenu Childer = new()
            {
                Id = item.Id,
                Cid = item.Cid,
                Name = item.Name,
                Type = item.Type,
                Icon = item.Icon,
                PermCode = item.PermCode,
                Sort = item.Sort,
                Status = item.Status,
                Path = item.Path,
                Component = item.Component,
                Redirect = item.Redirect,
                Hide = item.Hide,
            };
            Childer.Children = await GetSonAsync(Childer, cates);
            lists.Add(Childer);
        }
        async Task<List<ViewMenu>> GetSonAsync(ViewMenu cates, List<Menu> sonCates)
        {
            if (!sonCates.Exists(x => x.Pid == cates.Id)) return [];
            else return await GetCatesAsync(cates.Id, sonCates);
        }
        return [.. lists.OrderBy(o => o.Sort).ThenBy(o => o.Id)];
    }


    /// <summary>
    /// 根据系统配置，生成指定菜单
    /// </summary>
    /// <returns></returns>
    public async Task<List<AsMenu>> GetMenuListAsync(int[] cids)
    {
        //if (string.IsNullOrEmpty(cids))
        //    return [];
        //string[] stringArray = cids.Split(',');
        //int[] ids = Array.ConvertAll(stringArray, int.Parse);
        if (cids.Length < 1)
            return [];

        List<ViewMenu> newMenus = [];
        var user = applicationContext.GetRequiredCurrentUser();

        var menus = await userAuthenticationService.GeUserMenusAsync(user);
        var viewMenus = menus.OfType<ViewMenu>();
        if (viewMenus.Any())
        {
            foreach (var cid in cids)
            {
                var list = viewMenus.Where(x => x.Cid == cid && x.Type != (int)QyMenuType.Botton);
                newMenus.AddRange(list);
            }
        }
        return await BuildRouteListAsync(0, newMenus);
    }
    /// <summary>
    /// 获取所有子级
    /// </summary>
    /// <param name="list"></param>
    /// <param name="pid"></param>
    /// <returns></returns>
    public static IEnumerable<Menu> GetMenuSonListWithPid(IEnumerable<Menu> list, int pid)
    {
        if (list == null)
            return [];
        IEnumerable<Menu> query = list.Where(p => p.Pid == pid);
        return query.Concat(query.SelectMany(t => GetMenuSonListWithPid(list, t.Id)));
    }


    private static async Task<List<AsMenu>> BuildRouteListAsync(int pid, List<ViewMenu> cates)
    {
        var parent = cates.Where(P => P.Pid == pid).ToList();
        var lists = new List<AsMenu>();
        foreach (var item in parent)
        {
            var child = new AsMenu
            {
                MenuId = item.Id,
                Path = item.Path,
                Type = item.Type,
                Meta = new AsMenuMeta
                {
                    Icon = item.Icon,
                    Hidden = item.Hide,
                    Title = item.Name,
                    KeepAlive = item.KeepAlive,
                    AlwaysShow = item.AlwaysShow,
                },
                Name = item.Path,
                Component = item.Component,
                Sort = item.Sort,
                Redirect = item.Redirect,
                Children = await GetSon(item.Id, cates)
            };
            lists.Add(child);
        }
        return lists.OrderBy(o => o.Sort).ThenBy(o => o.MenuId).ToList();
    }

    private static async Task<List<AsMenu>> GetSon(int menuId, List<ViewMenu> sonCates)
    {
        if (!sonCates.Any(x => x.Pid == menuId))
            return [];
        return await BuildRouteListAsync(menuId, sonCates);
    }
}
