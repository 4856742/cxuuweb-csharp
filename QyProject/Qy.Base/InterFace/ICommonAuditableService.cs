﻿using Qy.Base.AppBase;

namespace Qy.Base.InterFace;

public interface ICommonAuditableService
{
    void InsertLog(string doThing, string url, ProjectType groupName);

}
