﻿using PmSoft.Core.Domain.Auth;
using Qy.Base.Entities.Auth;

namespace Qy.Base.InterFace.User;
public interface IAuthUser : IAuthedUser
{
    int Id { get; set; }
    public UserTypeEnum Type { get; set; }
    int DeptId { get; set; }
    List<int> Dids { get; set; }
    string UserName { get; set; }
    string NickName { get; set; }
    string DeptName { get; set; }
    AccessLevelType AccessLevel { get; set; }
    bool Status { get; set; }
    string Avatar { get; set; }
    string Mobile { get; set; }
}
