﻿using Qy.Base.Entities.Auth;

namespace Qy.Base.InterFace.User;
public interface IViewUser
{
    int Id { get; set; }
    public UserTypeEnum Type { get; set; }
    int DeptId { get; set; }
    string? DeptName { get; set; }
    AccessLevelType? AccessLevel { get; set; }
    string? IdNumber { get; set; }
    string? UserName { get; set; }
    string? NickName { get; set; }
    string? PinyinName { get; set; }
    string? PyName { get; set; }
    bool? Status { get; set; }
    string? Avatar { get; set; }
    string? Mobile { get; set; }
    int? OrderNum { get; set; }
    DateTime? CreateTime { get; set; }
}
