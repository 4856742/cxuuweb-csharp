﻿using PmSoft.Core;
using Qy.Base.Services.Auth.QyUser;

namespace Qy.Base.Extensions;

public static   class ApplicationContextExtensions
{
    public static AuthUser GetRequiredCurrentUser(this IApplicationContext applicationContext)
    {
       return (AuthUser)applicationContext.RequiredCurrentUser;
    }

}
