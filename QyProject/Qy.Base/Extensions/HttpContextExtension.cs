﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace Qy.Base.Extensions;

/// <summary>  
/// HttpContext扩展类  
/// </summary>  
public static partial class HttpContextExtension
{

    /// <summary>  
    /// 获取客户端 IP 地址  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>客户端 IP 地址</returns>  
    public static string GetClientUserIp(this HttpContext context)
    {
        ArgumentNullException.ThrowIfNull(context);

        var ipAddress = context.Request.Headers["X-Forwarded-For"].FirstOrDefault()
                        ?? context.Connection.RemoteIpAddress?.ToString()
                        ?? throw new InvalidOperationException("无法获取客户端 IP 地址");

        // 替换 localhost 和IPv6表达方式  
        if (ipAddress.Contains("::1") || ipAddress.StartsWith("::ffff:"))
        {
            ipAddress = "127.0.0.1";
        }

        ipAddress = ipAddress.Split(':').FirstOrDefault() ?? "127.0.0.1";
        return IsIP(ipAddress) ? ipAddress : "127.0.0.1";
    }

    /// <summary>  
    /// 检查字符串是否是有效的 IP 地址  
    /// </summary>  
    /// <param name="ip">IP 地址字符串</param>  
    /// <returns>如果是有效的 IP 地址，则为真；否则为假</returns>  
    public static bool IsIP(string ip)
    {
        return MyRegex().IsMatch(ip);
    }
    [GeneratedRegex(@"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$")]
    private static partial Regex MyRegex();
    /// <summary>  
    /// 获取系统用户 UID  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>系统用户 UID</returns>  
    public static int GetUid(this HttpContext context)
    {
        return GetClaimUid(context, ClaimTypes.NameIdentifier);
    }

    private static int GetClaimUid(HttpContext context, string claimType)
    {
        var uid = context.User.FindFirstValue(claimType);
        return !string.IsNullOrEmpty(uid) ? int.Parse(uid) : 0;
    }

    /// <summary>  
    /// 获取用户代理字符串  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>用户代理字符串</returns>  
    public static string? GetUserAgent(this HttpContext context)
    {
        return context?.Request.Headers.UserAgent;
    }

    /// <summary>  
    /// 获取当前控制器名  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>控制器名</returns>  
    public static string? GetController(this HttpContext context)
    {
        return context?.Request.RouteValues["Controller"]?.ToString();
    }
    /// <summary>  
    /// 获取当前操作名  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>操作名</returns>  
    public static string? GetAction(this HttpContext context)
    {
        return context?.Request.RouteValues["Action"]?.ToString();
    }
    public static string GetMethod(this HttpContext context)
    {
        return context?.Request.Method ?? "";
    }
    /// <summary>  
    /// 获取客户端请求令牌  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>请求令牌</returns>  
    public static string? GetToken(this HttpContext context)
    {
        return context?.Request.Headers.Authorization;
    }

    /// <summary>  
    /// 获取请求方法（GET、POST等）  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>请求方法</returns>  
    public static string? GetRequestMethod(this HttpContext context)
    {
        return context?.Request.Method;
    }

    /// <summary>  
    /// 获取 ClaimsIdentity 集合  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>ClaimsIdentity 集合</returns>  
    public static IEnumerable<ClaimsIdentity> GetClaims(this HttpContext context)
    {
        return context?.User?.Identities ?? [];
    }

    /// <summary>  
    /// 获取请求的 URL  
    /// </summary>  
    /// <param name="context">HTTP 上下文</param>  
    /// <returns>请求的 URL</returns>  
    public static string GetRequestUrl(this HttpContext context)
    {
        return context?.Request.Path.Value ?? string.Empty;
    }


}