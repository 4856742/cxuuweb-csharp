﻿using PetaPoco;
using Qy.Base.Entities.Auth;
using Qy.Base.Services.Auth.QyUser;
using System.Runtime.CompilerServices;

namespace Qy.Base.Extensions;


public static partial class UserExtension
{

    /// <summary>
    /// 不是超级管理员与全部数据权限
    /// </summary>
    /// <returns></returns>
    public static bool IsNotSuperAdminAndAllAuthData(this AuthUser? user)
    {
        return user != null && !user.IsSuperAdmin() && user.AccessLevel != AccessLevelType.FullPermissions;
    }
    public static bool IsSuperAdminAndAllAuthData(this AuthUser? user)
    {
        return user != null && (user.IsSuperAdmin() || user.AccessLevel == AccessLevelType.FullPermissions);
    }

    public static void GenQueryPermissionsSql(this AuthUser? user, Sql sql, string uidFieldName, string didFieldName)
    {
        if (user != null && user.IsNotSuperAdminAndAllAuthData())
        {
            if (user.Dids.Count < 1)
                sql.Where($"{uidFieldName} = @0 AND {didFieldName}=@1", user.UserId, user.DeptId);
            else
                sql.Where($"{didFieldName} IN (@0)", user.Dids.ToArray());
        }
    }
    /// <summary>
    /// 检测是否有要对比数据所有权的用户ID及部门ID的编辑权限
    /// GetCurrentUserAsync
    /// </summary>
    /// <param name="user">当前登录用户</param>
    /// <param name="uid">要对比数据所有权用户ID</param>
    /// <param name="did">要对比数据所有权部门ID</param>
    /// <returns></returns>
    public static bool IsHaveDataEditAuthAsync(this AuthUser? user, int uid, int did)
    {
        if (user == null)
            return false;
        //超级管理员及指定直接定义允许的//自己可以编辑自己的 //有指定部门权限并且是自己的
        return user.IsEditOthersData || user.UserId == uid || user.UserId == uid && user.Dids.Exists(x => x == did);
    }

}
