﻿namespace Qy.Base.Extensions;

/// <summary>  
/// 表示具有父级和子级集合的层次结构实体  
/// </summary>  
public interface IHierarchicalEntity
{
    int Pid { get; set; }
    int Id { get; set; }
    int Type { get; set; }
    string Name { get; set; }
}

/// <summary>  
/// 公共选项类型  
/// </summary>  
public class OptionItemDto
{
    public int Value { get; set; }
    public string Label { get; set; } = string.Empty;
    public int Type { get; set; }
    public int Sort { get; set; }
    public List<OptionItemDto> Children { get; set; } = [];
}

public static class CommonOptionTypeEntityExtensions
{
    public static IList<OptionItemDto> AsIndentedOptionType(this IEnumerable<IHierarchicalEntity> commonOptionTypeEntities, int parentId = 0)
    {
        try
        {
            if (commonOptionTypeEntities == null || !commonOptionTypeEntities.Any())
                return new List<OptionItemDto>();

            // 将实体预处理成字典，以提高查找效率
            var entityDict = commonOptionTypeEntities.ToDictionary(e => e.Id);

            IList<OptionItemDto> commonOptionTypes = new List<OptionItemDto>();
            IndentedOptionType(entityDict, commonOptionTypes, parentId);
            return commonOptionTypes;
        }
        catch (Exception ex)
        {
            // 记录异常日志
            Console.WriteLine($"Error in AsIndentedOptionType: {ex.Message}");
            throw;
        }
    }

    /// <summary>  
    /// 递归形成缩进的选项类型  
    /// </summary>  
    /// <param name="entityDict">所有选项类型实体的字典</param>  
    /// <param name="commonOptionTypes">生成的选项类型列表</param>  
    /// <param name="parentId">父ID</param>  
    private static void IndentedOptionType(Dictionary<int, IHierarchicalEntity> entityDict, IList<OptionItemDto> commonOptionTypes, int parentId = 0)
    {
        // 查找直接子项  
        var cates = entityDict.Values.Where(a => a.Pid == parentId).ToList();

        foreach (var item in cates)
        {
            // 创建树形模型  
            var treeModel = new OptionItemDto
            {
                Value = item.Id,
                Label = item.Name,
                Type = item.Type,
                Children = new List<OptionItemDto>() // 初始化 Children 属性
            };
            commonOptionTypes.Add(treeModel);

            // 递归查找孩子项  
            IndentedOptionType(entityDict, treeModel.Children, item.Id);
        }
    }
}