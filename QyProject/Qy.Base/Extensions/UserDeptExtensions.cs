﻿using Qy.Base.Entities.Auth;

namespace Qy.Base.Extensions;

public static partial class UserDeptExtensions
{
    /// <summary>
    /// 根据PID获取所有子级
    /// </summary>
    /// <param name="list"></param>
    /// <param name="pid"></param>
    /// <returns></returns>
    public static IEnumerable<UserDept> GetDeptSonListWithPid(this IEnumerable<UserDept> list, int pid)
    {
        IEnumerable<UserDept> query = list.Where(p => p.Pid == pid);
        return query.Concat(query.SelectMany(t => list.GetDeptSonListWithPid(t.Id)));
    }

    /// <summary>
    /// 获取指定部门的层级路径（包含所有上级部门）
    /// </summary>
    /// <param name="departments">部门集合</param>
    /// <param name="targetDepartmentId">目标部门ID</param>
    /// <param name="maxLevels">最大显示层级数（0表示显示所有层级）</param>
    /// <param name="separator">层级分隔符，默认为"/"</param>
    /// <returns>部门层级路径字符串</returns>
    /// <exception cref="ArgumentNullException">当部门集合为空时抛出</exception>
    public static string GetDepartmentHierarchyPath(this IEnumerable<UserDept> departments, int targetDepartmentId, int maxLevels = 0, string separator = "/")
    {
        ArgumentNullException.ThrowIfNull(departments);

        var departmentMap = departments.ToDictionary(d => d.Id);
        var hierarchyPath = new List<string>();

        var currentDepartment = departmentMap.GetValueOrDefault(targetDepartmentId);
        int levelCount = 0;

        while (currentDepartment != null && (maxLevels <= 0 || levelCount < maxLevels))
        {
            hierarchyPath.Insert(0, currentDepartment.Name);
            currentDepartment = departmentMap.GetValueOrDefault(currentDepartment.Pid);
            levelCount++;
        }

        return string.Join(separator, hierarchyPath);
    }
}
