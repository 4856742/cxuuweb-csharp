﻿namespace Qy.Base.Extensions;

/// <summary>  
/// 分类实体接口  
/// </summary>  
public interface ICategoryEntity
{
    string Name { get; set; }
    int Id { get; set; }
    int Pid { get; set; }
    int Type { get; set; }
}

/// <summary>  
/// 分类视图模型  
/// </summary>  
public class CategoryViewModel
{
    public int Id { get; set; }
    public int Pid { get; set; }
    public string? Name { get; set; }
    public int Type { get; set; }
    public int Depth { get; set; }
    public int ChildrenCount => Children.Count; // 只读属性  
    public bool IsLast { get; set; }
    public bool Spread { get; set; } = true;
    public int Sort { get; set; }
    public List<CategoryViewModel> Children { get; set; } = [];
}

public static class CategoryExtensions
{


    /// <summary>  
    /// 递归构建树形结构  
    /// </summary>  
    /// <param name="cates">原始分类视图模型</param>  
    /// <param name="treeModels">树形分类视图模型列表</param>  
    /// <param name="parentId">父分类ID</param>  
    /// <param name="depth">当前深度</param>  
    public static void IndentedContentCates(IEnumerable<CategoryViewModel> cates, IList<CategoryViewModel> treeModels, int parentId = 0, int depth = 0)
    {
        depth++;
        var childrens = cates.Where(a => a.Pid == parentId).ToList();

        for (int i = 0; i < childrens.Count; i++)
        {
            var child = childrens[i];
            child.Depth = depth;
            child.IsLast = i == childrens.Count - 1; // IsLast 只需检查是否是最后一个元素  
            treeModels.Add(child);
            IndentedContentCates(cates, child.Children, child.Id, depth);
        }
    }

    /// <summary>  
    /// 扩展分类模型  
    /// </summary>  
    /// <param name="rootCates">根分类集合</param>  
    /// <param name="expandModels">扩展后的模型集合</param>  
    public static void ExpandContentCates(IEnumerable<CategoryViewModel> rootCates, IList<CategoryViewModel> expandModels)
    {
        foreach (var item in rootCates)
        {
            expandModels.Add(item);
            ExpandContentCates(item.Children, expandModels);
        }
    }

    /// <summary>  
    /// 将分类实体转换为带符号的树形列表  
    /// </summary>  
    /// <param name="categoryEntities">分类实体集合</param>  
    /// <returns>带符号的树形分类视图模型集合</returns>  
    public static IEnumerable<CategoryViewModel> AsCateListItems(this IEnumerable<ICategoryEntity> categoryEntities)
    {
        var categories = categoryEntities.AsViewCategoryModels();
        var treeModels = new List<CategoryViewModel>();
        IndentedContentCates(categories, treeModels);

        var expandModels = new List<CategoryViewModel>();
        ExpandContentCates(treeModels, expandModels);

        foreach (var item in expandModels)
        {
            item.Name = FormatCategoryName(item);
        }

        return expandModels;
    }

    /// <summary>  
    /// 将分类实体转换为视图模型  
    /// </summary>  
    /// <param name="categoryEntities">分类实体集合</param>  
    /// <returns>视图模型集合</returns>  
    public static IEnumerable<CategoryViewModel> AsViewCategoryModels(this IEnumerable<ICategoryEntity> categoryEntities)
    {
        return categoryEntities.Select(entity => new CategoryViewModel
        {
            Id = entity.Id,
            Name = entity.Name,
            Pid = entity.Pid,
            Type = entity.Type
        });
    }

    /// <summary>  
    /// 格式化分类名称  
    /// </summary>  
    /// <param name="item">分类视图模型</param>  
    /// <returns>格式化后的分类名称</returns>  
    private static string FormatCategoryName(CategoryViewModel item)
    {
        if (item.Depth <= 1) return item.Name;

        string headString = new string(' ', 2 * (item.Depth - 2)); // 根据深度生成前缀空格  
        headString += item.ChildrenCount > 0 ? "├ " : item.IsLast ? "└ " : "├ ";
        return $"{headString}{item.Name}";
    }
}