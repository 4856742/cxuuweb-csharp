namespace Qy.Base.Extensions;

public static partial class DictExtensions
{

    public static bool CheckAsDictBool(this string str, Dictionary<bool, string> dictData)
    {
        foreach (var kvp in dictData)
        {
            if (str.Equals(kvp.Value, StringComparison.OrdinalIgnoreCase))
            {
                return kvp.Key;
            }
        }
        return false;
    }
    public static Dictionary<bool, string> BoolTypes
    {
        get
        {
            return new Dictionary<bool, string>
                    {
                        { true, "是" },
                        { false, "否" },
                    };
        }
    }
    public static int CheckAsDictString(this string str, Dictionary<int, string> dictData)
    {
        foreach (var kvp in dictData)
        {
            if (str.Equals(kvp.Value, StringComparison.OrdinalIgnoreCase))
            {
                return kvp.Key;
            }
        }
        return 0;
    }

    public static string CheckAsDictInt(this int intVal, Dictionary<int, string> dictData)
    {
        if (dictData.TryGetValue(intVal, out string? value))
        {
            return value;
        }
        return "δ֪";
    }

    public static string CheckAsDictIntWeek(this int intVal)
    {
        var week = new Dictionary<int, string>
                    {
                        { 1, "星期一" },
                        { 2, "星期二" },
                        { 3, "星期三" },
                        { 4, "星期四" },
                        { 5, "星期五" },
                        { 6, "星期六" },
                        { 0, "星期天" },
                    };
        if (week.TryGetValue(intVal, out string? value))
        {
            return value;
        }
        return "δ֪";
    }
}
