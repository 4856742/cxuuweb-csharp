﻿namespace Qy.Base.Extensions;

public static partial class Extensions
{
    public static bool IsEmpty(this object value)
    {
        if (value != null && !string.IsNullOrEmpty(value.ParseToString()))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public static bool IsNotEmpty(this object value)
    {
        return !value.IsEmpty();
    }
    public static bool IsNullOrZero(this object value)
    {
        if (value == null || value.ParseToString().Trim() == "0")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool IsNullAndZero(this object value)
    {
        return value == null || value.ParseToString().Trim() == "0";
    }
    public static bool IsNotNullAndZero(this object value)
    {
        return !value.IsNullAndZero();
    }



}
