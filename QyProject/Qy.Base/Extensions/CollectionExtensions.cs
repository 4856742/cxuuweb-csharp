﻿using Qy.Base.Entities.Auth;

namespace Qy.Base.Extensions;

public static class CollectionExtensions
{
    /// <summary>
    /// 获取两个集合的交集（根据多个属性）
    /// </summary>
    public static IEnumerable<T> IntersectByKeys<T, TKey>(
        this IEnumerable<T> first, IEnumerable<T> second, Func<T, TKey> keySelector)
    {
        return first.Join(second, keySelector, keySelector, (f, s) => f);
    }

    /// <summary>
    /// 获取两个集合的差集（first 相对于 second 的差集，根据多个属性）
    /// </summary>
    public static IEnumerable<T> ExceptByKeys<T, TKey>(
        this IEnumerable<T> first, IEnumerable<T> second, Func<T, TKey> keySelector)
    {
        var secondKeys = second.Select(keySelector).ToHashSet();
        return first.Where(f => !secondKeys.Contains(keySelector(f)));
    }

    /// <summary>
    /// 获取两个集合的并集（根据多个属性去重）
    /// </summary>
    public static IEnumerable<T> UnionByKeys<T, TKey>(
        this IEnumerable<T> first, IEnumerable<T> second, Func<T, TKey> keySelector)
    {
        return first.Concat(second).GroupBy(keySelector).Select(g => g.First());
    }
}


internal class CollectionExtensionsDemoC
{
    private static void Demo()
    {
        var list1 = new List<UserDept>
        {
            new UserDept { Id = 1, Name = "Alice", Sort = 25 },
            new UserDept { Id = 2, Name = "Bob", Sort = 30 },
            new UserDept { Id = 3, Name = "Charlie", Sort = 35 }
        };

        var list2 = new List<UserDept>
        {
            new UserDept { Id = 2, Name = "Bob", Sort = 30 },
            new UserDept { Id = 3, Name = "Charlie", Sort = 40 }, // 年龄不同
            new UserDept { Id = 4, Name = "David", Sort = 28 }
};

        // 交集：相同 Id 和 Name 且 Age 也相同
        var intersection = list1.IntersectByKeys(list2, p => (p.Id, p.Name, p.Sort));

        // 差集：在 list1 中但不在 list2 中的元素（基于 Id, Name, Age）
        var difference = list1.ExceptByKeys(list2, p => (p.Id, p.Name, p.Sort));

        // 并集：合并去重（基于 Id, Name, Age）
        var union = list1.UnionByKeys(list2, p => (p.Id, p.Name, p.Sort));

        Console.WriteLine("交集:");
        foreach (var item in intersection) Console.WriteLine(item);

        Console.WriteLine("\n差集:");
        foreach (var item in difference) Console.WriteLine(item);

        Console.WriteLine("\n并集:");
        foreach (var item in union) Console.WriteLine(item);
    }
}