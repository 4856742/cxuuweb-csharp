﻿using System.ComponentModel.DataAnnotations;

namespace Qy.Base.Attributes;

/// <summary>
/// 自定义操作日志记录，用于验证属性值是否为空或空字符串
/// </summary>
[AttributeUsage( AttributeTargets.Property)]
public class ValidateIfNotEmptyAttribute(int minLength, int maxLength) : ValidationAttribute
{

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        // 如果值为空或空字符串，则不验证
        string? strValue = Convert.ToString(value);
        if (string.IsNullOrWhiteSpace(strValue))
        {
            return ValidationResult.Success;
        }

        // 如果有值，则验证长度
        if (strValue.Length < minLength || strValue.Length > maxLength)
        {
            return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName} 的长度必须在 {minLength} 到 {maxLength} 之间。");
        }

        return ValidationResult.Success;
    }
}
