﻿namespace Qy.Base.Utilities.EPPlusExcel;

/// <summary>
/// 创建表格时的数据类型
/// </summary>
public enum GreatExcelDataType
{
    /// <summary>
    /// 数字，含所有运算数字
    /// </summary>
    Number = 0,
    /// <summary>
    /// 文字、字符型
    /// </summary>
    String = 1,
    /// <summary>
    /// 数据报送的 统计列
    /// </summary>
    StatData = 2,
    /// <summary>
    /// 整型
    /// </summary>
    Int32,
    /// <summary>
    /// 完整时间
    /// </summary>
    DateTime,
    /// <summary>
    /// 只显示年月日
    /// </summary>
    Date,
    /// <summary>
    /// 图片相应地址，用于导入导出图片
    /// </summary>
    ImgUrl,
    /// <summary>
    /// 未知类型，一般用于返回空
    /// </summary>
    Unknown
}


/// <summary>
/// 用于灵活定义表格字段导出
/// Int32 FromRow, Int32 FromCol, Int32 ToRow, Int32 ToCol)
/// </summary>
public class GreatExcelField
{
    // 唯一标识ID  
    public int Value { get; set; }
    // 列宽  
    public int Width { get; set; }
    // 列名  
    public string Label { get; set; } = string.Empty;
    // 对应列名的字段名  
    public string FieldName { get; set; } = string.Empty;
    // 当前列数据类型  
    public GreatExcelDataType Type { get; set; }

    /// <summary>
    /// 开始行，Epplus索引从1开始
    /// </summary>
    public int FirstRow { get; set; }
    /// <summary>
    /// 结束行
    /// </summary>
    public int LastRow { get; set; }
    /// <summary>
    /// 开始列
    /// </summary>
    public int FirstCol { get; set; }
    /// <summary>
    /// 结束列
    /// </summary>
    public int LastCol { get; set; }
    /// <summary>
    /// 用于计算是否有子级，生成复杂表头
    /// </summary>
    public bool HaveChild { get; set; }
}

public class OutPutExcelExportOptions
{
    /// <summary>
    /// 表格头字段集合
    /// </summary>
    public List<GreatExcelField> InitialHeaderList { get; set; } = [];
    /// <summary>
    /// 选择的表格头字段，为空不采用，输出全部
    /// </summary>
    public string FieldsJson { get; set; } = string.Empty;
    /// <summary>
    /// 表格标题
    /// </summary>
    public string TableTitle { get; set; } = "无标题";
    /// <summary>
    /// 是否启用合计行
    /// </summary>
    public bool IncludeSummaryRow { get; set; } = false;
    /// <summary>
    /// 表格标题起始行
    /// </summary>
    public int TitleRowIndex { get; set; } = 1;
    /// <summary>
    /// 表格头起始行
    /// </summary>
    public int HeaderRowIndex { get; set; } = 2;
    /// <summary>
    /// 填充数据起始行
    /// </summary>
    public int FirstDataRowIndex { get; set; } = 3;
    /// <summary>
    /// 合计行名称
    /// </summary>
    public string SummaryLabel { get; set; } = "合计";
}