﻿namespace Qy.Base.Utilities.EPPlusExcel;

/// <summary>
/// 修改
/// </summary>
public class ImportExcelResult
{

    /// <summary>
    /// 表1写入条数
    /// </summary>
    public int InsertCountA { get; set; } = 0;
    public int UpdateCountA { get; set; } = 0;
    /// <summary>
    /// 表2写入条数
    /// </summary>
    public int InsertCountB { get; set; } = 0;
    public int UpdateCountB { get; set; } = 0;

    /// <summary>
    /// 读取EXCEL耗时秒
    /// </summary>
    public double ReadSeconds { get; set; } = 0;
    /// <summary>
    /// 写入EXCEL耗时秒
    /// </summary>
    public double ImportSeconds { get; set; } = 0;
}