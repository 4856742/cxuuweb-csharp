﻿using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Qy.Base.Utilities.EPPlusExcel;


public class OutPutExcelHelper()
{
    /// <summary>  
    /// 处理表头  
    /// </summary>  
    public static List<GreatExcelField> FilterHeaderList(string fieldsJson, List<GreatExcelField> headerList)
    {
        if (string.IsNullOrWhiteSpace(fieldsJson)) return headerList;
        List<int> fieldValues;
        try
        {
            fieldValues = JsonConvert.DeserializeObject<List<int>>(fieldsJson) ?? [];
        }
        catch
        {
            fieldValues = [];
        }
        return headerList.Where(field => fieldValues.Contains(field.Value)).ToList();
    }
    /// <summary>
    /// 设置表格标题
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="title"></param>
    /// <param name="headerCount"></param>
    /// <param name="titleRowIndex"></param>
    public static void SetTableTitle(ExcelWorksheet worksheet, string title, int headerCount, int titleRowIndex)
    {
        worksheet.Cells[titleRowIndex, 1].Value = title;
        //合并提供给标题使用
        // [开始行，开始列，结束行，结束列]
        worksheet.Cells[titleRowIndex, 1, titleRowIndex, headerCount].Merge = true;
        worksheet.Cells[titleRowIndex, 1].Style.Font.Size = 28;
        worksheet.Cells[titleRowIndex, 1].Style.Font.Bold = true;
        worksheet.Cells[titleRowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
    }
    /// <summary>
    /// 生成普通表头
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="headerList"></param>
    /// <param name="headerRowIndex"></param>
    public static void SetTableHeaders(ExcelWorksheet worksheet, List<GreatExcelField> headerList, int headerRowIndex)
    {
        for (int i = 1; i <= headerList.Count; i++)
        {
            var headerField = headerList[i - 1];
            worksheet.Cells[headerRowIndex, i].Value = headerField.Label;
            worksheet.Cells[headerRowIndex, i].Style.WrapText = true;
            worksheet.Column(i).Width = headerField.Width;
            worksheet.Cells[headerRowIndex, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }
        worksheet.Row(headerRowIndex).Style.Font.Bold = true;
    }

    /// <summary>
    /// 生成复杂表头
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="headerList"></param>
    /// <param name="headerRowIndex"></param>
    public static void SetTableComplexHeaders(ExcelWorksheet worksheet, List<GreatExcelField> headerList, int headerRowIndex)
    {
        for (int i = 0; i < headerList.Count; i++)
        {
            var headerField = headerList[i];
            int rowIndex = headerField.FirstRow + headerRowIndex;
            int colIndex = headerField.FirstCol + 1;
            int toRowIndex = headerField.LastRow + headerRowIndex;
            int toColIndex = headerField.LastCol + 1;

            if (headerField.FirstRow != headerField.LastRow || headerField.FirstCol != headerField.LastCol)
            {
                worksheet.Cells[rowIndex, colIndex, toRowIndex, toColIndex].Merge = true;
            }
            var cell = worksheet.Cells[rowIndex, colIndex];
            cell.Value = headerField.Label;

            cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            //前端以像素定义宽度
            worksheet.Column(colIndex).Width = headerField.Width > 0 ? headerField.Width / 10 : 10;
            worksheet.Row(rowIndex).Style.Font.Bold = true;
            //所有边框对复杂表头不支持
            //cell.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
            cell.Style.WrapText = true;//自动换行
            //worksheet.Cells.Style.ShrinkToFit = true;//单元格自动适应大小
            worksheet.Row(rowIndex).CustomHeight = true;//自动调整行高
        }
    }
    /// <summary>
    /// 按数据类型设置填写表格值及样式
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="row"></param>
    /// <param name="column"></param>
    /// <param name="value"></param>
    /// <param name="fieldType"></param>
    /// <param name="firstDataRowIndex"></param>
    public static void SetCellValue(ExcelWorksheet worksheet, int row, int column, object value, GreatExcelDataType fieldType, int firstDataRowIndex)
    {
        switch (fieldType)
        {
            case GreatExcelDataType.Int32:
                worksheet.Cells[row, column].Value = int.TryParse(value.ToString(), out int num) ? num : default;
                break;
            case GreatExcelDataType.Number:
                worksheet.Cells[row, column].Value = double.TryParse(value.ToString(), out double res) ? res : default;
                break;
            case GreatExcelDataType.String:
                worksheet.Cells[row, column].Value = value?.ToString();
                break;
            case GreatExcelDataType.ImgUrl:
                InsertImage(worksheet, row, column, value?.ToString(), firstDataRowIndex);
                break;
            case GreatExcelDataType.Date:
                worksheet.Cells[row, column].Value = value;
                worksheet.Cells[row, column].Style.Numberformat.Format = "yyyy-mm-dd";
                break;
            case GreatExcelDataType.DateTime:
                worksheet.Cells[row, column].Value = value;
                worksheet.Cells[row, column].Style.Numberformat.Format = "yyyy-mm-dd HH:mm:ss";
                break;
            default:
                worksheet.Cells[row, column].Value = value?.ToString();
                break;
        }
        worksheet.Cells[row, column].Style.WrapText = true;
        //worksheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        worksheet.Cells[row, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
    }

    public static void InsertImage(ExcelWorksheet worksheet, int row, int column, string? imageUrl, int firstDataRowIndex)
    {
        if (string.IsNullOrEmpty(imageUrl)) return;

        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", imageUrl.TrimStart('/'));
        var imageFile = new FileInfo(path);
        if (imageFile.Exists)
        {
            var picture = worksheet.Drawings.AddPicture($"Image_{row}_{column}", imageFile);
            picture.SetPosition(row - firstDataRowIndex, 0, column - 1, 0);
            picture.SetSize(80, 100);
            worksheet.Row(row).Height = 80;
        }
    }

    /// <summary>
    /// 填充字典类数据
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="dataDictList"></param>
    /// <param name="headerList"></param>
    /// <param name="firstDataRowIndex"></param>
    public static void PopulateDictionayData(ExcelWorksheet worksheet, List<Dictionary<string, object>> dataDictList, List<GreatExcelField> headerList, int firstDataRowIndex)
    {
        for (int rowIndex = firstDataRowIndex; rowIndex < firstDataRowIndex + dataDictList.Count; rowIndex++)
        {
            var dataItem = dataDictList[rowIndex - firstDataRowIndex]; // 获取数据行  

            for (int colIndex = 1; colIndex <= headerList.Count; colIndex++)
            {
                var fieldName = headerList[colIndex - 1].FieldName; // 从 headerList 获取字段名
                if (dataItem.TryGetValue(fieldName, out object value)) // 检查字典中是否包含对应的键  
                {
                    SetCellValue(worksheet, rowIndex, colIndex, value, headerList[colIndex - 1].Type, firstDataRowIndex);
                }
            }
        }
    }
    /// <summary>
    /// 统计字典类数据
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="dataDictList"></param>
    /// <param name="headerList"></param>
    /// <param name="firstDataRowIndex"></param>
    /// <param name="summaryLabel"></param>
    public static void AddDictionaySummaryRow(ExcelWorksheet worksheet, List<Dictionary<string, object>> dataDictList, List<GreatExcelField> headerList, int firstDataRowIndex, string summaryLabel)
    {
        int summaryRowIndex = dataDictList.Count + firstDataRowIndex; // 合计行放在数据下方  
        worksheet.Cells[summaryRowIndex, 1].Value = summaryLabel; // 第一列显示“合计”  
        for (int colIndex = 2; colIndex <= headerList.Count; colIndex++)
        {
            string fieldName = headerList[colIndex - 1].FieldName; // 获取当前列的字段名称 
            if (headerList[colIndex - 1].Type == GreatExcelDataType.Int32 || headerList[colIndex - 1].Type == GreatExcelDataType.Number || headerList[colIndex - 1].Type == GreatExcelDataType.StatData)
            {
                int sum = dataDictList.Sum(item =>
                {
                    // 从字典中获取对应字段的值，并确保返回正确的整数值                   
                    return item.TryGetValue(fieldName, out object? value) && value != null && int.TryParse(value.ToString(), out int num) ? num : 0;// value?.ParseToInt() ?? 0;
                });
                worksheet.Cells[summaryRowIndex, colIndex].Value = sum; // 将合计值放入合计行  
            }
            else
            {
                worksheet.Cells[summaryRowIndex, colIndex].Value = ""; // 清空合计行中非数字列的值  
            }
        }
    }
}

