﻿using OfficeOpenXml;
using System.Reflection;
using PmSoft.Core.Extensions;

namespace Qy.Base.Utilities.EPPlusExcel;

public class OutPutExcelOfEntityHelper<T> where T : class, new()
{

    public static async Task OutPutExcel(List<T> dataList, OutPutExcelExportOptions options, MemoryStream stream)
    {
        var headerList = OutPutExcelHelper.FilterHeaderList(options.FieldsJson, options.InitialHeaderList);
        if (headerList == null || headerList.Count == 0) return;

        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        using var package = new ExcelPackage(stream);
        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("sheet1");

        OutPutExcelHelper.SetTableTitle(worksheet, options.TableTitle, headerList.Count, options.TitleRowIndex);
        OutPutExcelHelper.SetTableHeaders(worksheet, headerList, options.HeaderRowIndex);
        PopulateData(worksheet, dataList, headerList, options.FirstDataRowIndex);

        if (options.IncludeSummaryRow)
        {
            AddSummaryRow(worksheet, dataList, headerList, options.FirstDataRowIndex, options.SummaryLabel);
        }
        await package.SaveAsync(); // 保存 Excel 文件  
    }



    private static void PopulateData(ExcelWorksheet worksheet, List<T> dataList, List<GreatExcelField> headerList, int firstDataRowIndex)
    {
        for (int rowIndex = firstDataRowIndex; rowIndex < firstDataRowIndex + dataList.Count; rowIndex++)
        {
            for (int colIndex = 1; colIndex <= headerList.Count; colIndex++)
            {
                var property = typeof(T).GetProperty(headerList[colIndex - 1].FieldName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (property != null)
                {
                    var value = property.GetValue(dataList[rowIndex - firstDataRowIndex]);
                    OutPutExcelHelper.SetCellValue(worksheet, rowIndex, colIndex, value, headerList[colIndex - 1].Type, firstDataRowIndex);
                }
            }
        }
    }

    private static void AddSummaryRow(ExcelWorksheet worksheet, List<T> dataList, List<GreatExcelField> headerList, int firstDataRowIndex, string summaryLabel)
    {
        int summaryRowIndex = dataList.Count + firstDataRowIndex; // 合计行放在数据下方  
        worksheet.Cells[summaryRowIndex, 1].Value = summaryLabel; // 第一列显示“合计”  

        for (int colIndex = 2; colIndex <= headerList.Count; colIndex++)
        {
            if (headerList[colIndex - 1].Type == GreatExcelDataType.Int32)
            {
                int sum = dataList.Sum(item =>
                {
                    var property = typeof(T).GetProperty(headerList[colIndex - 1].FieldName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                    var value = property?.GetValue(item);
                    return value?.ConvertTo<int>() ?? 0; //确保返回的int值正确  
                });
                worksheet.Cells[summaryRowIndex, colIndex].Value = sum; // 将合计值放入合计行  
            }
            else
            {
                worksheet.Cells[summaryRowIndex, colIndex].Value = ""; // 清空合计行中非数字列的值  
            }
        }
    }
}

