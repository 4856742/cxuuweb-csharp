﻿namespace Qy.Base.AppBase;
public class DownloadFileMimeType
{


    /// <summary>
    /// Excel文件类型
    /// </summary>
    public const string FileMimeTypeExcel = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
}
