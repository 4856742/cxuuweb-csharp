﻿using OfficeOpenXml;
using System.Diagnostics;

namespace Qy.Base.Utilities.EPPlusExcel;

public class ReadExcelXlsxHelper
{
    /// <summary>  
    /// 使用委托读取和导入EXCEL表格  
    /// </summary>  
    /// <param name="uploadInfo">Excel文件路径</param>  
    /// <param name="importData">创建导入数据委托</param>  
    /// <returns>导入结果</returns>  
    public static async Task<ImportExcelResult> ReadAndImportExcelAsync(string filePath, Func<List<string>, Task<ImportExcelResult>> importData)
    {
        var importExcelResult = new ImportExcelResult();
        var importWatch = Stopwatch.StartNew();

        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        var fileInfo = new FileInfo(filePath);

        using (var package = new ExcelPackage(fileInfo))
        {
            var workbook = package.Workbook;

            // 遍历所有工作表  
            foreach (var worksheet in workbook.Worksheets)
            {
                int rowCount = worksheet.Dimension.Rows;
                int colCount = worksheet.Dimension.Columns;

                // 遍历每一行  
                for (int row = 1; row <= rowCount; row++)
                {
                    var rowData = new List<string>(colCount);
                    for (int col = 1; col <= colCount; col++)
                    {
                        rowData.Add(worksheet.Cells[row, col].Text);
                    }

                    // 使用传入的委托调用 ImportDataAsync  
                    var importRes = await importData(rowData);
                    UpdateImportResult(importExcelResult, importRes);
                }
            }

            importExcelResult.ReadSeconds = importWatch.Elapsed.TotalSeconds;
        }

        CleanupFile(filePath);
        importExcelResult.ImportSeconds = importWatch.Elapsed.TotalSeconds;
        return importExcelResult;
    }

    /// <summary>  
    /// 更新导入结果  
    /// </summary>  
    /// <param name="importExcelResult">导入结果</param>  
    /// <param name="importRes">当前导入结果</param>  
    private static void UpdateImportResult(ImportExcelResult importExcelResult, ImportExcelResult importRes)
    {
        importExcelResult.InsertCountA += importRes.InsertCountA;
        importExcelResult.UpdateCountA += importRes.UpdateCountA;
        importExcelResult.InsertCountB += importRes.InsertCountB;
        importExcelResult.UpdateCountB += importRes.UpdateCountB;
    }

    /// <summary>  
    /// 清理文件  
    /// </summary>  
    /// <param name="filePath">文件路径</param>  
    private static void CleanupFile(string filePath)
    {
        if (FileUtility.FileExists(filePath))
        {
            FileUtility.FileDel(filePath);
        }
    }
}