using Newtonsoft.Json;
using PmSoft.Core;
using PmSoft.Core.Extensions;
using Qy.Base.Extensions;
using System.Runtime.InteropServices;

namespace Qy.Base.Utilities;

public class ComputerHelper
{
    public static MemoryMetrics GetComputerInfo()
    {
        try
        {
            MemoryMetricsClient client = new();
            MemoryMetrics memoryMetrics = IsUnix() ? MemoryMetricsClient.GetUnixMetrics() : MemoryMetricsClient.GetWindowsMetrics();

            memoryMetrics.FreeRam = Math.Round(memoryMetrics.Free / 1024, 2) + "GB";
            memoryMetrics.UsedRam = Math.Round(memoryMetrics.Used / 1024, 2) + "GB";
            memoryMetrics.TotalRAM = Math.Round(memoryMetrics.Total / 1024, 2) + "GB";
            memoryMetrics.RAMRate = Math.Ceiling(100 * memoryMetrics.Used / memoryMetrics.Total).ToString() + "%";
            memoryMetrics.CPURate = Math.Ceiling(GetCPURate().ConvertTo<double>()) + "%";
            return memoryMetrics;
        }
        catch (Exception ex)
        {
            throw new PmSoftException("系统信息获取失败 msg=" + ex.Message + "," + ex.StackTrace);
        }
    }
    public static List<DiskInfo> GetDiskInfos()
    {
        List<DiskInfo> diskInfos = [];

        if (IsUnix())
        {
            try
            {
                string output = ShellHelper.Bash("df -m / | awk '{print $2,$3,$4,$5,$6}'");
                var arr = output.Split('\n', StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length == 0) return diskInfos;

                var rootDisk = arr[1].Split(' ', (char)StringSplitOptions.RemoveEmptyEntries);
                if (rootDisk == null || rootDisk.Length == 0)
                {
                    return diskInfos;
                }
                DiskInfo diskInfo = new()
                {
                    DiskName = "/",
                    TotalSize = long.Parse(rootDisk[0]) / 1024,
                    Used = long.Parse(rootDisk[1]) / 1024,
                    AvailableFreeSpace = long.Parse(rootDisk[2]) / 1024,
                    AvailablePercent = decimal.Parse(rootDisk[3].Replace("%", ""))
                };
                diskInfos.Add(diskInfo);
            }
            catch (Exception ex)
            {
                Console.WriteLine("磁盘信息获取失败" + ex.Message);
            }
        }
        else
        {
            var driv = DriveInfo.GetDrives();
            foreach (var item in driv)
            {
                try
                {
                    if (item.DriveType == DriveType.CDRom)
                    {
                        continue;
                    }
                    var obj = new DiskInfo()
                    {
                        DiskName = item.Name,
                        TypeName = item.DriveType.ToString(),
                        TotalSize = item.TotalSize / 1024 / 1024 / 1024,
                        AvailableFreeSpace = item.AvailableFreeSpace / 1024 / 1024 / 1024,
                    };
                    obj.Used = obj.TotalSize - obj.AvailableFreeSpace;
                    obj.AvailablePercent = decimal.Ceiling(obj.Used / (decimal)obj.TotalSize * 100);
                    diskInfos.Add(obj);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("磁盘信息获取失败" + ex.Message);
                    continue;
                }
            }
        }

        return diskInfos;
    }

    public static bool IsUnix()
    {
        var isUnix = RuntimeInformation.IsOSPlatform(OSPlatform.OSX) || RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
        return isUnix;
    }

    public static string GetCPURate()
    {
        try
        {
            string cpuRate = "";
            if (IsUnix())
            {
                string output = ShellHelper.Bash("top -b -n1 | grep \"Cpu(s)\" | awk '{print $2 + $4}'");
                cpuRate = output.Trim();
            }
            else
            {
                string output = ShellHelper.Cmd("powershell", "Get-WmiObject Win32_Processor | Select-Object LoadPercentage");

                var lines = output.Trim().Split('\n', (char)StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length > 2)
                {
                    cpuRate = lines[2].Trim();
                }

            }
            return cpuRate;
        }
        catch
        {
            throw new PmSoftException("Cpu 获取失败");
        }

    }

    public static string GetRunTime()
    {
        string runTime = string.Empty;
        try
        {
            if (IsUnix())
            {
                string output = ShellHelper.Bash("uptime -s").Trim();
                runTime = DateUtility.FormatTime(Convert.ToInt64((DateTime.Now - output.ParseToDateTime()).TotalMilliseconds.ToString().Split('.')[0]));
            }
            else
            {
                string output = ShellHelper.Cmd("powershell", "Get-WmiObject Win32_OperatingSystem | Select-Object LastBootUpTime");
                var lines = output.Trim().Split('\n', (char)StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length > 2)
                {
                    runTime = DateUtility.FormatTime(Convert.ToInt64((DateTime.Now - lines[1].Split('.')[0].ParseToDateTime()).TotalMilliseconds.ToString().Split('.')[0]));
                }
            }
        }
        catch (Exception ex)
        {
            throw new PmSoftException("运行时间获取失败" + ex.Message);
        }
        return runTime;
    }
}


public class MemoryMetrics
{
    [JsonIgnore]
    public double Total { get; set; }
    [JsonIgnore]
    public double Used { get; set; }
    [JsonIgnore]
    public double Free { get; set; }

    public string UsedRam { get; set; } = string.Empty;
    public string CPURate { get; set; } = string.Empty;
    public string TotalRAM { get; set; } = string.Empty;
    public string RAMRate { get; set; } = string.Empty;
    public string FreeRam { get; set; } = string.Empty;
}

public class DiskInfo
{
    public string DiskName { get; set; } = string.Empty;
    public string TypeName { get; set; } = string.Empty;
    public long TotalFree { get; set; }
    public long TotalSize { get; set; }
    public long Used { get; set; }
    public long AvailableFreeSpace { get; set; }
    public decimal AvailablePercent { get; set; }
}

public class MemoryMetricsClient
{
    public static MemoryMetrics GetWindowsMetrics()
    {
        try
        {
            string output = ShellHelper.Cmd("powershell", "Get-WmiObject Win32_OperatingSystem | Select-Object FreePhysicalMemory,TotalVisibleMemorySize");
            var metrics = new MemoryMetrics();
            var lines = output.Trim().Split('\n', (char)StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length < 3) return metrics;
            string[] result = lines[2].Split([' '], StringSplitOptions.RemoveEmptyEntries);

            metrics.Total = Math.Round(double.Parse(result[1]) / 1024, 0);
            metrics.Free = Math.Round(double.Parse(result[0]) / 1024, 0);//m
            metrics.Used = metrics.Total - metrics.Free;

            return metrics;
        }
        catch
        {
            throw new PmSoftException("内存信息获取失败");
        }

    }

    public static MemoryMetrics GetUnixMetrics()
    {
        string output = ShellHelper.Bash("free -m | awk '{print $2,$3,$4,$5,$6}'");
        var metrics = new MemoryMetrics();
        var lines = output.Split('\n', (char)StringSplitOptions.RemoveEmptyEntries);

        if (lines.Length <= 0) return metrics;

        if (lines != null && lines.Length > 0)
        {
            var memory = lines[1].Split(' ', (char)StringSplitOptions.RemoveEmptyEntries);
            if (memory.Length >= 3)
            {
                metrics.Total = double.Parse(memory[0]);
                metrics.Used = double.Parse(memory[1]);
                metrics.Free = double.Parse(memory[2]);//m
            }
        }
        return metrics;
    }
}