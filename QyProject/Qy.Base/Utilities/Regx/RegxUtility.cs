﻿using Qy.Base.Utilities.Const;
using System.Text.RegularExpressions;

namespace Qy.Base.Utilities;

public static class RegxUtility
{
    //验证Email地址 
    public static bool IsValidEmail(string strIn)
    {
        string pattern = RegxPatterns.Email;
        return Regex.IsMatch(strIn, pattern);
    }

    //验证是否为小数 
    public static bool IsValidDecimal(string strIn)
    {
        string pattern = @"^[0]{0,1}[.]{1}[0-9]{1,}$";
        return Regex.IsMatch(strIn, pattern);
    }
    //验证两位小数
    public static bool Is2Decimal(string str_decimal)
    {
        string pattern = @"^[0-9]*[.]{1}[0-9]{2}$";
        return Regex.IsMatch(str_decimal, pattern);
    }
    //验证一年的12个月
    public static bool IsMonth(string str_Month)
    {
        string pattern = @"^(0?[[1-9]|1[0-2])$";
        return Regex.IsMatch(str_Month, pattern);
    }

    //验证一个月的31天
    public static bool IsDay(string str_day)
    {
        string pattern = @"^((0?[1-9])|((1|2)[0-9])|30|31)$";
        return Regex.IsMatch(str_day, pattern);
    }

    //验证是否为电话号码 座机
    public static bool IsValidTel(string strIn)
    {
        string pattern = @"(/d+-)?(/d{4}-?/d{7}|/d{3}-?/d{8}|^/d{7,8})(-/d+)?";
        return Regex.IsMatch(strIn, pattern);
    }

    //验证年月日 
    public static bool IsValidDate(string strIn)
    {
        string pattern = @"^2/d{3}-(?:0?[1-9]|1[0-2])-(?:0?[1-9]|[1-2]/d|3[0-1])(?:0?[1-9]|1/d|2[0-3]):(?:0?[1-9]|[1-5]/d):(?:0?[1-9]|[1-5]/d)$";
        return Regex.IsMatch(strIn, pattern);
    }

    //验证后缀名 图片
    public static bool IsValidPostfix(string strIn)
    {
        string pattern = @"/.(?i:gif|jpg|jpeg|png)$";
        return Regex.IsMatch(strIn, pattern);
    }

    //验证字符是否在4至12之间 
    public static bool IsValidByte(string strIn)
    {
        string pattern = @"^[a-z]{4,12}$";
        return Regex.IsMatch(strIn, pattern);
    }

    //验证IP 
    public static bool IsValidIp(string strIn)
    {
        string pattern = RegxPatterns.Ip;
        return Regex.IsMatch(strIn, pattern);
    }
    // 验证输入汉字  
    public static bool IsChinese(string str_chinese)
    {
        string pattern = RegxPatterns.Chs;
        return Regex.IsMatch(str_chinese, pattern);
    }

    //验证输入字符串 (至少8个字符)
    public static bool IsLength(string str_Length)
    {
        string pattern = @"^.{8,}$";
        return Regex.IsMatch(str_Length, pattern);
    }

    //验证数字输入
    public static bool IsNumber(string str_number)
    {
        string pattern = RegxPatterns.Number;
        return Regex.IsMatch(str_number, pattern);
    }

    //验证整数
    public static bool IsInteger(string str_number)
    {
        string pattern = @"^[-,+]?[0-9]+$";
        return Regex.IsMatch(str_number, pattern);
    }

    //验证手机
    public static bool IsCellphoneNum(string str_number)
    {
        string pattern = RegxPatterns.Mobile;
        return Regex.IsMatch(str_number, pattern);
    }
    //  验证密码长度 (6-18位)
    public static bool IsPasswLength(string str_Length)
    {
        string pattern = @"^/d{6,18}$";
        return Regex.IsMatch(str_Length, pattern);
    }
    /// <summary>
    /// 简单验证字符串是否为中国的一代或二代身份证号码
    /// </summary>
    /// <param name="idCard"></param>
    /// <returns></returns>
    public static bool IsValidIdCard(string idCard)
    {
        // 正则表达式匹配  
        string pattern = @"^(?:\d{15}|\d{17}[\dX])$";
        return Regex.IsMatch(idCard, pattern);
    }
}