﻿using SkiaSharp;

namespace Qy.Base.Utilities;

public static class ImgUtility
{
    /// <summary>
    /// 压缩图片
    /// </summary>
    /// <param name="source">原文件位置</param>
    /// <param name="maxWidth">最大宽度，根据此宽度计算是否需要缩放，计算新高度</param>
    /// <param name="quality">图片质量，范围0-100</param>
    public static void Compress(string source, int maxWidth, int quality)
    {
        using var file = File.OpenRead(source);
        using var fileStream = new SKManagedStream(file);
        using var bitmap = SKBitmap.Decode(fileStream);
        var width = (decimal)bitmap.Width;//只能用 decimal 类型才能成功压缩图片
        var height = (decimal)bitmap.Height;
        var newWidth = width;
        var newHeight = height;
        if (width > maxWidth)
        {
            newWidth = maxWidth;
            newHeight = height / width * maxWidth;
        }
        using var resized = bitmap.Resize(new SKImageInfo((int)newWidth, (int)newHeight), SKSamplingOptions.Default);
        if (resized != null)
        {
            var image = SKImage.FromBitmap(resized);
            file.Close();
            File.Delete(source);
            using var writeStream = File.OpenWrite(source);
            image.Encode(SKEncodedImageFormat.Jpeg, quality).SaveTo(writeStream);
        }
    }

    /// <summary>
    /// 压缩并裁切图片
    /// </summary>
    /// <param name="source">原文件位置</param>
    /// <param name="target">生成目标文件位置</param>
    /// <param name="maxWidth">最大宽度，根据此宽度计算是否需要缩放，计算新高度</param>
    /// <param name="top">顶部裁切高度，单位px</param>
    /// <param name="bottom">底部裁切高度，单位px</param>
    /// <param name="quality">图片质量，范围0-100</param>
    public static void CompressAndCut(string source, string target, decimal maxWidth, int top, int bottom, int quality)
    {
        using var file = File.OpenRead(source);
        using var fileStream = new SKManagedStream(file);
        using var bitmap = SKBitmap.Decode(fileStream);
        var width = (decimal)bitmap.Width;
        var height = (decimal)bitmap.Height;
        var newWidth = width;
        var newHeight = height;
        if (width > maxWidth)
        {
            newWidth = maxWidth;
            newHeight = height / width * maxWidth;
        }
        using var resized = bitmap.Resize(new SKImageInfo((int)newWidth, (int)newHeight), SKSamplingOptions.Default);//-------------------------------------------
        if (resized != null)
        {
            using var surface = SKSurface.Create(new SKImageInfo((int)newWidth, (int)newHeight - top - bottom));
            var canvas = surface.Canvas;
            canvas.DrawBitmap(resized, 0, 0 - top);
            using var image = surface.Snapshot();
            using var writeStream = File.OpenWrite(target);
            image.Encode(SKEncodedImageFormat.Jpeg, quality).SaveTo(writeStream);
        }
    }


    //public static SKBitmap Decode(SKBitmap bitmap)
    //{
    //    var rootPath = Directory.GetCurrentDirectory(); // 获取站点根目录
    //    using var surface = SKSurface.Create(new SKImageInfo(100, 80));
    //    var canvas = surface.Canvas;
    //    canvas.Clear(SKColors.Blue);
    //    var paint = new SKPaint()
    //    {
    //        Color = SKColors.Red,
    //        IsAntialias = true, // 抗锯齿
    //        Style = SKPaintStyle.Fill,
    //        TextAlign = SKTextAlign.Center,
    //        TextSize = 16f,
    //        Typeface = SKTypeface.FromFamilyName("Microsoft YaHei", SKFontStyle.Bold)
    //    };
    //    var coord = new SKPoint(50, 48);
    //    canvas.DrawText("my text", coord, paint);
    //    using var image = surface.Snapshot();
    //    using var writeStream = Attment.OpenWrite(rootPath + "\\code-img.png");
    //    image.Encode(SKEncodedImageFormat.Png, 80).SaveTo(writeStream);
    //}
}
