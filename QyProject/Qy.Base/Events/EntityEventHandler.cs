﻿using PmSoft.Core.EventBus;
using PmSoft.Data.Abstractions.Events;
using Qy.Base.AppBase;
using Qy.Base.InterFace;
using Qy.Base.Utilities;

namespace Qy.Base.Events;

internal class EntityAddedEventHandler(ICommonAuditableService commonLogService) : IAsyncEventHandler<EntityAddedEventArgs>
{
    public Task HandleAsync(EntityAddedEventArgs eventArgs)
    {
        if (eventArgs.EntityId != null && eventArgs.AuditableAttribute.GroupName != null)
        {
            string doThing = $"添加 {eventArgs.EntityDescription}({eventArgs.TableName}) ID:{eventArgs.EntityId}";
            commonLogService.InsertLog(doThing, eventArgs.DisplayUrl ?? "", (ProjectType)eventArgs.AuditableAttribute.GroupName);
        }
        //_logger.LogDebug($"添加了实体 【{descriptionAttribute.Description}】 主键{id} ");
        return Task.CompletedTask;
    }
}

internal class EntityUpdatedEventHandler(ICommonAuditableService commonLogService) : IAsyncEventHandler<EntityUpdatedEventArgs>
{
    public Task HandleAsync(EntityUpdatedEventArgs eventArgs)
    {
        string changedProperties = string.Join("; ", eventArgs.ChangedProperties.Select(m =>
        {
            string oldValue = m.OldValue == null ? "" : StringUtility.Trim(m.OldValue.ToString() ?? "", 20);
            string newValue = m.NewValue == null ? "" : StringUtility.Trim(m.NewValue.ToString() ?? "", 20);
            return $"{m.PropertyName}: '{oldValue} ~ {newValue}'";
        }));
        if (!string.IsNullOrWhiteSpace(changedProperties) && eventArgs.AuditableAttribute.GroupName != null)
        {
            string doThing = $"修改 {eventArgs.EntityDescription}({eventArgs.TableName}) ID:{eventArgs.EntityId} {changedProperties}";
            commonLogService.InsertLog(doThing, eventArgs.DisplayUrl ?? "", (ProjectType)eventArgs.AuditableAttribute.GroupName);
        }
        //logger.LogDebug($"EntityUpdatedEvent 【{eventArgs.OldEntity.GetType().Name}】 {changedProperties}");
        return Task.CompletedTask;
    }
}

internal class EntityDeletedEventHandler(ICommonAuditableService commonLogService) : IAsyncEventHandler<EntityDeletedEventArgs>
{
    public Task HandleAsync(EntityDeletedEventArgs eventArgs)
    {
        if (eventArgs.EntityId != null && eventArgs.AuditableAttribute.GroupName != null)
        {
            string doThing = $"删除 {eventArgs.EntityDescription}({eventArgs.TableName}) ID:{eventArgs.EntityId}";
            commonLogService.InsertLog(doThing, eventArgs.DisplayUrl ?? "", (ProjectType)eventArgs.AuditableAttribute.GroupName);
        }
        //logger.LogDebug($"删除了实体 【{descriptionAttribute.Description}】 主键{id} ");
        return Task.CompletedTask;
    }
}

