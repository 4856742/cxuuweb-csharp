﻿using Microsoft.Extensions.Logging;
using PmSoft.Core.EventBus;

namespace Qy.Base.Events;

public class UserCreatedEventHandler
    (ILogger<UserCreatedEventHandler> logger
    //, UserService userService
    )
    : IAsyncEventHandler<UserCreatedEventArgs>
{
    public async Task HandleAsync(UserCreatedEventArgs args)
    {
        logger.LogDebug("Delay");
        //var user = await userService.GetUserAsync(100);
       // var user = await userService.GetUserAsync(100);
        await Task.Delay(new Random().Next(8000, 20000));
        logger.LogDebug("User created");
    }
}