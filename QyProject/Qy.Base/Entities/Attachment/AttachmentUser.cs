﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Attachment;

/// <summary>
/// 用户附件表 不记录审计
///</summary>
[Table(DbTableName.QyAttachmentUser)]
[CacheSetting(true)]
[Description("用户附件表")]
//[Auditable(ProjectType.Sys)]
public class AttachmentUser : IEntity<int>
{
	[PrimaryKey("id", IsIdentity = true)]
	public int Id { get; set; }
	public int Uid { get; set; }
	public int Did { get; set; }
	[Column("attachment_id")]
	public string AttachmentId { get; set; }

    #region IEntity
    [NotMapped]
	object IEntity.Id => Id; 
	#endregion
}
