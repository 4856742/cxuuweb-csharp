﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Attachment;

/// <summary>
/// 附件信息
///</summary>
[Table(DbTableName.QyAttachment)]
[CacheSetting(true)]
[Description("附件信息")]
//[Auditable(ProjectType.Sys)]
public class SysAttachment : IDelEntity<string>
{
	/// <summary>
	/// 附件ID
	/// </summary>
	[PrimaryKey("id", IsIdentity = false)]
	public string Id { get; set; }
	/// <summary>
	/// 租户类型 
	///</summary>
	[Column("tenant_type")]
	public string TenantType { get; set; }
	/// <summary>
	/// 租户ID，与业务实体关联 
	///</summary>
	[Column("tenant_id")]
	public string? TenantId { get; set; }
	/// <summary>
	/// 文件名，guid加原始扩展名 
	///</summary>
	[Column("file_name")]
	public string FileName { get; set; }
	/// <summary>
	/// 存储桶名称，用于区分存储空间 
	///</summary>
	[Column("bucket_name")]
	public string BucketName { get; set; }
	/// <summary>
	/// 对象名称，包含文件路径
	///</summary>
	[Column("object_name")]
	public string ObjectName { get; set; }
	/// <summary>
	/// 用户自定义的友好名称，可选 
	///</summary>
	[Column("friendly_name")]
	public string FriendlyName { get; set; }
	/// <summary>
	/// 媒体类型，使用枚举定义，例如 Image, Document 
	///</summary>
	[Column("media_type")]
	public MediaType MediaType { get; set; }
	/// <summary>
	/// MIME 类型，例如 application/pdf 
	///</summary>
	[Column("mime_type")]
	public string MimeType { get; set; }
	/// <summary>
	/// 文件大小，单位：字节 
	///</summary>
	[Column("file_size")]
	public long FileSize { get; set; }
	/// <summary>
	/// 上传者的 IP 地址 
	///</summary>
	[Column("uploader_ip")]
	public string UploaderIp { get; set; }
	/// <summary>
	/// 附件描述 
	///</summary>
	[Column("description")]
	public string Description { get; set; }
	/// <summary>
	/// 是否已删除，默认为 false 
	/// 默认值: b'0'
	///</summary>
	[Column("is_deleted")]
	public bool IsDeleted { get; set; } = false;
	/// <summary>
	/// 删除签名
	/// </summary>
	[Column("delete_token")]
	public string? DeleteToken { get; set; }
	/// <summary>
	/// 是否为临时附件，默认为 true 
	/// 默认值: b'1'
	///</summary>
	[Column("is_temporary")]
	public bool IsTemporary { get; set; } = true;
	/// <summary>
	/// 上传时间 
	///</summary>
	[Column("create_time")]
	public DateTime CreateTime { get; set; } = DateTime.Now;

    #region IEntity
    [NotMapped]
	object IEntity.Id => Id; 
	#endregion
}
