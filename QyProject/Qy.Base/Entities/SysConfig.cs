﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities;

[Table(DbTableName.QySysConfig)]
[CacheSetting(true)]
[Description("系统配置")]
[Auditable(ProjectType.Sys)]
public class SysConfig : IEntity<string>
{
    /// <summary>
    /// 配置键
    /// </summary>
    [PrimaryKey("config_key", IsIdentity = false)]
    public string Id { get; set; }
    /// <summary>
    /// 配置值
    ///</summary>
    [Column("config_value")]
    public string ConfigValue { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
