﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Notice;

[Table(DbTableName.QyNoticeKey)]
[CacheSetting(true, PropertyNamesOfArea = "get_uid")]
[Description("系统消息Key")]
[Auditable(ProjectType.Sys)]
public class NoticeKey : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Nid { get; set; }
    [Column("get_uid")]
    public int GetUid { get; set; }
    [Column("post_uid")]
    public int PostUid { get; set; }
    public bool Status { get; set; }
    [Column("read_time")]
    public DateTime ReadTime { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion

}