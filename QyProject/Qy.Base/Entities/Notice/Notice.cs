﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Notice;

[Table(DbTableName.QyNotice)]
[CacheSetting(true)]
[Description("系统消息主表")]
[Auditable(ProjectType.Sys)]
public class Notice : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public string Title { get; set; }
    /// <summary>
    /// 通知类型  0 系统  1 用户
    /// </summary>
    public int Type { get; set; }
    public string Content { get; set; }
    [Column("post_uid")]
    public int PostUid { get; set; }
    public string Attments { get; set; }
    [Column("post_time")]
    public DateTime PostTime { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
