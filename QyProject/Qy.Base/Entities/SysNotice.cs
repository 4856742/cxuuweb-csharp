﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities;

[Table(DbTableName.QySysNotice)]
[CacheSetting(true)]
[Description("系统公告")]
[Auditable(ProjectType.Sys)]
public class SysNotice : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public string Title { get; set; }
    public bool Status { get; set; }
    public string Content { get; set; }
    public int Uid { get; set; }
    public string Attments { get; set; }
    [Column("great_time")]
    public DateTime GreatTime { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
