﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.QyMenu;

[Table(DbTableName.QyMenuCat)]
[CacheSetting(true)]
[Description("平台菜单类别")]
public class MenuCat : Entity<int>, IHierarchicalEntity
{
    [PrimaryKey("id")]
    public override int Id { get; set; }
    [Display(Name = "名称")]
    [Required(ErrorMessage = "请填分类名称")]
    public string Name { get; set; }
    public string Remark { get; set; }
    public int Pid { get; set; }
    public string Icon { get; set; }
    [Column("client_id")]
    public string ClientId { get; set; }
    [Column("client_url")]
    public string ClientUrl { get; set; }
    [Column("client_secret")]
    public string ClientSecret { get; set; }
    public int Type { get; set; }
    public int Sort { get; set; }
 
}
