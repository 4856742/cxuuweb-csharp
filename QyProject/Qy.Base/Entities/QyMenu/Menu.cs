﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Qy.Base.Extensions;
using PmSoft.Core.Domain.Auth;

namespace Qy.Base.Entities.QyMenu;

[Table(DbTableName.QyMenu)]
[Description("平台菜单")]
[CacheSetting(true, PropertyNamesOfArea = "cid,type")]
[Auditable(ProjectType.Sys)]
public class Menu : IHierarchicalEntity, IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Cid { get; set; }
    public int Pid { get; set; }
    public int Sort { get; set; }
    public int Type { get; set; }
    public string Name { get; set; } 
    [Column("path")]
    public string Path { get; set; } 
    [Column("component")]
    public string Component { get; set; }
    [Column("perm_code")]
    public string PermCode { get; set; } 
    public string Icon { get; set; } 
    public bool Status { get; set; }
    [Column("keep_alive")]
    public bool KeepAlive { get; set; }
    [Column("always_show")]
    public bool AlwaysShow { get; set; }
    public string Redirect { get; set; } 
    public bool Hide { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion

}

