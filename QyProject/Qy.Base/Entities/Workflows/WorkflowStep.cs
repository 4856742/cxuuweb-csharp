﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Workflows;

[Table(DbTableName.QyWorkflowStep)]
[Description("业务流程进度表")]
[CacheSetting(true, PropertyNamesOfArea = "WorkflowId")]
[Auditable(ProjectType.Sys)]

public class WorkflowStep : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Pid { get; set; }
    public string Type { get; set; }
    [Column("workflow_id")]
    public int WorkflowId { get; set; } // 关联 Workflow
    public string Name { get; set; }
    [Column("step_order")]
    public int StepOrder { get; set; } // 步骤顺序
    public string Remark { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public int Mode { get; set; }
    [Column("rule_type")]
    public int RuleType { get; set; }
    [Column("task_mode")]
    public string TaskMode { get; set; }
    [Column("need_sign")]
    public bool NeedSign { get; set; }
    [Column("assign_user")]
    public string AssignUser { get; set; }
    [Column("select_multiple")]
    public bool SelectMultiple { get; set; }
    public string Leader { get; set; }
    [Column("assign_dept")]
    public string AssignDept { get; set; }
    [Column("assign_dept_top")]
    public string AssignDeptTop { get; set; }
    [Column("assign_role")]
    public string AssignRole { get; set; }
    [Column("no_user_handler")]
    public string NoUserHandler { get; set; }
    [Column("same_root")]
    public string SameRoot { get; set; }
    public string Timeout { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
