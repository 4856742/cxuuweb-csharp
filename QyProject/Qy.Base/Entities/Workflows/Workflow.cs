﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Workflows;

[Table(DbTableName.QyWorkflow)]
[Description("业务流程主表")]
[CacheSetting(true)]
[Auditable(ProjectType.Sys)]
public class Workflow :IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    [Required(ErrorMessageResourceName = "RequiredField")]
    [Display(Name = "类别")]
    [Range(0, int.MaxValue,ErrorMessageResourceName = "InvalidNumber")]
    public int Cid { get; set; }
    public string Name { get; set; }
    public string Remark { get; set; }
    [Column("created_time")]
    public DateTime CreatedTime { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    // 是否启用
    [Column("is_active")]
    public bool IsActive { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
