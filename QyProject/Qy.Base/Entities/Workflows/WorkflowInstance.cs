﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Workflows;

[Table(DbTableName.QyWorkflowInstance)]
[Description("业务流程实例")]
[CacheSetting(true)]
[Auditable(ProjectType.Sys)]

public class WorkflowInstance : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    [Column("workflow_id")]
    public int WorkflowId { get; set; } // 关联 Workflow
    [Column("step_id")]
    public int StepId { get; set; } // 当前步骤序号   
    [Column("module_type")]
    public ProjectType ModuleType { get; set; } // 关联业务模型    
    [Column("business_id")]
    public int BusinessId { get; set; }// 关联业务数据（例如：订单ID、请假单ID）
    [Column("workflow_status")]
    public WorkflowStatus WorkflowStatus { get; set; } // 流程状态（进行中、完成、拒绝等）
    [Column("start_time")]
    public DateTime StartTime { get; set; }
    [Column("end_time")]
    public DateTime? EndTime { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
