﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Workflows;

[Table(DbTableName.QyWorkflowRecord)]
[Description("业务流程进度记录")]
[CacheSetting(true)]
//[Auditable(ProjectType.Sys)]

public class WorkflowRecord : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    [Column("instance_id")]
    public int InstanceId { get; set; }
    [Column("step_id")]
    public int StepId { get; set; }
    [Column("approve_time")]
    public DateTime ApproveTime { get; set; }
    public string Comment { get; set; } // 审批意见
    public WorkflowRecordStatus Status { get; set; } // 步骤状态
    public int Uid { get; set; }
    public int Did { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
