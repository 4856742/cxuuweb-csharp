﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Loging;

/// <summary>
/// 登录日志表
///</summary>
[Table(DbTableName.QyLogLogin)]
[CacheSetting(true)]
[Description("系统用户")]
public class LogLogin : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    /// <summary>
    /// 用户ID 
    /// 默认值: NULL
    ///</summary>
    [Column("uid")]
    public int Uid { get; set; }
    public int Did { get; set; }
    /// <summary>
    /// 登录IP 
    /// 默认值: NULL
    ///</summary>
    [Column("ip")]
    public string Ip { get; set; }
    [Column("user_agent")]
    public string UserAgent { get; set; }
    /// <summary>
    /// 时间 
    /// 默认值: NULL
    ///</summary>
    [Column("time")]
    public DateTime Time { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion

}

