﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Loging;

[Table(DbTableName.QyLogSql)]
[CacheSetting(true)]
[Description("系统操作日志")]
public class LogSys : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    [Column("contr_act")]
    public string ContrAct { get; set; }
    public string Method { get; set; }
    public string Ip { get; set; }
    public DateTime Time { get; set; }
    [Column("do_thing")]
    public string DoThing { get; set; }
    public bool Status { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion

}


