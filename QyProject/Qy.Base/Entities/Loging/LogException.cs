﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Loging;

[Table(DbTableName.QyLogException)]
[CacheSetting(true)]
[Description("系统异常日志")]
public class LogException : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    [Column("contr_act")]
    public string ContrAct { get; set; }
    public string Method { get; set; }
    public string Ip { get; set; }
    public DateTime Time { get; set; }
    [Column("stack_trace")]
    public string StackTrace { get; set; }
    public string Message { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}

