﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;

namespace Qy.Base.Entities.Auth;

/// <summary>
/// 角色信息
///</summary>
[Table(DbTableName.QyUserRole)]
[CacheSetting(true)]
[Description("角色信息")]
[Auditable(ProjectType.Sys)]
public class UserRole : Entity<int>
{
	/// <summary>
	/// 角色ID
	/// </summary>
	[PrimaryKey("id")]
	public override int Id { get; set; }
	/// <summary>
	/// 角色名称 
	///</summary>
	[Column("name")]
	public string Name { get; set; }
	/// <summary>
	/// 是否为系统保留 
	///</summary>
	[Column("is_keep")]
	public bool? IsKeep { get; set; }
    [Column("is_edit_others_data")]
	public bool? IsEditOthersData { get; set; }
	/// <summary>
	/// 备注
	/// </summary>
    public string Remark { get; set; }
	[Column("auth_dept_ids")]
    public string AuthDeptIds { get; set; }
	/// <summary>
	/// 排序
	/// </summary>
    public int Sort { get; set; }
	[Column("access_level")]
    public AccessLevelType AccessLevel { get; set; }
    /// <summary>
    /// 创建时间 
    ///</summary>
    [Column("create_time")]
	public DateTime CreateTime { get; set; } 
 
}
