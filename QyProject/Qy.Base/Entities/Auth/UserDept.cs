﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Auth;

[Table(DbTableName.QyUserDept)]
[CacheSetting(true)]
[Description("用户部门")]
[Auditable(ProjectType.Sys)]
public class UserDept : IEntity<int>, IHierarchicalEntity
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Pid { get; set; }
    public string Name { get; set; }
    public int Type { get; set; }
    public int Sort { get; set; }
    [Column("great_time")]
    public DateTime GreatTime { get; set; }
    public string Remark { get; set; }
    public bool Status { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
