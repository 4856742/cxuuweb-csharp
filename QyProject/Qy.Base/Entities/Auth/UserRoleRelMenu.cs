﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Auth
{
	/// <summary>
	/// 角色菜单关系表
	///</summary>
	[Table(DbTableName.QyUserRoleRelMenu)]
	[CacheSetting(true, PropertyNamesOfArea = "RoleId")]
	[Description("角色菜单关系表")]
	public class UserRoleRelMenu : Entity<int>
	{
		/// <summary>
		/// 主键
		/// </summary>
		[PrimaryKey("id")]
		public override int Id { get; set; }
		/// <summary>
		/// 角色ID 
		///</summary>
		[Column("role_id")]
		public int RoleId { get; set; }
		/// <summary>
		/// 菜单ID 
		///</summary>
		[Column("menu_id")]
		public int MenuId { get; set; } 
	}
}
