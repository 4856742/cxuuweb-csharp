﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;

namespace Qy.Base.Entities.Auth;

/// <summary>
/// 用户角色关联信息
///</summary>
[Table(DbTableName.QyUserRelRole)]
[CacheSetting(true, PropertyNamesOfArea = "UserId")]
[Description("用户角色关联信息")]
public class UserRelRole : Entity<int>
{
	/// <summary>
	/// 主键
	/// </summary>
	[PrimaryKey("id")]
	public override int Id { get; set; }
	/// <summary>
	/// 用户ID 
	///</summary>
	[Column("user_id")]
	public int UserId { get; set; }
	/// <summary>
	/// 角色ID 
	///</summary>
	[Column("role_id")]
	public int RoleId { get; set; }
 

}
