﻿namespace Qy.Base.Entities.Auth;

/// <summary>
/// 用户数据权限范围 按数字大小升序排列，数字越大权限越大
/// </summary>
public enum AccessLevelType
{
    /// <summary>
    /// 指定部门数据权限
    /// </summary>
    SpecifyDepartment = -1,
    /// <summary>
    /// 本用户数据权限
    /// </summary>
    ThisUser = 0,
    /// <summary>
    /// 本部门数据权限
    /// </summary>
    ThisDepartment = 1,
    /// <summary>
    /// 本部门及以下数据权限
    /// </summary>
    ThisDepartmentAndBelow = 2,
    /// <summary>
    /// 全部数据权限
    /// </summary>
    FullPermissions = 3,
}


/// <summary>
/// 用户类型
/// </summary>
public enum UserTypeEnum
{
    /// <summary>
    /// 普通用户
    /// </summary>
    User = 1,
    /// <summary>
    /// 超级管理员
    /// </summary>
    SuperAdministrator = 99
}

