﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Base.Entities.Auth;

[Table(DbTableName.QyUser)]
[CacheSetting(true, PropertyNamesOfArea = "DeptId")]
[Description("系统用户")]
[Auditable(ProjectType.Sys)]
public class User : IDelEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    [Column("dept_id")]
    public int DeptId { get; set; }
    public UserTypeEnum Type { get; set; }
    [Column("user_name")]
    public string UserName { get; set; }
    [Column("id_number")]
    public string IdNumber { get; set; }
    [Column("pass_word")]
    public string PassWord { get; set; }
    [Column("nick_name")]
    public string NickName { get; set; }
    [Column("pinyin_name")]
    public string PinyinName { get; set; } 
    [Column("py_name")]
    public string PyName { get; set; }
    [Column("status")]
    public bool Status { get; set; }
    [Column("Avatar")]
    public string Avatar { get; set; }
    [Column("mobile")]
    public string Mobile { get; set; }
    [Column("create_time")]
    public DateTime CreateTime { get; set; }

    /// <summary>
	/// 是否已删除，默认为 false 
	/// 默认值: b'0'
	///</summary>
	[Column("is_deleted")]
    public bool IsDeleted { get; set; } = false;
    /// <summary>
    /// 删除签名
    /// </summary>
    [Column("delete_token")]
    public int DeleteToken { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;

    #endregion
}
