﻿using Microsoft.Extensions.Logging;
using PmSoft.Core.FileStorage;
using PmSoft.Core.Job;
using Qy.Base.Services.Attachment;

namespace Qy.Base.Jobs;

/// <summary>
/// 定期删除附件作业
/// </summary>
/// <remarks>
/// 构造函数，注入 Scope 服务和日志记录器
/// </remarks>

[JobSchedule("CleanupAttachment", "0 0 0 * * ?")] // 每24小时执行（Quartz）
//[JobSchedule("SampleJob", "10")]
public class CleanupAttachment(
    IFileStorageProvider _fileStorageProvider,
    AttachmentRepository attachmentRepository,
    ILogger<CleanupAttachment> logger) : IJobDefinition
{
    /// <summary>
    /// 执行作业逻辑
    /// </summary>
    public async Task Execute()
    {
        logger.LogInformation("CleanupAttachment 开始执行");
        var list = await attachmentRepository.GetCleanupListAsync();
        foreach (var item in list)
        {
            try
            {
                var attachment = await attachmentRepository.DelAsync(item);
                if (attachment > 0)
                {
                    var FileExistsArgs = new FileExistsArgs
                    {
                        BucketName = item.BucketName,
                        ObjectName = item.ObjectName
                    };
                    var exists = await _fileStorageProvider.FileExistsAsync(FileExistsArgs);

                    if (exists)
                    {
                        var deleteArgs = new DeleteFileArgs
                        {
                            BucketName = item.BucketName,
                            ObjectName = item.ObjectName
                        };
                        await _fileStorageProvider.DeleteFileAsync(deleteArgs);
                    }
                    logger.LogInformation($"删除附件的数据及文件成功，附件ID：{item.Id}");
                }
                else
                {
                    logger.LogWarning($"删除附件数据失败，附件ID：{item.Id}");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"删除附件失败，附件ID：{item.Id}");
            }
        }
        logger.LogInformation("CleanupAttachment 执行完成");
    }
}
