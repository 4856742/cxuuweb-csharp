﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PmSoft.Cache.Abstractions;
using PmSoft.Cache.Redis;
using PmSoft.Core;
using PmSoft.Core.Job;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Authorization;
using PmSoft.Web.Abstractions.Middlewares;

namespace Qy.Base;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddQyProjectApplication(this IServiceCollection services, IConfiguration configuration)
    {
        // 添加核心服务
        services.AddCore(configuration);
        // 添加默认缓存服务并配置内存实体缓存
        services.AddDefaultCache(configuration);
        // 添加 Redis 缓存服务并配置 Redis 实体缓存
        services.AddRedisCache(configuration);
        // 添加数据库名称解析服务
        services.AddSingleton<IDbNamesResolver, SnakeNamesResolver>();
        // 添加 PetaPoco 数据库服务
        services.AddPetaPoco();
        // 添加 Web 核心服务
        services.AddWebCore(configuration);
        // 添加授权服务
        services.AddPmSoftAuthorization(configuration);
        // 添加通知服务
        services.AddNotification();
        // 添加文件上传服务
        services.AddFileUploadService(configuration);
        // 添加附件管理器服务
       services.AddAttachmentManager(configuration);
        //添加验证码服务
        services.AddCaptcha(configuration);
        //注入定时任务服务
        services.AddJobScheduler(typeof(ServiceCollectionExtensions).Assembly, true);
        // 添加 MVC 服务
        services.AddMvc();
        return services;
    }

    /// <summary>
    /// 扩展 IApplicationBuilder，配置 应用程序所需的中间件。
    /// </summary>
    /// <param name="builder">IApplicationBuilder 实例。</param>
    /// <returns>更新后的 IApplicationBuilder 实例。</returns>
    public static IApplicationBuilder UseQyProjectApplication(this IApplicationBuilder builder)
    {
        // 使用 HTTPS 重定向中间件
        builder.UseHttpsRedirection();
        // 使用路由中间件
        builder.UseRouting();
        // 使用限流中间件
        builder.UseRateLimit();
        // 使用 XSS 过滤中间件
        builder.UseXssFilter();
        // 使用授权中间件
        builder.UsePmSoftAuthorization();
        // 使用通知中间件
        builder.UseNotification("/chatHub");
        return builder;
    }
}
