﻿namespace Qy.Base.AppBase;


public enum ProjectType
{
    None = 0,
    Sys = 1,
    /// <summary>
    /// 门户信息管理系统
    /// </summary>
    Cms = 2,
    /// <summary>
    /// 督察管理系统
    /// </summary>
    Rpc = 3,
}


