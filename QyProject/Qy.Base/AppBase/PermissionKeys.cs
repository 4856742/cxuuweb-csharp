﻿namespace Qy.Base.AppBase;

public class PermissionKeys
{
    /// <summary>
    /// 系统管理
    /// </summary>
    public const string System_Management = "System_Management";
    /// <summary>
    /// 上传管理
    /// </summary>
    public const string Upload_Upload = "Upload_Upload";
    /// <summary>
    /// 用户部门管理
    /// </summary>
    public const string Auth_Edit = "Auth_Edit";
    /// <summary>
    /// 用户管理
    /// </summary>
    public const string User_Edit = "User_Edit";
    public const string User_Delete = "User_Delete";

}

