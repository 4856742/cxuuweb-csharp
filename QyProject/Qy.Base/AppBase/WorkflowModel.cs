﻿using Newtonsoft.Json;
using Qy.Base.InterFace.User;

namespace Qy.Base.AppBase;

/// <summary>
/// 工作流审核行为类型
/// </summary>
public enum WorkflowApproverType
{
    None = 1,
    Dept = 2,
    Post = 3,
    Group = 4,
    User = 5,
}

/// <summary>
/// 工作流通用模型
/// </summary>
public class WorkflowSetModel
{
    /// <summary>
    /// 如果是部门 0，该部门的岗位集合，如果是用户 1，存用户集合
    /// </summary>
    public WorkflowApproverType Type { get; set; } = WorkflowApproverType.None;
    public string Name { get; set; } = string.Empty;
    /// <summary>
    /// 对应部门的岗位集合
    /// </summary>
    public string DutyIds { get; set; } = string.Empty;
    public string UserIds { get; set; } = string.Empty;
    /// <summary>
    /// 时限 按分钟存，由前端换算，相对上一步完成时间
    /// </summary>
    public int TimeLimit { get; set; } = 0;
    /// <summary>
    /// 流程顺序
    /// </summary>
    public int Sort { get; set; } = 0;
    public string Remark { get; set; } = string.Empty;
    public List<IViewUser> Users { get; set; } = []; 
    public List<int> Uids { get; set; } = [];

    public static WorkflowSetModel AsNewList(WorkflowSetModel data)
    {
        return new WorkflowSetModel
        {
            Type = data.Type,   
            Name = data.Name,
            DutyIds = data.DutyIds,
            TimeLimit = data.TimeLimit,
            Sort = data.Sort,
            Remark = data.Remark,
            UserIds = data.UserIds,
            Uids = JsonConvert.DeserializeObject<List<int>>(data.UserIds ?? "[]")??[]
        };
    }

    public static WorkflowSetModel AsNewList2(WorkflowSetModel data, IEnumerable<IViewUser> users)
    {
        return new WorkflowSetModel
        {
            Type = data.Type,
            Name = data.Name,
            DutyIds = data.DutyIds,
            TimeLimit = data.TimeLimit,
            Sort = data.Sort,
            Remark = data.Remark,
            UserIds= data.UserIds,
            Users = users?.Where(x => data.Uids.Contains(x.Id)).ToList() ?? []
        };
    }
}
