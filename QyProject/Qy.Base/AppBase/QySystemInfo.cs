﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;

namespace Qy.Base.AppBase;

public static class QySystemInfo
{
    public static string Ver => "@琪耀管理系统 V2025 自主版权严禁抄袭";
    [Display(Name = "运行框架")]
    public static string FrameworkDescription => RuntimeInformation.FrameworkDescription;
    [Display(Name = "操作系统")]
    public static string OSDescription => RuntimeInformation.OSDescription;
    [Display(Name = "操作系统版本")]
    public static string OSVersion => Environment.OSVersion.ToString();
    [Display(Name = "平台架构")]
    public static string OSArchitecture => RuntimeInformation.OSArchitecture.ToString();
    [Display(Name = "机器名称")]
    public static string MachineName => Environment.MachineName;
    [Display(Name = "用户网络域名")]
    public static string UserDomainName => Environment.UserDomainName;
    [Display(Name = "分区磁盘")]
    public static string GetLogicalDrives => string.Join(", ", Environment.GetLogicalDrives());
    [Display(Name = "系统目录")]
    public static string SystemDirectory => Environment.SystemDirectory;
    [Display(Name = "系统已运行时间(毫秒)")]
    public static int TickCount => Environment.TickCount;


}
