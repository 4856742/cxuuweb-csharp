﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco;

namespace Qy.Base.AppBase;

[Inject(ServiceLifetime.Scoped, typeof(IDbContext))]
public class AppDbContext : PetaPocoDbContext
{
	public AppDbContext(IConfiguration configuration)
		: base(configuration.GetConnectionString("App") ?? string.Empty, "MySqlConnector")
	{
	}
}
