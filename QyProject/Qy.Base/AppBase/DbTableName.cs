﻿namespace Qy.Base.AppBase;
public class DbTableName
{
    //Auth
    public const string QyUser = "qy_user";
    public const string QyUserRole = "qy_user_role";
    public const string QyUserRelRole = "qy_user_rel_role";
    public const string QyUserRoleRelMenu = "qy_user_role_rel_menu";
    public const string QyUserDept = "qy_user_dept";
    public const string QyUserPost = "qy_user_post";
    //Base
    public const string QyAttachment = "qy_attachment";
    public const string QyAttachmentUser = "qy_attachment_user";
    public const string QySysConfig = "qy_sysconfig";
    public const string QyLogLogin = "qy_log_login";
    public const string QyLogSql = "qy_log_sql";
    public const string QyLogException = "qy_log_exception";
    public const string QyMenu = "qy_menu";
    public const string QyMenuCat = "qy_menu_cat";
    public const string QyNotice = "qy_notice";
    public const string QyNoticeKey = "qy_notice_key";
    public const string QySysNotice = "qy_sysnotice";//系统后台公告
    //Workflow
    public const string QyWorkflow = "qy_workflow";
    public const string QyWorkflowStep = "qy_workflow_step";
    public const string QyWorkflowInstance = "qy_workflow_instance";
    public const string QyWorkflowRecord = "qy_workflow_record";
}

