﻿namespace Qy.Base.AppBase;

/// <summary>
/// 修改
/// </summary>
public class ValChangeModel
{
    /// <summary>
    /// 文字标题或描述
    /// </summary>
    public string Title { get; set; } = string.Empty;
    /// <summary>
    /// 字段或键
    /// </summary>
    public string Key { get; set; } = string.Empty;
    /// <summary>
    /// 要修改数据的主键
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// 数据值对象
    /// </summary>
    public object? Val { get; set; } 
}