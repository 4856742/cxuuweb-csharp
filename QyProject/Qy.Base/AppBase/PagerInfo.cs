﻿namespace Qy.Base.AppBase;

public class PagerInfo
{
    public PagerInfo()
    {
        PageIndex = 1;
        PageSize = 20;
    }
    public PagerInfo(int pageIndex = 1, int pageSize = 20)
    {
        PageIndex = pageIndex;
        PageSize = pageSize;
    }
    /// <summary>
    /// 当前页码
    /// </summary>
    public int PageIndex { get; set; }
    /// <summary>
    /// 每页显示多少条
    /// </summary>
    public int PageSize { get; set; }
    /// <summary>
    /// 总记录数
    /// </summary>
    public int TotalNum { get; set; }
    /// <summary>
    /// 总页数
    /// </summary>
    public int TotalPage
    {
        get
        {
            return TotalNum > 0 ? TotalNum % PageSize == 0 ? TotalNum / PageSize : TotalNum / PageSize + 1 : 0;
        }
    }
    
    /// <summary>
    /// 排序字段
    /// </summary>
    public string Sort { get; set; } = string.Empty;
    /// <summary>
    /// 排序类型
    /// </summary>
    public string SortType { get; set; } = string.Empty;

}
