﻿using PmSoft.Web.Abstractions.RealTime;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;

namespace Qy.Base.AppBase.RealTime;

public class OnlineSysUsers : IOnlineClient
{
    public int? UserId { get; set; }
    public int? DeptId { get; set; }
    public string? UserName { get; set; } = string.Empty;
    /// <summary>
	/// 该客户端的唯一连接 ID。
	/// </summary>
	public string ConnectionId { get; set; } = string.Empty;
    /// <summary>
    /// 该客户端的 IP 地址。
    /// </summary>
    public string IpAddress { get; set; } = string.Empty;

    /// <summary>
    /// 租户类型。
    /// </summary>
    public string? TenantType { get; set; }

    /// <summary>
    /// 该客户端建立连接的时间。
    /// </summary>
    public DateTime ConnectTime { get; set; }

    /// <summary>
    /// 用于快速设置或获取 <see cref="Properties"/> 中的属性。
    /// </summary>
    /// <param name="key">属性键名。</param>
    /// <returns>对应键名的属性值。</returns>
	public object this[string key]
    {
        get { return Properties[key]; }
        set { Properties[key] = value; }
    }
    /// <summary>
    /// 可用于为此客户端添加自定义属性的字典。
    /// </summary>
    public Dictionary<string, object> Properties { get; set; } = [];

    public string NickName { get; set; } = string.Empty;
    public string UserAgent { get; set; } = string.Empty;
    public string DeptName { get; set; } = string.Empty;
}

public static partial class OnlineSysUsersExtensions
{
    public static OnlineSysUsers AsView(this IOnlineClient data, IEnumerable<User> users, IEnumerable<UserDept> depts)
    {
        var newData = new OnlineSysUsers();
        var user = users.FirstOrDefault(x => x.Id == data.UserId);
        if (user != null )
        {
            newData = new OnlineSysUsers()
            {
                ConnectionId = data.ConnectionId,
                UserId = data.UserId,
                UserName = user.UserName,
                ConnectTime = data.ConnectTime,
                IpAddress = data.IpAddress,
                DeptId = user.DeptId,
                NickName = user.NickName,
                Properties = data.Properties,
                DeptName = depts.GetDepartmentHierarchyPath(user.DeptId),
            };
        }
        return newData;
    }
}