﻿namespace Qy.Base.AppBase.RealTime;
public class SendMessageModel
{
    public int PostUid { get; set; }
    public string GetUid { get; set; } = string.Empty;
    public string Title { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public string Img { get; set; } = string.Empty;
    public bool Status { get; set; }
}
