﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using PmSoft.Web.Abstractions.Notifications;
using Qy.Base.Dtos;
using Qy.Base.Services.Attachment;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Notices;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class NoticeController(
    UserService userService,
    AttachmentUserService  attachmentUserService,
    NoticeKeyService noticeKeyService,
    IRealTimeNotifier realTimeNotifier,
    IApplicationContext appContext) : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> NoReadCountAsync()
    {
        if (appContext.CurrentUser == null)
            return Ok(new { noReadCount = 0 });
        var uid = appContext.CurrentUser.UserId;
        var noReadCount = await noticeKeyService.GetNoticeCountAsync(uid);
        return Ok(new { noReadCount });
    }

    [HttpGet]
    public async Task<IPagedList<ViewNoticeKey>> IndexAsync([FromQuery] NoticeQuery query)
    {
        if (appContext.CurrentUser == null)
            throw new PmSoftException("登录信息获取失败");
        query.Uid = appContext.CurrentUser.UserId;
        IPagedList<ViewNoticeKey>? pageResult = await noticeKeyService.GetPageListAsync(query);
        return pageResult;
    }

    [HttpGet, ActionName("Read")]
    public async Task<ApiResult<object>> ReadAsync([FromQuery] int id)
    {
        if (id < 1) return ApiResult.Error("ID错误！");
        int uid = appContext.RequiredCurrentUser.UserId;
        var data = await noticeKeyService.ReadAsync(id, uid);
        if (data == null) return ApiResult.Error("消息不存在");
        var attments = await attachmentUserService.GetListAsync(data.Attments);
        return ApiResult.Ok(new { data, attments });
    }
    [HttpGet, ActionName("RePly")]
    public async Task<IActionResult> RePlyAsync([FromQuery] int uid)
    {
        var data = await userService.GetUnionOneAsync(uid);
        return Ok(new { data });
    }

    [HttpPost, ActionName("Create")]
    public async Task<ApiResult> CreatePostAsync([FromBody] EditNotice editNotice)
    {
        //0 为系统  1 用户
        editNotice.Type = 1;
        bool res = await noticeKeyService.InsertAsync(editNotice, appContext.RequiredCurrentUser.UserId);
        if (res)
        {
            return ApiResult.Ok("发送消息 目标ID：" + editNotice.GetUid);
        }
        return ApiResult.Error("发送失败");
    }

    /// <summary>
    /// 发送通知公告表
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ApiResult> SendNoticeAsync(int noticeId = 0)
    {
        if (noticeId <= 0)
            return ApiResult.Error("请求实体为空");
        var response = await noticeKeyService.ReadOneAsync(noticeId);
        if (response == null)
            return ApiResult.Error("消息不存在");
        if (response != null)
        {
            var sendMsg = new UserNotification()
            {
                UserId = response.GetUid,
                Notification = new TenantNotification()
                {
                    NotificationName = response.Title,
                    Data = response.Content,
                    //EntityTypeName = 1,
                    EntityId = noticeId
                }
            };
            await realTimeNotifier.SendNotificationsAsync([sendMsg]);
        }
        return ApiResult.Ok("发送成功！");
    }

    [HttpDelete]
    public async Task<ApiResult> Delete(int id)
    {
        if (await noticeKeyService.DeleteAsync(id) > 0)
            return ApiResult.Ok("删除消息 ID：" + id);
        return ApiResult.Error("删除失败 ID：" + id);
    }

}
