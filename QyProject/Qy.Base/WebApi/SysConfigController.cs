﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Services.SysConfigs;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class AppConfigController(AppConfigService  appConfigService) : ControllerBase
{

    [HttpGet]
    public async Task<AppConfig> GetAsync()
    {
        var appConfig = await appConfigService.GetAsync();
        return appConfig;
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    
    public async ValueTask<ApiResult> EditPostAsync([FromBody] AppConfig configParm)
    {
        await appConfigService.SaveAsync(configParm);
        return ApiResult.Ok("更新成功");
    }
}
