﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Services.Loging;
using Qy.Base.Services.Loging.Exceptions;
using Qy.Base.Services.Loging.Login;
using Qy.Base.Services.Loging.Sys;
using Qy.Base.Utilities.EPPlusExcel;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class SysLogController(
    LogSysService logSysService,
    LogLoginService logLoginService,
    LogExceptionService logExceptionService,
    UserService userService,
    UserDeptService userDeptService) : Controller
{
    [HttpGet]
    public async Task<ApiResult<object>> LogExceptionPageAsync([FromQuery] LogingQuery query)
    {
        var pageList = await logExceptionService.GetPagingAsync(query);
        return ApiResult.Ok(new { pageList, exportFields = LogConsts.ViewLogExceptionFields });
    }
    [HttpGet]
    public async Task<IActionResult> LogExceptionExportExcelAsync([FromQuery] LogingQuery query)
    {
        var pageList = await logExceptionService.GetPagingAsync(query);
        var options = new OutPutExcelExportOptions
        {
            InitialHeaderList = LogConsts.ViewLogExceptionFields,
            FieldsJson = query.Fields,
            TableTitle = "系统异常日志",
        };
        MemoryStream stream = new();
        await OutPutExcelOfEntityHelper<ViewLogException>.OutPutExcel([.. pageList.Items], options, stream);
        stream.Position = 0;
        var fileResult = File(stream, DownloadFileMimeType.FileMimeTypeExcel, options.TableTitle);
        // 在这里结束时手动调用Dispose  ,防止出现写入失败问题
        return fileResult;
    }
    [HttpGet]
    public async Task<ApiResult<object>> LogExceptionAsync(int id)
    {
        try
        {
            var get = await logExceptionService.GetAsync(id);
            if (get == null)
                return ApiResult.Error("数据获取为空");
            var user = await userService.GetAsync(get.Uid);
            if (user == null)
                return ApiResult.Error("数据获取为空");
            var detp = await userDeptService.GetAsync(get.Did);
            if (detp == null)
                return ApiResult.Error("数据获取为空");
            return ApiResult.Ok(new { data = get.AsOne(user, detp) });
        }
        catch
        {
            return ApiResult.Error("数据获取为空");
        }
    }
    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> ClearLogExceptionAsync()
    {
        await logExceptionService.ClearAsync();
        return ApiResult.Ok("清除成功");
    }
    [HttpGet]
    public async Task<ApiResult<object>> SqlLogListAsync([FromQuery] LogingQuery query)
    {
        var pageResult = await logSysService.GetPagingAsync(query);
        return ApiResult.Ok(new { pageResult, exportFields = LogConsts.ViewLogSysFields });

    }
    [HttpGet]
    public async Task<IActionResult> SqlLogListExportExcelAsync([FromQuery] LogingQuery query)
    {
        var pageList = await logSysService.GetPagingAsync(query);
        var options = new OutPutExcelExportOptions
        {
            InitialHeaderList = LogConsts.ViewLogSysFields,
            FieldsJson = query.Fields,
            TableTitle = "系统管理操作日志",
        };
        MemoryStream stream = new();
        await OutPutExcelOfEntityHelper<ViewLogSys>.OutPutExcel([.. pageList.Items], options, stream);
        stream.Position = 0;
        var fileResult = File(stream, DownloadFileMimeType.FileMimeTypeExcel, options.TableTitle);
        // 在这里结束时手动调用Dispose  ,防止出现写入失败问题
        return fileResult;
    }
    [HttpGet]
    public async Task<ApiResult<object>> LoginLogListAsync([FromQuery] LogingQuery query)
    {
        var pageResult = await logLoginService.GetPagingAsync(query);
        return ApiResult.Ok(new { pageResult, exportFields = LogConsts.LogLoginViewFields });
    }
    [HttpGet]
    public async Task<IActionResult> LoginLogExportExcelAsync([FromQuery] LogingQuery query)
    {
        var pageList = await logLoginService.GetPagingAsync(query);
        var options = new OutPutExcelExportOptions
        {
            InitialHeaderList = LogConsts.LogLoginViewFields,
            FieldsJson = query.Fields,
            TableTitle = "系统登录日志",
        };
        MemoryStream stream = new();
        await OutPutExcelOfEntityHelper<ViewLogLogin>.OutPutExcel([.. pageList.Items], options, stream);
        stream.Position = 0;
        var fileResult = File(stream, DownloadFileMimeType.FileMimeTypeExcel, options.TableTitle);
        // 在这里结束时手动调用Dispose  ,防止出现写入失败问题
        return fileResult;

    }
}
