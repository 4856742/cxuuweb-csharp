﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attachment;
using PmSoft.Web.Abstractions.Attributes;
using PmSoft.Web.Abstractions.Authorization;
using Qy.Base.AppBase;
using Qy.Base.AppBase.RealTime;
using Qy.Base.Dtos.Auth.UserDto;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.InterFace.User;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Services.Auth.QyUserRole;
using Qy.Base.Services.Menus;
using Qy.Base.Services.SysConfigs;
using Qy.Base.Utilities.EPPlusExcel;
namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class UserController(
    UserService userService,
    AppConfigService appConfigService,
    MenuService menuService,
    UserRoleService userRoleService,
    UserDeptService userDeptService,
    IJwtService jwtService,
    List<TenantStorageConfig> tenantStorageConfigs,
    IApplicationContext applicationContext) : ControllerBase
{
    [HttpGet]
    public async Task<ApiResult<object>> GetUserInfoAsync()
    {
        var userInfo = await userService.GetLoginUserBaseInfoAsync();
        var sysInfo = await appConfigService.GetAsync();
        IEnumerable<string> permCodes = await menuService.GetUserBottonRolesAsync();
        return ApiResult.Ok(new { userInfo, tenantStorageConfigs, sysInfo, permCodes });
    }
    [HttpGet]
    public async Task<ApiResult<object>> IndexAsync([FromQuery] UserQuery query)
    {
        var pageResult = await userService.GetPageListAsync(query);
        return ApiResult.Ok(new { pageResult, exportFields = UserConsts.UserFields });
    }

    [HttpGet]
    public async Task<IActionResult> UserExportExcelAsync([FromQuery] UserQuery query)
    {
        var pageResult = await userService.GetPageListAsync(query);
        var options = new OutPutExcelExportOptions
        {
            InitialHeaderList = UserConsts.UserFields,
            FieldsJson = query.Fields,
            TableTitle = "系统用户表",
        };
        MemoryStream stream = new();
        await OutPutExcelOfEntityHelper<ViewUser>.OutPutExcel([.. pageResult.Items], options, stream);
        stream.Position = 0;
        var fileResult = File(stream, DownloadFileMimeType.FileMimeTypeExcel, options.TableTitle);
        // 在这里结束时手动调用Dispose  ,防止出现写入失败问题
        return fileResult;

    }
    [HttpGet]
    public async Task<IPagedList<ViewUser>> IndexNoAuthAsync([FromQuery] UserQuery query)
    {
        var pageResult = await userService.GetPageListAsync(query);
        return pageResult;
    }
    [HttpGet]
    public async Task<ApiResult<object>> QueryUserOptionsAsync([FromQuery] UserQuery query)
    {
        var users = await userService.GetUserOptionsAsync(query);
        int? currentUserId = applicationContext.CurrentUser?.UserId;
        return ApiResult.Ok(new { userList = users, currentUserId });
    }
    [HttpGet]
    public async Task<ApiResult<object>> QueryUserOptionsWhitIdAsync([FromQuery] UserQuery query)
    {
        var users = await userService.QueryUserOptionsWhitIdAsync(query);
        int? currentUserId = applicationContext.CurrentUser?.UserId;
        return ApiResult.Ok(new { userList = users, currentUserId });
    }
    [HttpGet]
    public async Task<ViewUserOptions?> GetOneOptionAsync(int id)
    {
        var user = await userService.GetOneOptionAsync(id);
        return user;
    }
    /// <summary>
    ///  读取内存获取在线用户列表并分页
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<PagedList<OnlineSysUsers>> OnlineUserListAsync([FromQuery] UserQuery query)
    {
        var pageResult = await userService.OnlineUserListAsync(query);
        return pageResult;
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.User_Edit)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        IEnumerable<OptionItemDto> roleItems = await userRoleService.IndentedOptionAsync();
        var user = applicationContext.GetRequiredCurrentUser();
        IEnumerable<OptionItemDto> userDepts = await userDeptService.IndentedOptionAuthAsync(user);
        if (!user.IsSuperAdmin())
        {
            //当前用户不能添加比自己级别高的角色
            IEnumerable<UserRole> roles = await userRoleService.GetUserRolesAsync(user.Id);
            roleItems = roleItems.Where(x => x.Sort >= roles.Max(x => x.Sort));
        }
        if (id > 0)
        {
            var viewUser = await userService.GetAsync(id) ?? throw new PmSoftException("用户不存在");
            IEnumerable<UserRole> userRoles = await userRoleService.GetUserRolesAsync(viewUser.Id);
            return ApiResult.Ok(new { data = viewUser?.ToEditView(userRoles), roleItems, userDepts });
        }
        return ApiResult.Ok(new { roleItems, userDepts });
    }
    [HttpGet]
    public async Task<ViewUser?> UserChangeAsync()
    {
        ViewUser user = await userService.GetUnionOneAsync(applicationContext.RequiredCurrentUser.UserId);
        return user;
    }
    [HttpPut]

    public async Task<ApiResult<object>> UserChangePostAsync([FromBody] EditUser editUser)
    {
        int res = await userService.EditUserInfoAsync(editUser, applicationContext.RequiredCurrentUser.UserId);
        switch (res)
        {
            case 0:
                return ApiResult.Error("修改失败");
            case -1:
                return ApiResult.Error("旧密码不正确！");
            case -2:
                return ApiResult.Error("登录用户名或手机号已存在，请重新输入! ");
            case 101:
                await jwtService.InvalidateUserTokensAsync(applicationContext.RequiredCurrentUser);
                return ApiResult.Ok(new { changePassWord = 1, msg = $"登录名：{editUser.NickName} 修改个人信息" });
            default:
                return ApiResult.Ok($"登录名：{editUser.NickName} 修改个人信息");
        }
    }
    [HttpPut]
    public async Task<ApiResult<object>> EditPasswordAsync([FromBody] UserPasswordChangeForm editUser)
    {
        int res = await userService.EditPasswordAsync(editUser, applicationContext.RequiredCurrentUser.UserId);
        switch (res)
        {
            case -1:
                throw new PmSoftException("旧密码不正确！");
            case 101:
                await jwtService.InvalidateUserTokensAsync(applicationContext.RequiredCurrentUser);
                return ApiResult.Ok(new { changePassWord = 1, msg = $"用户ID：{applicationContext.RequiredCurrentUser.UserId} 修改个人密码" });
            default:
                throw new PmSoftException("修改失败");
        }
    }
    [HttpPut]
    public async Task<ApiResult> EditUserProfileAsync([FromBody] EditUser editUser)
    {
        editUser.Id = applicationContext.RequiredCurrentUser.UserId;
        int res = await userService.EditUserProfileAsync(editUser);
        if (res > 0)
            return ApiResult.Ok($"修改头像成功");
        throw new PmSoftException("修改失败");
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.User_Edit)]
    public async Task<ApiResult> CreatePostAsync([FromBody] EditUser user)
    {
        if (await userService.ExistsByUserNameAsync(user.UserName))
            throw new PmSoftException("该登录名已存在，请重新输入！");
        if (!string.IsNullOrEmpty(user.Mobile) && await userService.ExistsByMobileAsync(user.Mobile))
            throw new PmSoftException("手机号已存在，请重新输入！");
        if (!string.IsNullOrEmpty(user.IdNumber) && await userService.ExistsByIdNumberAsync(user.IdNumber))
            throw new PmSoftException("身份证号已存在，请重新输入！");
        int res = await userService.InsertAsync(user);
        if (res > 0)
            return ApiResult.Ok($"添加成功ID:{res} 用户名：{user.UserName}");
        throw new PmSoftException("添加失败");
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.User_Edit)]

    public async Task<ApiResult> EditPostAsync([FromBody] EditUser user)
    {
        int res = await userService.UpdateAsync(user);
        return res switch
        {
            0 => throw new PmSoftException("编辑 Id:" + user.Id),
            -1 => throw new PmSoftException("请勿非法提交 Id:" + user.Id),
            -2 => throw new PmSoftException("该用户名、手机号或身份证号已存在 Id:" + user.Id),
            _ => ApiResult.Ok("Id:" + user.Id),
        };
    }
    [HttpPut, ActionName("ChStatus")]
    [MenuAuthorize(PermissionKeys.User_Edit)]

    public async Task<ApiResult> ChStatusAsync([FromQuery] int id, bool status)
    {
        if (0 < await userService.ChStatusUpdateAsync(id, status))
            return ApiResult.Ok("设置为 " + status.ToString() + ", Id:" + id);
        return ApiResult.Error("设置为 " + status.ToString() + ", Id:" + id);
    }
    [HttpPut, ActionName("UpdateUserPassWord")]
    [MenuAuthorize(PermissionKeys.User_Edit)]

    public async Task<ApiResult> UpdateUserPassWordAsync([FromQuery] int id, string? passWord)
    {
        if (id < 0) throw new PmSoftException("提交错误");
        if (0 < await userService.UpdateUserPassWordAsync(id, passWord))
            return ApiResult.Ok("恢复用户默认密码");
        throw new PmSoftException("恢复默认密码失败");
    }
    [HttpDelete]
    [MenuAuthorize(PermissionKeys.User_Delete)]

    public async Task<ApiResult> DeleteAsync(int id)
    {
        if (await userService.Delete(id) > 0)
            return ApiResult.Ok($"ID：{id}");
        return ApiResult.Error($"ID：{id}");
    }

    [HttpDelete]
    [MenuAuthorize(PermissionKeys.User_Delete)]

    public async Task<ApiResult> ForceOfflineAsync(int id)
    {
        var currentUser = new AuthUser()
        {
            UserId = id
        };
        await jwtService.InvalidateUserTokensAsync(currentUser);
        return ApiResult.Ok("强制下线用户");
    }

}
