﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Menu;
using Qy.Base.Entities.QyMenu;
using Qy.Base.Extensions;
using Qy.Base.Services.Menus.MenuCats;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class MenuCatController(MenuCatService qyMenuCatService) : ControllerBase
{

    [HttpGet]
    //[MenuAuthorize(PermissionKeys.System_Management)]
    public async ValueTask<IEnumerable<OptionItemDto>> OptionTypesAsync()
    {
        var data = await qyMenuCatService.IndentedOptionAsync(); 
        return data;
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<IEnumerable<ViewQyMenuCat>> IndentedListAsync()
    {
        IEnumerable<ViewQyMenuCat>? data = await qyMenuCatService.IndentedListAsync();
        return data;
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<IEnumerable<ViewQyMenuCat>> IndexAsync()
    {
        var data = await qyMenuCatService.GetCatsAsync();
        return data;
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<IActionResult> AddOrEditAsync(int id)
    {
        var catList = await qyMenuCatService.IndentedOptionAsync();
       var data = await qyMenuCatService.GetAsync(id);
        return Ok(new { data, catList });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    
    public async Task<ApiResult> CreatePostAsync([FromBody] MenuCat edit)
    {
        if (await qyMenuCatService.InsertAsync(edit) != null)
            return ApiResult.Ok(edit.Name);
        return ApiResult.Error("新增失败");
    }
    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    
    public async Task<ApiResult> EditPostAsync([FromBody] MenuCat edit)
    {
        if (await qyMenuCatService.UpdateAsync(edit) > 0)
            return ApiResult.Ok(edit.Name);
        return ApiResult.Error("修改失败！");
    }
    [HttpDelete]
    [MenuAuthorize(PermissionKeys.System_Management)]
    
    public async Task<ApiResult> Delete(int id)
    {
        if (await qyMenuCatService.DeleteAsync(id) > 0)
            return ApiResult.Ok("ID：" + id);
        return ApiResult.Error("删除失败，是否有菜单");
    }
}
