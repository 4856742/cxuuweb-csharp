﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Authorization;
using PmSoft.Web.Abstractions.Captcha;
using Qy.Base.Dtos;
using Qy.Base.Dtos.Auth;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.SysConfigs;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
public class LoginController(
    AppConfigService  appConfigService,
    IJwtService jwtService,
    IApplicationContext applicationContext,
    UserAuthenticationService userAuthenticationService,
    CaptchaService captchaService) : ControllerBase
{


    [HttpGet]
    public async Task<ApiResult<object>> CaptchaImageAsync()
    {
        var appConfig = await appConfigService.GetAsync();
        if (!appConfig.IsOnValidCode)
            return ApiResult.Ok(new { appConfig.IsOnValidCode });

        //var validCodeType = await configService.CheckValidCodeTypeAsync();
        //if (validCodeType == 1)
        //{
        var uuid = Guid.NewGuid().ToString();
        var data = await captchaService.GenerateImageAsync(uuid);
        var img = "data:image/png;base64," + Convert.ToBase64String(data);
        return ApiResult.Ok(new { uuid, img = img[(img.IndexOf(',') + 1)..], appConfig.IsOnValidCode });
        // }
        throw new PmSoftException("验证码获取失败");
    }


    [HttpPost]
    public async Task<ApiResult<object>> LoginAsync([FromBody] LoginDto login)
    {
        var appConfig = await appConfigService.GetAsync();
        if (appConfig.IsOnValidCode)
        {
            //var validCodeType = await configService.CheckValidCodeTypeAsync();
            //if (validCodeType == 1)
            //{
            //CaptchaStateModel sItem = new()
            //{
            //    DataCode = login.ValidCode,
            //    Id = login.Uuid,
            //    Type = validCodeType,
            //};
            //var getCap = memoryCache.Get<CaptchaStateModel>("Captchas::" + login.Uuid);
            //if (getCap == null)
            //    return ApiResult.Error("验证失败");
            //bool checkCap = CaptchaHelper.Verify(sItem, getCap);
            var ret = await captchaService.CheckAsync(login.ValidCode, login.Uuid);
            if (!ret)
                throw new PmSoftException("验证码验证失败");
            //}
            //else
            //{
            //    return ApiResult.Error("验证码设置有误，请联系管理员");
            //}
        }
        var authedUser = await userAuthenticationService.CheckLoginAsync(login) ?? throw new PmSoftException("用户名密码错误！");
        var (AccessToken, RefreshToken) = await jwtService.GenerateTokenAsync(authedUser);
        return ApiResult.Ok(new { AccessToken, RefreshToken });
    }

    [HttpPost]
    public async Task<ApiResult<object>> RefreshTokenAsync(RefreshTokenIn dto)
    {
        var token = await jwtService.RefreshTokenAsync(dto.AccessToken, dto.RefreshToken);
        return token == null ? throw new PmSoftException("刷新token失败")
            : ApiResult.Ok(new { token?.NewAccessToken, token?.NewRefreshToken });
    }

    /// <summary>
    /// 登出
    /// </summary>
    /// <returns></returns>
    [HttpDelete]
    public async Task<ApiResult> LoginOutAsync()
    {
        if (applicationContext.CurrentUser != null)
            await jwtService.InvalidateUserTokensAsync(applicationContext.CurrentUser);
        return ApiResult.Ok("成功退出");
    }
}
