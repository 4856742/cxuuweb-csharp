﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Services.Auth.QyUserRole;
using Qy.Base.Services.Menus;
using Qy.Base.Services.Menus.MenuCats;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class UserRoleController(
    UserRoleService userRoleService,
    MenuCatService menuCatService,
    UserDeptService userDeptService,
    MenuService menuService) : ControllerBase
{

    [HttpGet]
    public async Task<IEnumerable<UserRole>> IndexAsync()
    {
        var data = await userRoleService.GetAllAsync();
        data = data.Where(x => x.Id != 1);//过滤超级管理员组
        return data;
    }
    [HttpGet]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        var menuList = await menuService.IndentedOptionAsync();
        var menuCats = await menuCatService.GetByAsOptionsAsync();
        var deptList = await userDeptService.IndentedOptionAsync();
        if (id < 1)
            return ApiResult.Ok(new { menuList, menuCats });
        var data = await userRoleService.GetViewUserRoleAsync(id);
        return ApiResult.Ok(new { data, menuList, menuCats, deptList });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]

    public async Task<ApiResult> CreatePostAsync([FromBody] EditUserRole role)
    {
        if (string.IsNullOrEmpty(role.Name))
            return ApiResult.Error("角色名不能为空！");
        if (await userRoleService.ExistsByNameAsync(role.Name))
            return ApiResult.Error("该角色名已存在，请重新输入！");
        object res = await userRoleService.GreatOrUpdateAsync(role);
        return ApiResult.Ok("添加角色 ID：" + res.ToString());
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]

    public async Task<ApiResult> EditPost([FromBody] EditUserRole role)
    {
        if (role.Id == UserConsts.SuperAdmin)
            return ApiResult.Error("不能修改超级管理员！");
        var get = await userRoleService.GetAsync(role.Id);
        if (get == null)
            return ApiResult.Error("获取角色信息失败！");
        if (string.IsNullOrEmpty(role.Name))
            return ApiResult.Error("角色名不能为空！");
        if (role.Name != get.Name && await userRoleService.ExistsByNameAsync(role.Name))
            return ApiResult.Error("角色名已存在，请重新输入！");
        role.Id = get.Id;
        await userRoleService.GreatOrUpdateAsync(role);
        return ApiResult.Ok($"修改角色及系统权限:{role.Name} ID：{role.Id}");
    }

    [HttpPut, ActionName("ChangeSort")]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]

    public async Task<ApiResult> ChangeSortAsync([FromBody] OptionItemDto[] optionType)
    {
        var res = await userRoleService.ChangeSortAsync(optionType);
        if (res)
            return ApiResult.Ok($"修改岗位排序");
        return ApiResult.Error($"修改岗位排序");
    }

    [HttpDelete]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult> Delete(int id)
    {
        if (id == 1)
            return ApiResult.Error("超级管理员组不能删除");
        if (await userRoleService.Delete(id))
            return ApiResult.Ok("删除角色 ID：" + id);
        return ApiResult.Error("删除失败！可能该角色下分配有用户");
    }

}
