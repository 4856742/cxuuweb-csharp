﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Entities;
using Qy.Base.Entities.Attachment;
using Qy.Base.Services.Attachment;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.SysNotices;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class SysNoticeController(
    UserService userService,
   AttachmentUserService attachmentUserService,
    SysNoticeService sysNoticeService) : ControllerBase
{

    [HttpGet]
    public async Task<IPagedList<ViewSysNotice>> IndexAsync([FromQuery] SysNoticeQuery query)
    {
        var pageResult = await sysNoticeService.GetPageDAsync(query);
        return pageResult;
    }

    [HttpGet]
    public async Task<ApiResult<object>> GetAsync(int id)
    {
        if (id < 1)
            return ApiResult.Error("传参错误");
        var get = await sysNoticeService.GetAsync(id);
        if (get == null)
            return ApiResult.Error("传参错误");
        var user = await userService.GetAsync(get.Uid);
        if (user == null)
            return ApiResult.Error("传参错误");
        ViewSysNotice data = new()
        {
            Id = get.Id,
            Title = get.Title,
            Content = get.Content,
            Uid = get.Uid,
            NickName = user.NickName,
            GreatTime = get.GreatTime,
        };
        var attments = await attachmentUserService.GetListAsync(get.Attments);
        return ApiResult.Ok(new { data, attments });
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        if (id < 1)
            return ApiResult.Ok("传参错误");
        var data = await sysNoticeService.GetAsync(id);
        if (data == null)
            return ApiResult.Error("要编辑的数据不存在");
        var attments = await attachmentUserService.GetListAsync(data.Attments);
        return ApiResult.Ok(new { data, attments });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> CreatePostAsync([FromBody] SysNotice notice)
    {
        var res = await sysNoticeService.InsertAsync(notice);
        if (res != null)
        {
            await sysNoticeService.PublishAttachments(notice.Content, res);
            return ApiResult.Ok($"成功发布系统后台公告");
        }
        return ApiResult.Error("发布失败");
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> EditAsync([FromBody] SysNotice notice)
    {
        int res = await sysNoticeService.UpdateAsync(notice);
        if (res > 0)
        {
            await sysNoticeService.PublishAttachments(notice.Content, notice.Id);
            return ApiResult.Ok($"编辑系统后台公告 ID：{notice.Id}");
        }
        return ApiResult.Error("失败");
    }

    [HttpDelete]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> Delete(int id)
    {
        if (await sysNoticeService.DeleteAsync(id) > 0)
            return ApiResult.Ok($"删除系统后台公告 ID：{id}");
        return ApiResult.Error($"删除失败 ID：{id}");
    }

}
