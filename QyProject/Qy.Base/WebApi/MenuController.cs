﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Menu;
using Qy.Base.Entities.QyMenu;
using Qy.Base.Extensions;
using Qy.Base.Services.Menus;
using Qy.Base.Services.Menus.MenuCats;
namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class MenuController(
    MenuService menuService,
    MenuCatService menuCatService
    ) : ControllerBase
{

    [HttpGet]
    public async Task<List<AsMenu>> GetRouteAsync(int[] cids)
    {
        var data = await menuService.GetMenuListAsync(cids);
        return data;
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult<object>> IndexAsync(int id)
    {
        if (id < 1)
            return ApiResult.Error("平台参数错误");
        var data = await menuService.GetListAsync(id);
        return ApiResult.Ok(data);
    }

    [HttpGet]
    //[MenuAuthorize(PermissionKeys.Menu_Index)]
    public async Task<IEnumerable<OptionItemDto>> OptionTypesAsync(int cid)
    {
        var data = await menuService.IndentedMenuOptionAsync(cid);
        return data;
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<IActionResult> AddOrEditAsync(int id)
    {
        var catList = await menuCatService.IndentedOptionAsync();
        if (id > 0)
        {
            var data = await menuService.GetAsync(id);
            return Ok(new { data, catList });
        }
        return Ok(new { catList });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.System_Management)]    
    public async Task<ApiResult> CreateAsync([FromBody] Menu menu)
    {
        menu.Component = menu.Type == (int)QyMenuType.Catalog ? "Layout" : menu.Component;
        var res = await menuService.InsertAsync(menu);
        return ApiResult.Ok("新增成功 ID:" + res.ToString());
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.System_Management)]    
    public async Task<ApiResult> EditAsync([FromBody] Menu menu)
    {
        Menu? getMenu = await menuService.GetAsync(menu.Id);
        if(getMenu == null)
            return ApiResult.Error($"修改菜单:{menu.Name} Id:{menu.Id} 失败，菜单不存在");
        getMenu.Pid = menu.Pid;
        getMenu.Cid = menu.Cid;
        getMenu.Name = menu.Name;
        getMenu.Type = menu.Type;
        getMenu.Sort = menu.Sort;
        getMenu.Status = menu.Status;
        getMenu.Path = menu.Path;
        getMenu.Component = menu.Type == (int)QyMenuType.Catalog ? "Layout" : menu.Component;
        getMenu.PermCode = menu.PermCode;
        getMenu.Icon = menu.Icon;
        getMenu.Redirect = menu.Redirect;
        getMenu.Hide = menu.Hide;
        getMenu.KeepAlive = menu.KeepAlive;
        getMenu.AlwaysShow = menu.AlwaysShow;
        await menuService.UpdateAsync(getMenu);
        return ApiResult.Ok($"修改菜单:{getMenu.Name} Id:{menu.Id}");
    }

    [HttpDelete, ActionName("Delete")]
    [MenuAuthorize(PermissionKeys.System_Management)]  
    public async Task<ApiResult> DeleteAsync(int id)
    {
        if (await menuService.DeleteByEntityIdAsync(id) > 0)
            return ApiResult.Ok("删除菜单,Id： " + id);
        return ApiResult.Error("删除失败，其有子菜单？");
    }

}
