﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attachment;
using PmSoft.Web.Abstractions.Attributes;
using PmSoft.Web.Abstractions.Services;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Services.Attachment;
using Qy.Base.Utilities;
using System.Threading.Tasks;

namespace Qy.Base.WebApi;

[Route("Api/[controller]")]
[RestrictAccess]
public class FileController(
    IFileUploadService fileUploadService,
    AttachmentManagerService attachmentManagerService,
    AttachmentUserService attachmentUserService,
    AttachmentService attachmentService,
    IApplicationContext applicationContext
    ) : ControllerBase
{

    /// <summary>
    /// 普通上传,不走框架上传,不进行附件用户及租户关联
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("upload")]
    public async Task<ApiResult<object>> UploadFile([FromForm] UploadDto uploadDto)
    {
        try
        {
            var files = uploadDto.File;
            if (files.Count < 1)
                return ApiResult.Error("上传的文件不存在！");
            var file = uploadDto.File[0];     
            var url = await attachmentService.SaveFileAsync(file);
            return ApiResult.Ok(new { file.FileName, url });
        }
        catch (Exception ex)
        {
            return ApiResult.Error(ex.Message);

        }
    }

    /// <summary>
    /// 上传时删除文件，不走框架上传, 不能用静态方法
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    [HttpDelete("del")]
    public ApiResult DelFileUrl(string filePath)
    {
        try
        {
            // 验证 filePath 是否为合法路径
            if (string.IsNullOrEmpty(filePath) || Path.GetFullPath(filePath).Contains(".."))
            {
                return ApiResult.Error("无效的文件路径");
            }
            // 构建完整的文件路径
            string file = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + "\\" + filePath;
            // 检查文件是否存在
            if (FileUtility.FileExists(file))
            {
                // 尝试删除文件
                FileUtility.FileDel(file);
                return ApiResult.Ok("文件删除成功");
            }
            else
            {
                // 文件不存在时返回错误信息
                return ApiResult.Error("文件不存在");
            }
        }
        catch
        {
            return ApiResult.Error("文件删除失败，请稍后再试");
        }
    }

    [HttpDelete("del-attid")]
    public async Task<int> Delete(string attachmentId)
    {
      return  await attachmentService.DeleteAsync(attachmentId);
    }

        /// <summary>
        /// 普通分片上传
        /// </summary>
        /// <param name="file"></param>
        /// <param name="fileName"></param>
        /// <param name="chunkNumber"></param>
        /// <param name="totalChunks"></param>
        /// <returns></returns>
        [HttpPost("chunk")]
    public async Task<IActionResult> UploadChunk(
        IFormFile file,
        [FromQuery] string fileName,
        [FromQuery] int chunkNumber,
        [FromQuery] int totalChunks)
    {
        try
        {
            var isCompleted = await fileUploadService.UploadChunkAsync(file, fileName, chunkNumber, totalChunks, "uploads");
            if (isCompleted.IsCompleted)
                return Ok($"文件 {fileName} 分片上传完成并合并成功。");
            return Ok($"分片 {chunkNumber}/{totalChunks} 上传成功。");
        }
        catch (ArgumentException ex)
        {
            return BadRequest(ex.Message);
        }
    }


    /// <summary> 
	/// 上传临时附件的 Web API 方法（普通文件上传）
	/// </summary>
	/// <param name="file">上传的文件</param>
	/// <param name="tenantType">租户类型，例如 "Company" 或 "User"</param>
	/// <returns>上传结果，包含附件ID或错误信息</returns>
	[HttpPost("upload-temporary")]
    public async Task<IAttachment?> UploadTemporaryAttachmentAsync([FromForm] UploadDto uploadDto)
    {
        var files = uploadDto.File;
        if (files.Count < 1 || uploadDto.Data == null)
            throw new PmSoftException("未提供有效的文件。");
        if (string.IsNullOrEmpty(uploadDto.Data.ClassName))
            throw new PmSoftException("租户类型未指定。");

        var file = uploadDto.File[0];

        //通过请求参数中的type获取文件类型是否为进行缩略
        if (!string.IsNullOrEmpty(uploadDto.Data.ProcessType) && uploadDto.Data.ProcessType != "is_compress")
        {
            //缩略图处理
        }

        // 调用附件管理服务的普通上传方法
        var attachment = await attachmentManagerService.UploadTemporaryAttachmentAsync(file, uploadDto.Data.ClassName);
        return attachment;
    }
    /// <summary> 
    /// 上传临时附件的 Web API 方法（文件流上传）
    /// </summary>
    /// <param name="file">上传的文件</param>
    /// <param name="tenantType">租户类型，例如 "Company" 或 "User"</param>
    /// <returns>上传结果，包含附件ID或错误信息</returns>
    [HttpPost("upload-temporary-byte")]
    public async Task<IAttachment?> UploadTemporaryAttachmentByteAsync()
    {
        // 读取请求体中的字节数组
        using var memoryStream = new MemoryStream();
        await Request.Body.CopyToAsync(memoryStream);
        var fileBytes = memoryStream.ToArray();

        if (fileBytes == null || fileBytes.Length == 0)
            throw new ArgumentNullException(nameof(fileBytes), "文件数据不能为空。");

        // 从请求头中获取元数据
        var fileName = Request.Headers["FileName"].ToString();
        var className = Request.Headers["ClassName"].ToString();
        var processType = Request.Headers["ProcessType"].ToString();

        if (string.IsNullOrEmpty(className))
            throw new PmSoftException("租户类型未指定。");

        // 处理缩略图
        if (!string.IsNullOrEmpty(processType) && processType == "is_compress")
        {
            // 缩略图处理
        }
        // 调用附件管理服务的普通上传方法
        var attachment = await attachmentManagerService.UploadTemporaryAttachmentAsync(fileBytes, fileName, className);
        return attachment;
    }
    /// <summary> 
    /// 分片文件上传 根据条件判断是否进行直接发布
    /// </summary>
    /// <param name="file">上传的文件</param>
    /// <param name="tenantType">租户类型，例如 "Company" 或 "User"</param>
    /// <returns>上传结果，包含附件ID或错误信息</returns>
    [HttpPost("upload-chunk")]
    public async Task<IAttachment?> UploadChunkAsync([FromForm] UploadDto uploadDto)
    {
        var files = uploadDto.File;
        if (files.Count < 1 || uploadDto.Data == null)
            throw new PmSoftException("未提供有效的文件");
        var upData = uploadDto.Data;
        if (string.IsNullOrEmpty(upData.ClassName))
            throw new PmSoftException("租户类型未指定");

        var file = uploadDto.File[0];
        int chunkNumber = Convert.ToInt32(upData.ChunkIndex);
        int totalChunks = Convert.ToInt32(upData.TotalChunks);
        string isPublish = upData.IsPublish ?? "";
        string fileName = upData.FileName;
        var attachment = await attachmentManagerService.UploadTemporaryAttachmentAsync(file, upData.OriginalFileName, fileName, upData.ClassName, chunkNumber, totalChunks);
        // 判断是否需要进行直接发布
        if (attachment != null && isPublish == "is_publish")
        {
            var tenantId = applicationContext.RequiredCurrentUser.UserId.ToString();
            attachment = await attachmentManagerService.PublishAttachmentAsync(attachment.AttachmentId, tenantId, attachment.FriendlyName);
            if (attachment != null)
            {
                //发布成功后，存入用户附件关联表
                await attachmentUserService.InsertAttachmentUserAsync(attachment);
            }
        }
        return attachment;
    }

    /// <summary>
    /// 发布临时附件为正式附件的 Web API 方法
    /// </summary>
    /// <param name="attachmentId">附件ID，用于标识要发布的附件</param>
    /// <param name="tenantId">租户ID，用于关联具体租户</param>
    /// <param name="friendlyName">友好名称，可选，默认为上传时的原始文件名</param>
    /// <returns>发布结果，包含附件ID或错误信息</returns>
    [HttpPost("publish")]
    public async Task<IActionResult> PublishAttachmentAsync(
        [FromQuery] string attachmentId,
        [FromQuery] string tenantId,
        [FromQuery] string? friendlyName = null)
    {

        // 检查输入参数
        if (string.IsNullOrEmpty(attachmentId))
            throw new PmSoftException("附件ID无效。");
        if (string.IsNullOrEmpty(tenantId))
            throw new PmSoftException("租户ID未指定。");

        // 调用附件管理服务的发布方法
        var attachment = await attachmentManagerService.PublishAttachmentAsync(attachmentId, tenantId, friendlyName);

        // 返回成功结果
        return Ok(new
        {
            attachment.AttachmentId,
            attachment.TenantType,
            attachment.TenantId,
            attachment.FileName,
            attachment.FriendlyName,
            attachment.ObjectName,
            attachment.BucketName,
            MediaType = attachment.MediaType.ToString(),
            attachment.MimeType,
            attachment.FileSize,
            attachment.UploaderIp,
            attachment.Description,
            attachment.IsTemporary,
            Message = "附件已发布为正式文件。"
        });

    }

    /// <summary>
    /// 根据附件ID获取附件信息及文件流的 Web API 方法
    /// </summary>
    /// <param name="attachmentId">附件ID</param>
    /// <returns>附件信息和文件流，或错误信息</returns>

    [HttpGet("{attachmentId}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetAttachment(string attachmentId)
    {
        if (string.IsNullOrEmpty(attachmentId))
            PmSoftException.Throw("附件ID无效。");

        var attachmentWithContent = await attachmentManagerService.GetAttachmentByIdAsync(attachmentId);
        if(attachmentWithContent == null)
            PmSoftException.Throw("附件不存在。");
        // 返回文件流和附件信息
        return File(attachmentWithContent.Content, attachmentWithContent.Attachment.MimeType,
            attachmentWithContent.Attachment.FriendlyName);
    }
}
