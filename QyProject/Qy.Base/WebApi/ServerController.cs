using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Utilities;
using System.Diagnostics;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class ServerController(IWebHostEnvironment hostEnvironment) : Controller
{
    private readonly IWebHostEnvironment hostEnvironment = hostEnvironment;
    [HttpGet]
    public ApiResult<object> IndexAsync()
    {
        int cpuNum = Environment.ProcessorCount;
        string computerName = Environment.MachineName;
        string osName = QySystemInfo.OSDescription;
        string osArch = QySystemInfo.OSVersion + " " + QySystemInfo.OSArchitecture.ToString();// ϵͳ�ܹ�
        string version = QySystemInfo.FrameworkDescription;
        string appRAM = ((double)Process.GetCurrentProcess().WorkingSet64 / 1048576).ToString("N2") + " MB";
        string startTime = Process.GetCurrentProcess().StartTime.ToString("yyyy-MM-dd HH:mm:ss");
        string sysRunTime = ComputerHelper.GetRunTime();
        string serverIP = Request.HttpContext.Connection.LocalIpAddress?.MapToIPv4().ToString() + ":" + Request.HttpContext.Connection.LocalPort;//��ȡ������IP
        string qyvar = QySystemInfo.Ver;

        var programStartTime = Process.GetCurrentProcess().StartTime;
        string programRunTime = DateUtility.FormatTime(Convert.ToInt64((DateTime.Now - programStartTime).TotalMilliseconds.ToString().Split('.')[0]));
        var data = new
        {
            cpu = ComputerHelper.GetComputerInfo(),
            disk = ComputerHelper.GetDiskInfos(),
            sys = new { cpuNum, computerName, osName, osArch, serverIP, runTime = sysRunTime, qyvar },
            app = new
            {
                name = hostEnvironment.EnvironmentName,
                rootPath = hostEnvironment.ContentRootPath,
                systemDirectory = QySystemInfo.SystemDirectory,
                version,
                appRAM,
                startTime,
                runTime = programRunTime,
                host = serverIP
            },
        };
        return ApiResult.Ok(data);
    }



}
