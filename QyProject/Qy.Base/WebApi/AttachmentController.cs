﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Services.Attachment;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class AttachmentController(
    AttachmentUserService  attachmentUserService
    ) : ControllerBase
{

    [HttpGet]
    public async ValueTask<IPagedList<ViewAttachmentUser>> PageAttachmentUserAsync([FromQuery] AttachmentUserQuery query)
    {
        var pageResult = await attachmentUserService.GetPageListAsync(query);
        return pageResult;
    }

    [HttpDelete]
    public async Task<ApiResult> DeleteAsync(string attachmentId)
    {
        if (await attachmentUserService.DeleteAsync(attachmentId) > 0)
        {
            return ApiResult.Ok("删除成功");
        }
        return ApiResult.Error("删除失败");
    }
}
