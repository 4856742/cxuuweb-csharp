﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;
using Qy.Base.Services.Workflows.Config;
using Qy.Base.Services.Workflows.Workflows;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class WorkflowController(
    WorkflowService workflowService,
    WorkflowCategoryConfigService workflowCategoryConfigService
    ) : ControllerBase
{
    [HttpGet]
    public async Task<ApiResult<object>> IndexAsync([FromQuery] QueryWorkflow query)
    {
        var pageResult = await workflowService.GetPagingAsync(query);
        var catList = await workflowService.GetCatAsync();
        return ApiResult.Ok(new { pageResult, catList });
    }
    [HttpPut, ActionName("EditCat")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async ValueTask<ApiResult> EditPostAsync([FromBody] List<OptionTypeDto> configs)
    {
        await workflowCategoryConfigService.SaveAsync(configs);
        return ApiResult.Ok("更新成功");
    }

    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        var catList = await workflowService.GetCatAsync();
        if (id < 1)
            return ApiResult.Ok(new { catList });
        var data = await workflowService.GetAsync(id);
        return ApiResult.Ok(new { data, catList });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> CreatePostAsync([FromBody] Workflow workflow)
    {
        var res = await workflowService.InsertAsync(workflow);
        if (res != null)
            return ApiResult.Ok("发布成功");
        return ApiResult.Error("发布失败");
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> EditAsync([FromBody] Workflow workflow)
    {
        int res = await workflowService.UpdateAsync(workflow);
        if (res > 0)
            return ApiResult.Ok("编辑成功");
        return ApiResult.Error("编辑失败");
    }

    [HttpDelete]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> Delete(int id)
    {
        if (await workflowService.DeleteByEntityIdAsync(id) > 0)
            return ApiResult.Ok($"删除成功");
        return ApiResult.Error($"删除失败");
    }

}
