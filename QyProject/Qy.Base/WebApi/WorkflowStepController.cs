﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;
using Qy.Base.Services.Workflows.Workflows;
using Qy.Base.Services.Workflows.WorkflowSteps;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class WorkflowStepController(
    WorkflowStepService workflowStepService,
    WorkflowService workflowService
    ) : ControllerBase
{

    [HttpGet]
    public async Task<ApiResult<object>> IndexAsync([FromQuery] QueryWorkflowStep query)
    {
        if (query == null || query.WorkflowId < 1)
            return ApiResult.Error("参数错误");
        var page = await workflowStepService.GetPagingAsync(query);
        return ApiResult.Ok(page);
    }
    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult<object>> ListByWorkflowIdAsync(int wflowId)
    {
        if (wflowId < 1)
            return ApiResult.Error("参数错误");
        var list = await workflowStepService.GetByWorkflowIdAsync(wflowId);
        var data = list.AsParallel();
        return ApiResult.Ok(data);
    }
    [HttpGet]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        if (id < 1)
            return ApiResult.Ok("");
        var get = await workflowStepService.GetAsync(id);
        if(get == null)
            return ApiResult.Error("数据不存在");
        ViewWorkflowStep data = get.AsView();
        return ApiResult.Ok(data);
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> CreatePostAsync([FromBody] WorkflowStep workflow)
    {
        var res = await workflowStepService.InsertAsync(workflow);
        if (res != null)
            return ApiResult.Ok("发布工作流");
        return ApiResult.Error("发布失败");
    }

    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> EditAsync([FromBody] PutEditWorkflowSteps data)
    {
        if (data.WorkflowId <= 1)
        {
            return ApiResult.Error("流程信息有误");
        }
        var workflow = await workflowService.GetAsync(data.WorkflowId);
        if(workflow == null)
            return ApiResult.Error("流程信息有误");
        int res = await workflowStepService.UpdateAsync(data.Data, workflow);
        if (res > 0)
            return ApiResult.Ok("编辑工作流");
        return ApiResult.Error("失败");
    }

    [HttpDelete]
    [MenuAuthorize(PermissionKeys.System_Management)]
    public async Task<ApiResult> Delete(int id)
    {
        if (await workflowStepService.DeleteByEntityIdAsync(id) > 0)
            return ApiResult.Ok("删除工作流");
        return ApiResult.Error("删除失败");
    }

}
