﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUserDept;

namespace Qy.Base.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class UserDeptController(
    UserDeptService userDeptService,
    IApplicationContext applicationContext
    ) : ControllerBase
{

    [HttpGet]
    public async Task<IEnumerable<ViewUserDept>> IndexAsync()
    {
        IEnumerable<ViewUserDept>? data = await userDeptService.GetUserDeptsAsync();
        return data;
    }

    [HttpGet]
    public async Task<IEnumerable<ViewUserDeptOptions>> QueryDeptListAsync([FromQuery] UserDeptQuery query)
    {
        var user = applicationContext.GetRequiredCurrentUser();
        IEnumerable<ViewUserDeptOptions> deptList = await userDeptService.GetPagingOptionsAsync(query);
        //排除当前用户
        deptList = deptList.Where(x => x.Value != user.DeptId);
        return deptList;
    }

    [HttpGet]
    public async Task<IEnumerable<OptionItemDto>> OptionTypesAsync()
    {
        var data = await userDeptService.IndentedOptionAsync();
        return data;
    }
    [HttpGet]
    public async Task<IEnumerable<OptionItemDto>> OptionTypesAuthAsync()
    {
        var user = applicationContext.GetRequiredCurrentUser();
        var data = await userDeptService.IndentedOptionAuthAsync(user);
        return data;
    }
    [HttpGet]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult<object>> GetAsync(int id)
    {
        var deptList = await userDeptService.IndentedOptionAsync();
        if (id < 1) return ApiResult.Ok(new { deptList });
        var data = await userDeptService.GetAsync(id);
        return ApiResult.Ok(new { data, deptList });
    }
    [HttpPost, ActionName("Create")]
    //[ModelStateFilter]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult> CreatePostAsync([FromBody] EditUserDept cateModel)
    {
        cateModel.GreatTime = DateTime.Now;
        object insertPri = await userDeptService.InsertAsync(cateModel);
        return ApiResult.Ok("新增部门 ID:" + insertPri.ToString());
    }
    [HttpPut, ActionName("Edit")]
    //[ModelStateFilter]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult> EditAsync([FromBody] EditUserDept editModel)
    {
        UserDept? userDept = await userDeptService.GetAsync(editModel.Id);
        if(userDept == null)
            return ApiResult.Error("部门不存在！");
        userDept.Pid = editModel.Pid;
        userDept.Name = editModel.Name;
        userDept.Type = editModel.Type;
        userDept.Sort = editModel.Sort;
        userDept.Remark = editModel.Remark;
        userDept.Status = editModel.Status;
        if (0 < await userDeptService.UpdateAsync(userDept))
            return ApiResult.Ok("修改部门:" + userDept.Name + " Id： " + editModel.Id);
        return ApiResult.Error("修改部门:" + userDept.Name + " Id： " + editModel.Id);
    }
    [HttpPut]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult> ValChangeAsync([FromBody] ValChangeModel valChangeModel)
    {
        if (await userDeptService.ValChangeAsync(valChangeModel) < 1)
            return ApiResult.Error("设置失败！");
        return ApiResult.Ok($"设置 {valChangeModel.Title} ID：{valChangeModel.Id}");
    }
    [HttpDelete, ActionName("Delete")]
    [MenuAuthorize(PermissionKeys.Auth_Edit)]
    public async Task<ApiResult> DeleteAsync(int id)
    {
        if (await userDeptService.DeleteByEntityIdAsync(id) > 0)
            return ApiResult.Ok($"ID：{id}");
        return ApiResult.Error("删除失败！当前部门已经被调用或有下级部门！");
    }
}
