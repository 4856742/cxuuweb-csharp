﻿using Qy.Base.AppBase;

namespace Qy.Base.Dtos;

public class SysConfigSysInfo
{
    public string SysName { get; set; } = string.Empty;
    public string UseUnitName { get; set; } = string.Empty;
    public string SysUrl { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Copyright { get; set; } = string.Empty;
    public string Beian { get; set; } = string.Empty;
}

public class SysUpLoadConfig
{
    public string BaseDir { get; set; } = "uploads";
    public int? ImageSize { get; set; } = 2 * 1024 * 1024;
    public string ImageExt { get; set; } = ".jpg,.gif,.png,.jpeg";
    public bool? IsImageThumb { get; set; } = false;
    public int? ImageWidth { get; set; } = 900;
    public int? ImageQuality { get; set; } = 80;
    public string FileExt { get; set; } = ".txt,.zip,.7z,.doc,.docx,.xls,.xlsx";
    public int? FileSize { get; set; } = 20 * 1024 * 1024;
    public string VideoExt { get; set; } = ".mp4,.mov";
    public int? VideoSize { get; set; } = 50 * 1024 * 1024;
}
public class SysConfigQuery : PagerInfo
{
    public string KeyWords { get; set; } = string.Empty;

}



