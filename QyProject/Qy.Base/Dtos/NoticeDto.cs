﻿using Qy.Base.Entities.Notice;
using Qy.Base.AppBase;
using System.ComponentModel.DataAnnotations;

namespace Qy.Base.Dtos;


public class EditNotice
{
    [Display(Name = "标题")]
    public string Title { get; set; } = string.Empty;

    [Display(Name = "信息内容")]
    public string Content { get; set; } = string.Empty;
    [Display(Name = "附件")]
    public string Attments { get; set; } = string.Empty;
    [Display(Name = "收件人")]
    [Required(ErrorMessage = "收件人未选择")]
    public string GetUid { get; set; } = string.Empty;
    public int Type { get; set; }

}

public class ViewNoticeKey
{
    public int Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public int Type { get; set; }
    public string Content { get; set; } = string.Empty;
    public bool Status { get; set; }
    public int GetUid { get; set; }
    public int PostUid { get; set; }
    public string Attments { get; set; } = string.Empty;
    public DateTime PostTime { get; set; }
    public DateTime ReadTime { get; set; }
    public string GetNickName { get; set; } = string.Empty;
    public string PostAvatar { get; set; } = string.Empty;
    public string GetUserAvatar { get; set; } = string.Empty;
    public string PostNickName { get; set; } = string.Empty;
    public int NoticeId { get; set; }
}
public static partial class NoticeExtensions
{
    public static ViewNoticeKey AsView(this NoticeKey data)
    {
        return new ViewNoticeKey
        {
            Id = data.Id,
            NoticeId = data.Nid,
            GetUid = data.GetUid,
            PostUid = data.PostUid,
            Status = data.Status,
            ReadTime = data.ReadTime,
        };
    }
}
public class NoticeQuery : PagerInfo
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public string KeyWords { get; set; } = string.Empty;
    public bool? Status { get; set; } 
    public int ListType { get; set; }//空或1为收到消息列表,2是已发消息列表
}
