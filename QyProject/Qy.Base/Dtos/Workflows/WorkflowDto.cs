﻿using Qy.Base.Entities.Workflows;
using Qy.Base.AppBase;

namespace Qy.Base.Dtos.Workflows;

public class ViewWorkflow
{
    public int Id { get; set; }
    public int? Cid { get; set; }
    public string CatName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string DeptName { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public DateTime CreatedTime { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public bool IsActive { get; set; }
}

/// <summary>
/// 查询模型
/// </summary>
public class QueryWorkflow : PagerInfo
{
    public int Cid { get; set; }
    public string Name { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
}


public static partial class WorkflowExtensions
{
    public static ViewWorkflow AsView(this Workflow data)
    {
        return new ViewWorkflow
        {
            Id = data.Id,
            Name = data.Name,
            Cid = data.Cid,
            Remark = data.Remark,
            CreatedTime = data.CreatedTime,
            Uid = data.Uid,
            Did = data.Did,
            IsActive = data.IsActive
        };
    }
}