﻿using Qy.Base.Dtos.Workflows;
using Qy.Base.Entities.Workflows;
using Qy.Base.AppBase;

namespace Qy.Base.Dtos.Workflows;

/// <summary>
/// 工作流审核行为类型
/// </summary>
public enum WorkflowApproverType
{
    None = 1,
    Dept = 2,
    Post = 3,
    Group = 4,
    User = 5,
}

public class ViewWorkflowStep
{
    public int Id { get; set; }
    public int Pid { get; set; }
    public string Type { get; set; } = string.Empty;
    public int WorkflowId { get; set; } // 关联 Workflow
    public string Name { get; set; } = string.Empty;
    public int StepOrder { get; set; } // 步骤顺序
    public string Remark { get; set; } = string.Empty;
    public WorkflowStepProps? Props { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public string WorkflowName { get; set; } = string.Empty;

}

public static partial class WorkflowInstanceExtensions
{
    public static ViewWorkflowStep AsView(this WorkflowStep data)
    {
        return new ViewWorkflowStep
        {
            Id = data.Id,
            Pid = data.Pid,
            Type = data.Type,
            WorkflowId = data.WorkflowId,
            StepOrder = data.StepOrder,
            Name = data.Name,
            Remark = data.Remark,
            Uid = data.Uid,
            Did = data.Did,
        };
    }

    public static ViewWorkflowStep AsNew(WorkflowStep data)
    {
        return new ViewWorkflowStep
        {
            Id = data.Id,
            Pid = data.Pid,
            Type = data.Type,
            WorkflowId = data.WorkflowId,
            Name = data.Name,
            Remark = data.Remark,
            StepOrder = data.StepOrder,
            Props = new WorkflowStepProps
            {
                Mode = data.Mode,
                RuleType = data.RuleType,
                TaskMode = data.TaskMode,
                NeedSign = data.NeedSign,
                AssignUser = data.AssignUser,
                SelectMultiple = data.SelectMultiple,
                Leader = data.Leader,
                AssignDept = data.AssignDept,
                AssignDeptTop = data.AssignDeptTop,
                AssignRole = data.AssignRole,
                NoUserHandler = data.NoUserHandler,
                SameRoot = data.SameRoot,
                Timeout = data.Timeout
            }
        };
    }

    public static IEnumerable<ViewWorkflowStep> AsPropsList(IEnumerable<WorkflowStep> datas)
    {
        return datas.Select(data => new ViewWorkflowStep
        {
            Id = data.Id,
            Pid = data.Pid,
            Type = data.Type,
            WorkflowId = data.WorkflowId,
            Name = data.Name,
            Remark = data.Remark,
            StepOrder = data.StepOrder,
            Props = new WorkflowStepProps
            {
                Mode = data.Mode,
                RuleType = data.RuleType,
                TaskMode = data.TaskMode,
                NeedSign = data.NeedSign,
                AssignUser = data.AssignUser,
                SelectMultiple = data.SelectMultiple,
                Leader = data.Leader,
                AssignDept = data.AssignDept,
                AssignDeptTop = data.AssignDeptTop,
                AssignRole = data.AssignRole,
                NoUserHandler = data.NoUserHandler,
                SameRoot = data.SameRoot,
                Timeout = data.Timeout
            }
        }
        );

    }
}
public class WorkflowStepProps
{
    public int Mode { get; set; }
    public int RuleType { get; set; }
    public string TaskMode { get; set; } = string.Empty;
    public bool NeedSign { get; set; }
    public string AssignUser { get; set; } = string.Empty;
    public bool SelectMultiple { get; set; }
    public string Leader { get; set; } = string.Empty;
    public string AssignDept { get; set; } = string.Empty;
    public string AssignDeptTop { get; set; } = string.Empty;
    public string AssignRole { get; set; } = string.Empty;
    public string NoUserHandler { get; set; } = string.Empty;
    public string SameRoot { get; set; } = string.Empty;
    public string Timeout { get; set; } = string.Empty;
}
public class PutEditWorkflowSteps
{
    public string Data { get; set; } = string.Empty;
    public int WorkflowId { get; set; }
}
/// <summary>
/// 查询模型
/// </summary>
public class QueryWorkflowStep : PagerInfo
{
    public string Name { get; set; } = string.Empty;
    public int WorkflowId { get; set; }
}