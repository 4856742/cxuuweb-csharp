﻿using Qy.Base.AppBase;
using Qy.Base.Entities.Workflows;

namespace Qy.Base.Dtos.Workflows;

public enum WorkflowStatus
{
    Draft,      // 草稿
    Running,    // 进行中
    Approved,   // 通过
    Rejected,   // 拒绝
    Cancelled   // 取消
}
public class ViewWorkflowInstance
{
    public int Id { get; set; }
    public int WorkflowId { get; set; } // 关联 Workflow
    public int StepId { get; set; } // 当前步骤序号
    // 关联业务模型
    public ProjectType ModuleType { get; set; }
    // 关联业务数据（例如：订单ID、请假单ID）
    public int BusinessId { get; set; }
    public WorkflowStatus WorkflowStatus { get; set; } // 流程状态（进行中、完成、拒绝等）
    public DateTime StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public string WorkflowName { get; set; } = string.Empty;
    public string StepName { get; set; } = string.Empty;

    
}
public static partial class ViewWorkflowInstanceExtensions
{
    public static ViewWorkflowInstance AsView(this WorkflowInstance data)
    {
        return new ViewWorkflowInstance
        {
            Id = data.Id,
            WorkflowId = data.WorkflowId,
            StepId = data.StepId,
            ModuleType = data.ModuleType,
            BusinessId = data.BusinessId,
            WorkflowStatus = data.WorkflowStatus,
            StartTime = data.StartTime,
            EndTime = data.EndTime,
        };
    }
}
/// <summary>
/// 查询模型
/// </summary>
public class QueryWorkflowInstance : PagerInfo
{
    public int ModuleType { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}
