﻿using Qy.Base.AppBase;

namespace Qy.Base.Dtos.Workflows;

public enum WorkflowRecordStatus
{
    Pending,    // 待处理
    Approved,   // 通过
    Rejected,   // 拒绝
    Timeout     // 超时
}

/// <summary>
/// 查询模型
/// </summary>
public class QueryWorkflowRecord : PagerInfo
{
}