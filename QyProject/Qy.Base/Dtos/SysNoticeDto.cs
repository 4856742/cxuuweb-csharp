﻿using Qy.Base.Entities;
using Qy.Base.AppBase;

namespace Qy.Base.Dtos;

public class ViewSysNotice 
{
    public int Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public bool Status { get; set; }
    public string Content { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public int Uid { get; set; }
    public string Attments { get; set; } = string.Empty;
    public DateTime GreatTime { get; set; }


}
public static partial class LogSysExtensions
{
    public static ViewSysNotice AsView(this SysNotice data)
    {
        return new ViewSysNotice
        {
            Id = data.Id,
            Status = data.Status,
            Title = data.Title,
            GreatTime = data.GreatTime,
            Content = data.Content,
            Attments = data.Attments,
        };
    }
}

public class SysNoticeQuery : PagerInfo
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public string KeyWords { get; set; } = string.Empty;
    public bool? Status { get; set; } 
}
