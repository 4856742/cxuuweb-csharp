﻿namespace Qy.Base.Dtos;
public class OptionTypeDto
{
    public int? Value { get; set; }
    public string? Label { get; set; }
}