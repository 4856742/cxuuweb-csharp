﻿using Qy.Base.Entities.Loging;

namespace Qy.Base.Dtos.Loging;

public class ViewLogSys
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public string? ContrAct { get; set; }
    public string? Method { get; set; }
    public string? Ip { get; set; }
    public string? Localtion { get; set; }
    public DateTime? Time { get; set; }
    public string? DoThing { get; set; }
    public string? DeptName { get; set; }
    public string? NickName { get; set; }
    public bool Status { get; set; }
}

public static partial class LogSysExtensions
{
    public static ViewLogSys AsView(this LogSys data)
    {
        return new ViewLogSys
        {
            Id = data.Id,
            Uid = data.Uid,
            Did = data.Did,
            Time = data.Time,
            Ip = data.Ip,
            Localtion = data.Ip,
            ContrAct = data.ContrAct,
            Method = data.Method,
            DoThing = data.DoThing,
        };
    }
}