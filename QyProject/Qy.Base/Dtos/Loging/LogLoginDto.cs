﻿using Qy.Base.Entities.Loging;

namespace Qy.Base.Dtos.Loging;

public class ViewLogLogin
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public string? Ip { get; set; }
    public string? Localtion { get; set; }
    public string? UserAgent { get; set; }
    public string? UserName { get; set; }
    public string? NickName { get; set; }
    public string? DeptName { get; set; }
    public DateTime? Time { get; set; }
}

public static partial class LogSysExtensions
{
    public static ViewLogLogin AsView(this LogLogin data)
    {
        return new ViewLogLogin
        {
            Id = data.Id,
            Uid = data.Uid,
            Time = data.Time,
            Ip = data.Ip,
            Localtion = data.Ip,
            UserAgent = data.UserAgent,
        };
    }
}