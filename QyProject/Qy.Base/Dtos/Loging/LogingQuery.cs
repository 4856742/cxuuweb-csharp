﻿using Qy.Base.AppBase;

namespace Qy.Base.Dtos.Loging;

public class LogingQuery : PagerInfo
{
    public string Fields { get; set; } = string.Empty;
    public string SelectUids { get; set; } = string.Empty;
    public int? SelectDid { get; set; }
    public string Method { get; set; } = string.Empty;
    public bool? Status { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
}