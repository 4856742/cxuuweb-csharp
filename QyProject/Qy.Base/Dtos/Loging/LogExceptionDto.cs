﻿using Qy.Base.Entities.Auth;
using Qy.Base.Entities.Loging;

namespace Qy.Base.Dtos.Loging;


public class ViewLogException
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public string? ContrAct { get; set; }
    public string? Method { get; set; }
    public string? Ip { get; set; }
    public string? Localtion { get; set; }
    public DateTime? Time { get; set; }
    public string? StackTrace { get; set; }
    public string? Message { get; set; }
    public string? DeptName { get; set; }
    public string? NickName { get; set; }
    
}

public static partial class LogExceptionExtensions
{
    public static ViewLogException AsView(this LogException data)
    {
        return new ViewLogException
        {
            Id = data.Id,
            Uid = data.Uid,
            Did = data.Did,
            Time = data.Time,
            Ip = data.Ip,
            Localtion = data.Ip,
            ContrAct = data.ContrAct,
            Method = data.Method,
            //StackTrace = log.StackTrace,
        };
    }
    public static ViewLogException AsOne(this LogException log, User user, UserDept dept)
    {
        return new ViewLogException
        {
            Id = log.Id,
            Uid = log.Uid,
            Did = log.Did,
            Time = log.Time,
            Ip = log.Ip,
            Localtion = log.Ip,
            ContrAct = log.ContrAct,
            Method = log.Method,
            StackTrace = log.StackTrace,
            Message = log.Message,
            NickName = user.NickName,
            DeptName = dept.Name
        };
    }
}

