﻿namespace Qy.Base.Dtos.Menu;

public class ViewQyMenuCat
{
    public int Id { get; set; }
    public int Pid { get; set; }
    public string? Name { get; set; }
    public string? Title { set; get; }
    public int Sort { get; set; }
    public string? Remark { get; set; }
    public string? Icon { get; set; }
    public string? ClientUrl { get; set; }
    public string? ClientId { get; set; }
    public string? ClientSecret { get; set; }
    public int Depth { get; set; }
    public int Type { get; set; }
    public bool Spread { get; set; }
    public int ChildrenCount { get; set; }
    public bool HaveChild { get; set; }
    public bool Open { get; set; }
    public List<ViewQyMenuCat> Children { set; get; } = [];

}

public class EditQyMenuCat
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Remark { get; set; }
    public int Pid { get; set; }
    public int Type { get; set; }
    public string? Icon { get; set; }
    public string? ClientUrl { get; set; }
    public string? ClientId { get; set; }
    public string? ClientSecret { get; set; }
    public int Sort { get; set; }
    public string? SubmitDept { get; set; }
}

public class MenuCatOptionType
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int Pid { get; set; }
    public int Type { get; set; }
}
