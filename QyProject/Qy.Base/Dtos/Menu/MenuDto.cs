﻿using PmSoft.Core.Domain.Auth;

namespace Qy.Base.Dtos.Menu;

public class ViewMenu : IMenu
{
    public int Id { get; set; }
    public int Cid { get; set; }
    public int Pid { get; set; }
    public int Type { get; set; }
    public string Name { get; set; } = string.Empty;
    public int Sort { get; set; }
    public string Path { get; set; } = string.Empty;
    public string Component { get; set; } = string.Empty;
    public string PermCode { get; set; } = string.Empty;
    public string Icon { get; set; } = string.Empty;
    public bool KeepAlive { get; set; }
    public bool AlwaysShow { get; set; }
    public bool Status { get; set; }
    public string Redirect { get; set; } = string.Empty;
    public bool Hide { get; set; }
    public List<ViewMenu> Children { set; get; } = [];
    int? IMenu.ParentId => Pid;
    int IMenu.MenuId => Id;
    string IMenu.Title => Name;
}

public class AsMenu
{
    public int MenuId { get; set; }
    public string Path { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public AsMenuMeta? Meta { get; set; }
    public int Type { get; set; }
    public string Component { get; set; } = string.Empty;
    public int Sort { get; set; }
    public string Redirect { get; set; } = string.Empty;
    public List<AsMenu> Children { set; get; } = [];
}

public class AsMenuMeta
{
    public string Icon { get; set; } = string.Empty;
    public string Title { get; set; } = string.Empty;
    public bool KeepAlive { get; set; }
    public bool Hidden { get; set; }
    public bool AlwaysShow { get; set; }
}


public class MenuOptionType
{
    public int Cid { get; set; }
    public int Type { get; set; }
    public int Value { get; set; }
    public string Label { get; set; } = string.Empty;
    public List<MenuOptionType> Children { set; get; } = [];
}