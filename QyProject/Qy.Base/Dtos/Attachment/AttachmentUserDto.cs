﻿using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.AppBase;
using Qy.Base.Entities.Attachment;

namespace Qy.Base.Dtos.Attachment;
public class ViewAttachmentUser : IAttachment
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    /// <summary>
    /// 租户类型，例如 "Company" 或 "User"
    /// </summary>
    public string TenantType { get; set; } = string.Empty;
    /// <summary>
    /// 租户ID，与业务实体关联
    /// </summary>
    public string? TenantId { get; set; }
    /// <summary>
    /// 附件ID，使用 GUID 唯一标识
    /// </summary>
    public string AttachmentId { get; set; } = string.Empty;
    /// <summary>
    /// 原始文件名，例如 "document.pdf"
    /// </summary>
    public string FileName { get; set; } = string.Empty;
    /// <summary>
    /// 存储桶名称，用于区分存储空间
    /// </summary>
    public string BucketName { get; set; } = string.Empty;
    /// <summary>
    /// 对象名称，包含文件路径，例如 "files/document.pdf"
    /// </summary>
    public string ObjectName { get; set; } = string.Empty;
    /// <summary>
    /// 用户自定义的友好名称，可选
    /// </summary>
    public string FriendlyName { get; set; } = string.Empty;
    /// <summary>
    /// 媒体类型，使用枚举定义，例如 Image, Document
    /// </summary>
    public MediaType MediaType { get; set; }
    /// <summary>
    /// MIME 类型，例如 "application/pdf"
    /// </summary>
    public string MimeType { get; set; } = string.Empty;
    /// <summary>
    /// 文件大小，单位：字节
    /// </summary>
    public long FileSize { get; set; }
    /// <summary>
    /// 上传者的 IP 地址
    /// </summary>
    public string UploaderIp { get; set; } = string.Empty;
    /// <summary>
    /// 附件描述，可选
    /// </summary>
    public string? Description { get; set; }
    /// <summary>
    /// 是否已删除，默认为 false
    /// </summary>
    public bool IsDeleted { get; set; } = false;
    /// <summary>
    /// 是否为临时附件，默认为 true
    /// </summary>
    public bool IsTemporary { get; set; } = true;
    public DateTime CreateTime { get; set; }
    public string? DeleteToken { get; set; }
    public string? NickName { get; set; }
    public string? DeptName { get; set; }
}

public static partial class AttachmentExtensions
{
    public static ViewAttachmentUser AsViewAttachmentUser(this AttachmentUser data)
    {
        return new ViewAttachmentUser
        {
            Id = data.Id,
            AttachmentId = data.AttachmentId,
            Did = data.Did,
            Uid = data.Uid,
        };
    }

    public static ViewAttachmentUser AsViewAttachmentUser(this AttachmentUser data, IEnumerable<SysAttachment> sysAttachments)
    {
        var attachment = sysAttachments.FirstOrDefault(x => x.Id == data.AttachmentId);
        return new ViewAttachmentUser
        {
            Id = data.Id,
            AttachmentId = data.AttachmentId,
            Did = data.Did,
            Uid = data.Uid,
            TenantType = attachment?.TenantType ?? "",
            TenantId = attachment?.TenantId,
            FileName = attachment?.FileName ?? "",
            ObjectName = attachment?.ObjectName ?? "",
            FriendlyName = attachment?.FriendlyName ?? "",
            FileSize = attachment?.FileSize ?? 0,
            CreateTime = attachment?.CreateTime ?? DateTime.MinValue,
        };
    }
}

/// <summary>
/// 查询模型
/// </summary>
public class AttachmentUserQuery : PagerInfo
{
    public string? FriendlyName { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
}


