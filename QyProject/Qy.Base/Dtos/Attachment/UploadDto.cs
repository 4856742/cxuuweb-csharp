﻿using Microsoft.AspNetCore.Http;

namespace Qy.Base.Dtos.Attachment;

public class UploadDto
{
    public UploadDtoData? Data { get; set; } 
    public string Headers { get; set; } = string.Empty;
    public List<IFormFile> File { get; set; } = [];
    public byte[] Bytes { get; set; } = [];
}
public class UploadDtoData
{
    public string ChunkIndex { get; set; } = string.Empty;
    public string TotalChunks { get; set; } = string.Empty;
    public string TotalSize { get; set; } = string.Empty;
    public string FileName { get; set; } = string.Empty;
    /// <summary>
    /// 租户类型  前端以 系统模块区分  如:sys,cms,rpc等字符串
    /// </summary>
    public string ClassName { get; set; } = string.Empty;
    //判断上传文件的处理类型 如： is_images_source 进行缩略处理 
    public string ProcessType { get; set; } = string.Empty;
    public string IsPublish { get; set; } = string.Empty;
    public string OriginalFileName { get; set; } = string.Empty;
}
