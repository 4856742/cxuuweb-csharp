﻿namespace Qy.Base.Dtos;

public class RefreshTokenIn
{
	public string AccessToken { get; set; }
	public string RefreshToken { get; set; }
}
