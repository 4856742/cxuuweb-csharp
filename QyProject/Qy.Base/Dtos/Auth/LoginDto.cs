﻿using System.ComponentModel.DataAnnotations;

namespace Qy.Base.Dtos.Auth;

/// <summary>
/// 用户登录
/// </summary>
public class LoginDto
{
    /// <summary>
    /// 登录名
    /// </summary>
    public string Account { get; set; } = string.Empty;
    /// <summary>
    /// 密码
    /// </summary>
    [Required(ErrorMessage = "请填写登录密码")]
    public string PassWord { get; set; } = string.Empty;
    /// <summary>
    /// 持久登录
    /// </summary>
    public bool Remember { get; set; }
    /// <summary>
    /// 验证码
    /// </summary>
    public string ValidCode { get; set; } = string.Empty;
    /// <summary>
    ///  唯一标识
    /// </summary>
    public string Uuid { get; set; } = "";
    public string ClientId { get; set; } = string.Empty;
}
