﻿using Qy.Base.AppBase;
using Qy.Base.Utilities.Const;
using System.ComponentModel.DataAnnotations;

namespace Qy.Base.Dtos.Auth;

public class EditUserDept
{
    public int Id { get; set; }
    [Required(ErrorMessage = "请输入{0}")]
    [Display(Name = "上级部门")]
    public int Pid { get; set; }
    [Required(ErrorMessage = "请输入{0}")]
    [Display(Name = "部门名称")]
    public string Name { get; set; }
    [Display(Name = "部门排序")]
    [RegularExpression(RegxPatterns.Number, ErrorMessage = "{0}必须为数字")]
    public int Sort { get; set; }
    public int Type { get; set; }
    public DateTime GreatTime { get; set; }
    public string Remark { get; set; }
    public bool Status { get; set; }
}


public class ViewUserDept
{
    public int Id { get; set; }
    public int Pid { get; set; }
    public int Type { get; set; }
    public string Name { get; set; }
    public int Sort { get; set; }
    public string Remark { get; set; }
    public bool HaveChild { get; set; }
    public bool Open { get; set; }
    public bool Status { get; set; }
    public List<ViewUserDept> Children { set; get; } = [];
}

public class ViewUserDeptLittle
{
    public int Id { get; set; }
    public string Name { get; set; }
}

public class CmsCatRoles
{
    public int Id { get; set; }
    [Display(Name = "网站栏目权限")]
    public string CmsCatRole { get; set; }
    public string Name { get; set; }
}
public class ViewUserDeptOptions
{
    public int Value { get; set; }
    public string Label { get; set; }
}
public class WorkCatRoles
{
    public int Id { get; set; }
    [Display(Name = "业务工作权限")]
    public string WorkCatRole { get; set; }
    public string Name { get; set; }
}
public class DocReceivedCatRoles
{
    public int Id { get; set; }
    [Display(Name = "收文类别权限")]
    public string DocReceivedCatRole { get; set; }
    public string Name { get; set; }
}
public class DocStatementCatRoles
{
    public int Id { get; set; }
    [Display(Name = "台账登记权限")]
    public string DocStatementCatRole { get; set; }
    public string Name { get; set; }
}
public class UserDeptQuery : PagerInfo
{
    public string KeyWords { get; set; }
}