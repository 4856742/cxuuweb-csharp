﻿using Qy.Base.Entities.Auth;

namespace Qy.Base.Dtos.Auth.UserDto;


public class LoginUserBaseInfo()
{
    public int UserId { get; set; }
    public string NickName { get; set; } = string.Empty;
    public string Avatar { get; set; } = string.Empty;
    public string Mobile { get; set; } = string.Empty;
    public UserTypeEnum UserType { get; set; }
}
