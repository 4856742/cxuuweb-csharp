﻿using Qy.Base.Attributes;
using Qy.Base.Entities.Auth;
using Qy.Base.Utilities.Const;
using System.ComponentModel.DataAnnotations;

namespace Qy.Base.Dtos.Auth.UserDto;
public class EditUser
{
    public int Id { get; set; }
    [Required(ErrorMessage = "请输入{0}")]
    [Display(Name = "所属部门")]
    public int DeptId { get; set; }
    [Required(ErrorMessage = "请输入{0}")]
    [Display(Name = "用户类型")]
    public UserTypeEnum Type { get; set; }
    [Required(ErrorMessage = "请输入{0}")]
    [Display(Name = "角色")]
    public string Roles { get; set; } = string.Empty;
    [Display(Name = "登录名")]
    [Required(ErrorMessage = "请输入{0}")]
    [RegularExpression(RegxPatterns.EnOrNumber, ErrorMessage = "{0}英文下划线或数字")]
    [StringLength(20, MinimumLength = 4, ErrorMessage = "{0}至少{2}位,并且不能超过{1}位")]
    public string UserName { get; set; } = string.Empty;
    [Display(Name = "身份证号")]
    [ValidateIfNotEmpty(18, 18, ErrorMessage = "身份证号长度不正确")]
    public string IdNumber { get; set; } = string.Empty;
    [Display(Name = "密码")]
    [ValidateIfNotEmpty(6, 50, ErrorMessage = "密码至少6位,并且不能超过50位")]
    public string PassWord { get; set; } = string.Empty;
    [Display(Name = "旧密码")]
    public string OldPassword { get; set; } = string.Empty;
    [Display(Name = "姓名")]
    [RegularExpression(RegxPatterns.Chs, ErrorMessage = "{0}必须为中文")]
    [StringLength(16, MinimumLength = 2, ErrorMessage = "{0}至少{2}位,并且不能超过{1}位")]
    [Required(ErrorMessage = "请输入{0}")]
    public string NickName { get; set; } = string.Empty;
    [Display(Name = "用户状态")]
    [Required(ErrorMessage = "请选择用户状态")]
    public bool Status { get; set; } = true;
    [Display(Name = "头像")]
    public string Avatar { get; set; } = string.Empty;
    [Display(Name = "联系电话")]
    public string Mobile { get; set; } = string.Empty;
    public string PinyinName { get; set; } = string.Empty;
    public string PyName { get; set; } = string.Empty;
}


public static partial class UserExtension
{
    public static EditUser ToEditUser(this User user)
    {
        return new EditUser
        {
            Id = user.Id,
            DeptId = user.DeptId,
            UserName = user.UserName,
            IdNumber = user.IdNumber,
            NickName = user.NickName,
            Avatar = user.Avatar,
            Mobile = user.Mobile,
            Status = user.Status,
            PyName = user.PyName,
            PinyinName = user.PinyinName,
        };
    }
}