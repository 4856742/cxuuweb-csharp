﻿using Qy.Base.AppBase;

namespace Qy.Base.Dtos.Auth.UserDto;

public class UserQuery : PagerInfo
{
    public string Ids { get; set; } = string.Empty;
    public string Fields { get; set; } = string.Empty;
    public int Type { get; set; } = 0;
    public int DeptId { get; set; } = 0;
    public bool? Status { get; set; }
    public string KeyWords { get; set; } = string.Empty;
    public string UserName { get; set; } = string.Empty;
    public string Mobile { get; set; } = string.Empty;
    public bool IsLimitDept { get; set; } = false;
}