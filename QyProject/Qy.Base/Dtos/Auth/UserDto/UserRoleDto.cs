﻿using PmSoft.Core.Domain.Auth;

namespace Qy.Base.Dtos.Auth.UserDto;

public class UserRoleDto : IRole
{
	public int Id { get; set; }

	public string Name { get; set; } = string.Empty;

	public string Code { get; set; } = string.Empty;
}
