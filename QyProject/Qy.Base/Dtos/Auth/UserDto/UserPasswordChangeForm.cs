﻿using System.ComponentModel.DataAnnotations;

namespace Qy.Base.Dtos.Auth.UserDto;
public class UserPasswordChangeForm
{
    [Display(Name = "旧密码")]
    public string OldPassword { get; set; } = string.Empty;
    [Display(Name = "新密码")]
    public string NewPassword { get; set; } = string.Empty;
}