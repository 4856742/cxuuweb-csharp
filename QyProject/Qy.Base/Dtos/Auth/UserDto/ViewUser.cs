﻿using PmSoft.Core.Domain.Auth;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.InterFace.User;

namespace Qy.Base.Dtos.Auth.UserDto;

public class ViewUser : IViewUser
{
    public int Id { get; set; }
    public int DeptId { get; set; }
    public UserTypeEnum Type { get; set; }
    public string? DeptName { get; set; }
    public AccessLevelType? AccessLevel { get; set; }
    public string? IdNumber { get; set; }
    public string? UserName { get; set; }
    public string? NickName { get; set; }
    public string? PinyinName { get; set; }
    public string? PyName { get; set; }
    public bool? Status { get; set; }
    public string? Avatar { get; set; }
    public string? Mobile { get; set; }
    public int? OrderNum { get; set; }
    public DateTime? CreateTime { get; set; }
    public IEnumerable<IRole> Roles { get; set; } = [];
}


public static partial class UserExtension
{
    public static ViewUser AsViewUser(this User user)
    {
        return new ViewUser
        {
            Id = user.Id,
            DeptId = user.DeptId,
            UserName = user.UserName,
            IdNumber = user.IdNumber,
            NickName = user.NickName,
            Avatar = user.Avatar,
            Mobile = user.Mobile,
            Status = user.Status,
            PyName = user.PyName,
            Type = user.Type,
            PinyinName = user.PinyinName,
            CreateTime = user.CreateTime,
        };
    }
    public static ViewUser ToEditView(this User user, IEnumerable<UserRole> userRoles)
    {
        return new ViewUser
        {
            Id = user.Id,
            DeptId = user.DeptId,
            UserName = user.UserName,
            IdNumber = user.IdNumber,
            NickName = user.NickName,
            Avatar = user.Avatar,
            Mobile = user.Mobile,
            Status = user.Status,
            PyName = user.PyName,
            PinyinName = user.PinyinName,
            CreateTime = user.CreateTime,
            Type = user.Type,
            Roles = userRoles.Select(x => new UserRoleDto { Id = x.Id, Name = x.Name })
        };
    }
    public static ViewUser AsWithDept(this User user, IEnumerable<UserDept> depts)
    {
        return new ViewUser
        {
            DeptName = depts.GetDepartmentHierarchyPath(user.DeptId, 5, "/"),
            NickName = user.NickName ?? "",
            Id = user.Id,
            Avatar = user.Avatar ?? "",
        };
    }
}