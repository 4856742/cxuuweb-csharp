﻿using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;

namespace Qy.Base.Dtos.Auth.UserDto;

public class ViewUserOptions
{
    public int UserId { get; set; }
    public string NickName { get; set; } = string.Empty;
    public string DeptName { get; set; } = string.Empty;
    public string Mobile { get; set; } = string.Empty;
    public string Avatar { get; set; } = string.Empty;
    public static ViewUserOptions AsNewList(User user, IEnumerable<UserDept> depts)
    {
        return new ViewUserOptions
        {
            UserId = user.Id,
            NickName = user.NickName,
            DeptName = depts.FirstOrDefault(d => d.Id == user.DeptId)?.Name ?? ""
        };
    }

    public static ViewUserOptions AsOne(User user, IEnumerable<UserDept> depts)
    {
        return new ViewUserOptions
        {
            UserId = user.Id,
            NickName = user.NickName,
            Mobile = user.Mobile,
            Avatar = user.Avatar,
            DeptName = depts.GetDepartmentHierarchyPath(user.DeptId)
        };
    }
}
