﻿namespace Qy.Base.Dtos.UserDto;

public class UserCreateInput
{
    public string Name { get; set; }

    public string Email { get; set; }
}
