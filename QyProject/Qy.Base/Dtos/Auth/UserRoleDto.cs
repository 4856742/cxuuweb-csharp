﻿using Qy.Base.Entities.Auth;

namespace Qy.Base.Dtos.Auth;
public class EditUserRole
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public bool? IsKeep { get; set; }
    public bool? IsEditOthersData { get; set; }
    public string Remark { get; set; } = string.Empty;
    public string MenuRole { get; set; } = string.Empty;
    public string AuthDeptIds { get; set; } = string.Empty;
    public AccessLevelType AccessLevel { get; set; }
    public int Sort { get; set; }
    public DateTime CreateTime { get; set; }
}

public class ViewUserRole
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public bool? IsKeep { get; set; }
    public bool? IsEditOthersData { get; set; }
    public string Remark { get; set; } = string.Empty;
    public string MenuRole { get; set; } = string.Empty;
    public string AuthDeptIds { get; set; } = string.Empty;
    public AccessLevelType AccessLevel { get; set; }
    public int Sort { get; set; }
    public DateTime CreateTime { get; set; }
    public IEnumerable<int> UserRoleRelMenus { get; set; } = [];
}
public static class UserRoleDtoExtensions
{
    public static UserRole EditToUserRole(this EditUserRole userRole)
    {
        return new UserRole
        {
            Id = userRole.Id,
            Name = userRole.Name,
            IsKeep = userRole.IsKeep,
            IsEditOthersData = userRole.IsEditOthersData,
            Remark = userRole.Remark,
            AuthDeptIds = userRole.AuthDeptIds,
            AccessLevel = userRole.AccessLevel,
            Sort = userRole.Sort,        
            CreateTime = userRole.CreateTime
        };
    }
    public static ViewUserRole ToViewUserRole(this UserRole userRole, IEnumerable<UserRoleRelMenu> userRoleRelMenus)
    {
        return new ViewUserRole
        {
            Id = userRole.Id,
            Name = userRole.Name,
            IsKeep = userRole.IsKeep,
            IsEditOthersData = userRole.IsEditOthersData,
            Remark = userRole.Remark,
            AuthDeptIds = userRole.AuthDeptIds,
            AccessLevel = userRole.AccessLevel,
            Sort = userRole.Sort,
            CreateTime = userRole.CreateTime,
            UserRoleRelMenus = userRoleRelMenus.Select(x => x.MenuId)
        };
    }
}