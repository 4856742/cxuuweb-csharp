﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsConfig)]
[CacheSetting(true)]
[Description("CMS配置表")]
[Auditable(ProjectType.Cms)]

public class CmsConfig : IEntity<int>
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public string Label { get; set; }
    [Column("value_data")]
    public string ValueData { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}

