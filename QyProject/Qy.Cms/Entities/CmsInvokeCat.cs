﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsInvokeCat)]
[CacheSetting(true)]
[Description("CMS通用调用类别表")]
[Auditable(ProjectType.Cms)]


public class CmsInvokeCat : IEntity<int>, ICategoryEntity, IHierarchicalEntity
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Remark { get; set; }
    public string Url { get; set; }
    public int Pid { get; set; }
    public int Type { get; set; }
    public int Sort { get; set; }
    public int InvokeNum { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
