﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsContentHit)]
[CacheSetting(true)]
[Description("CMS内容访问记录统计表")]
//[Auditable(ProjectType.Cms)]


public class ContentHit : IEntity<int>
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public int Aid { get; set; }
    public int Hits { get; set; }
    public int Likes { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}