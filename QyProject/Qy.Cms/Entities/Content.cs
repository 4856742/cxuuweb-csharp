﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsContent)]
[CacheSetting(true, PropertyNamesOfArea = "Cid")]
[Description("CMS内容表")]
[Auditable(ProjectType.Cms)]

public class Content : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public ContentTypeEnum Type { get; set; }
    public int Cid { get; set; }
    public string Title { get; set; }
    public string Abstract { get; set; }
    public string Examine { get; set; }
    [Column("cover_url")]
    public string CoverUrl { get; set; }
    [Column("cover_count")]
    public int CoverCount { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public bool? Status { get; set; }
    [Column("att_a")]
    public bool? AttA { get; set; }
    [Column("att_b")]
    public bool? AttB { get; set; }
    [Column("att_c")]
    public bool? AttC { get; set; }
    [Column("create_time")]
    public DateTime CreateTime { get; set; } = DateTime.Now;
    [Column("edit_time")]
    public DateTime EditTime { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}

