﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsOnduty)]
[CacheSetting(true)]
[Description("值班安排表")]
[Auditable(ProjectType.Cms)]

public class CmsOnduty : IEntity<int>
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    [Column("date_in_month")]
    public int DateInMonth { get; set; }
    [Column("date_time")]
    public DateTime DateTime { get; set; }
    [Column("week_date")]
    public int? WeekDate { get; set; }
    public string Leader { get; set; }
    [Column("leader_phone")]
    public string LeaderPhone { get; set; }
    public string Dutyer { get; set; }
    [Column("dutyer_phone")]
    public string DutyerPhone { get; set; }
    [Column("insert_time")]
    public DateTime InsertTime { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
