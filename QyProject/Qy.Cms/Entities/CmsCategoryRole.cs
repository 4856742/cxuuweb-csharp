﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsCategoryRole)]
[CacheSetting(true)]
[Description("CMS类别权限表")]
//[Auditable(ProjectType.Cms)]

public class CmsCategoryRole :  IEntity<int>
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public int Did { get; set; }
    /// <summary>
    /// 有权限的类别集合
    /// </summary>
    [Column("role_cats")]
    public string RoleCats { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}