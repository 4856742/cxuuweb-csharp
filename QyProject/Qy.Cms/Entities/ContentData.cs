﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;


[Table(CmsDbTableNameConst.CmsContentData)]
[CacheSetting(true)]
[Description("CMS内容附表")]
//[Auditable(ProjectType.Cms)]

public class ContentData : IEntity<int>
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public int Aid { get; set; }
    public string Content { get; set; }
    [Column("video_url")]
    public string VideoUrl { get; set; }
    public string Attachments { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}