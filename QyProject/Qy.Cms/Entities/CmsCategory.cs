﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsCategory)]
[CacheSetting(true)]
[Description("CMS类别表")]
[Auditable(ProjectType.Cms)]

public class CmsCategory : ICategoryEntity, IEntity<int>, IHierarchicalEntity
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    public int Pid { get; set; }
    public string Name { get; set; }
    public int Type { get; set; }
    public string Theme { get; set; }
    public string Ico { get; set; }
    public string Ctheme { get; set; }
    public int Sort { get; set; }
    public int Num { get; set; }
    public bool Status { get; set; }
    [Column("banner_header")]
    public string BannerHeader { get; set; }
    /// <summary>
    /// 灵活调用图片
    /// </summary>
    [Column("banner_ad")]
    public string BannerAd { get; set; }
    [Column("banner_lit")]
    public string BannerLit { get; set; }

    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}

