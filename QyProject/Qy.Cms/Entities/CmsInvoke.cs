﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsInvoke)]
[CacheSetting(true, PropertyNamesOfArea = "Cid")]
[Description("CMS通用调用主表")]
[Auditable(ProjectType.Cms)]

public class CmsInvoke : IEntity<int>
{
    [PrimaryKey("Id")]
    public int Id { get; set; }
    [Column("Cid")]
    public int Cid { get; set; }
    public string Name { get; set; }
    public string Url { get; set; }
    [Column("url_int")]
    public int UrlInt { get; set; }
    public string Ico { get; set; }
    public string Img { get; set; }
    public string Content { get; set; }
    public int Sort { get; set; }
    public bool Status { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}