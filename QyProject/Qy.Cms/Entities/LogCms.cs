﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Cms.AppBase;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qy.Cms.Entities;

[Table(CmsDbTableNameConst.CmsLog)]
[CacheSetting(true)]
[Description("CMS系统操作记录表")]
//[Auditable(ProjectType.Cms)]

public class LogCms : IEntity<int>
{
    [PrimaryKey("id")]
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    [Column("contr_act")]
    public string ContrAct { get; set; }
    public string Method { get; set; }
    public string Ip { get; set; }
    public DateTime Time { get; set; }
    [Column("do_thing")]
    public string DoThing { get; set; }
    #region IEntity
    [NotMapped]
    object IEntity.Id => Id;
    #endregion
}
