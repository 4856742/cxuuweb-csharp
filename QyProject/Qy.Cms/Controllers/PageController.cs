﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.Categorys;
using Qy.Cms.Services.Contents.ContentHits;
using Qy.Cms.Services.Contents.Contents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Controllers;
/// <summary>
/// 单页调用
/// </summary>
public class PageController(
    ContentService contentService,
    ContentHitService contentHitService,
    CmsCategoryService cmsCategoryService) : Controller
{

    [HttpGet]
    public async Task<IActionResult> ListAsync(int id, int pageIndex = 1)
    {
        if (id < 1)
            return NotFound();
        CmsCategory cat = await cmsCategoryService.GetOneAsync(id);
        if (cat == null || cat.Status == false)
            return NotFound();
        var pageList = await contentService.GetCmsPageingAsync(new() { Cid = id, PageIndex = pageIndex, PageSize = cat.Num });
        IEnumerable<CmsCategory> catPath = await cmsCategoryService.FindPathAsync(id);
        CmsPagerInfo cmsPagerInfo = new()
        {
            Total = pageList.Total,
            Duration = pageList.QueryDuration,
            PageIndex = pageList.PageIndex,
            PageSize = pageList.PageSize,
        };
        var pageResult = CmsPagedList<ViewContentAndCate>.AsCmsPageDto(pageList.Items, cmsPagerInfo, catPath, cat);
        return View("./List/List", pageResult);
    }
    [HttpGet]
    public IActionResult AddressBookAsync()
    {
        return  View("./Detail/AddressBook");
    } 

    /// <summary>
    /// 文章内容表态提交
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ApiResult> PostPositionAsync(int id = 0)
    {
        if (id > 0 && await contentHitService.UpdateLikeAsync(id) > 0)
            return ApiResult.Ok("谢谢点赞");
        return ApiResult.Error("响应错误");
    }
}
