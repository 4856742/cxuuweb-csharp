﻿using Microsoft.AspNetCore.Mvc;

namespace Qy.Cms.Controllers;

public class HomeController() : Controller
{

    [HttpGet]
    public IActionResult Index()
    {
       return View();
    }

}
