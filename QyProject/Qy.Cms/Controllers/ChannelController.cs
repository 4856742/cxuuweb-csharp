﻿using Microsoft.AspNetCore.Mvc;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.Categorys;
using System.Threading.Tasks;

namespace Qy.Cms.Controllers;

public class ChannelController(CmsCategoryService cmsCategoryService) : Controller
{

    [HttpGet]
    public async Task<IActionResult> IndexAsync(int id)
    {
        if (id < 1)
            return NotFound();
        CmsCategory cat = await cmsCategoryService.GetOneAsync(id);
        if (cat == null || cat.Status == false)
            return NotFound();
        return !string.IsNullOrEmpty(cat.Theme) ? View("../Channel/Index", cat) : View("../Channel/" + cat.Theme, cat);
    }
}
