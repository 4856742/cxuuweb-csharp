﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.Categorys;
using Qy.Cms.Services.Contents.ContentHits;
using Qy.Cms.Services.Contents.Contents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Controllers;
public class ContentController(
    ContentService contentService,
    ContentHitService contentHitService,
    CmsCategoryService cmsCategoryService
    ) : Controller
{

    [HttpGet]
    public async Task<IActionResult> ListAsync(int id, int pageIndex = 1)
    {
        if (id < 1)
            return NotFound();
        CmsCategory cat = await cmsCategoryService.GetOneAsync(id);
        if (cat == null || cat.Status == false)
            return NotFound();
        IPagedList<ViewContentAndCate> pageList = await contentService.GetCmsPageingAsync(new() { Cid = id, PageIndex = pageIndex, PageSize = cat.Num });
        IEnumerable<CmsCategory> catPath = await cmsCategoryService.FindPathAsync(id);
        CmsPagerInfo cmsPagerInfo = new()
        {
            Total = pageList.Total,
            Duration = pageList.QueryDuration,
            PageIndex = pageList.PageIndex,
            PageSize = pageList.PageSize,
        };
        var pageResult = CmsPagedList<ViewContentAndCate>.AsCmsPageDto(pageList.Items, cmsPagerInfo, catPath, cat);
        return string.IsNullOrEmpty(cat.Theme)
            ? View("./List/Index", pageResult)
            : View("./List/" + cat.Theme, pageResult);
    }

    [HttpGet]
    public async Task<IActionResult> DetailAsync(int id = 0)
    {
        if (id < 1)
            return NotFound();
        ViewContentAndData content = await contentService.GetAndDataAsync(id);
        if (content == null)
            return NotFound();
        string theme = content.Type switch
        {
            ContentTypeEnum.Articles => "./Detail/Index",
            ContentTypeEnum.Video => "./Detail/Video",
            _ => "./Detail/" + content.Ctheme,
        };
        return View(theme, content);
    }

    /// <summary>
    /// 文章内容表态提交
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ApiResult> PostPositionAsync(int id = 0)
    {
        if (id > 0 && await contentHitService.UpdateLikeAsync(id) > 0)
            return ApiResult.Ok("谢谢点赞");
        return ApiResult.Error("响应错误");
    }


    /// <summary>
    /// 图片中心
    /// </summary>
    /// <param name="pageIndex"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<IActionResult> PicListAsync(int pageIndex = 1)
    {
        var pageList = await contentService.GetCmsPageingAsync(new() { PageIndex = pageIndex, PageSize = 9, IsCover = true });
        CmsPagerInfo cmsPagerInfo = new()
        {
            Total = pageList.Total,
            Duration = pageList.QueryDuration,
            PageIndex = pageList.PageIndex,
            PageSize = pageList.PageSize,
        };
        var pageResult = CmsPagedList<ViewContentAndCate>.AsCmsPageDto(pageList.Items, cmsPagerInfo);
        return View("./List/PicList", pageResult);

    }
}
