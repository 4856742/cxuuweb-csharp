﻿using Microsoft.AspNetCore.Mvc;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Services.Contents.Contents;
using System.Threading.Tasks;

namespace Qy.Cms.Controllers;
public class SearchController(
    ContentService contentService
    ) : Controller
{

    [HttpGet]
    public async Task<IActionResult> IndexAsync(string title = "", int pageIndex = 1)
    {
        if (string.IsNullOrWhiteSpace(title))
            return Ok("搜索关键字不能空");
        var pageList = await contentService.GetCmsPageingAsync(new() { PageIndex = pageIndex, PageSize = 15, Title = title });
        CmsPagerInfo cmsPagerInfo = new()
        {
            Total = pageList.Total,
            Duration = pageList.QueryDuration,
            PageIndex = pageList.PageIndex,
            PageSize = pageList.PageSize,
        };
        var pageResult = CmsPagedList<ViewContentAndCate>.AsCmsPageDto(pageList.Items, cmsPagerInfo);
        ViewBag.title = title;
        return View(pageResult);
    }

}
