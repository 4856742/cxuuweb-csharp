﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Invokes.CmsInvokeCats;
using Qy.Cms.Services.Invokes.CmsInvokes;
using System.Threading.Tasks;

namespace Qy.Cms.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class CmsInvokeController(
    CmsInvokeService cmsInvokeService,
    CmsInvokeCatService cmsInvokeCatService) : ControllerBase
{
    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<IPagedList<ViewCmsInvoke>> IndexAsync([FromQuery] QueryCmsInvoke query)
    {
        var pageList = await cmsInvokeService.GetPageingAsync(query);
        return pageList;
    }

    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        var catList = await cmsInvokeCatService.IndentedOptionAsync();
        var data = await cmsInvokeService.GetAsync(id);
        return ApiResult.Ok(new { data, catList });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> CreatePostAsync([FromBody] CmsInvoke editAppLink)
    {
        if (await cmsInvokeService.InsertAsync(editAppLink) != null)
            return ApiResult.Ok($"ID:{editAppLink.Id} 名称:{editAppLink.Name}");
        return ApiResult.Error("失败");
    }
    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> EditPostAsync([FromBody] CmsInvoke editAppLink)
    {
        if (await cmsInvokeService.UpdateAsync(editAppLink) > 0)
            return ApiResult.Ok($"ID:{editAppLink.Id} 名称:{editAppLink.Name}");
        return ApiResult.Error("编辑失败");
    }
    [HttpPut]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> SwitchValueAsync([FromBody] ValChangeModel valChangeModel)
    {
        if (await cmsInvokeService.ValChangeAsync(valChangeModel) < 1)
            return ApiResult.Error("编辑失败");
        return ApiResult.Ok($"ID：{valChangeModel.Id} 名称:{valChangeModel.Title}");
    }
    [HttpDelete]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> Delete(int id)
    {
        if (await cmsInvokeService.DeleteAsync(id) > 0)
            return ApiResult.Ok($"ID：{id}");
        return ApiResult.Error($"ID：{id}");
    }
}
