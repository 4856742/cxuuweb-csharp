﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.Extensions;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Invokes.CmsInvokeCats;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class CmsInvokeCatController(CmsInvokeCatService cmsInvokeCatService) : ControllerBase
{

    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<IEnumerable<ViewCmsInvokeCat>> IndexAsync()
    {
        var cateList = await cmsInvokeCatService.GetCatsAsync();
        return cateList;
    }
    [HttpGet]
    public async Task<IEnumerable<OptionItemDto>> OptionTypesAsync()
    {
        var data = await cmsInvokeCatService.IndentedOptionAsync();
        return data;
    }
    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        var asCats = await cmsInvokeCatService.IndentedOptionAsync();
        var data = await cmsInvokeCatService.GetAsync(id);
        return ApiResult.Ok(new { data, asCats });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> CreatePostAsync([FromBody] CmsInvokeCat edit)
    {
        if (await cmsInvokeCatService.InsertAsync(edit) != null)
            return ApiResult.Ok(edit.Name);
        return ApiResult.Error("新增失败");
    }
    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> EditPostAsync([FromBody] CmsInvokeCat edit)
    {
        if (await cmsInvokeCatService.UpdateAsync(edit) > 0)
            return ApiResult.Ok(edit.Name);
        return ApiResult.Error("修改失败！");
    }
    [HttpDelete]
    [MenuAuthorize(CmsPermissionKeys.Cms_Invoke)]
    public async Task<ApiResult> Delete(int id)
    {
        if (await cmsInvokeCatService.DeleteAsync(id) > 0)
            return ApiResult.Ok("ID：" + id);
        return ApiResult.Error("删除失败,可能该类别下有链接！");
    }
}
