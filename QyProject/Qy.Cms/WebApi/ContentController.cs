﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using PmSoft.Web.Abstractions.Authorization;
using Qy.Base.AppBase;
using Qy.Base.Entities.Attachment;
using Qy.Base.Extensions;
using Qy.Base.Services.Attachment;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.Categorys;
using Qy.Cms.Services.Contents.Contents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class ContentController(
  ContentService contentService,
  CmsCategoryService cmsCategoryService,
  AttachmentUserService  attachmentUserService,
  CachedMenuPermissionChecker _innerChecker,
  IApplicationContext applicationContext
 ) : ControllerBase
{

    [HttpGet]
    public async Task<ApiResult<object>> IndexAsync([FromQuery] ContentQuery query)
    {
        IEnumerable<CmsCategory> catList = await cmsCategoryService.CateRoleListAsync();
        if (catList == null)
            return ApiResult.Error("无栏目权限");
        var user = applicationContext.GetRequiredCurrentUser();
        var pageResult = await contentService.GetPageingAsync(query, catList, user);
        //判断是不有属性操作权限
        bool operateStatus = false;
        if (user.IsSuperAdmin() || await _innerChecker.HasPermissionAsync(user, [CmsPermissionKeys.Content_Att]))
            operateStatus = true;
        return ApiResult.Ok(new { pageResult, operateStatus });
    }
    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Content_AddOrEdit)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        //禁用选择及展开当前频道子栏目
        var catList = await cmsCategoryService.CmsCatOptionTypeAsync();
        if (id < 1)
            return ApiResult.Ok(new { catList });
        EditContentAndData data = await contentService.GetEditAsync(id);
        var attments = await attachmentUserService.GetListAsync(data.Attachments);
        return ApiResult.Ok(new { data, attments, catList });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(CmsPermissionKeys.Content_AddOrEdit)]
    public async Task<ApiResult> CreatePostAsync([FromBody] EditContentAndData contentPost)
    {
        //检测当前角色是否有直接发布权限
        var user = applicationContext.GetRequiredCurrentUser();
        if (!user.IsSuperAdmin() || !await _innerChecker.HasPermissionAsync(user, [CmsPermissionKeys.Content_Status]))
            contentPost.Status = false;

        Content contentRes = await contentService.InsertAsync(contentPost.AsContent(), contentPost.AsContentData());
        if (contentRes.Id > 1)
        {           
            return ApiResult.Ok($"新增内容 Id: {contentRes.Id}");
        }
        return ApiResult.Error("新增失败！");
    }
    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(CmsPermissionKeys.Content_AddOrEdit)]
    public async Task<ApiResult> EditPostAsync([FromBody] EditContentAndData contentPost)
    {
        var user = applicationContext.GetRequiredCurrentUser();
        if (!user.IsSuperAdmin() && !await _innerChecker.HasPermissionAsync(user, [CmsPermissionKeys.Content_Status]))
            contentPost.Status = false;
        int x = await contentService.UpdateAsync(contentPost.AsContent(), contentPost.AsContentData());
        switch (x)
        {
            case -1:
                return ApiResult.Error("权限不足");
            case 0:
                return ApiResult.Error("服务器响应错误");
            default:
                return ApiResult.Ok($"修改内容 ID：{contentPost.Id}");
        }
    }
    [HttpPut]
    [MenuAuthorize(CmsPermissionKeys.Content_Att)]
    public async Task<ApiResult> SwitchValueAsync([FromBody] ValChangeModel formChange)
    {
        var user = applicationContext.GetRequiredCurrentUser();
        if (formChange.Key == "Status" &&
        (!user.IsSuperAdmin() && !await _innerChecker.HasPermissionAsync(user, [CmsPermissionKeys.Content_Status])))
            return ApiResult.Error("无文章发布状态设置权限");

        if (await contentService.UpdateContentAttAsync(formChange) < 1)
            return ApiResult.Error("设置失败！是否权限不足？");
        return ApiResult.Ok($"设置 {formChange.Title} ID：{formChange.Id}");
    }
    [HttpDelete]
    [MenuAuthorize(CmsPermissionKeys.Content_Delete)]
    public async Task<ApiResult> Delete(int id)
    {
        if (await contentService.DeleteAsync(id) > 0)
            return ApiResult.Ok($"删除内容 ID：{id}");
        return ApiResult.Error("删除失败！");
    }

    [HttpPut]
    [MenuAuthorize(CmsPermissionKeys.Content_AddOrEdit)]
    public async Task<ApiResult> MultiTransferAsync([FromBody] ContentMultiSelect data)
    {
        if (data.Ids.Length < 1)
            return ApiResult.Error("未选择内容");
        int res = await contentService.MultiTransferAsync(data);
        if (res < 1)
            return ApiResult.Error($"批量转移栏目失败！ ID: {data.Ids} 目标Cid:{data.TargetCid}");
        return ApiResult.Ok($"批量转移栏目成功 ID: {data.Ids}");
    }
    [HttpPut]
    [MenuAuthorize(CmsPermissionKeys.Content_Delete)]
    public async Task<ApiResult> MultiDeleteAsync([FromBody] ContentMultiSelect data)
    {
        if (data.Ids.Length < 1)
            return ApiResult.Error("未选择内容");
        int res = await contentService.MultiDeleteAsync(data);
        if (res < 1)
            return ApiResult.Error($"批量删除失败！ ID: {data.Ids}");
        return ApiResult.Ok($"批量删除成功 ID: {data.Ids}");
    }
}
