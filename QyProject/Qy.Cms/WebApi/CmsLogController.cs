﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Services.Loging;
using Qy.Base.Utilities.EPPlusExcel;
using Qy.Cms.Dtos;
using Qy.Cms.Services.CmsLogs;
using System.IO;
using System.Threading.Tasks;

namespace Qy.Cms;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class CmsLogController(LogCmsService logCmsService) : ControllerBase
{
    [HttpGet]
    public async Task<ApiResult<object>> LogListAsync([FromQuery] LogingQuery query)
    {
        var pageResult = await logCmsService.GetPagingAsync(query);
        return ApiResult.Ok(new { pageResult, exportFields = LogCmsConsts.ViewLogCmsFields });
    }
    [HttpGet]
    public async Task<IActionResult> LogExceptionExportExcelAsync([FromQuery] LogingQuery query)
    {
        var pageList = await logCmsService.GetPagingAsync(query);

        var options = new OutPutExcelExportOptions
        {
            InitialHeaderList = LogConsts.ViewLogSysFields,
            FieldsJson = query.Fields,
            TableTitle = "网站管理操作日志",
        };
        MemoryStream stream = new();
        await OutPutExcelOfEntityHelper<ViewLogCms>.OutPutExcel([.. pageList.Items], options, stream);
        stream.Position = 0;
        var fileResult = File(stream, DownloadFileMimeType.FileMimeTypeExcel, options.TableTitle);
        // 在这里结束时手动调用Dispose  ,防止出现写入失败问题
        return fileResult;

    }
}
