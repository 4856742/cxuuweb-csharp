﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Base.Utilities;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.CategoryRoles;
using Qy.Cms.Services.Contents.Categorys;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Qy.Cms.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class CmsCategoryController(CmsCategoryService cmsCategoryService,
     CmsCategoryRoleService cmsCategoryRoleService) : ControllerBase
{
    [HttpGet]
    public async Task<IEnumerable<OptionItemDto>> OptionTypesAsync()
    {
        var data = await cmsCategoryService.IndentedOptionAsync();
        return data;
    }

    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<IEnumerable<ViewCmsCategory>> IndexAsync()
    {
        IEnumerable<ViewCmsCategory> pagedList = await cmsCategoryService.GetContentCatesAsync();
        return pagedList;
    }

    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult<object>> AddOrEditAsync(int id)
    {
        var contentList = FileUtility.ReadFileNames([Directory.GetCurrentDirectory(), "Views", "Content", "List"], "*.cshtml");
        var contentDetail = FileUtility.ReadFileNames([Directory.GetCurrentDirectory(), "Views", "Content", "Detail"], "*.cshtml");
        var channel = FileUtility.ReadFileNames([Directory.GetCurrentDirectory(), "Views", "Channel"], "*.cshtml");
        var data = await cmsCategoryService.GetOneAsync(id);
        var catList = await cmsCategoryService.IndentedOptionAsync();
        return ApiResult.Ok(new { data, catList, theme = new { contentDetail, contentList, channel } });
    }

    [HttpPost, ActionName("Create")]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult> CreatePostAsync([FromBody] CmsCategory cateModel)
    {
        var res = await cmsCategoryService.InsertAsync(cateModel);
        if (res != null)
            return ApiResult.Ok("成功新增内容类别！");
        return ApiResult.Error("新增类别失败");
    }


    [HttpPut, ActionName("Edit")]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult> EditPostAsync([FromBody] CmsCategory cateModel)
    {
        int res = await cmsCategoryService.UpdateAsync(cateModel);
        if (res > 0)
            return ApiResult.Ok($"修改类别");
        return ApiResult.Error("修改失败");
    }
    [HttpPut]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult> SwitchValueAsync([FromBody] ValChangeModel catValChangeModel)
    {
        if (await cmsCategoryService.UpdateValChangeAsync(catValChangeModel) < 1)
            return ApiResult.Error("设置失败！");
        return ApiResult.Ok($"设置 {catValChangeModel.Title} ID：{catValChangeModel.Id}");
    }
    [HttpDelete, ActionName("Delete")]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult> DeleteAsync(int id)
    {
        if (await cmsCategoryService.DeleteAsync(id) > 0)
            return ApiResult.Ok("删除类别,Id： " + id);
        return ApiResult.Error("删除失败！可能因为当前类别下有内容或已经指派部门权限！");
    }
    /// <summary>
    /// 类别部门权限
    /// </summary>
    /// <param name="id">部门id</param>
    /// <returns></returns>
    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult<object>> GetDeptRoleAsync(int id)
    {
        var catTree = await cmsCategoryService.IndentedOptionAsync();
        if (id < 1)
        {
            return ApiResult.Ok(new { catTree });
        }
        else
        {
            CmsCategoryRole data = await cmsCategoryRoleService.GetOneByDidAsync(id);
            return ApiResult.Ok(new { data, catTree });
        }
    }
    [HttpPut, ActionName("EditDeptRole")]
    [MenuAuthorize(CmsPermissionKeys.Content_Cate)]
    public async Task<ApiResult> EditDeptRoleAsync([FromBody] CmsCategoryRole cateModel)
    {
        int res = await cmsCategoryRoleService.InsertOrUpdateDidRoleAsync(cateModel);
        if (res > 0)
            return ApiResult.Ok($"修改部门授权 部门Id:{cateModel.Did} ");
        return ApiResult.Error("修改失败");
    }
}
