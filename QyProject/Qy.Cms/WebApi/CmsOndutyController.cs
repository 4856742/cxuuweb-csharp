﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using PmSoft.Web.Abstractions.Authorization;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Base.Utilities.EPPlusExcel;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.CmsConfigs;
using Qy.Cms.Services.Ondutys;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Qy.Cms.WebApi;

[Route("Api/[controller]/[action]")]
[RestrictAccess]
public class CmsOndutyController(
    CachedMenuPermissionChecker _innerChecker,
    AppConfigService appConfigService,
    CmsOndutyService cmsOndutyService,
    IApplicationContext applicationContext) : Controller
{

    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Cms_Onduty)]
    public async Task<ApiResult<object>> IndexAsync([FromQuery] QueryCmsOnduty query)
    {
        if (query.DateTime.Year < 1970)
            return ApiResult.Error("时间参数错误");
        var result = await cmsOndutyService.GetAsListAsync(query);
        //判断是不有新增或编辑操作权限
        bool operateEdit = false;
        var user = applicationContext.GetRequiredCurrentUser();
        if (user.IsSuperAdmin() || await _innerChecker.HasPermissionAsync(user, [CmsPermissionKeys.Cms_Onduty]))
            operateEdit = true;
        return ApiResult.Ok(new { result, exportFields = CmsOndutyConsts.ViewCmsOndutyFields, operateEdit });
    }

    public async Task<IActionResult> LogExceptionExportExcelAsync([FromQuery] QueryCmsOnduty query)
    {
        if (query.DateTime.Year < 1970)
            return Ok("时间参数错误");
        var result = await cmsOndutyService.GetAsListAsync(query);


        var options = new OutPutExcelExportOptions
        {
            InitialHeaderList = CmsOndutyConsts.ViewCmsOndutyFields,
            FieldsJson = query.Fields,
            TableTitle = query.DateTime.ToString("yyyy年MM月") + "值班表",
        };
        MemoryStream stream = new();
        await OutPutExcelOfEntityHelper<ViewCmsOnduty>.OutPutExcel(result, options, stream);
        stream.Position = 0;
        var fileResult = File(stream, DownloadFileMimeType.FileMimeTypeExcel, options.TableTitle);
        // 在这里结束时手动调用Dispose  ,防止出现写入失败问题
        return fileResult;

    }

    [HttpGet]
    [MenuAuthorize(CmsPermissionKeys.Cms_Onduty)]
    public async Task<ViewCmsOnduty> GetOneEditAsync([FromQuery] DateTime? dateTime)
    {
        ViewCmsOnduty data = await cmsOndutyService.GetOneAsync(dateTime);
        return data;
    }

    [HttpPut]
    [MenuAuthorize(CmsPermissionKeys.Cms_Onduty)]
    public async Task<ApiResult> EditAsync([FromBody] CmsOnduty model)
    {
        if (await cmsOndutyService.EditAsync(model) < 1)
            return ApiResult.Error("设置失败！");
        return ApiResult.Ok($"{model.DateTime:yyyy-MM-dd}");
    }


    [HttpGet]
    public async Task<ApiResult<object>> GetConfigAsync()
    {
        var leaders = await appConfigService.GetDutyerResetAsync(CmsConfigConsts.CmsOndutyLeaderKey);
        var dutyers = await appConfigService.GetDutyerResetAsync(CmsConfigConsts.CmsOndutyDutyerKey);
        return ApiResult.Ok(new { leaders, dutyers });
    }

    [HttpPut, ActionName("EditConfig")]
    [MenuAuthorize(CmsPermissionKeys.Cms_Onduty)]
    public async Task<ApiResult> EditConfigAsync([FromBody] CmsConfig[] appConfigs)
    {
        var configMappings = new Dictionary<string, string>
        {
            { CmsConfigConsts.CmsOndutyLeaderKey, appConfigs?.FirstOrDefault(x => x.Label == CmsConfigConsts.CmsOndutyLeaderKey)?.ValueData },
            { CmsConfigConsts.CmsOndutyDutyerKey, appConfigs?.FirstOrDefault(x => x.Label == CmsConfigConsts.CmsOndutyDutyerKey)?.ValueData }
        };

        int totalUpdates = 0;
        foreach (var mapping in configMappings)
        {
            if (!string.IsNullOrEmpty(mapping.Value))
            {
                var config = new CmsConfig
                {
                    Label = mapping.Key,
                    ValueData = mapping.Value,
                };
                int res = await appConfigService.InsertOrUpdateModelKeyAsync(config);
                totalUpdates += res;
            }
        }
        return totalUpdates > 0
            ? ApiResult.Ok("修改值班预设")
            : ApiResult.Error("修改失败");
    }
}
