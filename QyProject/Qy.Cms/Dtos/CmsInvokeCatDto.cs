﻿using Qy.Base.AppBase;
using System.Collections.Generic;

namespace Qy.Cms.Dtos;

public class ViewCmsInvokeCat
{
    public int Id { get; set; }
    public int Pid { get; set; }
    public string Name { get; set; }
    public string Url { get; set; }
    public string Title { set; get; }
    public int Sort { get; set; }
    public string Remark { get; set; }
    public int Depth { get; set; }
    public string Spread { get; set; }
    public int ChildrenCount { get; set; }
    public bool HaveChild { get; set; }
    public bool Open { get; set; }
    public int InvokeNum { get; set; }
    public List<ViewCmsInvokeCat> Children { set; get; } = [];
}

public class QueryCmsInvokeCat : PagerInfo
{

}


