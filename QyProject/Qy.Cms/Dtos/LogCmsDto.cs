﻿using Qy.Base.Utilities.EPPlusExcel;
using Qy.Cms.Entities;
using System;
using System.Collections.Generic;

namespace Qy.Cms.Dtos;


public class ViewLogCms
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public string ContrAct { get; set; }
    public string Method { get; set; }
    public string Ip { get; set; }
    public string Localtion { get; set; }
    public DateTime Time { get; set; }
    public string DoThing { get; set; }
    public string DeptName { get; set; }
    public string NickName { get; set; }
    public bool Status { get; set; }

}

public static class LogCmsExt
{
    public static ViewLogCms AsView(this LogCms log)
    {
        return new ViewLogCms
        {
            Id = log.Id,
            Uid = log.Uid,
            Did = log.Did,
            Time = log.Time,
            Ip = log.Ip,
            Localtion = log.Ip,
            ContrAct = log.ContrAct,
            Method = log.Method,
            DoThing = log.DoThing,
        };
    }
}
public static class LogCmsConsts
{
    public static List<GreatExcelField> ViewLogCmsFields
    {
        get
        {
            return [
                   new() { Value = 1, FieldName = "Id", Label = "Id", Width = 10, Type = GreatExcelDataType.Int32 },
                   new() { Value = 2, FieldName = "DeptName", Label = "部门", Width = 16, Type = GreatExcelDataType.String },
                   new() { Value = 3, FieldName = "NickName", Label = "姓名", Width = 13, Type = GreatExcelDataType.String },
                   new() { Value = 4, FieldName = "Ip", Label = "Ip", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 5, FieldName = "ContrAct", Label = "控制器", Width = 30, Type = GreatExcelDataType.String },
                   new() { Value = 7, FieldName = "Method", Label = "请求方法", Width = 30, Type = GreatExcelDataType.String },
                   new() { Value = 8, FieldName = "DoThing", Label = "内容", Width = 40, Type = GreatExcelDataType.String },
                   new() { Value = 9, FieldName = "Time", Label = "操作时间", Width = 30, Type = GreatExcelDataType.DateTime },
            ];
        }
    }
}