﻿using Qy.Base.Entities.Auth;
using Qy.Cms.Entities;
using System;
using System.Collections.Generic;
using Qy.Base.Extensions;
using System.Linq;

namespace Qy.Cms.Dtos;

public class ViewCmsOnduty
{
    public int Id { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public DateTime DateTime { get; set; }
    public int DateInMonth { get; set; }
    public int? WeekDate { get; set; }
    public string Leader { get; set; }
    public string LeaderPhone { get; set; }
    public string Dutyer { get; set; }
    public string DutyerPhone { get; set; }
    public DateTime InsertTime { get; set; }
    public string DeptName { get; set; }
    public string NickName { get; set; }
    public string DateTimeStr { get; set; }
    public string WeekDateStr { get; set; }
   

}
public static class CmsOndutyExtensions
{
    public static ViewCmsOnduty AsView(this CmsOnduty data, IEnumerable<UserDept> depts,
        IEnumerable<User> users)
    {
        return new ViewCmsOnduty
        {
            Id = data.Id,
            DateTime = data.DateTime,
            //WeekDate = data.WeekDate,
            DateInMonth = data.DateInMonth,
            Leader = data.Leader,
            LeaderPhone = data.LeaderPhone,
            Dutyer = data.Dutyer,
            DutyerPhone = data.DutyerPhone,
            DateTimeStr = data.DateTime.ToString("yyyy年MM月dd日"),
            WeekDateStr = data.WeekDate?.CheckAsDictIntWeek(),
            DeptName = depts.FirstOrDefault(x => x.Id == data.Did)?.Name,
            NickName = users.FirstOrDefault(x => x.Id == data.Uid)?.NickName,
            InsertTime = data.InsertTime,
        };
    }
}
public class QueryCmsOnduty
{
    public string KeyWords { get; set; }
    public DateTime DateTime { get; set; }
    public string Fields { get; set; }
    public bool ExportExcel { get; set; }
}


public class ValChangeCmsOnduty
{
    /// <summary>
    /// 文字标题或描述
    /// </summary>
    public string Title { get; set; }
    /// <summary>
    /// 字段或键
    /// </summary>
    public string Key { get; set; }
    /// <summary>
    /// 要修改数据的主键
    /// </summary>
    public DateTime DateTime { get; set; }
    /// <summary>
    /// 数据值对象
    /// </summary>
    public string Val { get; set; }
}

