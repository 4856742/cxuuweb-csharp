﻿using System.Collections.Generic;

namespace Qy.Cms.Dtos;

public class ViewCmsCategory
{
    public int Id { get; set; }
    public int Pid { get; set; }
    public string Name { get; set; }
    public int Type { get; set; }
    public string Theme { get; set; }
    public string Ico { get; set; }
    public string Ctheme { get; set; }
    public string BannerHeader { get; set; }
    public int Sort { get; set; }
    public int Num { get; set; }
    public bool Status { get; set; }
    public bool? Disabled { get; set; }
    public bool? Spread { get; set; }
    public List<ViewCmsCategory> Children { set; get; } = [];
}
