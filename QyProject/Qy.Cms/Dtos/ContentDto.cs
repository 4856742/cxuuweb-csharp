﻿using Newtonsoft.Json;
using PmSoft.Core;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Attachment;
using Qy.Base.Entities.Auth;
using Qy.Cms.AppBase;
using Qy.Cms.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Qy.Cms.Dtos;
public class EditContentAndData
{
    public int Id { get; set; }
    public ContentTypeEnum Type { get; set; }
    [Display(Name = "栏目")]
    [Required(ErrorMessage = "请选择栏目")]
    public int Cid { get; set; }
    [Display(Name = "标题")]
    [Required(ErrorMessage = "标题未填写")]
    public string Title { get; set; }
    public string Abstract { get; set; }
    public string Attachments { get; set; }
    [Display(Name = "作者")]
    [Required(ErrorMessage = "作者未填写")]
    public string Examine { get; set; }
    [Display(Name = "缩略图")]
    public string CoverUrl { get; set; }
    public int CoverCount { get; set; }
    public DateTime CreateTime { get; set; } = DateTime.Now;
    public DateTime EditTime { get; set; } = DateTime.Now;
    [Display(Name = "显示状态")]
    [Required(ErrorMessage = "选择内容显示状态")]
    public bool? Status { get; set; }
    [Display(Name = "头条")]
    public bool? AttA { get; set; }
    [Display(Name = "小头条")]
    public bool? AttB { get; set; }
    [Display(Name = "轮换")]
    public bool? AttC { get; set; }

    [Display(Name = "内容")]
    //[Required(ErrorMessage = "内容未填写")]
    public string Content { get; set; }
    public string VideoUrl { get; set; }
}

public class ViewContentAndData
{
    public int Id { get; set; }
    public int Cid { get; set; }
    public ContentTypeEnum Type { get; set; }
    public string Title { get; set; }
    public string Abstract { get; set; }
    public string Attachments { get; set; }
    public string CoverUrl { get; set; }
    public int CoverCount { get; set; }
    public string Examine { get; set; }
    public int? Hits { get; set; }
    public int? Likes { get; set; }
    public DateTime CreateTime { get; set; }
    public DateTime EditTime { get; set; }
    public int Aid { get; set; }
    public string Content { get; set; }
    public string NickName { get; set; }
    public string DeptName { get; set; }
    public string CateName { get; set; }
    public string Ctheme { get; set; }
    public string VideoUrl { get; set; }
    public IEnumerable<CmsCategory> CatPath { get; set; }
    public IEnumerable<ViewAttachmentUser> AttmentList { get; set; }
}


public class ContentQuery : PagerInfo
{
    public int Cid { get; set; } = 0;
    public int[] Cids { get; set; } = null;
    public int[] Dids { get; set; } = null;
    public string CacheName { get; set; } = null;
    /// <summary>
    /// 缓存秒 留空为（10,系统设定时间)间的随机
    /// </summary>
    public int? CacheTime { get; set; }
    /// <summary>
    /// 1:头条，2:小头条，3:轮换图
    /// </summary>
    public int Att { get; set; }
    /// <summary>
    /// 是否有封面图
    /// </summary>
    public bool IsCover { get; set; }
    /// <summary>
    /// 排序，默认1：id desc，2:id asc
    /// </summary>
    public int Orderby { get; set; } = 1;
    public bool? Status { get; set; }
    public int Limit { get; set; }
    public string Title { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
}

public class ViewContentAndCate
{
    public int Id { get; set; }
    public ContentTypeEnum Type { get; set; }
    public int Cid { get; set; }
    public int Uid { get; set; }
    public int Did { get; set; }
    public string Title { get; set; }
    public string Abstract { get; set; }
    public List<string> CoverUrlList { get; set; }
    public int CoverCount { get; set; }
    public string CateName { get; set; }
    public string Examine { get; set; }
    public string DeptName { get; set; }
    public string NickName { get; set; }
    public string VideoUrl { get; set; }
    public DateTime CreateTime { get; set; }
    public DateTime EditTime { get; set; }
    public bool? Status { get; set; }
    public bool? AttA { get; set; }
    public bool? AttB { get; set; }
    public bool? AttC { get; set; }
    public int? Hits { get; set; }
    public int? Likes { get; set; }
    public bool? HaveEditAuth { get; set; }
}

public static class ContentExtensions
{
    public static ContentData AsContentData(this EditContentAndData edit)
    {
        return new ContentData
        {
            Aid = edit.Id,
            Content = edit.Content,
            VideoUrl = edit.VideoUrl,
            Attachments = edit.Attachments,
        };
    }
    public static Content AsContent(this EditContentAndData edit)
    {
        return new Content
        {
            Id = edit.Id,
            Cid = edit.Cid,
            Type = edit.Type,
            Title = edit.Title,
            Abstract = edit.Abstract,
            Examine = edit.Examine,
            CoverUrl = edit.CoverUrl,
            CoverCount = edit.CoverCount,
            EditTime = edit.EditTime,
            Status = edit.Status,
            AttA = edit.AttA,
            AttB = edit.AttB,
            AttC = edit.AttC
        };
    }

    public static EditContentAndData ToEditContentAndData(this Content content, ContentData data)
    {
        return new EditContentAndData
        {
            Id = content.Id,
            Type = content.Type,
            Cid = content.Cid,
            Title = content.Title,
            Abstract = content.Abstract,
            Examine = content.Examine,
            CoverUrl = content.CoverUrl,
            CoverCount = content.CoverCount,
            CreateTime = content.CreateTime,
            Status = content.Status,
            AttA = content.AttA,
            AttB = content.AttB,
            AttC = content.AttC,
            Content = data?.Content,
            VideoUrl = data?.VideoUrl,
            Attachments = data?.Attachments            
        };
    }
    public static ViewContentAndCate AsView(this Content content)
    {
        try
        {
            List<string> coverList = Json.Parse<List<string>>(content.CoverUrl ?? "[]");
            return new ViewContentAndCate
            {
                Id = content.Id,
                Type = content.Type,
                Status = content.Status,
                CreateTime = content.CreateTime,
                Cid = content.Cid,
                Uid = content.Uid,
                Did = content.Did,
                Title = content.Title,
                Abstract = content.Abstract,
                CoverUrlList = coverList,
                CoverCount = content.CoverCount,
                Examine = content.Examine,
                AttA = content.AttA,
                AttB = content.AttB,
                AttC = content.AttC,
            };
        }
        catch
        {
            return null;
        }
    }

    public static ViewContentOption AsOptionList(this Content content)
    {
        try
        {
            List<string> coverList = Json.Parse<List<string>>(content.CoverUrl ?? "[]");
            return new ViewContentOption
            {
                Id = content.Id,
                Cid = content.Cid,
                Title = content.Title,
                ImgSrc = coverList.First(),
            };
        }
        catch
        {
            return null;
        }
    }

    public static ViewContentAndCate AsNewAndCate(this Content content, IEnumerable<CmsCategory> cates, IEnumerable<ContentHit> hits = null)
    {
        return new ViewContentAndCate
        {
            Id = content.Id,
            Status = content.Status,
            CreateTime = content.CreateTime,
            Cid = content.Cid,
            Title = content.Title,
            Abstract = content.Abstract,
            CoverUrlList = JsonConvert.DeserializeObject<List<string>>(content.CoverUrl ?? "[]"),
            CoverCount = content.CoverCount,
            Examine = content.Examine,
            AttA = content.AttA,
            AttB = content.AttB,
            AttC = content.AttC,
            CateName = cates.FirstOrDefault(d => d.Id == content.Cid)?.Name,
            Hits = hits?.FirstOrDefault(d => d.Aid == content.Id)?.Hits ?? 0,
            Likes = hits?.FirstOrDefault(d => d.Aid == content.Id)?.Likes,
        };
    }

    public static ViewContentAndData AsNewOne(this Content content, ContentData data, CmsCategory cate, User user, UserDept dept, ContentHit hits)
    {
        return new ViewContentAndData
        {
            Id = content.Id,
            Type = content.Type,
            CreateTime = content.CreateTime,
            EditTime = content.EditTime,
            Cid = content.Cid,
            Title = content.Title,
            Abstract = content.Abstract,
            CoverUrl = content.CoverUrl,
            CoverCount = content.CoverCount,
            Examine = content.Examine,
            Hits = hits?.Hits,
            Likes = hits?.Likes,
            Content = data?.Content,
            CateName = cate?.Name,
            Ctheme = cate?.Ctheme,
            NickName = user?.NickName,
            DeptName = dept?.Name,
            VideoUrl = data?.VideoUrl,
            Attachments = data?.Attachments,
        };
    }
}


public class ViewContentOption
{
    public string ImgSrc { get; set; }
    public string CateName { get; set; }
    public string Content { get; set; }
    public string Title { get; set; }
    public int Id { get; set; }
    public int Cid { get; set; }



}
public class ContentMultiSelect
{
    public string Ids { get; set; }
    public int TargetCid { get; set; }
    public bool Status { get; set; }
}


public class ContentStatsVO
{
    public int Total { get; set; }
    public int Hits { get; set; }
    public List<CountStats> CountStats { get; set; }
}
public class CountStats
{
    public string Name { get; set; }
    public int Value { get; set; }
    public int Status { get; set; }
}
