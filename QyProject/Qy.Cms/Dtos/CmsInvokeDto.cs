﻿using PmSoft.Core.Domain.Entities;
using PmSoft.Core.Domain.Entities.Caching;
using PmSoft.Data.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Cms.AppBase;
using Qy.Cms.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Qy.Cms.Dtos;

public class ViewCmsInvoke
{
    public int Id { get; set; }
    public int Cid { get; set; }
    public string Name { get; set; }
    public string CateName { get; set; }
    public string Url { get; set; }
    public int UrlInt { get; set; }
    public string Ico { get; set; }
    public string Img { get; set; }
    public int Sort { get; set; }
    public string Content { get; set; }
    public bool Status { get; set; }


}

public static class CmsInvokeExtensions
{
    public static ViewCmsInvoke AsNewList(this CmsInvoke appLink)
    {
        return new ViewCmsInvoke
        {
            Id = appLink.Id,
            Cid = appLink.Cid,
            Url = appLink.Url,
            UrlInt = appLink.UrlInt,
            Ico = appLink.Ico,
            Img = appLink.Img,
            Sort = appLink.Sort,
            Status = appLink.Status,
            Content = appLink.Content,
            Name = appLink.Name,
        };
    }
}
public class QueryCmsInvoke : PagerInfo
{
    public int Id { get; set; } = 0;
    public int Cid { get; set; } = 0;
    public int Limit { get; set; }
    public bool? Status { get; set; } = null;
    public string KeyWords { get; set; }

}

