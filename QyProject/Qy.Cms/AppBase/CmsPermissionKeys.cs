﻿namespace Qy.Cms.AppBase;

public class CmsPermissionKeys
{
    public const string Cms_Invoke = "Cms_Invoke";
    /// <summary>
    /// 内容管理
    /// </summary>
    public const string Content_Cate = "Content_Cate";
    public const string Content_AddOrEdit = "Content_AddOrEdit";
    public const string Content_Delete = "Content_Delete";
    public const string Content_Att = "Content_Att";
    public const string Content_Status = "Content_Status";
    public const string Cms_Onduty = "Cms_Onduty";
}

