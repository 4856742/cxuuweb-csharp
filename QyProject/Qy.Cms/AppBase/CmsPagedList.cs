﻿using Qy.Cms.Entities;
using System.Collections.Generic;

namespace Qy.Cms.AppBase;


public class CmsPagerInfo
{
    /// <summary>
    /// 当前页码
    /// </summary>
    public int? PageIndex { get; set; }
    /// <summary>
    /// 每页数量
    /// </summary>
    public int? PageSize { get; set; }
    /// <summary>
    /// 总数量
    /// </summary>
    public int? Total { get; set; }
    /// <summary>
    /// 查询耗时(毫秒)
    /// </summary>
    public double? Duration { get; set; }
}
/// <summary>
/// CMS内容列表分页转换
/// </summary>
public class CmsPagedList<T>
{
    public IEnumerable<T> Result { get; set; }
    public CmsCategory CatInfo { get; set; }
    public CmsPagerInfo PageInfo { get; set; }
    public IEnumerable<CmsCategory> CatPath { get; set; }

    public static CmsPagedList<T> AsCmsPageDto(IEnumerable<T> pageData, CmsPagerInfo page, IEnumerable<CmsCategory> CatPath, CmsCategory CatInfo)
    {
        return new CmsPagedList<T>
        {
            Result = pageData,
            PageInfo = page,
            CatPath = CatPath,
            CatInfo = CatInfo
        };
    }
    public static CmsPagedList<T> AsCmsPageDto(IEnumerable<T> pageData, CmsPagerInfo page)
    {
        return new CmsPagedList<T>
        {
            Result = pageData,
            PageInfo = page,
        };
    }
}
