﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco;

namespace Qy.Cms.AppBase;

[Inject(ServiceLifetime.Scoped, typeof(IDbContext))]
public class CmsDbContext : PetaPocoDbContext
{
	public CmsDbContext(IConfiguration configuration)
		: base(configuration.GetConnectionString("App") ?? string.Empty, "MySqlConnector")
	{
	}
}
