﻿namespace Qy.Cms.AppBase;
public class CmsDbTableNameConst
{
    public const string CmsConfig = "cms_config";
    public const string CmsLog = "cms_log";
    public const string CmsInvoke = "cms_invoke";
    public const string CmsInvokeCat = "cms_invoke_cat";

    public const string CmsCategory = "cms_category";
    public const string CmsCategoryRole = "cms_category_roles";
    public const string CmsCategoryDept = "cms_category_dept";

    public const string CmsContent = "cms_content";
    public const string CmsContentData = "cms_content_data";
    public const string CmsContentHit = "cms_content_hit";

    public const string CmsOnduty = "cms_onduty";
}
