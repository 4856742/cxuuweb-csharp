﻿namespace Qy.Cms.AppBase;
public enum ContentTypeEnum
{
    /// <summary>
    /// 专题频道
    /// </summary>
    Special = 1,
    /// <summary>
    /// 文章内容
    /// </summary>
    Articles = 2,
    /// <summary>
    /// 视频内容
    /// </summary>
    Video = 3,
    /// <summary>
    /// 图集内容
    /// </summary>
    Images = 4,
    /// <summary>
    /// 音频内容
    /// </summary>
    Audio = 5,
}

