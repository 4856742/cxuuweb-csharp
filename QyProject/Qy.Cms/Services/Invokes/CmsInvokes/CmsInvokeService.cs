﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.AppBase;
using Qy.Base.Services.Attachment;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Invokes.CmsInvokeCats;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Invokes.CmsInvokes;

[Inject(ServiceLifetime.Scoped)]
public class CmsInvokeService(
    CmsInvokeRepository cmsInvokeRepository,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    AttachmentService attmentService,
     AttachmentHelper  attachmentHelper,
    CmsInvokeCatService cmsInvokeCatService)
{

    public async Task<IPagedList<ViewCmsInvoke>> GetPageingAsync(QueryCmsInvoke query)
    {
        var pagedList = await cmsInvokeRepository.GetPageingAsync(query);
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsNewList();
            return v;
        })
            .WithRelatedAsync(
                foreignKeySelector: v => v.Cid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<CmsInvokeCat, int>,
                attachAction: (v, m) => v.CateName = m?.Name
            );
    }
    public async Task<IEnumerable<CmsInvoke>> GetAppLinkCatesAsync(IEnumerable<int> ids)
    {
        return await cmsInvokeRepository.GetEntitiesByIdsAsync(ids);
    }
    public async Task<CmsInvoke> GetAsync(int id)
    {
        return await cmsInvokeRepository.GetAsync(id);
    }
    public async Task<object> InsertAsync(CmsInvoke ent)
    {
        var insert = await cmsInvokeRepository.InsertAsync(ent);
        if (insert != null)
        {
            await attachmentHelper.PublishAttachmentAsync(ent.Img, insert.ToString());
            await UpdateCatInvokeNumAsync(ent);
        }
        return insert;
    }

    public async Task<int> UpdateAsync(CmsInvoke edit)
    {
        CmsInvoke get = await GetAsync(edit.Id);
        if (get == null) return 0;
        edit.Id = get.Id;
        var up = await cmsInvokeRepository.UpdateAsync(edit);

        //更新当前所在类别调用统计
        if (up > 0)
        {
            await attachmentHelper.PublishAttachmentAsync(edit.Img, edit.Id.ToString());
            await UpdateCatInvokeNumAsync(get);
            //更新之前所在类别调用统计
            if (edit.Cid != get.Cid)
            {
                await UpdateCatInvokeNumAsync(edit);
            }
        }
        return up;
    }
    /// <summary>
    /// 更新当前所在类别包含调用数量
    /// </summary>
    /// <param name="ent"></param>
    /// <returns></returns>
    public async Task UpdateCatInvokeNumAsync(CmsInvoke ent)
    {
        var invokdNum = await cmsInvokeRepository.GetCountForCidAsync(ent.Cid);
        var getCat = await cmsInvokeCatService.GetAsync(ent.Cid);
        if (getCat != null)
        {
            getCat.InvokeNum = invokdNum;
            await cmsInvokeCatService.UpdateAsync(getCat);
        }
    }

    public async Task<int> ValChangeAsync(ValChangeModel valChangeModel)
    {
        try
        {
            var get = await cmsInvokeRepository.GetAsync(valChangeModel.Id);
            if (get == null) return 0;
            var propertySetters = new Dictionary<string, Action<object>>
            {
                { "Name", val => get.Name = val.ToString() },
                { "Sort", val => get.Sort = Convert.ToInt32(val) },
                { "Url", val => get.Url = val.ToString() },
                { "UrlInt", val => get.UrlInt = Convert.ToInt32(val)},
                { "Ico", val => get.Ico = val.ToString() },
                { "Status", val => get.Status = Convert.ToBoolean(val) }
            };
            if (propertySetters.TryGetValue(valChangeModel.Key, out Action<object> value))
            {
                value(valChangeModel.Val);
                return await cmsInvokeRepository.UpdateAsync(get);
            }
            else
            {
                return 0;
            }            
        }
        catch (Exception)
        {
            return 0;
        }
    }
    public async Task<int> DeleteAsync(int id)
    {
        //删除该内容下的所有图片和视频
        await attmentService.DeleteBatchAsync("cms_invoke", id);
        return await cmsInvokeRepository.DeleteByEntityIdAsync(id);
    }
    public async Task<IEnumerable<CmsInvoke>> GetListAsync(QueryCmsInvoke query)
    {
        return await cmsInvokeRepository.GetListAsync(query);
    }
    public async Task<CmsInvoke> GetOneAsync(int id)
    {
        var one = await cmsInvokeRepository.GetAsync(id);
        if (one != null && one.Status == true)
            return one;
        return null;
    }
    public async Task<IEnumerable<CmsInvoke>> GetListForCidAsync(int cid)
    {
        return await cmsInvokeRepository.GetListForCidAsync(cid);
    }
    public async Task<IEnumerable<CmsInvoke>> GetListForCidsAsync(IEnumerable<int> cids)
    {
        return await cmsInvokeRepository.GetListForCidsAsync(cids);
    }
}
