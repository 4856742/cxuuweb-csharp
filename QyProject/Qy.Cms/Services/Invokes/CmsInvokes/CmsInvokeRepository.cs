﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System.Threading.Tasks;
using Qy.Cms.Entities;
using Qy.Base.Utilities;
using PmSoft.Data.Abstractions;
using Qy.Cms.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace Qy.Cms.Services.Invokes.CmsInvokes;

[Inject(ServiceLifetime.Scoped)]

public class CmsInvokeRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, CmsInvoke, int>(dbContext, applicationContext)
{

    public async Task<IPagedList<CmsInvoke>> GetPageingAsync(QueryCmsInvoke query)
    {
        Sql sql = Sql.Builder;
        sql.Select("Id").From(CmsDbTableNameConst.CmsInvoke);
        if (!string.IsNullOrEmpty(query.KeyWords))
            sql.Where("name like @0 or url like @0", "%" + StringUtility.StripSQLInjection(query.KeyWords) + "%");
        if (query.Cid > 0)
            sql.Where("Cid = @0", query.Cid);
        if (query.Status != null)
            sql.Where("Status = @0", query.Status);
        sql.OrderBy("Sort asc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    public async Task<IEnumerable<CmsInvoke>> GetListForCidAsync(int cid)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsInvoke);
        if (cid > 0)
            sql.Where("Cid = @0", cid);
        sql.Where("Status = @0", true);
        sql.OrderBy("Sort asc");
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }
    public async Task<IEnumerable<CmsInvoke>> GetListForCidsAsync(IEnumerable<int> cids)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsInvoke);
        if (cids.Any())
            sql.Where("Cid in (@0)", cids);
        else
            return [];
        sql.Where("Status = @0", true);
        sql.OrderBy("Sort asc");
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }
    public async Task<IEnumerable<CmsInvoke>> GetListAsync(QueryCmsInvoke query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsInvoke);
        if (query.Cid > 0)
            sql.Where("Cid = @0", query.Cid);
        sql.Where("Status = @0", true);
        sql.OrderBy("Sort asc");
        sql.Append("limit @0", query.Limit);
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }
    /// <summary>
    /// 统计当前类别及开放的调用条数
    /// </summary>
    /// <param name="cid"></param>
    /// <returns></returns>
    public async Task<int> GetCountForCidAsync(int cid)
    {
        var dao = DbContext;
        try
        {
            await dao.OpenSharedConnectionAsync();
            return await dao.ExecuteScalarAsync<int>($"SELECT count(1) FROM {CmsDbTableNameConst.CmsInvoke}  where Cid = {cid} and Status = 1");
        }
        finally
        {
            dao.CloseSharedConnection();
        }
    }

    public async Task<bool> ExistsByCidAsync(int cid)
    {
        return await DbContext.ExistsAsync<CmsInvoke>("cid =@0", cid);
    }
}
