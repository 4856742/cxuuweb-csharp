﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Invokes.CmsInvokeCats;

[Inject(ServiceLifetime.Scoped)]

public class CmsInvokeCatRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, CmsInvokeCat, int>(dbContext, applicationContext)
{
    public async Task<IPagedList<CmsInvokeCat>> GetPageingAsync(QueryCmsInvokeCat query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsInvokeCat);
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    public async Task<bool> ExistsDataByCidAsync(int cid)
    {
        return await DbContext.ExistsAsync<CmsInvoke>("cid =@0", cid);
    }
    public async Task<IEnumerable<CmsInvokeCat>> GetAllAsync()
    {
        var globalVer = await CacheVersionService.GetGlobalVersionAsync();
        return await GetTopEntitiesWithCacheAsync(1000, CachingExpirationType.Stable, () =>
        {
            return $"CmsInvokeCatAll:{globalVer}";
        }, () =>
        {
            Sql sql = Sql.Builder;
            sql.Select("id").From(CmsDbTableNameConst.CmsInvokeCat).OrderBy("sort asc");
            return sql;
        });
    }
}
