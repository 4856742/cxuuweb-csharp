﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.Extensions;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Invokes.CmsInvokeCats;

[Inject(ServiceLifetime.Scoped)]
public class CmsInvokeCatService(CmsInvokeCatRepository appLinkCateRepository)
{
    public async Task<IPagedList<CmsInvokeCat>> GetPageingAsync(QueryCmsInvokeCat query)
    {
        return await appLinkCateRepository.GetPageingAsync(query);
    }
    public async Task<IEnumerable<CmsInvokeCat>> GetAllAsync()
    {
        return await appLinkCateRepository.GetAllAsync();
    }
    public async Task<IEnumerable<OptionItemDto>> IndentedOptionAsync()
    {
        IEnumerable<CmsInvokeCat> list = await GetAllAsync();
        return list.AsIndentedOptionType();
    }

    public async Task<IEnumerable<ViewCmsInvokeCat>> GetSonCatForPidAsync(int pid)
    {
        var cates = await GetAllAsync();
        var cats = cates.Where(x => x.Pid == pid).ToList();//这里必须要加 ToList()  不然筛选不出来，记住
        return cats.Select(m => new ViewCmsInvokeCat
        {
            Id = m.Id,
            Name = m.Name,
            Remark = m.Remark,
            Url = m.Url,
            Pid = m.Pid,
            HaveChild = cates.Any(x => x.Pid == pid),
            Sort = m.Sort,
            InvokeNum = m.InvokeNum,
        });
    }

    public async Task<CmsInvokeCat> GetAsync(int id)
    {
        return await appLinkCateRepository.GetAsync(id);
    }
    public async Task<object> InsertAsync(CmsInvokeCat appLinkCate)
    {
        return await appLinkCateRepository.InsertAsync(appLinkCate);
    }

    public async Task<int> UpdateAsync(CmsInvokeCat appLinkCate)
    {
        return await appLinkCateRepository.UpdateAsync(appLinkCate);
    }

    public async Task<int> DeleteAsync(int id)
    {
        var res = await appLinkCateRepository.ExistsDataByCidAsync(id);
        if (res)
            return 0;
        return await appLinkCateRepository.DeleteByEntityIdAsync(id);
    }

    /// <summary>
    /// 列表
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<ViewCmsInvokeCat>> GetCatsAsync()
    {
        IEnumerable<CmsInvokeCat> cates = await GetAllAsync();
        return await AsCatsAsync(0, cates.ToList());
    }

    /// <summary>
    /// 递归栏目树
    /// </summary>
    /// <returns></returns>
    private static async Task<List<ViewCmsInvokeCat>> AsCatsAsync(int pid, List<CmsInvokeCat> cats = null)
    {
        var parent = cats.Where(P => P.Pid == pid);
        List<ViewCmsInvokeCat> lists = [];
        foreach (var item in parent)
        {
            ViewCmsInvokeCat Childer = new()
            {
                Id = item.Id,
                Name = item.Name,
                Sort = item.Sort,
                Remark = item.Remark,
                Url = item.Url,
                InvokeNum = item.InvokeNum,
            };
            Childer.Children = await GetSon(Childer, cats);
            if (Childer.Children != null)
            {
                Childer.HaveChild = true;
                Childer.Open = true;
            }
            lists.Add(Childer);
        }
        async Task<List<ViewCmsInvokeCat>> GetSon(ViewCmsInvokeCat cates, List<CmsInvokeCat> sonCates = null)
        {
            if (!sonCates.Exists(x => x.Pid == cates.Id))
            {
                return null;
            }
            else
            {
                return await AsCatsAsync(cates.Id, sonCates);
            }
        }
        return lists.OrderBy(o => o.Sort).ThenBy(o => o.Id).ToList();
    }
}
