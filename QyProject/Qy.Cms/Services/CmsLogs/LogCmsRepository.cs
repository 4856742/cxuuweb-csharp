﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System.Threading.Tasks;
using Qy.Cms.Entities;
using Qy.Base.Utilities;
using PmSoft.Data.Abstractions;
using Qy.Base.Dtos.Loging;
using Qy.Base.Extensions;

namespace Qy.Cms.Services.CmsLogs;

[Inject(ServiceLifetime.Scoped)]

public class LogCmsRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, LogCms, int>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;

    public async Task<IPagedList<LogCms>> GetPagingAsync(LogingQuery query)
    {
        Sql sql = Sql.Builder.Select("syslog.id").From(CmsDbTableNameConst.CmsLog + " as syslog");
        // .LeftJoin(DbTableName.QyUser + " as user").On("syslog.uid = user.id");
        //#region 根据用户权限判断数据权限
        //IUser user = await appContext.IUserService.CurrentUserIncDidsAsync();
        //user.GenQueryPermissionsSql(sql, "syslog.uid", "syslog.did");
        //#endregion

        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser();
        if (user.IsNotSuperAdminAndAllAuthData())
            sql.Where("syslog.uid = @0", user.UserId);
        else
        {
            if (!string.IsNullOrEmpty( query.SelectUids))
            {
                int[] uids = Json.Parse<int[]>(query.SelectUids);
                if (uids is not null && uids.Length > 0)
                    sql.Where("syslog.uid IN (@0)", uids);
            }
        }
        #endregion

        if (query.SelectDid != null && query.SelectDid != 0)
            sql.Where("syslog.did = @0", query.SelectDid);
        if (!string.IsNullOrEmpty(query.Method))
            sql.Where("syslog.method = @0", StringUtility.StripSQLInjection(query.Method));
        if (query.StartDate != null && query.EndDate != null)
            sql.Where("syslog.time between @0 AND @1", query.StartDate, query.EndDate);
        sql.OrderBy("syslog.id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    /// <summary>
    /// 记录日志不需要返回值，采用同步方式
    /// </summary>
    /// <param name="logSql"></param>
    /// <returns></returns>
    public override object Insert(LogCms logSql)
    {
        return DbContext.Insert(logSql);
    }

}
