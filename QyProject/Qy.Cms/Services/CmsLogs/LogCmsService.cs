﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using Qy.Base.AppBase;
using Qy.Base.Dtos.Loging;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using System;
using System.Threading.Tasks;

namespace Qy.Cms.Services.CmsLogs;

[Inject(ServiceLifetime.Scoped)]
public class LogCmsService(LogCmsRepository repository, 
    IApplicationContext applicationContext,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    UserDeptService userDeptService)
{
    public async Task<IPagedList<ViewLogCms>> GetPagingAsync(LogingQuery query)
    {
        var pagedList = await repository.GetPagingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            return v;
        })
            .WithRelatedAsync(
                foreignKeySelector: v => v.Uid,
                loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
                attachAction: (v, m) => v.NickName = m?.NickName
            );
    }
    public void InsertLog(string doThing, string url)
    {
        try
        {
            var logEntry = new LogCms()
            {
                Did = applicationContext.GetRequiredCurrentUser().DeptId,
                Method = applicationContext.HttpContext.GetMethod(),
                ContrAct = url,
                Ip = applicationContext.ClientInfo.ClientIpAddress,
                Time = DateTime.Now,
                DoThing = doThing,
                Uid = applicationContext.RequiredCurrentUser.UserId,
            };
            repository.Insert(logEntry);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
