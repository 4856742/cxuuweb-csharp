﻿using Qy.Base.Utilities.EPPlusExcel;
using System.Collections.Generic;

namespace Qy.Cms;


public class CmsOndutyConsts
{
    public static List<GreatExcelField> ViewCmsOndutyFields
    {
        get
        {
            return [
                   new() { Value = 1, FieldName = "DateTimeStr", Label = "日期", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 2, FieldName = "WeekDateStr", Label = "星期", Width = 10, Type = GreatExcelDataType.String },
                   new() { Value = 3, FieldName = "Leader", Label = "带班领导", Width = 20, Type =GreatExcelDataType.String },
                   new() { Value = 4, FieldName = "LeaderPhone", Label = "带班领导电话", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 5, FieldName = "Dutyer", Label = "值班员", Width = 20, Type = GreatExcelDataType.String },
                   new() { Value = 6, FieldName = "DutyerPhone", Label = "值班员电话", Width = 20, Type = GreatExcelDataType.String},                   
                   //new() { Value = 7, FieldName = "DeptName", Label = "登记部门", Width = 160, Md5 = GreatExcelDataType.String },
                   //new() { Value = 8, FieldName = "NickName", Label = "登记人", Width = 130, Md5 = GreatExcelDataType.String },
                   //new() { Value = 9, FieldName = "InsertTime", Label = "登记时间", Width = 230, Md5 = GreatExcelDataType.DateTime },
            ];
        }
    }
}


