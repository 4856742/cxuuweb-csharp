﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System.Threading.Tasks;
using Qy.Cms.Entities;
using Qy.Base.Utilities;
using System.Collections.Generic;
using Qy.Cms.Dtos;
using System;

namespace Qy.Cms.Services.Ondutys;

[Inject(ServiceLifetime.Scoped)]

public class CmsOndutyRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, CmsOnduty, int>(dbContext, applicationContext)
{

    public async Task<IEnumerable<CmsOnduty>> GetListAsync(QueryCmsOnduty query, DateTime startTime, DateTime endTime)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From($"{CmsDbTableNameConst.CmsOnduty}");
        //sql.LeftJoin(DbTableName.QyUser + " user").On("rece.uid=user.id");
        #region 根据用户权限判断数据权限
        //IUser user = await appContext.IUserService.LoginUserInfoIncludeDidsAsync();
        //判断当前用户是否为 审核员 如果是则显示全部内容
        //if (!isAuditor)
        //{
        //    user.GenQueryPermissionsSql(sql, "rece.uid", "rece.did");
        //}
        #endregion
        if (!string.IsNullOrEmpty( query.KeyWords))
            sql.Where("leader like @0 or dutyer like @0", "%" + StringUtility.StripSQLInjection(query.KeyWords) + "%");
        if (query.DateTime.Year > 1970)        
            sql.Where("date_time  between @0 AND @1", startTime, endTime);
        sql.OrderBy("id desc");
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
        //return await GetPagingEntitiesAsync(query.PageSize, query.PageIndex, sql);
    }

    public async Task<IEnumerable<CmsOnduty>> GetTodayAndTomorrowAsync()
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From($"{CmsDbTableNameConst.CmsOnduty}");
        sql.Where("date_time  >= CURDATE()  AND date_time < DATE_ADD(CURDATE(), INTERVAL 2 DAY)");
        sql.OrderBy("date_time asc");
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
        //return await GetPagingEntitiesAsync(query.PageSize, query.PageIndex, sql);
    }

    public async Task<CmsOnduty> GetOneBydateTimeAsync(DateTime dateTime)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsOnduty);
        sql.Where("date_time = @0 ", dateTime);
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }
}
