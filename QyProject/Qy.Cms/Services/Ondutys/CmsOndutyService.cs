﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Extensions;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Base.Utilities;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Qy.Cms.Services.Ondutys;

[Inject(ServiceLifetime.Scoped)]
public class CmsOndutyService(
    CmsOndutyRepository  cmsOndutyRepository,
    UserService userService,
    UserDeptService userDeptService, 
    IApplicationContext appContext)
{
    public async Task<IEnumerable<CmsOnduty>> GetTodayAndTomorrowAsync()
    {
        return await cmsOndutyRepository.GetTodayAndTomorrowAsync();
    }
    public async Task<ViewCmsOnduty> GetOneAsync(DateTime? startTime)
    {
        if (startTime == null)
            startTime = DateTime.Now;
        var time = startTime.Value.CurrentDay();
        var data = await cmsOndutyRepository.GetOneBydateTimeAsync(time);
        if (data == null) return null;
        return new ViewCmsOnduty()
        {
            Id = data.Id,
            //DateTime = data.DateTime,
            //DateInMonth = data.DateInMonth,
            Leader = data.Leader,
            LeaderPhone = data.LeaderPhone,
            Dutyer = data.Dutyer,
            DutyerPhone = data.DutyerPhone,
            //DateTimeStr = data.DateTime.ToString("yyyy年MM月dd日"),
            //WeekDateStr = data.WeekDate?.CheckAsDictIntWeek(),
        };
    }
    public async Task<List<ViewCmsOnduty>> GetAsListAsync(QueryCmsOnduty query)
    {
        int currentYear = query.DateTime.Year;
        int currentMonth = query.DateTime.Month;
        DateTime startTime = new(currentYear, currentMonth, 1);
        DateTime lastDayOfMonth = query.DateTime.AddMonths(1).AddDays(-1);
        int daysInMonth = lastDayOfMonth.Day;
        var pageList = await cmsOndutyRepository.GetListAsync(query, startTime, lastDayOfMonth);
        var pageListDict = pageList?.ToDictionary(x => x.DateInMonth);
        List<CmsOnduty> newList = [];

        for (int i = 1; i <= daysInMonth; i++)
        {
            CmsOnduty appOndutyCommand = new()
            {
                DateTime = new DateTime(currentYear, currentMonth, i),
                WeekDate = (int)new DateTime(currentYear, currentMonth, i).DayOfWeek
            };

            if (pageListDict.TryGetValue(i, out CmsOnduty inst))
            {
                appOndutyCommand.Id = inst.Id;
                appOndutyCommand.Uid = inst.Uid;
                appOndutyCommand.Did = inst.Did;
                appOndutyCommand.WeekDate = inst.WeekDate;
                appOndutyCommand.LeaderPhone = inst.LeaderPhone;
                appOndutyCommand.Leader = inst.Leader;
                appOndutyCommand.Dutyer = inst.Dutyer;
                appOndutyCommand.DutyerPhone = inst.DutyerPhone;
                appOndutyCommand.InsertTime = inst.InsertTime;
            }
            newList.Add(appOndutyCommand);
        }
        var users = await userService.GetUsersAsync(newList.Select(x => x.Uid));
        var depts = await userDeptService.GetAllDeptAsync();
        return [.. newList.Select(x => x.AsView(depts, users))];
    }

    public async Task<int> EditAsync(CmsOnduty model)
    {
        if (model.DateTime.Year < 1970)
            return 0;
        var dateTime = model.DateTime;
        var existingRecord = await cmsOndutyRepository.GetOneBydateTimeAsync(dateTime) ?? CreateNewRecordAsync(model);
        UpdateRecord(existingRecord, model);
        if (existingRecord.Id > 0)
            return await cmsOndutyRepository.UpdateAsync(existingRecord);
        var insert = await cmsOndutyRepository.InsertAsync(existingRecord);
        return Convert.ToInt32(insert);
    }
    private CmsOnduty CreateNewRecordAsync(CmsOnduty model)
    {
        var user = appContext.GetRequiredCurrentUser();
        return new CmsOnduty
        {
            Uid = user.UserId,
            Did = user.DeptId,
            InsertTime = DateTime.Now,
            WeekDate = (int)model.DateTime.DayOfWeek,
            DateInMonth = model.DateTime.Day,
            DateTime = model.DateTime,
            Leader = model.Leader,
            LeaderPhone = model.LeaderPhone,
            Dutyer = model.Dutyer,
            DutyerPhone = model.DutyerPhone,
        };
    }
    private static void UpdateRecord(CmsOnduty record, CmsOnduty inst)
    {
        record.LeaderPhone = inst.LeaderPhone;
        record.Leader = inst.Leader;
        record.Dutyer = inst.Dutyer;
        record.DutyerPhone = inst.DutyerPhone;
    }

    //private static void UpdateRecord(AppOndutyCommand record, AppOndutyCommand model)
    //{
    //    var stringValue = model.Val.ParseToString();
    //    var propertyMap = new Dictionary<string, Action>
    //        {
    //            { "Leader", () => record.Leader = stringValue },
    //            { "LeaderDuty", () => record.LeaderDuty = stringValue },
    //            { "Dutyer", () => record.Dutyer = stringValue },
    //            { "DutyerDuty", () => record.DutyerDuty = stringValue }
    //        };
    //    if (propertyMap.TryGetValue(model.Key, out Action value))
    //    {
    //        value();
    //    }
    //}

    public async Task<int> DeleteAsync(int id)
    {
        var get = await cmsOndutyRepository.GetAsync(id);
        if (get != null)
            return await cmsOndutyRepository.DeleteAsync(get);
        return 0;
    }
}
