﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Entities.Auth;
using Qy.Cms.AppBase;
using Qy.Cms.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.Categorys;

[Inject(ServiceLifetime.Scoped)]

public class CmsCategoryRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, CmsCategory, int>(dbContext, applicationContext)
{

    public async Task<IEnumerable<CmsCategory>> GetAllAsync()
    {
        var globalVer = await CacheVersionService.GetGlobalVersionAsync();
        return await GetTopEntitiesWithCacheAsync(1000, CachingExpirationType.Stable, () =>
        {
            return $"CmsCategoryAll:{globalVer}";
        }, () =>
        {
            Sql sql = Sql.Builder;
            sql.Select("id").From(CmsDbTableNameConst.CmsCategory).OrderBy("sort asc");
            return sql;
        });
    }
    public async Task<bool> ExistsByContentCidAsync(int cid)
    {
        return await DbContext.ExistsAsync<Content>("cid =@0", cid);
    }
}
