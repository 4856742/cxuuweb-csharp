﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PmSoft.Core;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Base.Services.Attachment;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.CategoryRoles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.Categorys;

[Inject(ServiceLifetime.Scoped)]
public class CmsCategoryService(
    CmsCategoryRepository cmsCategoryRepository,
    CmsCategoryRoleRepository categoryRoleRepository,
        AttachmentService attmentService,
     AttachmentHelper  attachmentHelper,
    IApplicationContext applicationContext)
{
    public async Task<IEnumerable<CmsCategory>> GetCategorysAsync(IEnumerable<int> userIds)
    {
        return await cmsCategoryRepository.GetEntitiesByIdsAsync(userIds);
    }
    public async Task<IEnumerable<CmsCategory>> GetAllAsync()
    {
        return await cmsCategoryRepository.GetAllAsync();
    }
    public async Task<CmsCategory> GetOneAsync(int id)
    {
        var cates = await GetAllAsync();
        return cates?.FirstOrDefault(a => a.Id == id);
    }

    public async Task<object> InsertAsync(CmsCategory cateModel)
    {
        var res = await cmsCategoryRepository.InsertAsync(cateModel);
        if (res != null)
        {
            await attachmentHelper.PublishAttachmentAsync(cateModel.BannerAd, res.ToString());
            await attachmentHelper.PublishAttachmentAsync(cateModel.BannerHeader, res.ToString());
        }
        return res;
    }
    public async Task<int> UpdateAsync(CmsCategory cateModel)
    {
        var get = await cmsCategoryRepository.GetAsync(cateModel.Id);
        if (get == null) return 0;
        cateModel.Id = get.Id;
        var res = await cmsCategoryRepository.UpdateAsync(cateModel);
        if (res > 0)
        {
            await attachmentHelper.PublishAttachmentAsync(cateModel.BannerAd, res.ToString());
            await attachmentHelper.PublishAttachmentAsync(cateModel.BannerHeader, res.ToString());
        }
        return res;
    }   
    public async Task<int> UpdateValChangeAsync(ValChangeModel catValChangeModel)
    {
        var get = await cmsCategoryRepository.GetAsync(catValChangeModel.Id);
        if (get == null) return 0;

        switch (catValChangeModel.Key)
        {
            case "Sort":
                get.Sort = Convert.ToInt32(catValChangeModel.Val);
                break;
            case "Name":
                get.Name = catValChangeModel.Val.ToString();
                break;
            case "Num":
                get.Num = Convert.ToInt32(catValChangeModel.Val);
                break;
            case "Ico":
                get.Ico = Convert.ToString(catValChangeModel.Val);
                break;
            case "Status":
                get.Status = Convert.ToBoolean(catValChangeModel.Val);
                break;
        }
        return await cmsCategoryRepository.UpdateAsync(get);
    }
    public async Task<int> DeleteAsync(int id)
    {
        if (await cmsCategoryRepository.ExistsByContentCidAsync(id))
            return -1;
        var res = await cmsCategoryRepository.DeleteByEntityIdAsync(id);
        if (res > 0)
        {
            //删除该内容下的所有图片和视频
            await attmentService.DeleteBatchAsync("cms_cat", id);
        }
        return res;
    }
    /// <summary>
    /// 检测资源权限
    /// </summary>
    public async Task<IEnumerable<CmsCategory>> CateRoleListAsync()
    {
        var cates = await GetAllAsync();
        if (applicationContext.GetRequiredCurrentUser().IsSuperAdmin())
            return cates;
        int deptId = applicationContext.GetRequiredCurrentUser().DeptId;
        IEnumerable<int> cids = await GetCatDeptRoleAsync(deptId);
        if (cids == null)
            return null;
        HashSet<int> idsToFind = new(cids);
        return cates?.Where(c => idsToFind.Contains(c.Id));
    }
    public async Task<IEnumerable<int>> GetCatDeptRoleAsync(int deptId)
    {
        var role = await categoryRoleRepository.GetOneByDidAsync(deptId);
        if (role == null || role.RoleCats == string.Empty)
            return null;
        return JsonConvert.DeserializeObject<IEnumerable<int>>(role.RoleCats);
    }



    /// <summary>
    /// 检测资源权限
    /// </summary>
    /// <param name="cid"></param>
    /// <returns></returns>
    public async Task<bool> CheckCatNotAuthAsync(int cid)
    {
        var cate = await CateRoleListAsync();
        return !cate.Any(x => x.Id == cid);
    }
    public async Task<IEnumerable<OptionItemDto>> IndentedOptionAsync()
    {
        IEnumerable<CmsCategory> list = await CateRoleListAsync();
        return list.AsIndentedOptionType();
    }

    /// <summary>
    /// 获取带权限的递归的全部栏目管理列表
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<OptionItemDto>> CmsCatOptionTypeAsync()
    {
        IEnumerable<CmsCategory> list = await CateRoleListAsync();
        return list.AsIndentedOptionType();
    }
    /// <summary>
    /// 获取递归的全部栏目管理列表
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<ViewCmsCategory>> GetContentCatesAsync()
    {
        var cates = await GetAllAsync();
        return await GetCatesRecursionAsync(0, cates.ToList());
    }
    /// <summary>
    /// 递归栏目树
    /// </summary>
    /// <param name="pid">父级Id</param>
    /// <param name="cates">数据源</param>
    /// <param name="selectConsole">禁用选择或展开控制</param>
    /// <returns></returns>
    public static async Task<List<ViewCmsCategory>> GetCatesRecursionAsync(int pid, List<CmsCategory> cates = null, bool selectConsole = false)
    {
        var parent = cates.Where(P => P.Pid == pid);
        List<ViewCmsCategory> lists = [];
        foreach (var item in parent)
        {
            ViewCmsCategory Childer = new()
            {
                Id = item.Id,
                Pid = item.Pid,
                Name = item.Name,
                Type = item.Type,
                Theme = item.Theme,
                Ctheme = item.Ctheme,
                BannerHeader = item.BannerHeader,
                Ico = item.Ico,
                Sort = item.Sort,
                Num = item.Num,
                Status = item.Status
            };
            if (item.Type == 1 && selectConsole == true)
            {
                Childer.Disabled = true;
                Childer.Spread = true;
            }
            Childer.Children = await GetSon(Childer, cates);
            lists.Add(Childer);
        }
        async Task<List<ViewCmsCategory>> GetSon(ViewCmsCategory cates, List<CmsCategory> sonCates = null)
        {
            if (!sonCates.Exists(x => x.Pid == cates.Id))
                return null;
            else
                return await GetCatesRecursionAsync(cates.Id, sonCates);
        }
        return lists.OrderBy(o => o.Sort).ThenBy(o => o.Id).ToList();
    }


    #region 前台用
    /// <summary>
    /// 获取指定栏目列表
    /// </summary>
    /// <param name="cids"></param>
    /// <returns></returns>
    public async Task<IEnumerable<CmsCategory>> CateListByIdAsync(int[] cids)
    {
        IEnumerable<CmsCategory> cats = await GetAllAsync();
        return cats?.Where(o => cids.Contains(o.Id)).OrderBy(o => o.Sort);
    }

    /// <summary>
    /// 获取子栏目Ids
    /// </summary>
    /// <param name="pid"></param>
    /// <returns></returns>
    public async Task<int[]> CatSonListIdsAsync(int pid)
    {
        IEnumerable<CmsCategory> cats = await GetAllAsync();
        IEnumerable<CmsCategory> sonCats = cats.Where(o => o.Pid == pid).OrderBy(o => o.Sort);
        return sonCats?.Select(o => o.Id)?.ToArray();
    }
    /// <summary>
    /// 获取子栏目列表
    /// </summary>
    /// <param name="pid"></param>
    /// <param name="exclude">排除频道类型</param>
    /// <returns></returns>
    public async Task<IEnumerable<CmsCategory>> CatSonListAsync(int pid, bool exclude = false)
    {
        IEnumerable<CmsCategory> cats = await GetAllAsync();
        if (exclude == false)
        {
            return cats.Where(o => o.Pid == pid && o.Status == true).OrderBy(o => o.Sort);
        }
        return cats.Where(o => o.Pid == pid && o.Status == true && o.Type != (int)ContentTypeEnum.Special).OrderBy(o => o.Sort);
    }
    /// <summary>
    /// 当前栏目路径
    /// </summary>
    /// <param name="nodeId"></param>
    /// <returns></returns>
    public async Task<IEnumerable<CmsCategory>> FindPathAsync(int nodeId)
    {
        IEnumerable<CmsCategory> nodes = await GetAllAsync();
        List<CmsCategory> path = [];
        CmsCategory currentNode = nodes.FirstOrDefault(n => n.Id == nodeId);
        while (currentNode != null)
        {
            path.Add(currentNode);
            currentNode = nodes.FirstOrDefault(n => n.Id == currentNode.Pid);
        }
        //反转
        path.Reverse();
        return path;
    }
    #endregion
}
