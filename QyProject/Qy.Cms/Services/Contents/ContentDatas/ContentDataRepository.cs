﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using Qy.Cms.Entities;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.ContentDatas;

[Inject(ServiceLifetime.Scoped)]

public class ContentDataRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, ContentData, int>(dbContext, applicationContext)
{

    public async Task<ContentData> GetOneByAidAsync(int aid)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsContentData);
        sql.Where("aid = @0 ", aid);
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }

    public async Task<bool> ExistsByAidAsync(int aid)
    {
        return await DbContext.ExistsAsync<ContentData>("aid =@0", aid);
    }
}
