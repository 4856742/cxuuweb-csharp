﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Cms.Entities;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.ContentDatas;

[Inject(ServiceLifetime.Scoped)]
public class ContentDataService(ContentDataRepository contentDataRepository)
{
    public async Task<ContentData> GetOneByAidAsync(int aid)
    {
        return await contentDataRepository.GetOneByAidAsync(aid);
    }
    public async Task<object> InsertAsync(ContentData  data)
    {
        return await contentDataRepository.InsertAsync(data);
    }
    public async Task<int> UpdateAsync(ContentData data)
    {
        return await contentDataRepository.UpdateAsync(data);
    }
    public async Task<int> DeleteAsync(int aid)
    {
        var get = await GetOneByAidAsync(aid);
        return await contentDataRepository.DeleteAsync(get);
    }

    public async Task<bool> ExistsByAidAsync(int id)
    {
        return await contentDataRepository.ExistsByAidAsync(id);
    }
}
