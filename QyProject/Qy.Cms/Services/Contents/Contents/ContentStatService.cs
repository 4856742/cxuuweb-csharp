﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using PmSoft.Cache.Abstractions;
using PmSoft.Core;
using Qy.Cms.Dtos;
using System;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.Contents;

[Inject(ServiceLifetime.Scoped)]
public class ContentStatService(
    ContentRepository contentRepository,
    IMemoryCache memoryCache)
{

    public async Task<ContentStatsVO> CountUserGroupAsync()
    {
        string cacheName = "Count:ContentStatsVO";
        ContentStatsVO count = memoryCache.Get<ContentStatsVO>(cacheName);
        if (count == null)
        {
            var ct = await contentRepository.CountContentAsync();
            count = new ContentStatsVO
            {
                CountStats = await contentRepository.CountUserGroupAsync(),
                Total = ct.Total,
                Hits = ct.Hits
            };
            memoryCache.Set(cacheName, count, TimeSpan.FromSeconds(1800));
        }
        return count;
    }
}
