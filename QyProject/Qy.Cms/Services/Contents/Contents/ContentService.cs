﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco;
using PmSoft.Web.Abstractions.Attachment;
using Qy.Base.AppBase;
using Qy.Base.Entities.Auth;
using Qy.Base.Extensions;
using Qy.Base.Services.Attachment;
using Qy.Base.Services.Auth.QyUser;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using Qy.Cms.Services.Contents.Categorys;
using Qy.Cms.Services.Contents.ContentDatas;
using Qy.Cms.Services.Contents.ContentHits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.Contents;

[Inject(ServiceLifetime.Scoped)]
public class ContentService(
    ContentRepository contentRepository,
    ContentDataService contentDataService,
    CmsCategoryService cmsCategoryService,
    ContentHitService contentHitService,
    AttachmentService attmentService,
     AttachmentUserService attachmentUserService,
    UserService userService,
      AttachmentHelper attachmentHelper,
      AttachmentManagerService attachmentManagerService,
    UserDeptService userDeptService,
    PetaPocoUnitOfWork<AppDbContext> unitOfWork,
    ICachedEntityLoader<AppDbContext> cachedEntityLoader,
    IApplicationContext applicationContext)
{
    public async Task<ViewContentOption> GetOpetionAsync(int id)
    {
        Content body = await contentRepository.GetAsync(id);
        if (body == null || body.Status == false)
            return null;
        var cate = await cmsCategoryService.GetOneAsync(body.Cid);
        if (cate?.Status == false)
            return null;
        var data = await contentDataService.GetOneByAidAsync(id);

        return new()
        {
            Id = id,
            Cid = body.Cid,
            CateName = cate?.Name,
            Content = data?.Content,
            Title = body?.Title,
        };
    }
    public async Task<ViewContentAndData> GetAndDataAsync(int id)
    {
        Content body = await contentRepository.GetAsync(id);
        if (body == null || body.Status == false)
            return null;
        var cate = await cmsCategoryService.GetOneAsync(body.Cid);
        if (cate?.Status == false)
            return null;
        var data = await contentDataService.GetOneByAidAsync(id);
        var hits = await contentHitService.GetOneByAidAsync(id);
        var user = await userService.GetAsync(body.Uid);
        UserDept dept = null;
        if (body.Did > 0)
            dept = await userDeptService.GetAsync(body.Did);
        var content = body.AsNewOne(data, cate, user, dept, hits);
        content.AttmentList = await attachmentUserService.GetListAsync(content.Attachments);
        content.CatPath = await cmsCategoryService.FindPathAsync(body.Cid);
        await contentHitService.UpdateHitAsync(id);//浏览次数+1
        return content;
    }

    public async Task<EditContentAndData> GetEditAsync(int id)
    {
        var content = await contentRepository.GetAsync(id);
        ContentData contentData = await contentDataService.GetOneByAidAsync(id);
        return content.ToEditContentAndData(contentData ?? null);
    }
    //管理分页列表使用
    public async Task<IPagedList<ViewContentAndCate>> GetPageingAsync(ContentQuery query, IEnumerable<CmsCategory> catList, AuthUser user)
    {
        if (!catList.Any()) query.Cids = null;//必须要判断，否则  linq select返回 0 
        else query.Cids = catList.Select(x => x.Id)?.ToArray();
        var pagedList = await contentRepository.GetPageingAsync(query);
        var deptAll = await userDeptService.GetAllDeptAsync();
        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            v.DeptName = deptAll.GetDepartmentHierarchyPath(m.Did);
            v.HaveEditAuth = user.IsHaveDataEditAuthAsync(v.Uid, v.Did);
            return v;
        })
           .WithRelatedAsync(
               foreignKeySelector: v => v.Uid,
               loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<User, int>,
               attachAction: (v, m) => v.NickName = m?.NickName
           ).WithRelatedAsync(
               foreignKeySelector: v => v.Cid,
               loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<CmsCategory, int>,
               attachAction: (v, m) => v.CateName = m?.Name
           );
    }
    public async Task<IPagedList<Content>> GetPageAsync(ContentQuery query)
    {
        return await contentRepository.GetPageAsync(query);
    }
    public async Task<IPagedList<ViewContentAndCate>> GetCmsPageingAsync(ContentQuery query)
    {
        //防止显示列表为空
        if (query.PageSize < 1)
            query.PageSize = 15;


        var pagedList = await contentRepository.GetCmsPageingAsync(query);

        return await pagedList.ToPagedDto(m =>
        {
            var v = m.AsView();
            return v;
        })
           .WithRelatedAsync(
               foreignKeySelector: v => v.Uid,
               loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<ContentHit, int>,
               attachAction: (v, m) =>
               {
                   v.Likes = m?.Likes;
                   v.Hits = m?.Hits;
               }
           ).WithRelatedAsync(
               foreignKeySelector: v => v.Cid,
               loadRelatedEntities: cachedEntityLoader.GetEntitiesByIdsAsync<CmsCategory, int>,
               attachAction: (v, m) => v.CateName = m?.Name
           );
    }
    public async Task<Content> InsertAsync(Content content, ContentData data)
    {
        if (await cmsCategoryService.CheckCatNotAuthAsync(content.Cid))
            return null;
        try
        {
            Content newContent = null;
            //执行事务
            await unitOfWork.ExecuteInTransactionAsync(async () =>
            {
                content.Uid = applicationContext.RequiredCurrentUser.UserId;
                content.Did = applicationContext.GetRequiredCurrentUser().DeptId;
                var insertContent = await contentRepository.InsertAsync(content);
                if (insertContent == null)
                    return null;
                // 批量处理内容数据和热度统计的插入
                await UpdateOrInsertContentDataAsync(content.Id, data);
                await InitializeContentHitAsync(content.Id);
                return newContent = content;
            });
            await PublishAttachmentAsync(content.Id, content, data);
            return newContent;
        }
        catch (Exception ex)
        {
            // 记录异常信息  
            Console.WriteLine(ex);
            return null;
        }
    }

    public async Task<int> UpdateAsync(Content content, ContentData data)
    {
        if (await cmsCategoryService.CheckCatNotAuthAsync(content.Cid))
            return -1;
        try
        {
            int upContent = 0;
            //执行事务
            await unitOfWork.ExecuteInTransactionAsync(async () =>
            {
                var getContent = await contentRepository.GetAsync(content.Id);
                if (getContent == null) return 0;
                var user = applicationContext.GetRequiredCurrentUser();
                if (!user.IsHaveDataEditAuthAsync(getContent.Uid, getContent.Did))
                    return -1;

                getContent.Type = content.Type;
                getContent.Title = content.Title;
                getContent.Abstract = content.Abstract;
                getContent.Status = content.Status;
                getContent.CoverUrl = content.CoverUrl;
                getContent.CoverCount = content.CoverCount;
                getContent.Cid = content.Cid;
                getContent.AttA = content.AttA;
                getContent.AttB = content.AttB;
                getContent.AttC = content.AttC;
                getContent.Examine = content.Examine;
                getContent.EditTime = DateTime.Now;

                upContent = await contentRepository.UpdateAsync(getContent);
                await UpdateOrInsertContentDataAsync(content.Id, data);
                //await InitializeContentHitAsync(content.Id);
                return upContent;
            });
            await PublishAttachmentAsync(content.Id, content, data);
            return upContent;
        }
        catch (Exception ex)
        {          // 记录异常信息  
            Console.WriteLine(ex);
            return 0;
        }
    }



    // 更新或插入内容数据  
    private async Task UpdateOrInsertContentDataAsync(int id, ContentData data)
    {
        var existingData = await contentDataService.GetOneByAidAsync(id);
        bool succeeded;
        if (existingData != null)
        {
            existingData.Content = data.Content;
            existingData.Attachments = data.Attachments;
            existingData.VideoUrl = data.VideoUrl;
            succeeded = await contentDataService.UpdateAsync(existingData) > 0;
        }
        else
        {
            data.Aid = id;
            var insert = await contentDataService.InsertAsync(data);
            succeeded = insert != null;
        }
    }
    // 发布附件视频和内容中的附件  
    private async Task PublishAttachmentAsync(int aid, Content content, ContentData data)
    {
        if (content.Type == ContentTypeEnum.Video)
        {
            //单独发布视频封面图
            if (!string.IsNullOrEmpty(content.CoverUrl)) {
                try
                {
                    List<string> attId = JsonConvert.DeserializeObject<List<string>>(content.CoverUrl);
                    if(attId.Count > 0 && !string.IsNullOrEmpty(attId[0]))
                        await attachmentManagerService.PublishAttachmentAsync(attId[0], aid.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
              
            }               
            if (!string.IsNullOrEmpty(data.VideoUrl))
                await PublishAttachmentVideoAsync(data.VideoUrl, aid);
        }
        if (content.Type == ContentTypeEnum.Articles && !string.IsNullOrEmpty(data.Content))
        {
            await attachmentHelper.PublishAttachmentsContentAsync(data.Content, aid);
        }
    }
    // 初始化热度统计  
    private async Task InitializeContentHitAsync(int id)
    {
        if (!await contentHitService.ExistsByAidAsync(id))
        {
            await contentHitService.InsertAsync(id);
        }
    }

    public async Task<int> UpdateContentAttAsync(ValChangeModel formChange)
    {
        var get = await contentRepository.GetAsync(formChange.Id);
        if (get == null) return 0;

        if (await cmsCategoryService.CheckCatNotAuthAsync(get.Cid))
            return 0;

        switch (formChange.Key)
        {
            case "Status":
                get.Status = formChange.Val.ParseToBool();
                break;
            case "AttA":
                get.AttA = formChange.Val.ParseToBool();
                break;
            case "AttB":
                get.AttB = formChange.Val.ParseToBool();
                break;
            case "AttC":
                get.AttC = formChange.Val.ParseToBool();
                break;
        }
        return await contentRepository.UpdateAsync(get);
    }

    public async Task<int> MultiTransferAsync(ContentMultiSelect data)
    {
        try
        {
            int count = 0;
            int[] uids = Array.ConvertAll(data.Ids.Split(','), int.Parse);
            if (uids.Length > 0 && data.TargetCid > 0)
            {
                if (await cmsCategoryService.CheckCatNotAuthAsync(data.TargetCid))
                    return 0;
                for (int i = 0; i < uids.Length; i++)
                {
                    Content get = await contentRepository.GetAsync(uids[i]);
                    if (get == null) continue;
                    //if (await cmsCategoryService.CheckCatNotAuthAsync(get.Cid))
                    //    continue;
                    get.Cid = data.TargetCid;
                    var insert = await contentRepository.UpdateAsync(get);
                    if (insert > 0) count++;
                }
            }
            return count;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            return 0;
        }
    }
    public async Task<int> MultiDeleteAsync(ContentMultiSelect data)
    {
        int count = 0;
        int[] uids = Array.ConvertAll(data.Ids.Split(','), int.Parse);
        if (uids.Length > 0)
        {
            for (int i = 0; i < uids.Length; i++)
            {
                var del = await DeleteAsync(uids[i]);
                if (del > 0) count++;
            }
        }
        return count;
    }
    public async Task<int> DeleteAsync(int id)
    {
        var content = await contentRepository.GetAsync(id);
        if (content == null) return 0;
        if (await cmsCategoryService.CheckCatNotAuthAsync(content.Cid))
            return 0;
        try
        {
            int i = 0;
            //执行事务
            await unitOfWork.ExecuteInTransactionAsync(async () =>
            {
                await contentDataService.DeleteAsync(id);//删除内容表
                await contentHitService.DeleteAsync(id);//删除点击量统计
                await contentRepository.DeleteByEntityIdAsync(id);
                //删除该内容下的所有图片和视频
                await attmentService.DeleteBatchAsync("cms_img", id);
                await attmentService.DeleteBatchAsync("cms_video", id);
                return i = 1;
            });
            return i;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    /// <summary>
    /// 发布附件并删除当前租户已经发布过的附件视频
    /// </summary>
    /// <param name="attachmentId"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public async Task PublishAttachmentVideoAsync(string attachmentId, object tenantId)
    {
        if (tenantId == null)
        {
            throw new ArgumentNullException(nameof(tenantId), "租户ID不能为空");
        }

        var attachment = await attachmentManagerService.PublishAttachmentAsync(attachmentId, tenantId.ToString());
        if (!string.IsNullOrEmpty(attachment.TenantType))
        {
            var sysAttachments = await attmentService.GetListAsync(attachment.TenantType, tenantId.ToString());
            //排除当前租户已经发布过的附件视频
            sysAttachments = sysAttachments.Where(m => m.Id != attachmentId).ToList();
            if (sysAttachments.Any())
            {
                await attmentService.DeleteBatchAsync([.. sysAttachments.Select(x => x.Id)]);
            }
        }
    }


    public async Task<List<ViewContentAndCate>> QyList(ContentQuery query)
    {
        var contents = await contentRepository.GetListByLimtAsync(query);
        IEnumerable<ContentHit> hits = await contentHitService.GetListByAidsAsync(contents.Select(x => x.Id));
        //IEnumerable<ContentPosition> positions = await contentPositionService.GetListByAidsAsync(contents.Select(x => x.Id));
        IEnumerable<CmsCategory> cates = await cmsCategoryService.GetCategorysAsync(contents.Select(x => x.Cid));
        return contents.Select(x => x.AsNewAndCate(cates, hits)).ToList();
    }
}
