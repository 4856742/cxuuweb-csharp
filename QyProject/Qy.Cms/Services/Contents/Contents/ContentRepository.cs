﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Base.Extensions;
using Qy.Base.Utilities;
using Qy.Cms.AppBase;
using Qy.Cms.Dtos;
using Qy.Cms.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.Contents;

[Inject(ServiceLifetime.Scoped)]

public class ContentRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, Content, int>(dbContext, applicationContext)
{
    private readonly IApplicationContext applicationContext = applicationContext;
    public async Task<IPagedList<Content>> GetPageingAsync(ContentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("Id").From(CmsDbTableNameConst.CmsContent);

        #region 根据用户权限判断数据权限
        var user = applicationContext.GetRequiredCurrentUser();
        user.GenQueryPermissionsSql(sql, "uid", "did");

        if (user.IsSuperAdmin())
        {
            //这里IF不能合并判断，因为超级管理员在CID不大于0时要显示全部内容
            if (query.Cid > 0)
                sql.Where("cid = @0", query.Cid);
        }
        else
        {
            //判断资源分配,Cid==0时，查询全部，数据库cid=0时，查询返回为空数据
            if (query.Cid > 0 && query.Cids.Any(x => x == query.Cid))
                sql.Where("cid = @0", query.Cid);
            else if (user.IsNotSuperAdminAndAllAuthData() && query.Cids != null)
                sql.Where("cid IN (@Cids)", new { query.Cids });
            else
                sql.Where("cid = 0");
        }

        #endregion

        //属性
        if (query.Att == 1)
            sql.Where("att_a = @0", true);
        if (query.Att == 2)
            sql.Where("att_b = @0", true);
        if (query.Att == 3)
            sql.Where("att_c = @0", true);

        if (query.Status != null)
            sql.Where("status = @0", query.Status);
        if (!string.IsNullOrEmpty(query.Title))
            sql.Where("title like @0", "%" + StringUtility.StripSQLInjection(query.Title) + "%");

        if (query.StartDate != null && query.EndDate != null)
            sql.Where("create_time between @0 AND @1", query.StartDate, query.EndDate);

        if (query.Orderby == 1)
            sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    /// <summary>
    /// 前端查询分页
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public async Task<IPagedList<Content>> GetPageAsync(ContentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("Id").From(CmsDbTableNameConst.CmsContent);
        //属性
        if (query.Att == 1)
            sql.Where("att_a = @0", true);
        if (query.Att == 2)
            sql.Where("att_b = @0", true);
        if (query.Att == 3)
            sql.Where("att_c = @0", true);
        sql.Where("status = @0", true);
        if (!string.IsNullOrEmpty(query.Title))
            sql.Where("title like @0", "%" + StringUtility.StripSQLInjection(query.Title) + "%");

        if (query.StartDate != null && query.EndDate != null)
            sql.Where("great_time between @0 AND @1", query.StartDate, query.EndDate);

        if (query.Orderby == 1)
            sql.OrderBy("id desc");

        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    public async Task<IPagedList<Content>> GetCmsPageingAsync(ContentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsContent);
        if (query.Cid > 0)
            sql.Where("cid = @0", query.Cid);
        if (query.IsCover == true)
            sql.Where("cover_count >0");
        if (!string.IsNullOrEmpty(query.Title))
            sql.Where("title like @0", "%" + StringUtility.StripSQLInjection(query.Title) + "%");
        sql.Where("status = @0", true);
        sql.OrderBy("id desc");
        return await GetPagedEntitiesAsync(sql, query.PageIndex, query.PageSize);
    }
    /// <summary>
    /// 分页调用，暂时无使用
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public async Task<IPagedList<Content>> GetListAsync(ContentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsContent);
        //属性
        if (query.Att == 1)
            sql.Where("att_a = @0", true);
        if (query.Att == 2)
            sql.Where("att_b = @0", true);
        if (query.Att == 3)
            sql.Where("att_c = @0", true);
        //图片
        if (query.IsCover)
            sql.Where("cover_count > @0", 0);
        if (query.Cids != null && query.Cids.Length > 0)
            sql.Where("cid in (@Cids)", new { query.Cids });
        sql.Where("status = @0", true);
        //排序
        if (query.Orderby == 1)
            sql.OrderBy("id desc");
        else
            sql.OrderBy("id asc");
        //sql.Append("limit @0", query.Limit);
        return await GetPagedEntitiesAsync(sql, 1, query.Limit);
    }

    /// <summary>
    /// 列表调用
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public async Task<IEnumerable<Content>> GetListByLimtAsync(ContentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsContent);
        //属性
        if (query.Att == 1)
            sql.Where("att_a = @0", true);
        if (query.Att == 2)
            sql.Where("att_b = @0", true);
        if (query.Att == 3)
            sql.Where("att_c = @0", true);
        //图片
        if (query.IsCover)
            sql.Where("cover_count > 0");
        if (query.Cids != null && query.Cids.Length > 0)
            sql.Where("cid in (@Cids)", new { query.Cids });
        if (query.Dids != null && query.Dids.Length > 0)
            sql.Where("did in (@Dids)", new { query.Dids });
        sql.Where("status = 1");
        //排序
        switch (query.Orderby)
        {
            case 1:
                sql.OrderBy("id DESC");
                break;
            case 2:
                sql.OrderBy("id ASC");
                break;
            default:
                sql.OrderBy("id ASC"); // 默认排序  
                break;
        }
        sql.Append("limit @0", query.Limit);
        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }
    /// <summary>
    /// 栏目内指定条数的集合，留下备用
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public async Task<IEnumerable<Content>> GetListByCidLimtAsync(ContentQuery query)
    {
        Sql sql = Sql.Builder;
        sql.Select("t1.id").From($"{CmsDbTableNameConst.CmsContent} t1");
        //图片
        if (query.IsCover)
            sql.Where("t1.cover_count > 0");
        if (query.Cids != null && query.Cids.Length > 0)
            sql.Where($"(SELECT COUNT(1) from {CmsDbTableNameConst.CmsContent} t2 where t1.cid = t2.cid and t1.id <= t2.id) <={query.Limit} and t1.cid in (@Cids)", new { query.Cids });
        else
            sql.Where($"(SELECT COUNT(1) from {CmsDbTableNameConst.CmsContent} t2 where t1.cid = t2.cid and t1.id <= t2.id) <={query.Limit}");
        if (query.Dids != null && query.Dids.Length > 0)
            sql.Where("t1.did in (@Dids)", new { query.Dids });
        sql.Where("t1.status = @0", true);
        sql.OrderBy("t1.cid,t1.id");

        var ids = await DbContext.FetchAsync<int>(sql);
        return await GetEntitiesByIdsAsync(ids);
    }


    public async Task<List<CountStats>> CountUserGroupAsync()
    {
        Sql sql = Sql.Builder;
        sql.Select("COUNT(1) AS Value, content.did AS did,dept.name AS Name")
            .From(CmsDbTableNameConst.CmsContent + " as content")
            .LeftJoin(DbTableName.QyUserDept + " as dept").On("content.did = dept.id");
        //sql.Where("DATE_FORMAT(content.great_time,'%Y') = DATE_FORMAT(NOW(),'%Y')");
        //sql.OrderBy("value desc");
        //sql.Where("dept.Id !=1 ");
        sql.GroupBy("did");
        return await DbContext.FetchAsync<CountStats>(sql);
    }

    public async Task<ContentStatsVO> CountContentAsync()
    {
        var dao = DbContext;
        try
        {
            await dao.OpenSharedConnectionAsync();
            return new()
            {
                Total = await dao.ExecuteScalarAsync<int>($"SELECT count(1) FROM {CmsDbTableNameConst.CmsContent}"),
                Hits = await dao.ExecuteScalarAsync<int>($"SELECT SUM(hits) FROM {CmsDbTableNameConst.CmsContentHit}"),
            };
        }
        finally
        {

            dao.CloseSharedConnection();
        }
    }

    public async Task<List<CountStats>> CountContentForDeptAsync()
    {
        Sql sql = Sql.Builder;
        sql.Select("COUNT(1) AS Value, content.did AS did,dept.name AS Name")
            .From(CmsDbTableNameConst.CmsContent + " as content")
            .LeftJoin(DbTableName.QyUserDept + " as dept").On("content.did = dept.id");
        sql.Where("DATE_FORMAT(content.great_time,'%Y') = DATE_FORMAT(NOW(),'%Y')");
        //sql.OrderBy("value desc");
        //sql.Where("dept.Id !=1 ");
        sql.GroupBy("did");
        return await DbContext.FetchAsync<CountStats>(sql);
    }
}
