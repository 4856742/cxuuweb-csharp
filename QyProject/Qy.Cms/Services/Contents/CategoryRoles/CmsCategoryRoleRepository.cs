﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using Qy.Cms.Entities;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.CategoryRoles;

[Inject(ServiceLifetime.Scoped)]

public class CmsCategoryRoleRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, CmsCategoryRole, int>(dbContext, applicationContext)
{
    public async Task<CmsCategoryRole> GetOneByDidAsync(int did)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsCategoryRole)
            .Where("did =@0", did).OrderBy("id desc");
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }


    public async Task<CmsCategoryRole> GetOneByRoleModelKeyAsync(string key)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsCategoryRole)
            .Where("role_model_key =@0", key).OrderBy("id desc");
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }
}
