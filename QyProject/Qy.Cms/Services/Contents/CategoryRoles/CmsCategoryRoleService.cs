﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Services.Auth.QyUserDept;
using Qy.Cms.Entities;
using System;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.CategoryRoles;

[Inject(ServiceLifetime.Scoped)]
public class CmsCategoryRoleService(
   UserDeptService userDeptService,
   CmsCategoryRoleRepository roleRepository)
{

    public async Task<CmsCategoryRole> GetOneByDidAsync(int did)
    {
        return await roleRepository.GetOneByDidAsync(did);
    }

    /// <summary>
    /// 添加或修改部门权限
    /// </summary>
    /// <param name="roles"></param>
    /// <returns></returns>
    public async Task<int> InsertOrUpdateDidRoleAsync(CmsCategoryRole roles)
    {
        var dept = await userDeptService.GetAsync(roles.Did);
        if (dept == null)
            return 0;
        var role = await roleRepository.GetOneByDidAsync(roles.Did);
        if (role != null)
        {
            role.Did = roles.Did;
            role.RoleCats = roles.RoleCats;
            return await roleRepository.UpdateAsync(role);
        }
        else
        {
            object insert = await roleRepository.InsertAsync(roles);
            return Convert.ToInt32(insert);
        }
    }


}
