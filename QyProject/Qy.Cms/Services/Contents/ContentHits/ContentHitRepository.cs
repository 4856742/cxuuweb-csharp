﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using Qy.Cms.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.ContentHits;

[Inject(ServiceLifetime.Scoped)]

public class ContentHitRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, ContentHit, int>(dbContext, applicationContext)
{

    public async Task<ContentHit> GetOneByAidAsync(int aid)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsContentHit);
        sql.Where("aid = @0 ", aid);
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }

    public async Task<IEnumerable<ContentHit>> GetListByAidsAsync(IEnumerable<int> aids)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsContentHit);
        if(!aids.Any())
            return null;
        sql.Where("aid in (@0) ", aids);
        var ids = await DbContext.FetchAsync<int>(sql);
        if (ids.Count <=0) return null;
        return await GetEntitiesByIdsAsync(ids);
    }

    public async Task<int> UpdateHitAsync(int aid)
    {
        Sql sql = Sql.Builder;
        sql.Append("SET hits = hits + 1").Where("aid =@0", aid);
        return await DbContext.UpdateAsync<ContentHit>(sql);
    }
    public async Task<int> UpdateLikeAsync(int aid)
    {
        Sql sql = Sql.Builder;
        sql.Append("SET likes = likes + 1").Where("aid =@0", aid);
        return await DbContext.UpdateAsync<ContentHit>(sql);
    }

    public async Task<bool> ExistsByAidAsync(int aid)
    {
        return await DbContext.ExistsAsync<ContentHit>("aid =@0", aid);
    } 
}
