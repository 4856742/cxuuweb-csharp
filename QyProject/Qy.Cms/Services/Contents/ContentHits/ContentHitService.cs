﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Cms.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Services.Contents.ContentHits;

[Inject(ServiceLifetime.Scoped)]
public class ContentHitService(ContentHitRepository contentHitRepository)
{
    public async Task<IEnumerable<ContentHit>> GetListByAidsAsync(IEnumerable<int> aids)
    {
        return await contentHitRepository.GetListByAidsAsync(aids);
    }
    public async Task<ContentHit> GetOneByAidAsync(int aid)
    {
        var get = await contentHitRepository.GetOneByAidAsync(aid);
        if (get == null)
        {
            var res = await InsertAsync(aid);
            return await contentHitRepository.GetAsync(Convert.ToInt32(res));
        }
        return get;
    }
    public async Task<object> InsertAsync(int aid)
    {
        var newData = new ContentHit { Aid = aid, Hits = 0, Likes = 0 };
        return await contentHitRepository.InsertAsync(newData);
    }
    public async Task<int> UpdateHitAsync(int id)
    {
        return await contentHitRepository.UpdateHitAsync(id);
    }
    public async Task<int> UpdateLikeAsync(int aid)
    {
        return await contentHitRepository.UpdateLikeAsync(aid);
    }
    public async Task<bool> ExistsByAidAsync(int id)
    {
        return await contentHitRepository.ExistsByAidAsync(id);
    }
    public async Task<int> DeleteAsync(int aid)
    {
        var get = await contentHitRepository.GetOneByAidAsync(aid);
        return await contentHitRepository.DeleteAsync(get);
    }
}
