﻿namespace Qy.Cms.Services.CmsConfigs;

public class PresetConfigCmsOnduty
{
    public string Name { get; set; }
    public string Phone { get; set; }
}

public class CmsConfigConsts
{
    /// <summary>
    /// 指挥中心值班带班领导预设
    /// </summary>
    public const string CmsOndutyLeaderKey = "onduty_leader";
    /// <summary>
    /// 指挥中心值班领导预设
    /// </summary>
    public const string CmsOndutyDutyerKey = "onduty_dutyer";

}
