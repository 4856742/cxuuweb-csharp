﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using Qy.Base.Dtos.Auth.UserDto;
using Qy.Base.Services.Auth.QyUser;
using Qy.Cms.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qy.Cms.Services.CmsConfigs;

[Inject(ServiceLifetime.Scoped)]
public class AppConfigService(
   UserService userService,
   CmsConfigRepository roleRepository)
{
    /// <summary>
    /// 值班人员预设
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public async Task<IEnumerable<PresetConfigCmsOnduty>> GetDutyerResetAsync(string key)
    {
        var userRoles = await roleRepository.GetOneByKeyAsync(key);
        if (userRoles == null && !string.IsNullOrEmpty( userRoles.ValueData ))
            return [];
        try
        {
            return Json.Parse<List<PresetConfigCmsOnduty>>(userRoles.ValueData);
        }
        catch 
        {
            return [];
        }
    }
    /// <summary>
    /// 用户列表
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public async Task<IEnumerable<ViewUser>> GetUserListAsync(string key)
    {
        var userRoles = await roleRepository.GetOneByKeyAsync(key);
        if (userRoles == null && !string.IsNullOrEmpty(userRoles.ValueData))
            return [];
        try
        {
            List<int> ids = Json.Parse<List<int>>(userRoles.ValueData);
            return await userService.GetUserListAsync(ids);
        }
        catch
        {
            return [];
        }


    }

    /// <summary>
    /// 添加或修改
    /// </summary>
    /// <param name="docCatRole"></param>
    /// <returns></returns>
    public async Task<int> InsertOrUpdateModelKeyAsync(CmsConfig docCatRole)
    {
        // ValueData  由前端序列化
        var role = await roleRepository.GetOneByKeyAsync(docCatRole.Label);
        if (role.Label != null)
        {
            role.ValueData = docCatRole.ValueData;
            return await roleRepository.UpdateAsync(role);
        }
        else
        {
            CmsConfig newInsert = new()
            {
                Label = docCatRole.Label,
                ValueData = docCatRole.ValueData
            };
            object insert = await roleRepository.InsertAsync(newInsert);
            return Convert.ToInt32(insert);
        }
    }
}
