﻿using Microsoft.Extensions.DependencyInjection;
using PetaPoco;
using PmSoft.Core;
using PmSoft.Data.PetaPoco.Repositories;
using Qy.Base.AppBase;
using Qy.Cms.AppBase;
using System.Threading.Tasks;
using Qy.Cms.Entities;
using Qy.Base.Utilities;

namespace Qy.Cms.Services.CmsConfigs;

[Inject(ServiceLifetime.Scoped)]

public class CmsConfigRepository(
    AppDbContext dbContext,
    IApplicationContext applicationContext
    ) : CacheRepository<AppDbContext, CmsConfig, int>(dbContext, applicationContext)
{
    public async Task<CmsConfig> GetOneByKeyAsync(string key)
    {
        Sql sql = Sql.Builder;
        sql.Select("id").From(CmsDbTableNameConst.CmsConfig)
            .Where("label =@0", StringUtility.StripSQLInjection(key)).OrderBy("id desc");
        int id = await DbContext.FirstOrDefaultAsync<int>(sql);
        if (id <= 0) return null;
        return await GetAsync(id);
    }
}
