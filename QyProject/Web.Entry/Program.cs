using Microsoft.OpenApi.Models;
using Qy.Base;
using Qy.Base.InterFace;
using Serilog;
using Web.Entry.Services;

namespace Web.Entry
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // 配置 Serilog 
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateBootstrapLogger();
            try
            {

                var builder = WebApplication.CreateBuilder(args);

                //注入Serilog
                builder.Configuration.AddJsonFile("serilog.json", false, true);
                builder.Services.AddSerilog((services, lc) => lc
                      .ReadFrom.Configuration(builder.Configuration)
                      .ReadFrom.Services(services)
                      .Enrich.FromLogContext()
                      .Enrich.WithMachineName()
                      .Enrich.WithThreadId());


                // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
                builder.Services.AddEndpointsApiExplorer();
                builder.Services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "PmSoft API", Version = "v1" });

                    // 配置 JWT Bearer 认证
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Description = "请输入 JWT Token，格式：Bearer {token}",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"
                    });

                    // 应用认证到所有接口
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                        {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference = new OpenApiReference
                                    {
                                        Type = ReferenceType.SecurityScheme,
                                        Id = "Bearer"
                                    }
                                },
                                Array.Empty<string>()
                            }
                        });
                });
                builder.Services.AddCors(options =>
                {
                    string[] origins = builder.Configuration.GetSection("corsUrls").Get<string[]>() ?? [];
                    options.AddPolicy("cors", policy =>
                    {
                        policy.WithOrigins(origins)
                                .AllowAnyHeader()//允许任意头
                                .AllowCredentials()//允许cookie
                                .AllowAnyMethod();//允许任意方法
                    });
                });

                builder.Services.AddQyProjectApplication(builder.Configuration);
                //注入通用日志审核服务
                builder.Services.AddScoped<ICommonAuditableService, CommonAuditableService>();
                //注入对CMS前端视图进行修改并立即生效的支持
                builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
                var app = builder.Build();

                //开启index.html
                //app.UseFileServer();
                //app.UseHttpsRedirection();

                app.UseCors("cors");

                app.UseSerilogRequestLogging();

                if (app.Environment.IsDevelopment())
                {
                    app.UseSwagger();
                    app.UseSwaggerUI();
                }
                app.UseQyProjectApplication();
                //开启静态文件访问支持
                app.UseStaticFiles();
                app.MapControllerRoute(
                         name: "default",
                         pattern: "{controller=Home}/{action=Index}/{id?}");
                app.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "An unhandled exception occurred during bootstrapping");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
