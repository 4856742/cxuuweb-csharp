﻿using PmSoft.Core;
using Qy.Base.AppBase;
using Qy.Base.InterFace;
using Qy.Base.Services.Loging.Sys;
using Qy.Cms.Services.CmsLogs;

namespace Web.Entry.Services;

public class CommonAuditableService(
    LogSysService logSysService,
    LogCmsService logCmsService
    ): ICommonAuditableService
{

    public void InsertLog(string doThing, string url, ProjectType groupName)
    {
        try
        {

            switch (groupName)
            {
                case ProjectType.Sys:
                    logSysService.InsertLog(doThing, url);
                    break;
                case ProjectType.Cms:
                    logCmsService.InsertLog(doThing, url);
                    break;
            }
        }
        catch
        {
            throw new PmSoftException("日志记录错误");
        }
    }
}
