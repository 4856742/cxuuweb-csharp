﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Core;
using PmSoft.Web.Abstractions.Attachment;

namespace Web.Entry.Api;

[Route("Api/[controller]")]

public class CommonController(
    AttachmentManagerService attachmentManagerService
    ) : ControllerBase
{
   
    /// <summary>
    /// 根据附件ID获取附件信息及文件流的 Web API 方法
    /// </summary>
    /// <param name="attachmentId">附件ID</param>
    /// <returns>附件信息和文件流，或错误信息</returns>
 
    [HttpGet("att/{attachmentId}")]
    public async Task<IActionResult> GetAttachment(string attachmentId)
    {
        if (string.IsNullOrEmpty(attachmentId))
            PmSoftException.Throw("附件ID无效。");

        var attachmentWithContent = await attachmentManagerService.GetAttachmentByIdAsync(attachmentId);

        // 返回文件流和附件信息
        return File(attachmentWithContent.Content, attachmentWithContent.Attachment.MimeType,
            attachmentWithContent.Attachment.FriendlyName);
    }



}
