﻿using Microsoft.AspNetCore.Mvc;
using PmSoft.Web.Abstractions;
using PmSoft.Web.Abstractions.Attributes;
using Qy.Base.AppBase;
using Qy.Base.Services.SysNotices;
using Qy.Cms.Dtos;
using Qy.Cms.Services.Contents.Contents;

namespace Web.Entry.Api;

[Route("Api/[controller]")]
[RestrictAccess]
public class StatController(
    ContentStatService contentStatService,
    SysNoticeService sysNoticeService) : ControllerBase
{

    [HttpGet("workspace")]
    public async Task<ApiResult<object>> WorkspaceAsync(int noticeNum = 2)
    {
        var sysNotices = await sysNoticeService.GetListAsync(noticeNum);
        return ApiResult.Ok(new
        {
            sysNotices,
            systemInfo = new
            {
                fd = QySystemInfo.FrameworkDescription,
                os = QySystemInfo.OSDescription,
                ver = QySystemInfo.Ver,
            }
        });
    }

    [HttpGet("cmscount")]
    public async Task<ContentStatsVO> CmsCountAsync()
    {
        var countStats = await contentStatService.CountUserGroupAsync();
        return countStats;
    }
}


