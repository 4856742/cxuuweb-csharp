-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        11.4.2-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  12.10.0.7000
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 qy_admin_next_open.cms_category 结构
CREATE TABLE IF NOT EXISTS `cms_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) DEFAULT NULL COMMENT '栏目名称',
  `type` int(5) NOT NULL DEFAULT 0 COMMENT '栏目类型-频道或列表',
  `sort` int(10) DEFAULT NULL COMMENT '栏目排序',
  `num` int(5) DEFAULT NULL COMMENT '前台列表显示内容条数',
  `ico` varchar(50) DEFAULT NULL COMMENT '栏目图标样式',
  `theme` varchar(200) DEFAULT NULL COMMENT '模板路径',
  `ctheme` varchar(200) DEFAULT NULL COMMENT '本栏目内容模板',
  `banner_header` varchar(200) DEFAULT NULL,
  `banner_ad` varchar(200) DEFAULT NULL,
  `banner_lit` varchar(200) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL COMMENT '统计开关',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文章内容栏目表';

-- 正在导出表  qy_admin_next_open.cms_category 的数据：~7 rows (大约)
INSERT INTO `cms_category` (`id`, `pid`, `name`, `type`, `sort`, `num`, `ico`, `theme`, `ctheme`, `banner_header`, `banner_ad`, `banner_lit`, `status`) VALUES
	(1, 0, '新闻资讯', 2, 1, 25, 'layui-icon-rate-solid\n', '', '', 'cc99d555-e3ec-43fb-8808-20c6462503ef', '', '0', b'1'),
	(2, 0, '法制简报', 2, 2, 25, 'layui-icon-senior', '', '', '', '', '', b'1'),
	(3, 0, '党建队建', 2, 1, 20, 'layui-icon-rate-solid', '', '', '', '', '22', b'1'),
	(4, 0, '执法监督', 2, 2, 25, 'layui-icon-heart-fill', '', '', '', '', '', b'1'),
	(5, 0, '制度建设', 2, 1, 25, 'layui-icon-senior', '', '', '', '', '', b'1'),
	(6, 0, '场所建设', 2, 6, 25, 'layui-icon-senior', NULL, NULL, '', '8ae78e4d-60ce-46b9-8cea-52077e83d237', NULL, b'1'),
	(7, 0, '视频', 2, 1, 0, 'layui-icon-rate-solid\n', '', '', '', '', '0', b'1');

-- 导出  表 qy_admin_next_open.cms_category_roles 结构
CREATE TABLE IF NOT EXISTS `cms_category_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT 0,
  `role_cats` text DEFAULT NULL COMMENT '有权限的类别集合',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='类别与部门权限关联表';

-- 正在导出表  qy_admin_next_open.cms_category_roles 的数据：~2 rows (大约)
INSERT INTO `cms_category_roles` (`id`, `did`, `role_cats`) VALUES
	(1, 2, '[]'),
	(2, 1, '[1,2,3,4,5,6]');

-- 导出  表 qy_admin_next_open.cms_config 结构
CREATE TABLE IF NOT EXISTS `cms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) DEFAULT NULL COMMENT '标识',
  `value_data` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='配置表';

-- 正在导出表  qy_admin_next_open.cms_config 的数据：~2 rows (大约)
INSERT INTO `cms_config` (`id`, `label`, `value_data`) VALUES
	(1, 'onduty_dutyer', '[{"name":"zx 扎西","phone":"123456"},{"name":"邓华","phone":"18988010903"}]'),
	(2, 'onduty_leader', '[{"name":"顿珠次旺","phone":"08916016617"},{"name":"邓华","phone":"18988010903"},{"name":"zx 扎西","phone":"123456"}]');

-- 导出  表 qy_admin_next_open.cms_content 结构
CREATE TABLE IF NOT EXISTS `cms_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '栏目ID',
  `type` tinyint(2) DEFAULT NULL,
  `title` varchar(160) DEFAULT NULL COMMENT '内容标题',
  `abstract` varchar(200) DEFAULT NULL COMMENT '内容摘要',
  `examine` varchar(10) DEFAULT NULL COMMENT '审核人',
  `cover_url` text DEFAULT NULL COMMENT '封面图地址序列',
  `cover_count` int(11) NOT NULL DEFAULT 0 COMMENT '封面图计数',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `edit_time` datetime DEFAULT NULL COMMENT '修改时间',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL COMMENT '录入人部门ID(新增数据时)',
  `att_a` bit(1) DEFAULT NULL COMMENT '属性A',
  `att_b` bit(1) DEFAULT NULL COMMENT '属性B',
  `att_c` bit(1) DEFAULT NULL COMMENT '属性C',
  `status` bit(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cid` (`cid`) USING BTREE,
  KEY `uid` (`uid`),
  KEY `did` (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文章内容基本信息表';

-- 正在导出表  qy_admin_next_open.cms_content 的数据：~23 rows (大约)
INSERT INTO `cms_content` (`id`, `cid`, `type`, `title`, `abstract`, `examine`, `cover_url`, `cover_count`, `create_time`, `edit_time`, `uid`, `did`, `att_a`, `att_b`, `att_c`, `status`) VALUES
	(1, 3, 2, '有底蕴！有创意！博物馆里过大年、逛“吉”市', '有底蕴！有创意！博物馆里过大年、逛“吉”市', '张三', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 2, b'1', b'1', b'0', b'1'),
	(2, 3, 2, '春晚导演组回应机器人是有裤子的', '春晚导演组回应机器人是有裤子的', '张三', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 22, b'1', b'1', b'0', b'1'),
	(3, 3, 2, 'DeepSeek将接入微软AI电脑', 'DeepSeek将接入微软AI电脑', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 23, b'1', b'1', b'0', b'1'),
	(4, 3, 2, '退出《巴黎协定》后 美国又计划推翻车辆减排限制', '退出《巴黎协定》后 美国又计划推翻车辆减排限制', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'0', b'1'),
	(5, 3, 2, '特朗普就“封口费”案判决提起上诉', '特朗普就“封口费”案判决提起上诉', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'0', b'1'),
	(6, 3, 2, 'OpenAI宣称DeepSeek违规“蒸馏”，但没有给出证据', 'OpenAI宣称DeepSeek违规“蒸馏”，但没有给出证据', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'0', b'1'),
	(7, 3, 2, '《哪吒2》火爆春节档，角色设计如何“征服”观众？', '《哪吒2》火爆春节档，角色设计如何“征服”观众？', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'0', b'1'),
	(8, 3, 2, '美国准商务部长扬言：要对中国征收最高水平关税', '美国准商务部长扬言：要对中国征收最高水平关税', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'0', b'1'),
	(9, 3, 2, '总台蛇年春晚让观众感受到“世界春节”的无穷魅力', '总台蛇年春晚让观众感受到“世界春节”的无穷魅力', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 22, b'1', b'1', b'0', b'1'),
	(10, 3, 2, '喜乐祥和中国年 贴心话语 饱含祝福', '喜乐祥和中国年 贴心话语 饱含祝福', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'0', b'1'),
	(11, 3, 2, '紫云供电局：全力保电，守护年货街的热闹与安全', '紫云供电局：全力保电，守护年货街的热闹与安全', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 23, b'1', b'1', b'0', b'1'),
	(12, 3, 2, '首个“非遗版”春节，与世界共享“年味儿”！', '首个“非遗版”春节，与世界共享“年味儿”！', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 21, b'1', b'1', b'0', b'1'),
	(13, 3, 2, '清华大学师生在西藏山南体验高原执勤', '清华大学师生在西藏山南体验高原执勤', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 8, b'1', b'1', b'0', b'1'),
	(14, 3, 2, '2025总台春晚丨扎西德勒！请查收来自西藏的新春祝福', '2025总台春晚丨扎西德勒！请查收来自西藏的新春祝福', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 8, b'1', b'1', b'0', b'1'),
	(15, 1, 2, '美国正对DeepSeek开展国家安全调查', '美国正对DeepSeek开展国家安全调查', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 8, b'1', b'1', b'0', b'1'),
	(16, 1, 2, '任敏差点成下一个尼格买提，春晚玩的是刘谦的心跳吧', '任敏差点成下一个尼格买提，春晚玩的是刘谦的心跳吧', '李强', '', 0, '2025-01-30 11:02:55', '2025-01-30 11:02:55', 2, 24, b'1', b'1', b'1', b'1'),
	(17, 1, 2, '21项年度荣誉！CMG第三届中国电视剧年度盛典揭晓', '21项年度荣誉！CMG第三届中国电视剧年度盛典揭晓', '李强', '[]', 0, '2025-01-30 11:02:55', '2025-03-15 22:49:53', 2, 24, b'1', b'1', b'1', b'1'),
	(18, 3, 2, '为构建和合共生的美好世界注入动力（和音）', '为构建和合共生的美好世界注入动力（和音）', '李强', '', 0, '2025-01-30 11:02:55', '2025-03-10 17:55:27', 2, 24, b'1', b'1', b'1', b'1'),
	(19, 5, 2, '文化中国行｜当民乐演奏遇上年味BGM', '文化中国行｜当民乐演奏遇上年味BGM', '李强', '', 0, '2025-01-30 11:02:55', '2025-03-10 17:55:16', 2, 24, b'1', b'1', b'1', b'1'),
	(20, 5, 2, '万家灯火团圆时 喜乐祥和中国年  学习进行时：贴心话语 饱含祝福', '2345', '李强', '["8df17503-fd23-4c70-b25c-dc8bbe6640f3","5d85c558-b690-46c2-ac7e-e4374085df06"]', 2, '2025-01-30 11:02:55', '2025-03-15 20:53:32', 2, 8, b'0', b'0', b'1', b'1'),
	(21, 5, 2, '这一刻，“冰雪同梦，亚洲同心”具象化了2', '“冰雪同梦，亚洲同心。”2月7日晚，万众期盼的第九届亚洲冬季运动会开幕式在黑龙江省哈尔滨市举行。此刻，“冰城”又一次吸引全亚洲的目光。', '线工', '[]', 0, '2025-02-08 22:13:45', '2025-03-15 21:10:30', 1, 1, b'1', b'1', b'1', b'1'),
	(26, 4, 3, '视频1', '大工', '33', '', 0, '2025-03-14 19:37:20', '2025-03-15 15:06:35', 1, 1, NULL, NULL, NULL, b'1'),
	(27, 2, 3, '视频2', NULL, '232', '["d87721e7-198b-4690-9b78-d6105289d6ce"]', 1, '2025-03-14 19:39:07', '2025-03-15 22:54:08', 1, 1, NULL, NULL, NULL, b'1');

-- 导出  表 qy_admin_next_open.cms_content_data 结构
CREATE TABLE IF NOT EXISTS `cms_content_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `content` mediumtext DEFAULT NULL COMMENT '详细内容',
  `video_url` text DEFAULT NULL COMMENT '视频地址',
  `attachments` text DEFAULT NULL COMMENT '附件ID集',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文章内容主体表';

-- 正在导出表  qy_admin_next_open.cms_content_data 的数据：~15 rows (大约)
INSERT INTO `cms_content_data` (`id`, `aid`, `content`, `video_url`, `attachments`) VALUES
	(1, 1, '<p><img title="" src="https://www.gov.cn/yaowen/liebiao/202409/W020240905567688396443_ORIGIN.jpg" alt=""></p>\n<p>9月5日上午，国家主席习近平在北京人民大会堂出席中非合作论坛北京峰会开幕式并发表题为《携手推进现代化，共筑命运共同体》的主旨讲话。新华社记者 李学仁 摄</p>\n<p>新华社北京9月5日电（记者杨依军、孙奕）9月5日上午，国家主席习近平在北京人民大会堂出席中非合作论坛北京峰会开幕式并发表主旨讲话。习近平宣布，中国同所有非洲建交国的双边关系提升到战略关系层面，中非关系整体定位提升至新时代全天候中非命运共同体，将实施中非携手推进现代化十大伙伴行动。</p>\n<p>中共中央政治局常委李强、赵乐际、王沪宁、蔡奇、丁薛祥、李希出席。</p>', NULL, NULL),
	(2, 2, '<p><img title="" src="https://www.gov.cn/yaowen/liebiao/202409/W020240905567688396443_ORIGIN.jpg" alt=""></p>\n<p>9月5日上午，国家主席习近平在北京人民大会堂出席中非合作论坛北京峰会开幕式并发表题为《携手推进现代化，共筑命运共同体》的主旨讲话。新华社记者 李学仁 摄</p>\n<p>新华社北京9月5日电（记者杨依军、孙奕）9月5日上午，国家主席习近平在北京人民大会堂出席中非合作论坛北京峰会开幕式并发表主旨讲话。习近平宣布，中国同所有非洲建交国的双边关系提升到战略关系层面，中非关系整体定位提升至新时代全天候中非命运共同体，将实施中非携手推进现代化十大伙伴行动。</p>\n<p>中共中央政治局常委李强、赵乐际、王沪宁、蔡奇、丁薛祥、李希出席。</p>', NULL, NULL),
	(3, 3, '<h1>李强同圣多美和普林西比总理特罗瓦达会谈</h1>', NULL, NULL),
	(4, 33, '<table style="border-collapse: collapse; width: 100%" border="1"><colgroup><col style="width: 25%"><col style="width: 25%"><col style="width: 25%"><col style="width: 25%"></colgroup>\n<tbody>\n<tr>\n<td>部门</td>\n<td>办公电话</td>\n<td>负责人/联系人</td>\n<td>联系电话</td>\n</tr>\n<tr>\n<td>办公室</td>\n<td>6231888</td>\n<td>张三</td>\n<td>13889008900</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n</tbody>\n</table>', NULL, NULL),
	(6, 32, '<p>36734655346346</p>', NULL, NULL),
	(8, 35, '<h1>新时代全天候中非命运共同体 习近平最新定位中非关系</h1>', NULL, NULL),
	(9, 36, '<p>学习贯彻党的二十届三中全会精神丨以高质量发展奋力书写中国式现代化新篇章——中管企业、中管金融企业和中管高校干部职工掀起学习贯彻党的二十届三中全会精神热潮</p>', NULL, NULL),
	(11, 30, '<p>4765</p>', NULL, NULL),
	(12, 20, '<p><img title="4a4c314f-b271-4535-bef8-9d0372ceb25d.jpeg" src="/dev-api/api/common/att/8df17503-fd23-4c70-b25c-dc8bbe6640f3" width="646" height="484" /></p>\n<p>&nbsp;</p>\n<p><img title="26c323d8-1675-4615-8e5a-776c3873e97c.jpg" src="/dev-api/api/common/att/5d85c558-b690-46c2-ac7e-e4374085df06" /></p>', NULL, '[]'),
	(13, 19, '<h3 style="line-height: 1.25; font-size: 1.35rem; margin-top: -4.5rem; padding-top: 5.5rem; margin-bottom: 0; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; background-color: rgba(255, 255, 255, 1);"><img title="9bf9d753-28cf-4bc6-97c1-ff2c31852938.jpeg" src="/dev-api/api/common/att/0ec628df-53d5-49f6-93b6-3851f4708892" /><img title="c83083ea-423a-418f-9afa-e27d9fd0ed76.jpg" src="/dev-api/api/common/att/cea843e2-9d38-42de-8e4f-bc21862265f3" /></h3>\n<table style="margin: 1rem 0; display: block; overflow-x: auto; position: relative; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">\n<thead>\n<tr style="border-top: 1px solid rgba(223, 226, 229, 1);">\n<th style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: left;">名称</th>\n<th style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: left;">类型</th>\n<th style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: left;">默认值</th>\n<th style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: center;">兼容性</th>\n<th style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: left;">描述</th>\n</tr>\n</thead>\n<tbody>\n<tr style="border-top: 1px solid rgba(223, 226, 229, 1);">\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">hover-class</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">string(<a class="" style="text-decoration-line: none; color: rgba(66, 185, 131, 1);" href="https://doc.dcloud.net.cn/uni-app-x/uts/data-type.html#ide-string">string.ClassString</a>)</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">"none"</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: center;">\n<div class="popover" style="position: relative;">\n<div class="popover-reference-wrapper" style="display: inline-block;">&nbsp;</div>\n</div>\n</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">指定按下去的样式类。当 hover-class="none" 时，没有点击态效果</td>\n</tr>\n<tr style="border-top: 1px solid rgba(223, 226, 229, 1); background-color: rgba(246, 248, 250, 1);">\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">hover-stop-propagation</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">boolean</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">false</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: center;">\n<div class="popover" style="position: relative;">\n<div class="popover-reference-wrapper" style="display: inline-block;">&nbsp;</div>\n</div>\n</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">指定是否阻止本节点的祖先节点出现点击态(祖先节点：指根节点到该节点路径上的所有节点都是这个节点的祖先节点)</td>\n</tr>\n<tr style="border-top: 1px solid rgba(223, 226, 229, 1);">\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">hover-start-time</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">number</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">50</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: center;">\n<div class="popover" style="position: relative;">\n<div class="popover-reference-wrapper" style="display: inline-block;">&nbsp;</div>\n</div>\n</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">按住后多久出现点击态，单位毫秒</td>\n</tr>\n<tr style="border-top: 1px solid rgba(223, 226, 229, 1); background-color: rgba(246, 248, 250, 1);">\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">hover-stay-time</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">number</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">400</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em; text-align: center;">\n<div class="popover" style="position: relative;">\n<div class="popover-reference-wrapper" style="display: inline-block;">&nbsp;</div>\n</div>\n</td>\n<td style="border: 1px solid rgba(223, 226, 229, 1); padding: 0.6em 1em;">手指松开后点击态保留时间，单位毫秒</td>\n</tr>\n</tbody>\n</table>\n<h4 style="line-height: 1.25; font-size: 1.15em; margin-top: -4.5rem; padding-top: 5.5rem; margin-bottom: 0; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; background-color: rgba(255, 255, 255, 1);"><a class="header-anchor" style="font-weight: 500; text-decoration-line: none; color: rgba(66, 185, 131, 1); font-size: 0.85em; float: left; margin-left: -0.87em; padding-right: 0.23em; margin-top: 0.125em; user-select: none; opacity: 0;" href="https://doc.dcloud.net.cn/uni-app-x/component/view.html#%E8%AF%B4%E6%98%8E">#</a>说明</h4>\n<ul style="padding-left: 1.2em; line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">\n<li style="word-break: break-all;">为什么使用<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>？使用 css :active伪类来实现点击态，很容易触发，并且滚动或滑动时点击态不会消失，体验较差。建议使用&nbsp;<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>&nbsp;属性来实现。并且App平台目前不支持css伪类。</li>\n</ul>\n<h4 style="line-height: 1.25; font-size: 1.15em; margin-top: -4.5rem; padding-top: 5.5rem; margin-bottom: 0; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; background-color: rgba(255, 255, 255, 1);"><a class="header-anchor" style="font-weight: 500; text-decoration-line: none; color: rgba(66, 185, 131, 1); font-size: 0.85em; float: left; margin-left: -0.87em; padding-right: 0.23em; margin-top: 0.125em; user-select: none; opacity: 0;" href="https://doc.dcloud.net.cn/uni-app-x/component/view.html#app">#</a>App平台</h4>\n<ul style="padding-left: 1.2em; line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">\n<li style="word-break: break-all;">HBuilder4.0以下版本<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>属性App端与微信小程序效果一样，手指按下进入<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>状态后，手指移动就会取消<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>状态</li>\n<li style="word-break: break-all;">HBuilder4.0及以上版本App端调整为手指在view范围内移动不会取消<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>状态，手指移动到view范围之外才会取消<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">hover-class</code>状态</li>\n</ul>\n<h4 style="line-height: 1.25; font-size: 1.15em; margin-top: -4.5rem; padding-top: 5.5rem; margin-bottom: 0; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; background-color: rgba(255, 255, 255, 1);"><a class="header-anchor" style="font-weight: 500; text-decoration-line: none; color: rgba(66, 185, 131, 1); font-size: 0.85em; float: left; margin-left: -0.87em; padding-right: 0.23em; margin-top: 0.125em; user-select: none; opacity: 0;" href="https://doc.dcloud.net.cn/uni-app-x/component/view.html#nativeview">#</a>获取原生view对象</h4>\n<p style="line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">为增强uni-app x组件的开放性，从&nbsp;<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">HBuilderX 4.25</code>&nbsp;起，UniElement对象提供了&nbsp;<a class="" style="text-decoration-line: none; color: rgba(66, 185, 131, 1); word-break: break-all;" href="https://doc.dcloud.net.cn/uni-app-x/dom/unielement.html#getandroidview">getAndroidView</a>&nbsp;和&nbsp;<a class="" style="text-decoration-line: none; color: rgba(66, 185, 131, 1); word-break: break-all;" href="https://doc.dcloud.net.cn/uni-app-x/dom/unielement.html#getiosview">getIOSView</a>&nbsp;方法。</p>\n<p style="line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">该方法可以获取到 view 组件对应的原生对象，即Android的<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">ViewGroup</code>对象、iOS的<code style="font-family: source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace; color: rgba(233, 105, 0, 1); padding: 0.25rem 0.5rem; margin: 0; font-size: 0.85em; background-color: rgba(27, 31, 35, 0.05);">UIView</code>对象。</p>\n<p style="line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">进而可以调用原生对象提供的方法，这极大的扩展了组件的能力。</p>\n<p style="line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);"><span style="font-weight: 600;">Android 平台：</span></p>\n<p style="line-height: 1.7; color: rgba(44, 62, 80, 1); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 1);">获取view组件对应的UniElement对象，通过UniElement对象的<a class="" style="text-decoration-line: none; color: rgba(66, 185, 131, 1); word-break: break-all;" href="https://doc.dcloud.net.cn/uni-app-x/dom/unielement.html#getandroidview-2">getAndroidView</a>函数获取组件原生ViewGroup对象</p>', NULL, NULL),
	(14, 21, '<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);"><br /><img style="margin: 0 auto; padding: 0; display: block; border: none; vertical-align: middle;" src="https://cxuu.top/uploads/Images/202502/dd83fbca3d6cb0dd.jpg" /></p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1); text-align: center;"><img style="margin: 0; padding: 0; display: inline-block; border: none; vertical-align: middle;" src="https://cxuu.top/uploads/Images/202502/8d602d1367fdd0b5.jpg" />&nbsp;</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">&ldquo;冰雪同梦，亚洲同心。&rdquo;2月7日晚，万众期盼的第九届亚洲冬季运动会开幕式在黑龙江省哈尔滨市举行。此刻，&ldquo;冰城&rdquo;又一次吸引全亚洲的目光。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">从虎头虎脑的&ldquo;滨滨&rdquo;和&ldquo;妮妮&rdquo;蹦蹦跳跳，到小女孩用冰灯&ldquo;点亮&rdquo;雪花摩天轮，再到舞者手持红蓝双色&ldquo;冰凌手绢花&rdquo;表演&hellip;&hellip;开幕式上，一幕幕具有创意的场景惊艳了世界，一个个出新出彩的节目令人赞叹。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">2月7日是大年初十，当亚冬会遇上中国年，擦出了令人眼前一亮的火花。今年春节是申遗成功后的首个春节，连日来，在神州大地上处处绽放芳华的非物质文化遗产代表性项目，也在此次开幕式上有所展现。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">在运动员入场式上，引导员身着白色礼服飘逸灵动。有一个细节，跃然裙摆之上的是一个特殊的图案，这个图案大有讲究，它是国家级非物质文化遗产代表性项目&ldquo;方正剪纸&rdquo;图案，与&ldquo;桦树皮画&rdquo;立体浮雕相得益彰。将科技、艺术和哈尔滨本地文化进行国际化、时尚化融合，正是此次开幕式的一大亮点。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">稍加留心，即可发现在开幕式上，中华文化和冰雪元素交相辉映，体现了自然之美、人文之美、运动之美，诠释了新时代中国可信、可爱、可敬的形象。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">运动员入场式是开幕式的重头戏。东道主中国代表团入场，全场沸腾。其他代表团入场，同样赢得热烈掌声。这一刻，&ldquo;冰雪同梦，亚洲同心&rdquo;已经具象化了；这一刻，人们为亚冬会所传递的价值理念所震撼和感动。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">本届亚冬会的主题是&ldquo;冰雪同梦，亚洲同心&rdquo;，承载着亚洲人民对于和平、发展、友谊的共同愿望和追求。坚守安宁和睦的共同梦想，坚持繁荣发展的共同追求，实现交融相亲的共同心愿，这是全亚洲的共同目标，此次开幕式通过不同方式表达和诠释了这一愿望和追求。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">亚冬时刻开启！哈尔滨亚冬会主火炬已经点燃，点燃的不仅是主火炬，也是希望和梦想。本届参赛国家和地区数量、运动员人数都创亚冬会历史之最。赴亚冬之约，为竞技而来，为友谊而来，也是为和平而来。</p>\n<p style="margin: 0; padding: 0; font-family: 微软雅黑, 黑体, \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.85); background-color: rgba(255, 255, 255, 1);">以体育之名，聚亚洲之力，筑未来之路。&ldquo;冰雪同梦，亚洲同心&rdquo;，期待哈尔滨为世界呈现一届&ldquo;中国特色、亚洲风采、精彩纷呈&rdquo;的体育盛会，谱写新的冰雪华章。</p>', NULL, ''),
	(15, 18, '<p><img title="5beb8e8f-4375-4dc2-a3b2-03646bf6ef5d.jpeg" src="/dev-api/api/common/att/44e4bdbc-cb1f-48b1-b17a-7b6e46b27df8" /><img title="6909a9fd-74ef-4f67-b935-4bd04e546907.jpg" src="/dev-api/api/common/att/dfb787e3-34c5-4891-9e07-bc12b8125ef4" /></p>', NULL, NULL),
	(19, 26, '<p>大工</p>', NULL, NULL),
	(20, 27, NULL, NULL, NULL),
	(21, 17, NULL, NULL, '[1,2,3]');

-- 导出  表 qy_admin_next_open.cms_content_hit 结构
CREATE TABLE IF NOT EXISTS `cms_content_hit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `hits` int(11) DEFAULT 0 COMMENT '点击阅读量',
  `likes` int(11) DEFAULT 0 COMMENT '点赞量',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文章浏览相关数据统计';

-- 正在导出表  qy_admin_next_open.cms_content_hit 的数据：~28 rows (大约)
INSERT INTO `cms_content_hit` (`id`, `aid`, `hits`, `likes`) VALUES
	(1, 1, 9, 0),
	(2, 2, 24, 0),
	(3, 3, 0, 0),
	(4, 12, 6, 0),
	(5, 15, 8, 0),
	(6, 16, 2, 0),
	(7, 17, 11, 0),
	(8, 18, 20, 0),
	(9, 19, 45, 0),
	(10, 20, 28, 0),
	(13, 25, 14, 0),
	(14, 26, 10, 0),
	(15, 27, 22, 15),
	(16, 28, 7, 0),
	(17, 29, 1, 0),
	(19, 32, 9, 0),
	(20, 33, 12, 0),
	(22, 35, 6, 0),
	(23, 36, 44, 0),
	(24, 37, 1, 0),
	(25, 38, 2, 0),
	(26, 40, 15, 0),
	(27, 41, 2, 0),
	(28, 30, 13, 0),
	(29, 21, 17, 0),
	(30, 13, 1, 0),
	(31, 11, 2, 0),
	(32, 6, 1, 0);

-- 导出  表 qy_admin_next_open.cms_invoke 结构
CREATE TABLE IF NOT EXISTS `cms_invoke` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '分类ID',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `url` varchar(200) DEFAULT NULL COMMENT '链接',
  `url_int` int(11) DEFAULT NULL COMMENT '数字整型链接数据',
  `ico` varchar(200) DEFAULT NULL COMMENT '图标',
  `img` varchar(200) DEFAULT NULL COMMENT '链接图片',
  `content` text DEFAULT NULL COMMENT '内容',
  `sort` int(11) NOT NULL COMMENT '排序',
  `status` bit(1) DEFAULT NULL COMMENT '显示状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cid` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='CMS调用链接表';

-- 正在导出表  qy_admin_next_open.cms_invoke 的数据：~21 rows (大约)
INSERT INTO `cms_invoke` (`id`, `cid`, `name`, `url`, `url_int`, `ico`, `img`, `content`, `sort`, `status`) VALUES
	(1, 8, '首页背景图', '', 0, '', '', '.cxuu-margin-top { height: 400px; }', 0, b'1'),
	(2, 7, '张三扎西', '局长、局党委副书记', 0, '', '', '主持全面工作', 1, b'1'),
	(3, 7, '成员2', '局党委委员、副局长', 0, '', '', '分管XX大队、XX大队', 19, b'1'),
	(4, 10, '首页下方广告条', '', 0, '', '', '', 0, b'1'),
	(5, 10, '十六字总要求', '', 0, '', '', '', 0, b'1'),
	(6, 1, '厅OA', 'http://www.www', 0, 'layui-icon-github', '', '', 0, b'1'),
	(7, 1, '局OA', 'http://www.www', 0, 'layui-icon-firefox', '', '', 0, b'1'),
	(8, 1, '情报', 'http://www.www', 0, 'layui-icon-ie', '', '', 0, b'1'),
	(9, 1, '藏历', '/lib/zangli', 0, 'layui-icon-tabs', '', '', 0, b'1'),
	(11, 1, '局OA', 'http://www.www', 0, 'layui-icon-find-fill', '', '', 0, b'1'),
	(12, 1, '关于网站', '/Page/Index?id=22', 0, '', '', '', 0, b'1'),
	(13, 1, 'XX公众号', '', 0, '', '', '', 0, b'1'),
	(14, 1, '工资查询', '', 0, 'layui-icon-find-fill', '', '', 0, b'1'),
	(15, 1, '在线报修', '/Repair', 0, 'layui-icon-find-fill', '', '', 0, b'1'),
	(16, 13, '第一张', '', 0, '', '', '', 0, b'1'),
	(23, 14, '张三', '手机：13889009375  办公电话：6833320', 0, '队长', '', '', 2, b'1'),
	(24, 14, '李四扎西', '手机：13889009376  (休假中)', 0, '党总支书记', '', '', 1, b'1'),
	(25, 16, '王二麻子', '手机：15389008900', 0, '主任', '', '', 0, b'1'),
	(26, 15, '喻苏', '手机：18281199684', 0, '员工', '', '', 0, b'1'),
	(27, 13, '第一张', '', 0, '', '', '', 0, b'1'),
	(28, 3, '张三', NULL, 0, NULL, NULL, '11', 0, b'1');

-- 导出  表 qy_admin_next_open.cms_invoke_cat 结构
CREATE TABLE IF NOT EXISTS `cms_invoke_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '分类名',
  `sort` int(11) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL COMMENT '本类别链接',
  `invoke_num` int(11) DEFAULT NULL COMMENT '本类别有几个调用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='CMS调用链接分类表';

-- 正在导出表  qy_admin_next_open.cms_invoke_cat 的数据：~20 rows (大约)
INSERT INTO `cms_invoke_cat` (`id`, `pid`, `type`, `name`, `sort`, `remark`, `url`, `invoke_num`) VALUES
	(1, 0, 0, '通用', 1, '', '', 9),
	(2, 0, 0, '导航及内容', 2, '调用的是这些栏目，前端需要按类型调用方法使用', '', 0),
	(3, 2, 0, '通讯录', 1, '在本目录下存放每个人的通讯信息2', '/page/AddressBook', 1),
	(4, 2, 0, '信息维护', 3, '', '/sys', 0),
	(5, 2, 0, '资格考试', 2, '', '/Content/List/7', 0),
	(6, 2, 0, 'X局主页', 4, '', 'http://155.11.11.33', 0),
	(7, 1, 0, '班子成员', 0, '', '领导信息', 2),
	(8, 1, 0, '头部背景图', 0, NULL, NULL, 1),
	(9, 2, 0, '首页', 0, NULL, '/', 0),
	(10, 1, 0, '首页横幅广告', 0, '', '', 2),
	(11, 1, 0, '应用平台链接', 0, NULL, NULL, 9),
	(12, 18, 0, '常用文本', 2, '', '', 1),
	(13, 18, 0, '首页轮播', 1, '', '', 1),
	(14, 3, 0, '支队领导', 1, '', '', 2),
	(15, 3, 0, 'A队', 2, '', '', 1),
	(16, 3, 0, 'B大队', 3, '', '', 1),
	(17, 3, 0, 'C大队', 4, '', '', 0),
	(18, 0, 0, '移动端', 0, '', '移动端综合调用，不局限于CMS', 0),
	(19, 3, 0, 'A部门', 0, NULL, NULL, 0),
	(20, 3, 0, 'B部门', 0, NULL, NULL, 0);

-- 导出  表 qy_admin_next_open.cms_log 结构
CREATE TABLE IF NOT EXISTS `cms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contr_act` varchar(255) DEFAULT NULL COMMENT '操作控制器和方法',
  `method` varchar(10) DEFAULT NULL COMMENT '请求方法',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL COMMENT '登录IP',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `do_thing` text DEFAULT NULL COMMENT '操作内容',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.cms_log 的数据：~5 rows (大约)
INSERT INTO `cms_log` (`id`, `contr_act`, `method`, `uid`, `did`, `ip`, `time`, `do_thing`) VALUES
	(1, 'http://localhost:8080/api/CmsInvokeCat/edit', 'PUT', 1, 1, '', '2025-03-18 20:53:01', '修改 CMS通用调用类别表(cms_invoke_cat) ID:15 Name: \'执法监督大队 ~ A队\''),
	(2, 'http://localhost:8080/api/CmsInvokeCat/edit', 'PUT', 1, 1, '', '2025-03-18 20:53:07', '修改 CMS通用调用类别表(cms_invoke_cat) ID:16 Name: \'案件管理大队 ~ B大队\''),
	(3, 'http://localhost:8080/api/CmsInvokeCat/edit', 'PUT', 1, 1, '', '2025-03-18 20:53:12', '修改 CMS通用调用类别表(cms_invoke_cat) ID:17 Name: \'复议应诉大队 ~ C大队\''),
	(4, 'http://localhost:8080/api/CmsInvokeCat/edit', 'PUT', 1, 1, '', '2025-03-18 20:53:19', '修改 CMS通用调用类别表(cms_invoke_cat) ID:6 Name: \'市局主页 ~ X局主页\''),
	(5, 'http://localhost:8080/api/CmsInvokeCat/edit', 'PUT', 1, 1, '', '2025-03-18 20:53:25', '修改 CMS通用调用类别表(cms_invoke_cat) ID:5 Name: \'执法资格考试 ~ 资格考试\'');

-- 导出  表 qy_admin_next_open.cms_onduty 结构
CREATE TABLE IF NOT EXISTS `cms_onduty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `date_time` date DEFAULT NULL COMMENT '年月日',
  `date_in_month` tinyint(2) DEFAULT NULL COMMENT '1-31天',
  `week_date` tinyint(2) DEFAULT NULL COMMENT '星期几1-7',
  `leader` varchar(50) DEFAULT NULL COMMENT '带班领导',
  `leader_phone` varchar(80) DEFAULT NULL,
  `dutyer` varchar(50) DEFAULT NULL COMMENT '值班',
  `dutyer_phone` varchar(80) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `did` (`did`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `date_time` (`date_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='指挥中心带班表';

-- 正在导出表  qy_admin_next_open.cms_onduty 的数据：~5 rows (大约)
INSERT INTO `cms_onduty` (`id`, `uid`, `did`, `date_time`, `date_in_month`, `week_date`, `leader`, `leader_phone`, `dutyer`, `dutyer_phone`, `insert_time`) VALUES
	(1, 1, 1, '2025-01-30', 30, 4, '顿珠次旺', '08916016617', 'zx 扎西', '123456', '2025-01-30 10:38:12'),
	(2, 1, 1, '2025-01-31', 31, 5, 'zx 扎西', '123456', 'zx 扎西', '123456', '2025-01-30 10:38:20'),
	(3, 1, 1, '2025-02-01', 1, 6, '张三', '18989010903', 'zx 扎西', '123456', '2025-01-31 15:40:08'),
	(4, 1, 1, '2025-03-05', 5, 3, '李四', '18989010903', 'zx 扎西', '123456', '2025-03-10 17:19:09'),
	(5, 1, 1, '2025-03-06', 6, 4, '王五', '18989010903', NULL, NULL, '2025-03-10 17:23:26');

-- 导出  表 qy_admin_next_open.qy_attachment 结构
CREATE TABLE IF NOT EXISTS `qy_attachment` (
  `id` varchar(64) NOT NULL COMMENT '附件ID，使用 GUID 唯一标识',
  `tenant_type` varchar(50) NOT NULL COMMENT '租户类型，例如 "Company" 或 "User"',
  `tenant_id` varchar(50) DEFAULT NULL COMMENT '租户ID，与业务实体关联',
  `file_name` varchar(100) NOT NULL COMMENT '文件名，guid加原始扩展名',
  `bucket_name` varchar(50) NOT NULL COMMENT '存储桶名称，用于区分存储空间',
  `object_name` varchar(255) NOT NULL COMMENT '对象名称，包含文件路径，例如 "files/document.pdf"',
  `friendly_name` varchar(100) DEFAULT NULL COMMENT '用户自定义的友好名称，可选',
  `media_type` tinyint(2) DEFAULT NULL COMMENT '媒体类型，使用枚举定义，例如 Image, Document',
  `mime_type` varchar(100) DEFAULT NULL COMMENT 'MIME 类型，例如 "application/pdf"',
  `file_size` bigint(20) NOT NULL COMMENT '文件大小，单位：字节',
  `uploader_ip` varchar(20) DEFAULT NULL COMMENT '上传者的 IP 地址',
  `description` varchar(255) DEFAULT NULL COMMENT '附件描述',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否已删除，默认为 false',
  `delete_token` varchar(64) DEFAULT NULL COMMENT '删除签名',
  `is_temporary` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否为临时附件，默认为 true',
  `create_time` datetime NOT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`,`delete_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='附件信息';

-- 正在导出表  qy_admin_next_open.qy_attachment 的数据：~26 rows (大约)
INSERT INTO `qy_attachment` (`id`, `tenant_type`, `tenant_id`, `file_name`, `bucket_name`, `object_name`, `friendly_name`, `media_type`, `mime_type`, `file_size`, `uploader_ip`, `description`, `is_deleted`, `delete_token`, `is_temporary`, `create_time`) VALUES
	('06fb6fbe-70b9-4771-af0e-9d7115f68b4f', 'sys_user_avatar', '1', '896608c1-5d6a-4c28-bb91-b61fe4255603.jpeg', 'sys-avatar-bucket', 'sys_user_avatar/1/202503/896608c1-5d6a-4c28-bb91-b61fe4255603.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', '', b'0', NULL, b'0', '2025-03-18 20:47:34'),
	('129c00f1-9bbd-4c12-bf24-1aaecf73478c', 'cms_invoke', '1', 'ea93ac39-8252-4402-8de0-524de5af6634.jpg', 'cms-invoke-bucket', 'cms_invoke/1/202503/ea93ac39-8252-4402-8de0-524de5af6634.jpg', 'MAIN17396732318424Y6EEBTQG2.jpg', 0, 'image/jpeg', 329012, '', 'form-data; name="file"; filename="MAIN17396732318424Y6EEBTQG2.jpg"', b'1', '129c00f1-9bbd-4c12-bf24-1aaecf73478c', b'0', '2025-03-17 11:18:30'),
	('14750dc2-13f8-43d4-be27-53efb877daec', 'cms_cat', '1', '8e3afcc7-0e80-402d-b4ae-982545153c37.jpg', 'cms-cat-bucket', 'cms_cat/1/202503/8e3afcc7-0e80-402d-b4ae-982545153c37.jpg', 'MAIN17396732318424Y6EEBTQG2.jpg', 0, 'image/jpeg', 329012, '', 'form-data; name="file"; filename="MAIN17396732318424Y6EEBTQG2.jpg"', b'1', '14750dc2-13f8-43d4-be27-53efb877daec', b'0', '2025-03-17 12:35:41'),
	('1e9fc7e5-71fa-42e3-9011-43b1666752ab', 'sys_notice_img', '2', 'dcaf8705-9cd7-432d-a8fb-9885862289d2.jpeg', 'sys-notice-img-bucket', 'sys_notice_img/2/202503/dcaf8705-9cd7-432d-a8fb-9885862289d2.jpeg', '6c6515466de37629f9b67a959c6c8ed5.jpeg', 0, 'image/jpeg', 185786, '', 'form-data; name="file"; filename="6c6515466de37629f9b67a959c6c8ed5.jpeg"', b'0', NULL, b'0', '2025-03-17 11:22:26'),
	('257943e8-6cc2-4ac5-9849-8aaaf67e2f4e', 'sys', '1', '5537437691新建 文本文档.txt', 'sys-bucket', 'sys/1/202503/5537437691新建 文本文档.txt', '新建 文本文档.txt', 4, 'application/octet-stream', 73, '', '', b'0', NULL, b'0', '2025-03-15 20:59:13'),
	('2955d84c-0756-47ea-b3b2-1233f5da4883', 'sys_user_avatar', '1', '45aa2d6a-5f52-4ea9-981e-04429b44ef54.jpeg', 'sys-avatar-bucket', 'sys_user_avatar/1/202503/45aa2d6a-5f52-4ea9-981e-04429b44ef54.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', '', b'0', NULL, b'0', '2025-03-15 23:11:04'),
	('34d37fdc-7d94-4997-9c70-73a5e8d14bc6', 'sys_user_avatar', NULL, '4e8d0d02-29bf-4778-aa51-c6c69ff2e511.jpg', 'sys-avatar-bucket', 'temp/4e8d0d02-29bf-4778-aa51-c6c69ff2e511.jpg', 'R-C.jpg', 0, 'image/jpeg', 57839, '', '', b'0', NULL, b'1', '2025-03-18 20:50:42'),
	('4179a217-cbf5-4a18-bd54-3d617c0e99a2', 'sys', '1', '1845282513新建 XLS 工作表.xls', 'sys-bucket', 'sys/1/202503/1845282513新建 xls 工作表.xls', '新建 XLS 工作表.xls', 1, 'application/vnd.ms-excel', 6656, '', '', b'0', NULL, b'0', '2025-03-15 22:49:44'),
	('556aa5d8-5ecf-4171-8565-41668524f8e1', 'notice_img', '4', '7eec228a-ec84-4352-bdbf-297df51fbc73.jpg', 'notice-img-bucket', 'notice_img/4/202503/7eec228a-ec84-4352-bdbf-297df51fbc73.jpg', 'MAIN17396732318424Y6EEBTQG2.jpg', 0, 'image/jpeg', 329012, '', 'form-data; name="file"; filename="MAIN17396732318424Y6EEBTQG2.jpg"', b'0', NULL, b'0', '2025-03-17 11:21:11'),
	('5d85c558-b690-46c2-ac7e-e4374085df06', 'cms_img', '20', '26c323d8-1675-4615-8e5a-776c3873e97c.jpg', 'cms-img-bucket', 'cms_img/20/202503/26c323d8-1675-4615-8e5a-776c3873e97c.jpg', 'R-C.jpg', 0, 'image/jpeg', 57839, '', 'form-data; name="file"; filename="R-C.jpg"', b'0', NULL, b'0', '2025-03-15 20:52:23'),
	('5df06224-d0ff-4252-bf37-3afaf1a2628d', 'cms_img', NULL, '35e08037-ce1b-4f1a-9bd9-bd66e881c007.jpeg', 'cms-img-bucket', 'temp/35e08037-ce1b-4f1a-9bd9-bd66e881c007.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', 'form-data; name="file"; filename="6ac54ccb31a09a5c1223677ba07c283f.jpeg"', b'0', NULL, b'1', '2025-03-15 15:31:26'),
	('71db8c29-d5a0-43d1-9c67-c5d7149e200a', 'cms_img', NULL, '1353fcd3-4ba9-4f93-bb7d-89ad2093e3bb.jpeg', 'cms-img-bucket', 'temp/1353fcd3-4ba9-4f93-bb7d-89ad2093e3bb.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', 'form-data; name="file"; filename="6ac54ccb31a09a5c1223677ba07c283f.jpeg"', b'0', NULL, b'1', '2025-03-15 15:23:01'),
	('764e5b29-5539-46c5-b45b-c46861bc3a64', 'sys', '1', '5537963048汇报视频制作合同.docx', 'sys-bucket', 'sys/1/202503/5537963048汇报视频制作合同.docx', '汇报视频制作合同.docx', 1, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 14186, '', '', b'0', NULL, b'0', '2025-03-15 20:59:14'),
	('7ffca9ed-4dd5-4dea-a499-470cc528c36b', 'cms_cat', '1', '8a636ced-52a2-44b9-9f2f-dcf27ca1f603.jpeg', 'cms-cat-bucket', 'cms_cat/1/202503/8a636ced-52a2-44b9-9f2f-dcf27ca1f603.jpeg', '6c6515466de37629f9b67a959c6c8ed5.jpeg', 0, 'image/jpeg', 185786, '', 'form-data; name="file"; filename="6c6515466de37629f9b67a959c6c8ed5.jpeg"', b'1', '7ffca9ed-4dd5-4dea-a499-470cc528c36b', b'0', '2025-03-17 12:26:03'),
	('8ae78e4d-60ce-46b9-8cea-52077e83d237', 'cms_cat', '1', '15bc710a-5c6d-4d4a-9c90-ede729eb7637.jpg', 'cms-cat-bucket', 'cms_cat/1/202503/15bc710a-5c6d-4d4a-9c90-ede729eb7637.jpg', 'MAIN17396732318424Y6EEBTQG2.jpg', 0, 'image/jpeg', 329012, '', 'form-data; name="file"; filename="MAIN17396732318424Y6EEBTQG2.jpg"', b'0', NULL, b'0', '2025-03-17 12:26:08'),
	('8df17503-fd23-4c70-b25c-dc8bbe6640f3', 'cms_img', '20', '4a4c314f-b271-4535-bef8-9d0372ceb25d.jpeg', 'cms-img-bucket', 'cms_img/20/202503/4a4c314f-b271-4535-bef8-9d0372ceb25d.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', 'form-data; name="file"; filename="6ac54ccb31a09a5c1223677ba07c283f.jpeg"', b'0', NULL, b'0', '2025-03-15 20:52:23'),
	('a09daaa9-5f14-40ba-84cc-fecc0501b958', 'cms_cat', '1', '9adb9848-83fc-4c81-8727-e6c69a3bf204.jpeg', 'cms-cat-bucket', 'cms_cat/1/202503/9adb9848-83fc-4c81-8727-e6c69a3bf204.jpeg', '6c6515466de37629f9b67a959c6c8ed5.jpeg', 0, 'image/jpeg', 185786, '', 'form-data; name="file"; filename="6c6515466de37629f9b67a959c6c8ed5.jpeg"', b'1', 'a09daaa9-5f14-40ba-84cc-fecc0501b958', b'0', '2025-03-17 12:35:36'),
	('a146d54d-4218-4d53-9883-97c26f050dd8', 'cms_img', NULL, 'a05a7582-914c-4752-aa10-f71008f63cac.jpg', 'cms-img-bucket', 'temp/a05a7582-914c-4752-aa10-f71008f63cac.jpg', 'R-C.jpg', 0, 'image/jpeg', 57839, '', 'form-data; name="file"; filename="R-C.jpg"', b'1', 'a146d54d-4218-4d53-9883-97c26f050dd8', b'1', '2025-03-15 15:36:26'),
	('a4c0b02c-e766-486f-9cbd-86a9ba7ba510', 'cms_cat', '1', 'c3254461-f153-4b30-9b26-b1cff777bfa4.jpeg', 'cms-cat-bucket', 'cms_cat/1/202503/c3254461-f153-4b30-9b26-b1cff777bfa4.jpeg', '6c6515466de37629f9b67a959c6c8ed5.jpeg', 0, 'image/jpeg', 185786, '', 'form-data; name="file"; filename="6c6515466de37629f9b67a959c6c8ed5.jpeg"', b'1', 'a4c0b02c-e766-486f-9cbd-86a9ba7ba510', b'0', '2025-03-17 11:26:46'),
	('bd17eb26-42e6-4efa-9966-8ceac8b75449', 'cms_img', NULL, '673a0712-965a-4834-aa09-ab91cdec594d.jpg', 'cms-img-bucket', 'temp/673a0712-965a-4834-aa09-ab91cdec594d.jpg', 'R-C.jpg', 0, 'image/jpeg', 57839, '', 'form-data; name="file"; filename="R-C.jpg"', b'0', NULL, b'1', '2025-03-15 15:27:34'),
	('cc2d0cbe-8ee1-4640-85d8-aa80cb911e1b', 'cms_cat', '1', 'd5a3c752-4f6d-4bd8-a34e-b43223f0d9c8.jpg', 'cms-cat-bucket', 'cms_cat/1/202503/d5a3c752-4f6d-4bd8-a34e-b43223f0d9c8.jpg', 'MAIN17396732318424Y6EEBTQG2.jpg', 0, 'image/jpeg', 329012, '', 'form-data; name="file"; filename="MAIN17396732318424Y6EEBTQG2.jpg"', b'1', 'cc2d0cbe-8ee1-4640-85d8-aa80cb911e1b', b'0', '2025-03-17 11:27:25'),
	('cc99d555-e3ec-43fb-8808-20c6462503ef', 'cms_cat', '1', '5583ec42-5417-481d-b5f0-bc71a4038a20.jpg', 'cms-cat-bucket', 'cms_cat/1/202503/5583ec42-5417-481d-b5f0-bc71a4038a20.jpg', 'MAIN17396732318424Y6EEBTQG2.jpg', 0, 'image/jpeg', 329012, '', 'form-data; name="file"; filename="MAIN17396732318424Y6EEBTQG2.jpg"', b'0', NULL, b'0', '2025-03-17 12:36:27'),
	('ce8b8055-1d9f-40be-8f3c-bc469439cad3', 'sys_user_avatar', '1', '0061b4d6-c64c-487d-b341-c23856246c52.jpeg', 'sys-avatar-bucket', 'sys_user_avatar/1/202503/0061b4d6-c64c-487d-b341-c23856246c52.jpeg', '6c6515466de37629f9b67a959c6c8ed5.jpeg', 0, 'image/jpeg', 185786, '', '', b'0', NULL, b'0', '2025-03-17 09:34:33'),
	('d87721e7-198b-4690-9b78-d6105289d6ce', 'cms_img', '27', '2c56496d-476d-4c8d-a4d3-943eceacc36b.jpeg', 'cms-img-bucket', 'cms_img/27/202503/2c56496d-476d-4c8d-a4d3-943eceacc36b.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', 'form-data; name="file"; filename="6ac54ccb31a09a5c1223677ba07c283f.jpeg"', b'0', NULL, b'0', '2025-03-15 15:36:53'),
	('de418426-3c05-4c7b-bef9-961ebf8903b1', 'sys_user_avatar', NULL, '28fc9d65-6b10-47a2-aec6-955cf7b9e42e.jpeg', 'sys-avatar-bucket', 'temp/28fc9d65-6b10-47a2-aec6-955cf7b9e42e.jpeg', '6ac54ccb31a09a5c1223677ba07c283f.jpeg', 0, 'image/jpeg', 206144, '', '', b'0', NULL, b'1', '2025-03-15 23:06:44'),
	('ece675d5-3042-45ab-ba4e-572c1d659afe', 'cms_img', NULL, '70e0d741-6d03-41e9-882a-60a2c2cd50c5.jpg', 'cms-img-bucket', 'temp/70e0d741-6d03-41e9-882a-60a2c2cd50c5.jpg', 'R-C.jpg', 0, 'image/jpeg', 57839, '', 'form-data; name="file"; filename="R-C.jpg"', b'1', 'ece675d5-3042-45ab-ba4e-572c1d659afe', b'1', '2025-03-15 15:32:16');

-- 导出  表 qy_admin_next_open.qy_attachment_user 结构
CREATE TABLE IF NOT EXISTS `qy_attachment_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `attachment_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attachment_id` (`attachment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 正在导出表  qy_admin_next_open.qy_attachment_user 的数据：~3 rows (大约)
INSERT INTO `qy_attachment_user` (`id`, `uid`, `did`, `attachment_id`) VALUES
	(1, 1, 1, '257943e8-6cc2-4ac5-9849-8aaaf67e2f4e'),
	(2, 1, 1, '764e5b29-5539-46c5-b45b-c46861bc3a64'),
	(3, 1, 1, '4179a217-cbf5-4a18-bd54-3d617c0e99a2');

-- 导出  表 qy_admin_next_open.qy_log_exception 结构
CREATE TABLE IF NOT EXISTS `qy_log_exception` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contr_act` varchar(50) DEFAULT NULL COMMENT '操作控制器和方法',
  `method` varchar(10) DEFAULT NULL COMMENT '请求方法',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL COMMENT '登录IP',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `message` text DEFAULT NULL,
  `stack_trace` text DEFAULT NULL COMMENT '操作内容',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.qy_log_exception 的数据：~5 rows (大约)
INSERT INTO `qy_log_exception` (`id`, `contr_act`, `method`, `uid`, `did`, `ip`, `time`, `message`, `stack_trace`) VALUES
	(1, 'Upload/UploadChunk', 'POST', 1, 1, '127.0.0.1', '2025-02-16 11:53:25', 'Object reference not set to an instance of an object.', '   at Qy.Scenario.SystemApi.UploadController.UploadChunkAsync(UploadDto uploadDto) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Service\\Attment\\UploadController.cs:line 92\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ActionMethodExecutor.TaskOfIActionResultExecutor.Execute(ActionContext actionContext, IActionResultTypeMapper mapper, ObjectMethodExecutor executor, Object controller, Object[] arguments)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeActionMethodAsync>g__Awaited|12_0(ControllerActionInvoker invoker, ValueTask`1 actionResultValueTask)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeNextActionFilterAsync>g__Awaited|10_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Rethrow(ActionExecutedContextSealed context)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Next(State& next, Scope& scope, Object& state, Boolean& isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeInnerFilterAsync>g__Awaited|13_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ResourceInvoker.<InvokeNextExceptionFilterAsync>g__Awaited|26_0(ResourceInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)'),
	(2, 'Upload/UploadChunk', 'POST', 1, 1, '127.0.0.1', '2025-02-16 11:53:25', 'Object reference not set to an instance of an object.', '   at Qy.Scenario.SystemApi.UploadController.UploadChunkAsync(UploadDto uploadDto) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Service\\Attment\\UploadController.cs:line 92\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ActionMethodExecutor.TaskOfIActionResultExecutor.Execute(ActionContext actionContext, IActionResultTypeMapper mapper, ObjectMethodExecutor executor, Object controller, Object[] arguments)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeActionMethodAsync>g__Awaited|12_0(ControllerActionInvoker invoker, ValueTask`1 actionResultValueTask)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeNextActionFilterAsync>g__Awaited|10_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Rethrow(ActionExecutedContextSealed context)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Next(State& next, Scope& scope, Object& state, Boolean& isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeInnerFilterAsync>g__Awaited|13_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ResourceInvoker.<InvokeNextExceptionFilterAsync>g__Awaited|26_0(ResourceInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)'),
	(3, 'Upload/UploadChunk', 'POST', 1, 1, '127.0.0.1', '2025-02-16 11:53:48', 'Object reference not set to an instance of an object.', '   at Qy.Scenario.SystemApi.UploadController.UploadChunkAsync(UploadDto uploadDto) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Service\\Attment\\UploadController.cs:line 92\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ActionMethodExecutor.TaskOfIActionResultExecutor.Execute(ActionContext actionContext, IActionResultTypeMapper mapper, ObjectMethodExecutor executor, Object controller, Object[] arguments)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeActionMethodAsync>g__Awaited|12_0(ControllerActionInvoker invoker, ValueTask`1 actionResultValueTask)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeNextActionFilterAsync>g__Awaited|10_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Rethrow(ActionExecutedContextSealed context)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Next(State& next, Scope& scope, Object& state, Boolean& isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeInnerFilterAsync>g__Awaited|13_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ResourceInvoker.<InvokeNextExceptionFilterAsync>g__Awaited|26_0(ResourceInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)'),
	(4, 'UserPost/ChangeSort', 'PUT', 1, 1, '127.0.0.1', '2025-02-19 11:51:49', 'Parameter count mismatch.', '   at System.Reflection.MethodBaseInvoker.ThrowTargetParameterCountException()\r\n   at System.Reflection.RuntimeMethodInfo.Invoke(Object obj, BindingFlags invokeAttr, Binder binder, Object[] parameters, CultureInfo culture)\r\n   at System.Reflection.RuntimePropertyInfo.GetValue(Object obj, Object[] index)\r\n   at Qy.Scenario.ModelStateFilter.ModelXssFieldFilter(Object obj, Type type) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Common\\Filters\\ModelStateFilter.cs:line 132\r\n   at Qy.Scenario.ModelStateFilter.OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Common\\Filters\\ModelStateFilter.cs:line 88\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeNextActionFilterAsync>g__Awaited|10_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Rethrow(ActionExecutedContextSealed context)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Next(State& next, Scope& scope, Object& state, Boolean& isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeInnerFilterAsync>g__Awaited|13_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ResourceInvoker.<InvokeNextExceptionFilterAsync>g__Awaited|26_0(ResourceInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)'),
	(5, 'UserPost/ChangeSort', 'PUT', 1, 1, '127.0.0.1', '2025-02-19 11:53:56', 'Parameter count mismatch.', '   at System.Reflection.MethodBaseInvoker.ThrowTargetParameterCountException()\r\n   at System.Reflection.RuntimeMethodInfo.Invoke(Object obj, BindingFlags invokeAttr, Binder binder, Object[] parameters, CultureInfo culture)\r\n   at System.Reflection.RuntimePropertyInfo.GetValue(Object obj, Object[] index)\r\n   at Qy.Scenario.ModelStateFilter.ModelXssFieldFilter(Object obj, Type type) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Common\\Filters\\ModelStateFilter.cs:line 132\r\n   at Qy.Scenario.ModelStateFilter.ModelXssFieldFilter(Object obj, Type type) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Common\\Filters\\ModelStateFilter.cs:line 132\r\n   at Qy.Scenario.ModelStateFilter.ModelXssFieldFilter(Object obj, Type type) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Common\\Filters\\ModelStateFilter.cs:line 117\r\n   at Qy.Scenario.ModelStateFilter.OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) in D:\\makecode\\QyAdmin-next\\CoreSystem\\Qy.Scenario.Base\\Common\\Filters\\ModelStateFilter.cs:line 88\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeNextActionFilterAsync>g__Awaited|10_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Rethrow(ActionExecutedContextSealed context)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.Next(State& next, Scope& scope, Object& state, Boolean& isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker.<InvokeInnerFilterAsync>g__Awaited|13_0(ControllerActionInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)\r\n   at Microsoft.AspNetCore.Mvc.Infrastructure.ResourceInvoker.<InvokeNextExceptionFilterAsync>g__Awaited|26_0(ResourceInvoker invoker, Task lastTask, State next, Scope scope, Object state, Boolean isCompleted)');

-- 导出  表 qy_admin_next_open.qy_log_login 结构
CREATE TABLE IF NOT EXISTS `qy_log_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL COMMENT '所在部门ID',
  `ip` varchar(15) DEFAULT NULL COMMENT '登录IP',
  `user_agent` text DEFAULT NULL COMMENT '用户客户端信息',
  `time` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`),
  KEY `did` (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='登录日志表';

-- 正在导出表  qy_admin_next_open.qy_log_login 的数据：~25 rows (大约)
INSERT INTO `qy_log_login` (`id`, `uid`, `did`, `ip`, `user_agent`, `time`) VALUES
	(1, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/133.0.0.0 Safari/537.36 Edg/133.0.0.0', '2025-03-07 19:30:10'),
	(2, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-08 11:00:33'),
	(3, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-08 11:51:34'),
	(4, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-10 12:01:32'),
	(5, 3, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-10 15:02:34'),
	(6, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-10 15:03:38'),
	(7, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-11 09:37:25'),
	(8, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-12 10:18:23'),
	(9, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 09:41:09'),
	(10, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 09:56:15'),
	(11, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 10:00:40'),
	(12, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 10:01:46'),
	(13, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 10:02:45'),
	(14, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 10:08:52'),
	(15, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 11:30:16'),
	(16, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 16:22:53'),
	(17, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-13 17:27:46'),
	(18, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-14 12:54:26'),
	(19, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-14 19:23:03'),
	(20, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-15 12:29:06'),
	(21, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-17 10:51:19'),
	(22, 5, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-17 10:52:54'),
	(23, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-17 10:53:20'),
	(24, 5, 2, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-17 11:13:48'),
	(25, 1, 1, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36 Edg/134.0.0.0', '2025-03-18 20:44:57');

-- 导出  表 qy_admin_next_open.qy_log_sql 结构
CREATE TABLE IF NOT EXISTS `qy_log_sql` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contr_act` varchar(255) DEFAULT NULL COMMENT '操作控制器和方法',
  `method` varchar(10) DEFAULT NULL COMMENT '请求方法',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL COMMENT '登录IP',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `do_thing` text DEFAULT NULL COMMENT '操作内容',
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`),
  KEY `did` (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.qy_log_sql 的数据：~1 rows (大约)
INSERT INTO `qy_log_sql` (`id`, `contr_act`, `method`, `uid`, `did`, `ip`, `time`, `do_thing`, `status`) VALUES
	(1, 'http://localhost:8080/api/user/UserChangePost', 'PUT', 1, 1, '', '2025-03-18 20:47:36', '修改 系统用户(qy_user) ID:1 Avatar: \'ce8b8055-1d9f-40be-8f3c-bc469439cad3 ~ 06fb6fbe-70b9-4771-af0e-9d7115f68b4f\'', b'0');

-- 导出  表 qy_admin_next_open.qy_menu 结构
CREATE TABLE IF NOT EXISTS `qy_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT 0 COMMENT '上级菜单',
  `cid` int(10) NOT NULL DEFAULT 0,
  `type` tinyint(4) DEFAULT NULL COMMENT '类型',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `path` varchar(100) DEFAULT NULL COMMENT '路由地址',
  `component` varchar(100) DEFAULT NULL COMMENT '组件路径',
  `perm_code` varchar(50) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `hide` tinyint(1) DEFAULT NULL COMMENT '是否隐藏',
  `redirect` varchar(100) DEFAULT NULL COMMENT '默认显示下级页面',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` bit(1) DEFAULT NULL,
  `keep_alive` bit(1) DEFAULT NULL COMMENT '【菜单】是否开启页面缓存',
  `always_show` bit(1) DEFAULT NULL COMMENT '【目录】只有一个子路由是否始终显示',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统菜单';

-- 正在导出表  qy_admin_next_open.qy_menu 的数据：~33 rows (大约)
INSERT INTO `qy_menu` (`id`, `pid`, `cid`, `type`, `name`, `path`, `component`, `perm_code`, `icon`, `hide`, `redirect`, `sort`, `status`, `keep_alive`, `always_show`) VALUES
	(1, 0, 3, 0, '系统管理', '/sys', 'Layout', '', 'shezhi', 0, '/system', 1, b'1', b'0', b'0'),
	(2, 1, 3, 1, '权限中心', '/auth', 'auth/index', '', 'user', 0, '', 2, b'1', b'1', b'0'),
	(3, 1, 3, 1, '系统调配', '/system', 'system/index', '', 'dpeizhi', 0, '', 1, b'1', b'1', b'0'),
	(4, 3, 3, 2, '编辑', '', '', 'System_Management', '', 0, '', 1, b'1', b'0', b'0'),
	(5, 2, 3, 2, '权限管理', '', '', 'Auth_Edit', '', 0, '', 1, b'0', b'0', b'0'),
	(6, 1, 3, 1, '流程中心', '/wrokflow', 'wflow/index', '', 'ceng', 0, '', 3, b'0', b'0', b'0'),
	(7, 6, 3, 2, '流程管理', '', '', 'Workflow_Edit', '', 0, '', 1, b'0', b'0', b'0'),
	(8, 2, 3, 2, '编辑用户', '', '', 'User_Edit', '', 0, '', 2, b'0', b'0', b'0'),
	(9, 2, 3, 2, '删除用户', '', '', 'User_Delete', '', 0, '', 3, b'0', b'0', b'0'),
	(10, 0, 4, 0, '门户网站', '/cms', 'Layout', '', 'dnwang', 0, '/cms/content', 3, b'0', b'0', b'0'),
	(11, 10, 4, 1, '内容管理', '/cms/content', 'cms/content/index', '', 'houyezhuye', 0, '', 1, b'0', b'1', b'0'),
	(12, 10, 4, 1, '调用管理', '/cms/invoke', 'cms/invoke/index', '', 'daima', 0, '', 1, b'0', b'1', b'0'),
	(13, 10, 4, 1, '操作日志', '/cms/oplog', 'cms/oplog/index', '', 'wenng', 0, '', 6, b'0', b'1', b'0'),
	(14, 11, 4, 2, '系统设置', '', '', 'Content_Cate', '', 0, '', 1, b'0', b'1', b'0'),
	(15, 0, 3, 2, '备用', '', '', '', '', 0, '', 3, b'0', b'0', b'0'),
	(16, 11, 4, 2, '增改', '', '', 'Content_AddOrEdit', '', 0, '', 1, b'0', b'0', b'0'),
	(17, 11, 4, 2, '删除', '', '', 'Content_Delete', '', 0, '', 1, b'0', b'0', b'0'),
	(18, 11, 4, 2, '属性', '', '', 'Content_Att', '', 0, '', 1, b'0', b'0', b'0'),
	(19, 11, 4, 2, '发布权', '', '', 'Content_Status', '', 0, '', 1, b'0', b'0', b'0'),
	(20, 12, 4, 2, '增删改查', '', '', 'Cms_Invoke', '', 0, '', 1, b'0', b'0', b'0'),
	(21, 10, 4, 1, '值班管理', '/cms/onduty', 'cms/onduty/index', '', 'Calling-r', 0, '', 1, b'0', b'1', b'0'),
	(22, 21, 4, 2, '管理', '', '', 'Cms_Onduty', 'typescript', 0, '', 1, b'0', b'0', b'0'),
	(23, 0, 5, 0, '维权中心', '/rpc', 'Layout', '', 'dpaibaohu', 0, '/rpc/index', 1, b'0', b'0', b'0'),
	(24, 23, 5, 1, '维权办理', '/rpc/index', 'rpc/index', '', 'dfangshi', 0, '', 1, b'0', b'0', b'0'),
	(25, 23, 5, 1, '系统设置', '/rpc/category', 'rpc/category/index', '', 'chajian', 0, '', 2, b'0', b'0', b'0'),
	(26, 23, 5, 1, '维权大屏', '/rpc/bigscreen', 'rpc/bigscreen/index', '', 'monitor', 0, '', 1, b'0', b'0', b'0'),
	(27, 23, 5, 1, '信息审核', '/rpc/share', 'rpc/share/index', '', 'fengxin', 0, '', 1, b'0', b'0', b'0'),
	(28, 23, 5, 1, '维权日志', '/rpc/oplog', 'rpc/oplog/index', '', 'bianji', 0, '', 1, b'0', b'1', b'0'),
	(29, 25, 5, 2, '设置', '', '', 'Rpc_Config', '', 0, '', 1, b'0', b'0', b'0'),
	(30, 24, 5, 2, '增改', '', '', 'RpcPost_AddOrEdit', '', 0, '', 1, b'0', b'0', b'0'),
	(31, 24, 5, 2, '删除', '', '', 'RpcPost_Delete', '', 0, '', 1, b'0', b'0', b'0'),
	(32, 24, 5, 2, '流转推送', '', '', 'RpcPost_Share', '', 0, '', 1, b'0', b'0', b'0'),
	(33, 27, 5, 2, '签收办理', '', '', 'RpcPost_ShareSign', '', 0, '', 1, b'0', b'0', b'0');

-- 导出  表 qy_admin_next_open.qy_menu_cat 结构
CREATE TABLE IF NOT EXISTS `qy_menu_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '此ID不能随意改动',
  `pid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `icon` varchar(50) DEFAULT NULL,
  `client_id` varchar(100) DEFAULT NULL,
  `client_secret` varchar(100) DEFAULT NULL,
  `client_url` varchar(600) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `type` tinyint(2) DEFAULT NULL COMMENT '类型',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='平台管理表';

-- 正在导出表  qy_admin_next_open.qy_menu_cat 的数据：~5 rows (大约)
INSERT INTO `qy_menu_cat` (`id`, `pid`, `name`, `icon`, `client_id`, `client_secret`, `client_url`, `sort`, `type`, `remark`) VALUES
	(1, 0, '中心平台', 'client', NULL, NULL, NULL, 1, 1, ''),
	(2, 0, '应用平台', 'cart', '', '', '', 2, 1, ''),
	(3, 1, '主系统', 'bank', '9fef24-b218-8d0e7ad94140', '', 'http://localhost:10922/', 0, 2, ''),
	(4, 2, '内容管理', 'ditu', 'ea7eb5-8bf7-510908729880', 'a9b10c-c6060377b4d5', 'http://localhost:10923/', 1, 2, ''),
	(5, 2, '维权中心', 'dpaibaohu', '', '', '', 2, 2, '');

-- 导出  表 qy_admin_next_open.qy_notice 结构
CREATE TABLE IF NOT EXISTS `qy_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `content` mediumtext DEFAULT NULL COMMENT '内容',
  `post_uid` int(11) DEFAULT NULL COMMENT '发布用户ID',
  `attments` text DEFAULT NULL COMMENT '附件ID',
  `post_time` datetime DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `post_uid` (`post_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统通知';

-- 正在导出表  qy_admin_next_open.qy_notice 的数据：~4 rows (大约)
INSERT INTO `qy_notice` (`id`, `title`, `type`, `content`, `post_uid`, `attments`, `post_time`) VALUES
	(1, '测试信息通知', 1, '<p>测试信息通知</p>', 2, '[11]', '2023-04-18 12:44:36'),
	(2, '测试信息通知', 1, '<p><img title="9e33d103-e607-454f-8118-3b354ddb0b83.jpeg" src="/dev-api/api/common/att/9053a9e2-b812-4618-ab9d-0079f1a38d5e" /></p>', 1, '[]', '2025-03-12 12:20:37'),
	(3, '测试信息通知', 1, '<p>由</p>', 1, '', '2025-03-12 12:21:52'),
	(4, '测试信息通知', 1, '<p>32<img title="7eec228a-ec84-4352-bdbf-297df51fbc73.jpg" src="/dev-api/api/common/att/556aa5d8-5ecf-4171-8565-41668524f8e1" /></p>', 1, '[3,2,1]', '2025-03-17 11:21:18');

-- 导出  表 qy_admin_next_open.qy_notice_key 结构
CREATE TABLE IF NOT EXISTS `qy_notice_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) NOT NULL COMMENT '通知ID',
  `get_uid` int(11) DEFAULT NULL COMMENT '收件用户ID',
  `post_uid` int(11) DEFAULT NULL COMMENT '发件用户ID',
  `status` bit(1) DEFAULT NULL COMMENT '读取状态',
  `read_time` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Nid` (`nid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='通知信件索引表';

-- 正在导出表  qy_admin_next_open.qy_notice_key 的数据：~6 rows (大约)
INSERT INTO `qy_notice_key` (`id`, `nid`, `get_uid`, `post_uid`, `status`, `read_time`) VALUES
	(2, 1, 1, 2, b'1', '2025-01-25 15:17:40'),
	(3, 1, 1, 2, b'1', '2025-01-25 15:20:49'),
	(4, 1, 1, 2, b'1', '2025-02-16 13:23:18'),
	(5, 2, 2, 1, b'0', '0001-01-01 00:00:00'),
	(6, 3, 2, 1, b'0', '0001-01-01 00:00:00'),
	(7, 4, 5, 1, b'0', '0001-01-01 00:00:00');

-- 导出  表 qy_admin_next_open.qy_sysconfig 结构
CREATE TABLE IF NOT EXISTS `qy_sysconfig` (
  `config_key` varchar(255) NOT NULL COMMENT '设置类型',
  `config_value` text NOT NULL COMMENT '设置正文',
  PRIMARY KEY (`config_key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='系统设置';

-- 正在导出表  qy_admin_next_open.qy_sysconfig 的数据：~2 rows (大约)
INSERT INTO `qy_sysconfig` (`config_key`, `config_value`) VALUES
	('AppConfig', '{\r\n  "appName": "琪耀管理系统",\r\n  "maxConnections": 100,\r\n  "isEnabled": true,\r\n  "unitOfUse": "琪耀管理系统",\r\n  "appUrl": "http://localhost:8080/",\r\n  "attUrlPre": "/dev-api/api/common/att/",\r\n  "description": "琪耀管理系统",\r\n  "copyright": "© 2025 琪耀管理系统",\r\n  "beianWeb": "渝ICP备20004602号-1",\r\n  "beianApp": "渝ICP备20004602号-1",\r\n  "userDefaultPass": "123456",\r\n  "isOnValidCode": false,\r\n  "validCodeType": 1\r\n}'),
	('WorkflowCategoryConfigs', '[\r\n  {\r\n    "value": 1,\r\n    "label": "门户网站"\r\n  },\r\n  {\r\n    "value": 2,\r\n    "label": "维权中心"\r\n  },\r\n  {\r\n    "value": 3,\r\n    "label": "办公业务"\r\n  }\r\n]');

-- 导出  表 qy_admin_next_open.qy_sysnotice 结构
CREATE TABLE IF NOT EXISTS `qy_sysnotice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `content` mediumtext DEFAULT NULL COMMENT '内容',
  `uid` int(11) DEFAULT NULL COMMENT '发布用户ID',
  `attments` text DEFAULT NULL COMMENT '附件ID',
  `great_time` datetime DEFAULT NULL COMMENT '发布时间',
  `status` bit(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `post_uid` (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='系统后台公告';

-- 正在导出表  qy_admin_next_open.qy_sysnotice 的数据：~2 rows (大约)
INSERT INTO `qy_sysnotice` (`id`, `title`, `content`, `uid`, `attments`, `great_time`, `status`) VALUES
	(1, '测试系统公告', '<p><img title="646c1537-232e-4c54-9c6e-7a992492121f.jpeg" src="/dev-api/api/common/att/fea1ae01-6f0d-498d-b5fa-e219ad38d55f" /><img title="52f0b5f7-f9aa-4109-8947-0bd672885993.jpg" src="/dev-api/api/common/att/bfa008ff-ac09-47f2-800c-55ee5364c0c9" />最近经常有人随2314意填写，数据看起来非常的假，不真66666666666</p>', 1, '[1]', '2024-02-02 12:34:00', b'1'),
	(2, '测试系统公告', '<p>1241222222223342<img title="9e039be0-9aea-41df-875d-c49ed2a34d6d.jpeg" src="/dev-api/api/common/att/f98f98c4-1142-4958-84a4-b8719148f49e" /><img title="452eb7b4-5c7e-47ac-a017-ab017c87e901.jpg" src="/dev-api/api/common/att/b3743194-67e8-462c-a4cf-fb35ad489a4f" /></p>\n<table style="border-collapse: collapse; width: 100%;" border="1"><colgroup><col style="width: 19.962%;" /><col style="width: 19.962%;" /><col style="width: 19.962%;" /><col style="width: 19.962%;" /><col style="width: 19.962%;" /></colgroup>\n<tbody>\n<tr>\n<td>ssfa</td>\n<td>遥特任的有&nbsp;<img src="/dev-api/api/common/5e8a3f6b-4cb0-4156-b39f-36782930ae67" /></td>\n<td><img title="dcaf8705-9cd7-432d-a8fb-9885862289d2.jpeg" src="/dev-api/api/common/att/1e9fc7e5-71fa-42e3-9011-43b1666752ab" /></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td><img title="69e62aa0-9588-4f25-b4f8-6ec03b148430.jpg" src="/dev-api/api/common/att/e980eb02-c307-42c5-bb8c-c6b4c52f15df" /></td>\n<td><img title="9a3b92f7-8167-4ca6-9e7e-3a39a7f772d3.jpeg" src="/dev-api/api/common/att/e2569551-d26d-4cee-9b32-72a3754a9582" /></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n</tbody>\n</table>', 1, '[]', '2024-02-02 12:34:29', b'1');

-- 导出  表 qy_admin_next_open.qy_user 结构
CREATE TABLE IF NOT EXISTS `qy_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `type` tinyint(2) DEFAULT NULL COMMENT '用户类型',
  `mobile` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `id_number` varchar(50) DEFAULT NULL COMMENT '身份证号',
  `pass_word` varchar(50) DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `py_name` varchar(50) DEFAULT NULL COMMENT '昵称或姓名的拼音第一个字母',
  `pinyin_name` varchar(100) DEFAULT NULL COMMENT '昵称或姓名的拼音全拼',
  `status` bit(1) DEFAULT NULL COMMENT '状态',
  `avatar` varchar(200) DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL COMMENT '软删除标识',
  `delete_token` int(11) DEFAULT NULL COMMENT '要约束的用户ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`,`delete_token`),
  KEY `dept_id` (`dept_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统管理员表';

-- 正在导出表  qy_admin_next_open.qy_user 的数据：~5 rows (大约)
INSERT INTO `qy_user` (`id`, `dept_id`, `type`, `mobile`, `user_name`, `id_number`, `pass_word`, `nick_name`, `py_name`, `pinyin_name`, `status`, `avatar`, `create_time`, `is_deleted`, `delete_token`) VALUES
	(1, 1, 99, '18989008900', 'admin', '', 'e10adc3949ba59abbe56e057f20f883e', '技术支持', 'jszc', 'jishuzhichi', b'1', '06fb6fbe-70b9-4771-af0e-9d7115f68b4f', '2025-02-04 17:49:28', b'0', 0),
	(2, 1, 1, '18989090909', 'gszx2', '540102199909031055', 'e10adc3949ba59abbe56e057f20f883e', '管理员', 'gly', 'guanliyuan', b'1', 'b1653318-4acd-46d7-bbb3-1b9cf0d40224', '2025-02-06 19:31:50', b'0', 0),
	(3, 1, 1, '', 'admin2', '540102199809031052', 'e10adc3949ba59abbe56e057f20f883e', '张三', 'zs', 'zhangsan', b'1', '', '2025-02-06 20:47:32', b'0', 0),
	(4, 2, 1, '', 'admin5', '', 'd41d8cd98f00b204e9800998ecf8427e', '顿王', 'dw', 'duwang', b'1', '', '2025-03-06 13:54:09', b'0', 0),
	(5, 2, 1, '', 'admin4', '', 'e10adc3949ba59abbe56e057f20f883e', '顿王', 'dw', 'duwang', b'1', '', '2025-03-06 13:59:52', b'0', 0);

-- 导出  表 qy_admin_next_open.qy_user_dept 结构
CREATE TABLE IF NOT EXISTS `qy_user_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `pid` int(11) DEFAULT NULL COMMENT '父部门id',
  `name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `sort` int(11) DEFAULT 0 COMMENT '显示及级别顺序',
  `status` bit(1) DEFAULT NULL COMMENT '部门启用状态',
  `great_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户角色部门表';

-- 正在导出表  qy_admin_next_open.qy_user_dept 的数据：~2 rows (大约)
INSERT INTO `qy_user_dept` (`id`, `pid`, `name`, `type`, `sort`, `status`, `great_time`, `remark`) VALUES
	(1, 0, 'XX局', 3, 0, b'1', '0001-01-01 00:00:00', ''),
	(2, 1, '办公室', 6, 1, b'1', '2025-01-29 15:37:56', '');

-- 导出  表 qy_admin_next_open.qy_user_rel_role 结构
CREATE TABLE IF NOT EXISTS `qy_user_rel_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uq_user_role` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户角色关联信息';

-- 正在导出表  qy_admin_next_open.qy_user_rel_role 的数据：~5 rows (大约)
INSERT INTO `qy_user_rel_role` (`id`, `user_id`, `role_id`) VALUES
	(7, 2, 5),
	(6, 3, 4),
	(8, 3, 5),
	(9, 4, 5),
	(3, 5, 3);

-- 导出  表 qy_admin_next_open.qy_user_role 结构
CREATE TABLE IF NOT EXISTS `qy_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(255) NOT NULL COMMENT '角色名称',
  `is_keep` bit(1) NOT NULL COMMENT '是否为系统保留',
  `is_edit_others_data` bit(1) NOT NULL COMMENT '能否修改他人数据',
  `access_level` tinyint(2) DEFAULT NULL COMMENT '访问层级：self（本级）、child（下级）、parent（上级）、self_with_child（本级+下级）',
  `remark` text DEFAULT NULL COMMENT '备注',
  `auth_dept_ids` text DEFAULT NULL COMMENT '指定部门时的部门ID集合',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `sort` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色信息';

-- 正在导出表  qy_admin_next_open.qy_user_role 的数据：~4 rows (大约)
INSERT INTO `qy_user_role` (`id`, `name`, `is_keep`, `is_edit_others_data`, `access_level`, `remark`, `auth_dept_ids`, `create_time`, `sort`) VALUES
	(2, '资料管理员', b'0', b'1', -1, '', '[2]', '2025-03-14 10:52:45', 4),
	(3, '新闻发布员', b'0', b'1', -1, '', '[1]', '2025-03-14 09:54:12', 2),
	(4, '管理员', b'1', b'0', -1, '你推销我我', '[1,2]', '2025-03-14 09:53:48', 1),
	(5, '员工查看', b'0', b'0', -1, '', '[2]', '2025-03-14 09:54:27', 3);

-- 导出  表 qy_admin_next_open.qy_user_role_rel_menu 结构
CREATE TABLE IF NOT EXISTS `qy_user_role_rel_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uq_role_menu` (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色菜单关系表';

-- 正在导出表  qy_admin_next_open.qy_user_role_rel_menu 的数据：~40 rows (大约)
INSERT INTO `qy_user_role_rel_menu` (`id`, `role_id`, `menu_id`) VALUES
	(64, 2, 10),
	(67, 2, 12),
	(68, 2, 20),
	(65, 2, 21),
	(66, 2, 22),
	(56, 3, 23),
	(57, 3, 24),
	(60, 3, 30),
	(59, 3, 31),
	(58, 3, 32),
	(22, 4, 1),
	(23, 4, 2),
	(28, 4, 5),
	(31, 4, 6),
	(32, 4, 7),
	(29, 4, 8),
	(30, 4, 9),
	(44, 4, 10),
	(49, 4, 11),
	(47, 4, 12),
	(55, 4, 13),
	(54, 4, 14),
	(53, 4, 16),
	(50, 4, 19),
	(48, 4, 20),
	(45, 4, 21),
	(46, 4, 22),
	(33, 4, 23),
	(38, 4, 24),
	(42, 4, 25),
	(37, 4, 26),
	(35, 4, 27),
	(34, 4, 28),
	(43, 4, 29),
	(41, 4, 30),
	(39, 4, 32),
	(36, 4, 33),
	(61, 5, 10),
	(62, 5, 11),
	(63, 5, 17);

-- 导出  表 qy_admin_next_open.qy_workflow 结构
CREATE TABLE IF NOT EXISTS `qy_workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(5) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `remark` text DEFAULT NULL COMMENT '备注',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL COMMENT '时间',
  `is_active` bit(1) DEFAULT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `did` (`did`) USING BTREE,
  KEY `cid` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.qy_workflow 的数据：~2 rows (大约)
INSERT INTO `qy_workflow` (`id`, `cid`, `name`, `remark`, `uid`, `did`, `created_time`, `is_active`) VALUES
	(2, 2, '自诉案件审批', '秀', 1, 1, '2025-02-17 20:55:31', b'1'),
	(3, 3, '文章发布审批', '', 1, 1, '2025-02-17 21:36:13', b'1');

-- 导出  表 qy_admin_next_open.qy_workflow_instance 结构
CREATE TABLE IF NOT EXISTS `qy_workflow_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(10) NOT NULL DEFAULT 0 COMMENT '工作流ID',
  `step_id` int(10) NOT NULL DEFAULT 0 COMMENT '当前步数ID，每次record表有记录时，这里都要对应更新',
  `module_type` int(10) NOT NULL DEFAULT 0 COMMENT '实例系统模型如：CMS，OA',
  `business_id` int(10) NOT NULL DEFAULT 0 COMMENT '实例实体ID如 cms的id',
  `workflow_status` int(11) DEFAULT NULL COMMENT '流程状态（进行中、完成、拒绝等）',
  `start_time` datetime DEFAULT NULL COMMENT '实例开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '实例流程结束时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `workflow_id` (`workflow_id`) USING BTREE,
  KEY `step_id` (`step_id`) USING BTREE,
  KEY `module_type` (`module_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.qy_workflow_instance 的数据：~0 rows (大约)

-- 导出  表 qy_admin_next_open.qy_workflow_record 结构
CREATE TABLE IF NOT EXISTS `qy_workflow_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instance_id` int(10) NOT NULL DEFAULT 0 COMMENT '工作流 实例ID',
  `step_id` int(10) NOT NULL DEFAULT 0 COMMENT '当前步数ID',
  `approve_time` datetime DEFAULT NULL COMMENT '审批时间',
  `comment` int(10) NOT NULL DEFAULT 0 COMMENT '审批意见',
  `status` int(11) DEFAULT NULL COMMENT '审批状态',
  `uid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `step_id` (`step_id`),
  KEY `workflow_id` (`instance_id`) USING BTREE,
  KEY `uid` (`uid`),
  KEY `did` (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.qy_workflow_record 的数据：~0 rows (大约)

-- 导出  表 qy_admin_next_open.qy_workflow_step 结构
CREATE TABLE IF NOT EXISTS `qy_workflow_step` (
  `id` int(11) NOT NULL,
  `pid` int(10) NOT NULL DEFAULT 0,
  `type` varchar(50) NOT NULL DEFAULT '0',
  `workflow_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `remark` text DEFAULT NULL COMMENT '备注',
  `step_order` int(5) DEFAULT NULL COMMENT '步骤顺序',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL,
  `mode` tinyint(2) DEFAULT NULL COMMENT '审核模式',
  `rule_type` tinyint(2) DEFAULT NULL COMMENT '审批规则类型',
  `task_mode` varchar(100) DEFAULT NULL COMMENT '任务模式，会签/或签 且签/会签百分比{ type: TaskModeType.AND, percentage: 100 }',
  `need_sign` bit(1) DEFAULT NULL COMMENT '审批同意时是否需要签字',
  `assign_user` text DEFAULT NULL COMMENT '指定的审批人',
  `select_multiple` bit(1) DEFAULT NULL COMMENT '自选一个人或自选多个人',
  `leader` varchar(50) DEFAULT NULL COMMENT '上级领导{ level: 1, emptySkip: false }',
  `assign_dept` text DEFAULT NULL COMMENT '部门领导{ dept: [], type: ApprovalRuleType.LEADER }',
  `assign_dept_top` text DEFAULT NULL COMMENT '部门领导{ dept: [], type: ApprovalRuleType.LEADER_TOP },',
  `assign_role` text DEFAULT NULL COMMENT '审核角色[]',
  `no_user_handler` text DEFAULT NULL COMMENT '无审批人处理{ type: NoUserHandlerType.TO_NEXT, assigned: [] }',
  `same_root` text DEFAULT NULL COMMENT '当审批人与提交人为同一人时{ type: SameRootType.TO_SELF, assigned: [] }',
  `timeout` text DEFAULT NULL COMMENT ' { enable: false, time: 1, timeUnit: TimeoutTimeUnit.M, type: TimeoutType.TO_PASS }',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `did` (`did`) USING BTREE,
  KEY `workflow_id` (`workflow_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.qy_workflow_step 的数据：~0 rows (大约)

-- 导出  表 qy_admin_next_open.rpc_cat 结构
CREATE TABLE IF NOT EXISTS `rpc_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '分类名',
  `serial_number_set` text DEFAULT NULL COMMENT '案事件编号设置',
  `workflow_set` text DEFAULT NULL COMMENT '工作流设计',
  `sort` int(11) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='维权中心分类表';

-- 正在导出表  qy_admin_next_open.rpc_cat 的数据：~5 rows (大约)
INSERT INTO `rpc_cat` (`id`, `pid`, `type`, `name`, `serial_number_set`, `workflow_set`, `sort`, `remark`, `icon`, `status`) VALUES
	(1, 0, 0, '维权督察', '{"Type":0,"Prefix":"ffss","TimeFromat":"yyyyMMdd","InitNumber":1,"PadLeft":3,"PadLeftStyle":"0"}', NULL, 1, '不知道说什么', 'fullscreen', b'1'),
	(2, 1, 1, '自诉案件', '{"Type":0,"Prefix":"桑公督维第","Suffix":"号","TimeFromat":"","InitNumber":1,"PadLeft":5,"PadLeftStyle":"0"}', '[{"type":2,"name":"值班领导意见","dutyIds":"","userIds":"","timeLimit":0,"sort":1,"remark":"需要当天值班领导的意见"},{"type":2,"name":"派出所领导意见","dutyIds":"","userIds":"","timeLimit":0,"sort":2,"remark":""},{"type":2,"name":"督察大队审核","dutyIds":"","userIds":"","timeLimit":0,"sort":3,"remark":""},{"type":2,"name":"核查反馈","dutyIds":"","userIds":"","timeLimit":0,"sort":4,"remark":""},{"type":2,"name":"办结","dutyIds":"","userIds":"","timeLimit":0,"sort":5,"remark":""},{"type":2,"name":"办理结果","dutyIds":"","userIds":"","timeLimit":0,"sort":6,"remark":""}]', 1, '2', 'briefcase', b'1'),
	(3, 1, 1, '上级交办', '{"Type":0,"Prefix":"dd","Suffix":"","TimeFromat":"yyyyMMdd","InitNumber":1,"PadLeft":2,"PadLeftStyle":"0"}', '[{"type":1,"name":"第一步","dutyIds":"","userIds":"","timeLimit":0,"sort":1,"remark":""},{"type":2,"name":"测试第二步","dutyIds":"","userIds":"","timeLimit":0,"sort":2,"remark":""}]', 2, '', 'down', b'1'),
	(4, 1, 1, '110投诉', '{"Type":0,"Prefix":"","Suffix":"","TimeFromat":"yyyyMMdd","InitNumber":1,"PadLeft":0,"PadLeftStyle":"0"}', '[{"sort":1,"name":"","dutyIds":"","userIds":"","remark":"","timeLimit":0,"type":2},{"sort":2,"name":"","dutyIds":"","userIds":"","remark":"","timeLimit":0,"type":2}]', 1, '', 'backtop', b'1'),
	(5, 1, 1, '12389投诉', '{"Type":0,"Prefix":"","Suffix":"","TimeFromat":"yyyyMMdd","InitNumber":1,"PadLeft":0,"PadLeftStyle":"0"}', '[{"type":2,"name":"","dutyIds":"","userIds":"","timeLimit":0,"sort":1,"remark":""},{"type":2,"name":"","dutyIds":"","userIds":"","timeLimit":0,"sort":2,"remark":""}]', 1, '', 'Calling-r', b'1');

-- 导出  表 qy_admin_next_open.rpc_cat_roles 结构
CREATE TABLE IF NOT EXISTS `rpc_cat_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT 0,
  `role_cats` text DEFAULT NULL COMMENT '有权限的类别集合',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='类别与部门权限关联表';

-- 正在导出表  qy_admin_next_open.rpc_cat_roles 的数据：~6 rows (大约)
INSERT INTO `rpc_cat_roles` (`id`, `did`, `role_cats`) VALUES
	(1, 14, '[21,22,23,1,2]'),
	(2, 20, '[1,2]'),
	(3, 11, '[21,22,23]'),
	(4, 22, '[1,2]'),
	(5, 1, '[]'),
	(6, 31, '[1,3]');

-- 导出  表 qy_admin_next_open.rpc_config 结构
CREATE TABLE IF NOT EXISTS `rpc_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) DEFAULT NULL COMMENT '标识',
  `value_data` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='维权中心配置表';

-- 正在导出表  qy_admin_next_open.rpc_config 的数据：~5 rows (大约)
INSERT INTO `rpc_config` (`id`, `label`, `value_data`) VALUES
	(1, 'rpc_auditor', '[5,4,9]'),
	(2, 'case_type', '[{"value":1,"label":"受到暴力袭击的"},{"value":2,"label":"被车辆冲撞、碾轧、拖拽、刚蹭的"},{"value":3,"label":"被聚众哄闹、围堵拦截、冲击、阻碍的"},{"value":4,"label":"受到扣押、撕咬、拉扯、推搡等侵害的"},{"value":5,"label":"本人及其近亲属受到威胁、恐吓、侮辱、诽谤、骚扰的"}]'),
	(3, 'text_content', '[{"value":2,"label":"这是第一条工作动态信息"},{"value":3,"label":"这是第二条工作动态信息"}]'),
	(4, 'count_dept', '[{"name":"金珠派出所","topDeptId":37,"areaDeptId":[37,55,61,56,57,62,58,59],"mapMarkStyle":"left: 10%; top: 54%;","currentMapUrl":"/uploads/ImagesNoThumb/202411/2d4aa2ca551ed0d8.png","currentMapStyle":"left: 0%; top: 0%"},{"name":"公德林派出所","topDeptId":36,"areaDeptId":[36,82,86,85,84,87,88],"mapMarkStyle":"left: 26%; top: 46%;","currentMapUrl":"/uploads/ImagesNoThumb/202411/b08ab258175c69e2.png","currentMapStyle":"left: 0%; top: 0%"},{"name":"娘热派出所","topDeptId":24,"areaDeptId":[24,3,48,21,49,50,51],"mapMarkStyle":"left: 36.5%; top: 24%;","currentMapStyle":"left: 0%; top: 0%","currentMapUrl":"/uploads/ImagesNoThumb/202411/cb365896042e620e.png"},{"name":"扎细派出所","topDeptId":22,"areaDeptId":[22,40,41],"currentMapUrl":"/uploads/ImagesNoThumb/202411/f4e8ff625ccd3693.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 42%; top: 40%;"},{"name":"夺底派出所","topDeptId":34,"areaDeptId":[34,79],"currentMapUrl":"/uploads/ImagesNoThumb/202411/6f5331e8e10c1d54.png","currentMapStyle":"left: -0.6%; top: 0%","mapMarkStyle":"left: 50%; top: 18%;"},{"name":"广场派出所","topDeptId":23,"areaDeptId":[23],"currentMapUrl":"/uploads/ImagesNoThumb/202411/9700caf26522c74f.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 34%; top: 54%;"},{"name":"吉日派出所","topDeptId":12,"areaDeptId":[12],"currentMapUrl":"/uploads/ImagesNoThumb/202411/468dee03ef142a7e.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 44%; top: 59%;"},{"name":"纳金派出所","topDeptId":13,"areaDeptId":[13],"currentMapUrl":"/uploads/ImagesNoThumb/202411/4fc93b2c6799efe1.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 63%; top: 48%;"},{"name":"嘎玛贡桑派出所","topDeptId":32,"areaDeptId":[32],"currentMapUrl":"/uploads/ImagesNoThumb/202411/f0dbe2a1d2f61c37.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 45%; top: 50%;"},{"name":"蔡公堂派出所","topDeptId":33,"areaDeptId":[33,78],"currentMapUrl":"/uploads/ImagesNoThumb/202411/7b8618ab9ae6c196.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 73.5%; top: 63%;"},{"name":"两岛派出所","topDeptId":35,"areaDeptId":[35,52,53,54],"currentMapUrl":"/uploads/ImagesNoThumb/202411/64d20b859b37f639.png","currentMapStyle":"left: 0%; top: 0%","mapMarkStyle":"left: 35%; top: 62%;"}]'),
	(5, 'map_background', '/uploads/ImagesNoThumb/202411/9012729d8dd2f169.png');

-- 导出  表 qy_admin_next_open.rpc_log 结构
CREATE TABLE IF NOT EXISTS `rpc_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contr_act` varchar(50) DEFAULT NULL COMMENT '操作控制器和方法',
  `method` varchar(10) DEFAULT NULL COMMENT '请求方法',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `did` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL COMMENT '登录IP',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `do_thing` varchar(300) DEFAULT NULL COMMENT '操作内容',
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库操作日志表';

-- 正在导出表  qy_admin_next_open.rpc_log 的数据：~0 rows (大约)

-- 导出  表 qy_admin_next_open.rpc_post 结构
CREATE TABLE IF NOT EXISTS `rpc_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '分类ID',
  `serial_number` int(11) DEFAULT NULL COMMENT '序号',
  `case_type` int(5) DEFAULT NULL COMMENT '案件类型',
  `build_number` varchar(50) DEFAULT NULL COMMENT '生成的编号',
  `name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `id_number` varchar(30) DEFAULT NULL COMMENT '身份证号',
  `work_number` varchar(30) DEFAULT NULL COMMENT '警号',
  `gender` tinyint(1) DEFAULT NULL COMMENT '姓别',
  `age` int(5) DEFAULT NULL COMMENT '年龄',
  `addr` varchar(200) DEFAULT NULL COMMENT '住址',
  `department` int(5) DEFAULT NULL COMMENT '部门',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `image` varchar(300) DEFAULT NULL COMMENT '提供图片',
  `accept_time` datetime DEFAULT NULL COMMENT '受理时间',
  `accept_type` int(11) DEFAULT NULL COMMENT '受理类型',
  `content` text DEFAULT NULL COMMENT '图标',
  `uid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `great_time` datetime DEFAULT NULL,
  `status` smallint(2) DEFAULT NULL COMMENT '办理状态',
  `rate` smallint(2) DEFAULT NULL COMMENT '满意度',
  `is_true` smallint(2) DEFAULT NULL COMMENT '是否查实 1实2否0办理中',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cid` (`cid`) USING BTREE,
  KEY `did` (`did`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='维权中心主表';

-- 正在导出表  qy_admin_next_open.rpc_post 的数据：~15 rows (大约)
INSERT INTO `rpc_post` (`id`, `cid`, `serial_number`, `case_type`, `build_number`, `name`, `id_number`, `work_number`, `gender`, `age`, `addr`, `department`, `phone`, `image`, `accept_time`, `accept_type`, `content`, `uid`, `did`, `great_time`, `status`, `rate`, `is_true`) VALUES
	(1, 2, 1, 3, '00001', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 3, '08916016617', '', '2024-11-01 16:00:00', 1, '城关区XX分局XX派出所', 2, 7, '2023-12-01 16:00:00', 0, 3, 1),
	(2, 2, 2, 5, '00002', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 2, '08916016617', '', '2024-11-01 16:00:00', 2, '城关区XX分局XX派出所', 2, 7, '2024-11-01 16:00:00', 1, -1, 1),
	(3, 2, 3, 1, '城关督维第00003', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 3, '08916016617', '', '2024-11-01 16:00:00', 3, '城关区XX分局XX派出所', 2, 7, '2024-09-01 16:00:00', 2, -1, 1),
	(4, 2, 4, 4, '城关督维第00004号', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 3, '08916016617', '', '2024-11-01 16:00:00', 2, '城关区XX分局XX派出所城关区XX分局XX派出所', 2, 7, '2024-11-01 16:00:00', 2, -1, 2),
	(5, 2, 5, 1, '城关督维第00005号', '邓中华', '540102198109031055', '11', 1, 43, '', 3, '18989010903', '', '2024-11-01 16:00:00', 2, '柘城苛', 9, 22, '2024-10-01 16:00:00', 2, -1, 0),
	(6, 2, 6, 5, '城关督维第00006号', '邓中华', '540102198109031058', '100215', 1, 43, 'd ', 3, '18989010903', '/uploads/ImagesNoThumb/202410/787e66757723a43d.jpg', '2024-11-01 16:00:00', 1, 'dda萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要dda萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨', 9, 22, '2024-11-01 16:00:00', 1, -1, 1),
	(7, 5, 1, NULL, '202410281', '邓中华', '540102198103091055', '', 1, 43, '', 5, '18989010903', '', '2024-11-01 16:00:00', 1, '154', 2, 7, '2024-11-01 16:00:00', 0, -1, NULL),
	(8, 4, 1, 5, '202410301', '邓中华', '540102199203154022', '', 0, 32, '我是阿萨', 14, '18989010903', '', '2024-11-01 16:00:00', 2, '士大夫2', 2, 7, '2024-11-01 16:00:00', 0, -1, NULL),
	(9, 3, 1, 3, 'dd2024103001', '邓中华', '540102199203154022', '1', 0, 32, '我是阿萨', 1, '18989010903', '', '2024-11-01 16:00:00', 4, '2134', 2, 7, '2024-11-01 16:00:00', 0, -1, NULL),
	(10, 3, 1, 3, 'dd2024103001', '邓中华', '540102199203154022', '1', 0, 32, '我是阿萨', 1, '18989010903', '', '2024-11-01 16:00:00', 4, '2134', 2, 7, '2024-11-01 16:00:00', 0, -1, NULL),
	(11, 2, 4, 3, '城关督维第00004号', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 4, '08916016617', '', '2024-11-01 16:00:00', 2, '城关区XX分局XX派出所城关区XX分局XX派出所', 2, 7, '2024-11-01 16:00:00', 2, -1, 2),
	(12, 2, 6, 5, '城关督维第00006号', '邓中华', '540102198109031058', '100215', 1, 43, 'd ', 79, '18989010903', '/uploads/ImagesNoThumb/202410/787e66757723a43d.jpg', '2024-11-01 16:00:00', 1, 'dda萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要dda萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨', 9, 22, '2024-06-01 16:00:00', 1, -1, 1),
	(13, 4, 1, 5, '202410301', '邓中华', '540102199203154022', '', 0, 32, '我是阿萨', 14, '18989010903', '', '2024-11-01 16:00:00', 2, '士大夫2', 2, 7, '2024-11-01 16:00:00', 0, -1, NULL),
	(14, 2, 2, 2, '00002', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 2, '08916016617', '', '2024-11-01 16:00:00', 2, '城关区XX分局XX派出所', 2, 7, '2024-11-01 16:00:00', 1, -1, 1),
	(15, 2, 4, 2, '城关督维第00004号', '顿珠次旺', '51028119950115603x', '', 1, 29, '', 4, '08916016617', '', '2024-11-01 16:00:00', 2, '城关区XX分局XX派出所城关区XX分局XX派出所', 2, 7, '2024-11-01 16:00:00', 2, -1, 2);

-- 导出  表 qy_admin_next_open.rpc_post_share 结构
CREATE TABLE IF NOT EXISTS `rpc_post_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL COMMENT '工作项目ID',
  `target_uid` int(11) DEFAULT NULL COMMENT '推送对象人员ID',
  `flow_sort` int(11) DEFAULT NULL COMMENT '流程顺序ID',
  `share_uid` int(11) DEFAULT NULL COMMENT '推送人分享人UID',
  `share_did` int(11) DEFAULT NULL COMMENT '推送人分享人DID',
  `share_times` int(11) DEFAULT NULL COMMENT '推送次数',
  `remark` text DEFAULT NULL COMMENT '推送备注',
  `great_time` datetime DEFAULT NULL COMMENT '推送时间',
  `sign_status` int(5) DEFAULT NULL COMMENT '签收\\拒签',
  `sign_uid` int(11) DEFAULT NULL COMMENT '签收人ID',
  `sign_did` int(11) DEFAULT NULL COMMENT '签收人部门ID',
  `sign_remark` text DEFAULT NULL COMMENT '签收意见',
  `sign_time` datetime DEFAULT NULL COMMENT '签收时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `share_uid` (`share_uid`) USING BTREE,
  KEY `share_did` (`share_did`) USING BTREE,
  KEY `table_id` (`post_id`) USING BTREE,
  KEY `dept_id` (`target_uid`) USING BTREE,
  KEY `flow_sort` (`flow_sort`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='推送或转发目标人员关联表';

-- 正在导出表  qy_admin_next_open.rpc_post_share 的数据：~38 rows (大约)
INSERT INTO `rpc_post_share` (`id`, `post_id`, `target_uid`, `flow_sort`, `share_uid`, `share_did`, `share_times`, `remark`, `great_time`, `sign_status`, `sign_uid`, `sign_did`, `sign_remark`, `sign_time`) VALUES
	(4, 1, 2, 1, 9, 22, 0, '', '2024-10-16 15:23:43', 1, 2, 7, '4455', '2024-10-16 15:24:16'),
	(5, 1, 3, 1, 9, 22, 0, '', '2024-10-16 15:23:43', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(6, 1, 2, 2, 9, 22, 0, '', '2024-10-16 17:42:52', 1, 2, 7, '455454', '2024-10-16 17:43:13'),
	(7, 1, 4, 2, 9, 22, 0, '', '2024-10-16 17:42:52', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(8, 1, 2, 3, 2, 7, 0, '', '2024-10-16 17:58:30', 1, 2, 7, '', '2024-10-16 17:58:49'),
	(9, 1, 2, 4, 9, 22, 0, '', '2024-10-16 17:59:25', 1, 2, 7, '', '2024-10-16 17:59:33'),
	(10, 1, 2, 5, 9, 22, 0, '', '2024-10-16 17:59:50', 1, 2, 7, '', '2024-10-16 18:00:05'),
	(11, 1, 2, 6, 9, 22, 0, '', '2024-10-16 18:00:31', 1, 2, 7, '', '2024-10-16 18:00:53'),
	(12, 2, 13, 1, 2, 7, 0, '', '2024-10-17 16:09:48', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(18, 4, 4, 1, 2, 7, 0, '', '2024-10-17 18:06:24', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(19, 4, 5, 1, 2, 7, 0, '', '2024-10-17 18:39:40', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(20, 4, 3, 1, 2, 7, 0, '', '2024-10-17 18:39:40', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(21, 4, 2, 1, 2, 7, 0, '', '2024-10-17 18:39:40', 0, 9, 22, '3关区XX分局XX派出所城关区XX分局XX派出', '2024-10-26 19:50:14'),
	(22, 4, 3, 2, 2, 7, 0, '', '2024-10-18 09:48:29', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(23, 4, 2, 2, 2, 7, 0, '', '2024-10-18 09:48:29', 0, 9, 22, '2223公告:本站使用优质CDN加速,速度快到无法想象,无水资源画质极优,全新上线,欢迎测试！2', '2024-10-26 21:55:30'),
	(24, 4, 2, 3, 9, 22, 0, '', '2024-10-26 18:57:02', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(25, 4, 3, 3, 9, 22, 0, '', '2024-10-26 18:57:02', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(26, 4, 8, 3, 9, 22, 0, '', '2024-10-26 19:00:53', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(27, 4, 9, 3, 9, 22, 0, '', '2024-10-26 19:00:53', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(28, 4, 0, 1, 9, 0, 0, '', '2024-10-26 21:50:16', 0, 9, 22, '- 秦元 于： 2024-10-26 21:50:16 批示为: 不同意', '2024-10-26 21:59:12'),
	(29, 4, 0, 1, 9, 0, 0, '', '2024-10-26 21:50:28', 0, 9, 22, '124231412412', '2024-10-26 21:59:45'),
	(30, 4, 0, 2, 9, 22, 0, '', '2024-10-26 21:54:39', 0, 9, 22, 'XX分局XX派出所城关区XX', '2024-10-26 21:54:54'),
	(33, 3, 0, 3, 9, 22, 0, '', '2024-10-26 22:35:10', 0, 11, 18, '夺取', '2024-10-26 22:35:10'),
	(36, 2, 0, 2, 9, 22, 0, '', '2024-10-26 22:52:14', 2, 10, 42, '1234', '2024-10-26 22:52:14'),
	(37, 5, 0, 1, 9, 22, 0, '', '2024-10-26 23:20:44', 1, 12, 44, '113412', '2024-10-26 23:22:13'),
	(41, 6, 11, 1, 9, 22, 0, '', '2024-10-27 09:46:03', 1, 11, 18, 'dda萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要萨达姆要', '2024-10-27 11:05:20'),
	(42, 6, 12, 1, 9, 22, 0, '', '2024-10-27 10:00:24', 1, 12, 44, '14', '2024-10-27 10:00:24'),
	(43, 6, 10, 1, 9, 22, 0, '', '2024-10-27 10:09:42', 1, 10, 42, '44', '2024-10-27 10:09:42'),
	(44, 6, 10, 2, 2, 7, 1, '', '2024-10-30 09:40:06', 0, 10, 42, '444', '2024-10-27 10:09:50'),
	(45, 6, 12, 2, 9, 22, 0, '', '2024-10-27 10:19:34', 1, 12, 44, '134', '2024-10-27 11:08:49'),
	(46, 6, 4, 3, 2, 7, 0, '', '2024-10-30 09:41:18', 1, 4, 42, '325', '2024-10-30 09:41:18'),
	(47, 6, 3, 4, 2, 7, 0, '', '2024-10-30 09:41:32', 1, 3, 14, '3453254', '2024-10-30 09:41:38'),
	(48, 6, 3, 5, 2, 7, 0, '', '2024-10-30 09:41:46', 1, 3, 14, '32453425', '2024-10-30 09:41:46'),
	(49, 6, 4, 6, 2, 7, 0, '', '2024-10-30 09:41:57', 1, 4, 42, '22', '2024-10-30 09:41:57'),
	(50, 9, 11, 1, 2, 7, 0, '', '2024-10-30 10:56:52', 1, 11, 18, '1·243', '2024-10-30 10:56:52'),
	(51, 12, 1881, 1, 1, 1, 0, '', '2024-12-14 13:50:54', 1, 1881, 14, '111', '2024-12-14 13:50:54'),
	(52, 14, 2, 1, 1, 1, 0, '', '2025-02-12 22:01:26', 0, 0, 0, NULL, '0001-01-01 00:00:00'),
	(53, 14, 9, 1, 1, 1, 0, '', '2025-02-12 22:01:26', 0, 0, 0, NULL, '0001-01-01 00:00:00');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
