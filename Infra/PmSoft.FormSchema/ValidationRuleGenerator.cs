﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace PmSoft.FormSchema;

/// <summary>
/// 验证规则生成器 - 用于根据属性特性生成表单验证规则
/// </summary>
public static class ValidationRuleGenerator
{
	/// <summary>
	/// 添加所有验证规则
	/// </summary>
	public static void AddValidationRules(FormSchema schema, PropertyInfo prop)
	{
		AddRequiredRule(schema, prop);
		AddEmailRule(schema, prop);
		AddLengthRules(schema, prop);
	}

	/// <summary>
	/// 添加必填规则
	/// </summary>
	private static void AddRequiredRule(FormSchema schema, PropertyInfo prop)
	{
		var requiredAttr = ReflectionCacheManager.GetCachedAttribute<RequiredAttribute>(prop);
		if (requiredAttr != null)
		{
			schema.Rules.Add(new FormRule
			{
				Type = schema.Component.Contains("Select") || schema.Component.Contains("Radio") || schema.Component.Contains("Checkbox")
					? "selectRequired"
					: "required",
				Message = requiredAttr.ErrorMessage != null
					? string.Format(requiredAttr.ErrorMessage, schema.Label)
					: $"{schema.Label}为必填项"
			});
		}
	}

	/// <summary>
	/// 添加邮箱规则
	/// </summary>
	private static void AddEmailRule(FormSchema schema, PropertyInfo prop)
	{
		var emailAttr = ReflectionCacheManager.GetCachedAttribute<EmailAddressAttribute>(prop);
		if (emailAttr != null)
		{
			schema.Rules.Add(new FormRule
			{
				Type = "email",
				Message = "请输入正确的电子邮箱格式"
			});
		}
	}

	/// <summary>
	/// 添加长度相关规则
	/// </summary>
	private static void AddLengthRules(FormSchema schema, PropertyInfo prop)
	{
		AddMaxLengthRule(schema, prop);
		AddMinLengthRule(schema, prop);
		AddLengthRule(schema, prop);
		AddStringLengthRule(schema, prop);
	}

	/// <summary>
	/// 添加最大长度规则
	/// </summary>
	private static void AddMaxLengthRule(FormSchema schema, PropertyInfo prop)
	{
		var maxLengthAttr = ReflectionCacheManager.GetCachedAttribute<MaxLengthAttribute>(prop);
		if (maxLengthAttr != null)
		{
			schema.Rules.Add(new FormRule
			{
				Type = "maxLength",
				Max = maxLengthAttr.Length,
				Message = maxLengthAttr.ErrorMessage
			});
		}
	}

	/// <summary>
	/// 添加最小长度规则
	/// </summary>
	private static void AddMinLengthRule(FormSchema schema, PropertyInfo prop)
	{
		var minLengthAttr = ReflectionCacheManager.GetCachedAttribute<MinLengthAttribute>(prop);
		if (minLengthAttr != null)
		{
			schema.Rules.Add(new FormRule
			{
				Type = "minLength",
				Min = minLengthAttr.Length,
				Message = minLengthAttr.ErrorMessage
			});
		}
	}

	/// <summary>
	/// 添加长度范围规则
	/// </summary>
	private static void AddLengthRule(FormSchema schema, PropertyInfo prop)
	{
		var lengthAttr = ReflectionCacheManager.GetCachedAttribute<LengthAttribute>(prop);
		if (lengthAttr != null)
		{
			schema.Rules.Add(new FormRule
			{
				Type = "len",
				Min = lengthAttr.MinimumLength,
				Max = lengthAttr.MaximumLength,
				Message = lengthAttr.ErrorMessage != null
					? string.Format(lengthAttr.ErrorMessage, schema.Label, lengthAttr.MinimumLength, lengthAttr.MaximumLength)
					: $"{schema.Label}只允许输入{lengthAttr.MinimumLength}至{lengthAttr.MaximumLength}个字符"
			});
		}
	}

	/// <summary>
	/// 添加字符串长度规则
	/// </summary>
	private static void AddStringLengthRule(FormSchema schema, PropertyInfo prop)
	{
		var stringLengthAttr = ReflectionCacheManager.GetCachedAttribute<StringLengthAttribute>(prop);
		if (stringLengthAttr != null)
		{
			schema.Rules.Add(new FormRule
			{
				Type = "stringLength",
				Min = stringLengthAttr.MinimumLength,
				Max = stringLengthAttr.MaximumLength,
				Message = stringLengthAttr.ErrorMessage != null
					? string.Format(stringLengthAttr.ErrorMessage, schema.Label, stringLengthAttr.MinimumLength, stringLengthAttr.MaximumLength)
					: $"{schema.Label}只允许输入{stringLengthAttr.MinimumLength}至{stringLengthAttr.MaximumLength}个字符"
			});
		}
	}
}
