﻿using System.Text.Json.Serialization;

namespace PmSoft.FormSchema;

// 表单项的规则定义
public class FormRule
{
	[JsonPropertyName("type")]
	public string Type { get; set; }

	[JsonPropertyName("message")]
	public string? Message { get; set; }

	[JsonPropertyName("min")]
	public object? Min { get; set; }

	[JsonPropertyName("max")]
	public object? Max { get; set; }

	[JsonPropertyName("value")]
	public object Value { get; set; }
}
