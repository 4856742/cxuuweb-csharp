﻿using PmSoft.FormSchema.Attributes;
using System.Reflection;

namespace PmSoft.FormSchema;

/// <summary>
/// 组件类型推断器 - 用于根据属性类型推断表单组件和数据类型
/// </summary>
public class ComponentTypeInferer
{
	/// <summary>
	/// 推断组件类型
	/// </summary>
	public string InferComponentType(PropertyInfo propertyInfo)
	{
		// 优先检查是否有 FormComponentAttribute 或其派生类的标记
		var componentAttribute = ReflectionCacheManager.GetCachedAttribute<FormComponentAttribute>(propertyInfo);
		if (componentAttribute != null)
		{
			return componentAttribute.ComponentType; // 使用标记指定的组件类型
		}

		// 未标记时，根据属性类型推断
		Type propType = propertyInfo.PropertyType;
		var underlyingType = Nullable.GetUnderlyingType(propType) ?? propType;

		if (underlyingType.IsEnum)
		{
			return "Select"; // 枚举类型使用下拉框
		}
		if (propType.IsArray && propType.GetElementType()?.IsEnum == true)
		{
			return "CheckboxGroup"; // 枚举数组使用多选框组
		}
		if (underlyingType == typeof(string) || underlyingType == typeof(int))
		{
			return "Input"; // 字符串或整数使用输入框
		}

		return "Input"; // 默认使用文本输入框
	}

	/// <summary>
	/// 推断数据类型
	/// </summary>
	/// <param name="propType">属性类型</param>
	/// <returns>推断出的数据类型</returns>
	public string InferDataType(Type propType)
	{
		var underlyingType = Nullable.GetUnderlyingType(propType) ?? propType;

		if (underlyingType == typeof(int) || underlyingType == typeof(float) || underlyingType == typeof(double))
		{
			return "number"; // 数值类型
		}
		if (underlyingType == typeof(string))
		{
			return "string"; // 字符串类型
		}
		if (underlyingType.IsEnum)
		{
			return "number"; // 枚举类型作为数值
		}

		return "string"; // 默认字符串
	}
}
