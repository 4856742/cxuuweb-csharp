﻿namespace PmSoft.FormSchema.Configs;

// 表单字段选项
public class FormFieldOptions
{
	/// <summary>
	/// 是否在失去焦点时验证
	/// </summary>
	public bool? ValidateOnBlur { get; set; }

	/// <summary>
	/// 是否在值改变时验证
	/// </summary>
	public bool? ValidateOnChange { get; set; }

	/// <summary>
	/// 是否在输入时验证
	/// </summary>
	public bool? ValidateOnInput { get; set; }

	/// <summary>
	/// 是否在模型更新时验证
	/// </summary>
	public bool? ValidateOnModelUpdate { get; set; }
	// 这里可以添加 vee-validate 的其他 FieldOptions 属性
}