﻿namespace PmSoft.FormSchema.Configs;

// 表单布局（静态类，使用常量）
public static class FormLayout
{
	/// <summary>
	/// 水平布局
	/// </summary>
	public const string Horizontal = "horizontal";

	/// <summary>
	/// 垂直布局
	/// </summary>
	public const string Vertical = "vertical";

	/// <summary>
	/// 默认布局（水平布局）
	/// </summary>
	public const string Default = Horizontal;

	/// <summary>
	/// 所有可能的布局值
	/// </summary>
	public static readonly List<string> AllValues = new List<string>
	{
		Horizontal, Vertical
	};
}