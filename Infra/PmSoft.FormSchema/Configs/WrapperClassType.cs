﻿namespace PmSoft.FormSchema.Configs;

// 包裹类类型（静态类，使用常量生成）
public static class WrapperClassType
{
	/// <summary>
	/// 生成 CSS 类名，例如："{breakpoint}grid-cols-{gridCols}"，如 "sm:grid-cols-2"
	/// </summary>
	/// <param name="breakpoint">断点值，来自 Breakpoints</param>
	/// <param name="gridCols">网格列数，来自 GridCols</param>
	/// <returns>生成的 CSS 类名</returns>
	public static string Generate(string breakpoint, int gridCols)
	{
		GridCols.IsValid(gridCols); // 验证网格列数
		return $"{breakpoint}grid-cols-{gridCols}";
	}

	// 示例常量
	public const string SmGridCols2 = "sm:grid-cols-2";
	public const string LgGridCols4 = "lg:grid-cols-4";
}
