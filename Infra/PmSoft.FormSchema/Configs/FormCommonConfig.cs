﻿namespace PmSoft.FormSchema.Configs;

// 表单通用配置
public class FormCommonConfig
{
	/// <summary>
	/// 在标签后添加冒号
	/// </summary>
	public bool? Colon { get; set; }

	/// <summary>
	/// 表单项的通用配置，每个配置都会传递到每个表单项，表单项可覆盖	
	/// </summary>
	public MaybeComponentProps? ComponentProps { get; set; }

	/// <summary>
	/// 表单控件的 CSS 类
	/// </summary>
	public string? ControlClass { get; set; }

	/// <summary>
	/// 禁用所有表单项，默认值为 false
	/// </summary>
	public bool Disabled { get; set; }

	/// <summary>
	/// 禁用所有表单项的更改事件监听，默认值为 true
	/// </summary>
	public bool DisabledOnChangeListener { get; set; } = true;

	/// <summary>
	/// 禁用所有表单项的输入事件监听，默认值为 true
	/// </summary>
	public bool DisabledOnInputListener { get; set; } = true;

	/// <summary>
	/// 表示空状态的值，可选值：null, ""（空字符串）
	/// </summary>
	public string? EmptyStateValue { get; set; }

	/// <summary>
	/// 表单字段的属性
	/// </summary>
	public FormFieldOptions? FormFieldProps { get; set; }

	/// <summary>
	/// 表单项的 CSS 类，默认值为空字符串
	/// </summary>
	public string? FormItemClass { get; set; } = "";

	/// <summary>
	/// 隐藏所有表单项的标签，默认值为 false
	/// </summary>
	public bool HideLabel { get; set; }

	/// <summary>
	/// 隐藏必填标记，默认值为 false
	/// </summary>
	public bool HideRequiredMark { get; set; }

	/// <summary>
	/// 标签的 CSS 类，默认值为空字符串
	/// </summary>
	public string? LabelClass { get; set; } = "";

	/// <summary>
	/// 标签宽度
	/// </summary>
	public int? LabelWidth { get; set; }

	/// <summary>
	/// 包裹容器的 CSS 类
	/// </summary>
	public string? WrapperClass { get; set; }
}
