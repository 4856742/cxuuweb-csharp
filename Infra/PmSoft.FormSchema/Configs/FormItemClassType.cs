﻿namespace PmSoft.FormSchema.Configs;

// 表单项类类型（静态类，使用常量生成）
public static class FormItemClassType
{
	/// <summary>
	/// 生成 CSS 类名，例如：
	/// - "{breakpoint}cols-end-{auto | 1-13}"
	/// - "{breakpoint}cols-span-{auto | full | 1-13}"
	/// - "{breakpoint}cols-start-{auto | 1-13}"
	/// </summary>
	/// <param name="breakpoint">断点值，来自 Breakpoints</param>
	/// <param name="type">类型：end, span, start</param>
	/// <param name="value">值：auto, full（仅 span 支持） 或 1-13</param>
	/// <returns>生成的 CSS 类名</returns>
	public static string Generate(string breakpoint, string type, string value)
	{
		if (type != "end" && type != "span" && type != "start")
			throw new ArgumentException("类型必须是 'end', 'span' 或 'start'。");

		if (value == "auto" || (type == "span" && value == "full"))
			return $"{breakpoint}cols-{type}-{value}";

		if (int.TryParse(value, out int gridCols))
		{
			GridCols.IsValid(gridCols);
			return $"{breakpoint}cols-{type}-{gridCols}";
		}

		throw new ArgumentException("值必须是 'auto'、'full'（仅 span 支持）或 1-13 的数字。");
	}

	// 示例常量
	public const string SmColsEndAuto = "sm:cols-end-auto";
	public const string MdColsSpanFull = "md:cols-span-full";
	public const string LgColsStart3 = "lg:cols-start-3";
}
