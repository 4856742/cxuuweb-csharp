﻿namespace PmSoft.FormSchema.Configs;

// 表单项依赖
public class FormItemDependencies
{
	/// <summary>
	/// 是否禁用字段
	/// </summary>
	public bool? Disabled { get; set; }

	/// <summary>
	/// 是否渲染字段（移除 DOM）
	/// </summary>
	public bool? If { get; set; }

	/// <summary>
	/// 是否显示字段（通过 CSS 隐藏）
	/// </summary>
	public bool? Show { get; set; }

	/// <summary>
	/// 触发此依赖的字段
	/// </summary>
	public List<string> TriggerFields { get; set; } = new();
}
