﻿namespace PmSoft.FormSchema.Configs;

// 断点（静态类，使用常量）
public static class Breakpoints
{
	/// <summary>
	/// 无前缀
	/// </summary>
	public const string None = "";

	/// <summary>
	/// 小屏幕
	/// </summary>
	public const string Sm = "sm:";

	/// <summary>
	/// 中屏幕
	/// </summary>
	public const string Md = "md:";

	/// <summary>
	/// 大屏幕
	/// </summary>
	public const string Lg = "lg:";

	/// <summary>
	/// 超大屏幕
	/// </summary>
	public const string Xl = "xl:";

	/// <summary>
	/// 双倍超大屏幕
	/// </summary>
	public const string Xxl = "2xl:";

	/// <summary>
	/// 三倍超大屏幕
	/// </summary>
	public const string Xxxl = "3xl:";

	/// <summary>
	/// 所有可能的断点值
	/// </summary>
	public static readonly List<string> AllValues = new List<string>
{
	None, Sm, Md, Lg, Xl, Xxl, Xxxl
};
}
