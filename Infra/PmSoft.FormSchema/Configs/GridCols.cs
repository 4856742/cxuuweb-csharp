﻿namespace PmSoft.FormSchema.Configs;

// 网格列（静态类，使用常量）
public static class GridCols
{
	/// <summary>
	/// 网格列数可选值：1 到 13
	/// </summary>
	public const int MinValue = 1;
	public const int MaxValue = 13;

	/// <summary>
	/// 验证网格列数是否有效
	/// </summary>
	/// <param name="value">要验证的值</param>
	/// <returns>如果有效返回 true，否则抛出异常</returns>
	public static bool IsValid(int value)
	{
		if (value < MinValue || value > MaxValue)
			throw new ArgumentException("网格列数必须介于 1 和 13 之间。");
		return true;
	}

	/// <summary>
	/// 所有可能的网格列数值
	/// </summary>
	public static readonly List<int> AllValues = new List<int>
{
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13
};
}
