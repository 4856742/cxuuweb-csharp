﻿namespace PmSoft.FormSchema.Configs;

/// <summary>
/// 表单属性配置
/// </summary>
public class FormProps
{
	/// <summary>
	/// 表单项的通用配置，每个配置都会传递到每个表单项，表单项可覆盖
	/// </summary>
	public FormCommonConfig CommonConfig { get; set; }
	/// <summary>
	/// 表单的布局，基于tailwindcss	
	/// </summary>
	public string WrapperClass { get; set; }
	/// <summary>
	/// 表单操作区域class	
	/// </summary>
	public string ActionWrapperClass { get; set; }
	/// <summary>
	/// 是否显示默认操作按钮	
	/// </summary>
	public bool? ShowDefaultActions { get; set; } = false;
	/// <summary>
	/// 表单项布局
	/// </summary>
	public string Layout { get; set; } = FormLayout.Default;
	/// <summary>
	/// 是否显示折叠按钮	
	/// </summary>
	public bool? ShowCollapseButton { get; set; }
	/// <summary>
	/// 是否折叠，在showCollapseButton为true时生效
	/// </summary>
	public bool? Collapsed { get; set; }
	/// <summary>
	/// 折叠时，触发resize事件	
	/// </summary>
	public bool? CollapseTriggerResize { get; set; }
	/// <summary>
	/// 折叠时保持的行数	
	/// </summary>
	public int? CollapsedRows { get; set; }
	/// <summary>
	/// 按下回车健时提交表单	
	/// </summary>
	public bool? SubmitOnEnter { get; set; }
	/// <summary>
	/// 字段值改变时提交表单(内部防抖，这个属性一般用于表格的搜索表单)	
	/// </summary>
	public bool? SubmitOnChange { get; set; }
}
