﻿using PmSoft.FormSchema.Configs;

namespace PmSoft.FormSchema;

// 表单项的 Schema 定义
public class FormSchema
{
	/// <summary>
	/// 要渲染的组件
	/// </summary>
	public string Component { get; set; }

	/// <summary>
	/// 组件的属性
	/// </summary>
	public MaybeComponentProps ComponentProps { get; set; } = new MaybeComponentProps();

	/// <summary>
	/// 明确数据类型
	/// </summary>
	public string Type { get; set; } 

	/// <summary>
	/// 字段的默认值
	/// </summary>
	public object? DefaultValue { get; set; }

	/// <summary>
	/// 字段的依赖项
	/// </summary>
	public FormItemDependencies? Dependencies { get; set; }

	/// <summary>
	/// 字段描述
	/// </summary>
	public string? Description { get; set; }

	/// <summary>
	/// 字段名称
	/// </summary>
	public string FieldName { get; set; } = string.Empty;

	/// <summary>
	/// 字段的帮助信息
	/// </summary>
	public string? Help { get; set; }

	/// <summary>
	/// 表单项的标签
	/// </summary>
	public string? Label { get; set; }

	/// <summary>
	/// 字段的验证规则
	/// </summary>
	public List<FormRule> Rules { get; set; } = new List<FormRule>();

	/// <summary>
	/// 后缀内容
	/// </summary>
	public string? Suffix { get; set; }
}
