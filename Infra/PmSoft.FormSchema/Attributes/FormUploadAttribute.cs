﻿namespace PmSoft.FormSchema.Attributes;

public class FormUploadAttribute : FormComponentAttribute
{
	public FormUploadAttribute() : base("Upload")
	{
	}
}