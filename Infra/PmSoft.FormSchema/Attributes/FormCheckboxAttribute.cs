﻿namespace PmSoft.FormSchema.Attributes;

public class FormCheckboxAttribute : FormComponentAttribute
{
	public FormCheckboxAttribute() : base("Checkbox")
	{
	}
}
