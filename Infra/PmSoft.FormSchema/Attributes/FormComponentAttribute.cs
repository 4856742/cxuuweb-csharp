﻿namespace PmSoft.FormSchema.Attributes;

// 自定义属性，用于指定组件类型
[AttributeUsage(AttributeTargets.Property)]
public class FormComponentAttribute : Attribute
{
	public string ComponentType { get; }
	public FormComponentAttribute(string componentType)
	{
		ComponentType = componentType;
	}

	/// <summary>
	/// 样式类名
	/// </summary>
	public string? ClassName { get; set; }
	/// <summary>
	/// 是否禁用	
	/// </summary>
	public bool? Disabled { get; set; }

	/// <summary>
	/// 配置组件属性，将特有属性添加到 ComponentProps 中
	/// </summary>
	public virtual void ConfigureComponentProps(FormSchema schema)
	{
		if (!string.IsNullOrEmpty(ClassName))
			schema.ComponentProps.Add("class", ClassName);
		if (Disabled.HasValue)
			schema.ComponentProps.Add("disabled", Disabled.Value);
	}
}