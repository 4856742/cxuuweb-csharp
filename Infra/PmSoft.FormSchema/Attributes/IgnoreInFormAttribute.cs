﻿namespace PmSoft.FormSchema.Attributes;

/// <summary>
/// 用于标记在表单 schema 中忽略的属性
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class IgnoreInFormAttribute : Attribute
{
}
