﻿namespace PmSoft.FormSchema.Attributes;

public class FormTimePickerAttribute : FormComponentAttribute
{
	public FormTimePickerAttribute() : base("TimePicker")
	{
	}
}
