﻿namespace PmSoft.FormSchema.Attributes;

// 指定表单名称（应用于类）
[AttributeUsage(AttributeTargets.Class)]
public class FormNameAttribute : Attribute
{
	public string Name { get; }
	public FormNameAttribute(string name)
	{
		Name = name;
	}
}
