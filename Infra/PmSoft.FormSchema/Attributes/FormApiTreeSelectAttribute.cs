﻿namespace PmSoft.FormSchema.Attributes;

public class FormApiTreeSelectAttribute : FormApiComponentAttribute
{
	public FormApiTreeSelectAttribute(string apiUrl) : base("ApiTreeSelect", apiUrl)
	{
	}
}
