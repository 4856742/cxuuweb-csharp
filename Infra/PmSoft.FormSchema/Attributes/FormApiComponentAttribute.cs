﻿namespace PmSoft.FormSchema.Attributes;

/// <summary>
/// Api组件属性
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class FormApiComponentAttribute : FormComponentAttribute
{
	public FormApiComponentAttribute(string componentType, string apiUrl) : base(componentType)
	{
		ApiUrl = apiUrl;
	}

	/// <summary>
	/// api地址
	/// </summary>
	public string ApiUrl { get; set; }
	/// <summary>
	/// 是否立即调用api
	/// </summary>
	public bool? Immediate { get; set; } = false;
	/// <summary>
	/// label字段名
	/// </summary>
	public string? labelField { get; set; }
	/// <summary>
	/// children字段名，需要层级数据的组件可用
	/// </summary>
	public string? childrenField { get; set; }
	/// <summary>
	/// value字段名
	/// </summary>
	public string? valueField { get; set; }
	/// <summary>
	/// 组件接收options数据的属性名
	/// </summary>
	public string? optionsPropName { get; set; }

	public override void ConfigureComponentProps(FormSchema schema)
	{
		if (!string.IsNullOrEmpty(ApiUrl))
		{
			schema.ComponentProps["apiUrl"] = ApiUrl;
		}
	}
}
