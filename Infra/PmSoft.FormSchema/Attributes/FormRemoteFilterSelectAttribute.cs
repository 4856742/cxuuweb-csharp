﻿namespace PmSoft.FormSchema.Attributes;

public class FormRemoteFilterSelectAttribute : FormComponentAttribute
{
	public FormRemoteFilterSelectAttribute(string apiUrl) : base("RemoteFilterSelect")
	{
		ApiUrl = apiUrl;
	}

	public string ApiUrl { get; set; }

	public override void ConfigureComponentProps(FormSchema schema)
	{
		base.ConfigureComponentProps(schema);
		schema.ComponentProps.Add("apiUrl", ApiUrl);
	}
}
