﻿namespace PmSoft.FormSchema.Attributes;

public class FromRadioGroupAttribute : FormComponentAttribute
{
	public FromRadioGroupAttribute() : base("RadioGroup")
	{
	}

	public override void ConfigureComponentProps(FormSchema schema)
	{
	}
}
