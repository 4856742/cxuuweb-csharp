﻿namespace PmSoft.FormSchema.Attributes;

public class FormDatePickerAttribute : FormComponentAttribute
{
	public FormDatePickerAttribute() : base("DatePicker")
	{
	}
}
