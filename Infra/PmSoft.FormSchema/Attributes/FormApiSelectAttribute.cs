﻿namespace PmSoft.FormSchema.Attributes;

[AttributeUsage(AttributeTargets.Property)]
public class FormApiSelectAttribute : FormApiComponentAttribute
{
	public FormApiSelectAttribute(string apiUrl) : base("ApiSelect", apiUrl) { }
	/// <summary>
	/// Select 组件是否可筛选
	/// </summary>
	public bool Filterable { get; set; }
	/// <summary>
	/// 是否可以清空选项	
	/// </summary>
	public bool Clearable { get; set; }
	/// <summary>
	/// 是否多选	
	/// </summary>
	public bool Multiple { get; set; }

	public override void ConfigureComponentProps(FormSchema schema)
	{
		base.ConfigureComponentProps(schema);
		if (Filterable)
			schema.ComponentProps.Add("filterable", Filterable);
		if (Clearable)
			schema.ComponentProps.Add("clearable", Clearable);
		if (Multiple)
			schema.ComponentProps.Add("multiple", Multiple);
	}
}
