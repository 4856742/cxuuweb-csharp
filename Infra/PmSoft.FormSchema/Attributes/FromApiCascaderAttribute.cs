﻿namespace PmSoft.FormSchema.Attributes;

/// <summary>
/// 懒加载的Cascader 级联选择器
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class FromApiCascaderAttribute : FormApiComponentAttribute
{
	/// <summary>
	/// Api地址
	/// </summary>
	/// <param name="apiUrl"></param>
	public FromApiCascaderAttribute(string apiUrl) : base("ApiCascader", apiUrl)
	{
	}
 
}
