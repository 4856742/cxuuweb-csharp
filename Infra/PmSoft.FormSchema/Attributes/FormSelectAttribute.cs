﻿namespace PmSoft.FormSchema.Attributes;

public class FormSelectAttribute : FormComponentAttribute
{
	public FormSelectAttribute() : base("Select")
	{
	}
}
