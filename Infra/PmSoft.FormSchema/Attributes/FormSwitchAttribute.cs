﻿namespace PmSoft.FormSchema.Attributes;

public class FormSwitchAttribute: FormComponentAttribute
{
	public FormSwitchAttribute() : base("Switch")
	{
	}
}