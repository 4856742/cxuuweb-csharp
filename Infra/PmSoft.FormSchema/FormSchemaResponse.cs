﻿using PmSoft.FormSchema.Configs;
using System.Text.Json.Serialization;

namespace PmSoft.FormSchema;

// 添加表单整体的 Schema（包含名称）
public class FormSchemaResponse
{
	[JsonPropertyName("formName")]
	public string FormName { get; set; }

	[JsonPropertyName("schema")]
	public List<FormSchema> Schema { get; set; }

	[JsonPropertyName("props")]
	public FormProps Props { get; set; }
}
