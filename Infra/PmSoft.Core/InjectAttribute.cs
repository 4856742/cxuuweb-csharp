﻿using Microsoft.Extensions.DependencyInjection;

namespace PmSoft.Core;

/// <summary>
/// 自动注入特性，用于标记需要依赖注入的类，并指定其生命周期和目标接口。
/// </summary>
[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
public class InjectAttribute : Attribute
{
	private readonly List<Type> _targetTypes = new List<Type>();

	/// <summary>
	/// 获取服务的生命周期。
	/// </summary>
	public ServiceLifetime Lifetime { get; }

	/// <summary>
	/// 初始化 <see cref="InjectAttribute"/> 类的新实例。
	/// </summary>
	/// <param name="lifetime">服务的生命周期。</param>
	/// <param name="targets">目标接口类型数组。</param>
	public InjectAttribute(ServiceLifetime lifetime, params Type[] targets)
	{
		Lifetime = lifetime;
		_targetTypes.AddRange(targets);
	}

	/// <summary>
	/// 获取目标接口类型列表。
	/// </summary>
	/// <returns>目标接口类型列表。</returns>
	public List<Type> GetTargetTypes()
	{
		return _targetTypes;
	}
}