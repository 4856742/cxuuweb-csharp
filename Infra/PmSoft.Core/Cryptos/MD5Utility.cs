﻿using System.Security.Cryptography;
using System.Text;

namespace PmSoft.Core.Cryptos;

/// <summary>
/// MD5 加密工具类
/// </summary>
public static class MD5Utility
{
	/// <summary>
	/// 生成 32 位 MD5 哈希值
	/// </summary>
	/// <param name="input">输入字符串</param>
	/// <param name="toUpper">是否输出大写</param>
	/// <returns>32 位 MD5 哈希值</returns>
	public static string Generate32BitMD5(string input, bool toUpper = false)
	{
		using (MD5 md5 = MD5.Create())
		{
			byte[] inputBytes = Encoding.UTF8.GetBytes(input);
			byte[] hashBytes = md5.ComputeHash(inputBytes);
			string hash = BitConverter.ToString(hashBytes).Replace("-", "");
			return toUpper ? hash.ToUpper() : hash.ToLower();
		}
	}

	/// <summary>
	/// 生成 16 位 MD5 哈希值（截取32位MD5的前16个字符）
	/// </summary>
	/// <param name="input">输入字符串</param>
	/// <param name="toUpper">是否输出大写</param>
	/// <returns>16 位 MD5 哈希值</returns>
	public static string Generate16BitMD5(string input, bool toUpper = false)
	{
		string md5_32bit = Generate32BitMD5(input, toUpper);
		return md5_32bit.Substring(8, 16); // 截取32位MD5的中间16位
	}
}
