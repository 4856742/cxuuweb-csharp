﻿namespace PmSoft.Core.Cryptos;

/// <summary>
/// SM4 加密/解密的上下文信息
/// </summary>
internal class Sm4Context
{
	/// <summary>
	/// 构造函数，初始化上下文设置
	/// </summary>
	public Sm4Context()
	{
		// 默认加密模式（Mode 1 表示加密）
		Mode = Sm4Mode.Encrypt;
		IsPadding = true;  // 默认启用填充
		Key = new long[32]; // 密钥长度为 32，适用于 SM4 算法
	}

	/// <summary>
	/// 加密/解密模式
	/// <para>1表示加密（Encrypt），0表示解密（Decrypt）</para>
	/// </summary>
	public Sm4Mode Mode { get; set; }

	/// <summary>
	/// 密钥数组
	/// <para>用于加密和解密的密钥，通常为 32 个元素</para>
	/// </summary>
	public long[] Key { get; set; }

	/// <summary>
	/// 是否使用填充（Padding）
	/// </summary>
	public bool IsPadding { get; set; }
}

/// <summary>
/// SM4 加密/解密模式
/// </summary>
internal enum Sm4Mode
{
	/// <summary>
	/// 加密模式
	/// </summary>
	Encrypt = 1,

	/// <summary>
	/// 解密模式
	/// </summary>
	Decrypt = 0
}
