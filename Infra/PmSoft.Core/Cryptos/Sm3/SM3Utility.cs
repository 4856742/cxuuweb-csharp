﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Utilities.Encoders;
using System.Text;

namespace PmSoft.Core.Cryptos;

/// <summary>
/// 国密3加密工具类
/// </summary>
public static class SM3Utility
{
	/// <summary>
	/// 对输入数据进行SM3加密
	/// </summary>
	/// <param name="data">输入数据</param>
	/// <returns>加密后的SM3哈希值，十六进制字符串形式</returns>
	public static string Encrypt(string data)
	{
		byte[] input = Encoding.UTF8.GetBytes(data);

		IDigest digest = new SM3Digest();
		byte[] hashBytes = new byte[digest.GetDigestSize()];

		digest.BlockUpdate(input, 0, input.Length);
		digest.DoFinal(hashBytes, 0);

		string hashString = Hex.ToHexString(hashBytes);

		return hashString;
	}
}