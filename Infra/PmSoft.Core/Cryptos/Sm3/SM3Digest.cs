﻿namespace PmSoft.Core.Cryptos;

public class SM3Digest : GeneralDigest
{
    public override string AlgorithmName
    {
        get
        {
            return "SM3";
        }
    }

    public override int GetDigestSize()
    {
        return DIGEST_LENGTH;
    }

    private const int DIGEST_LENGTH = 32;

    private static readonly int[] _v0 = new int[] { 
        0x7380166f, 
        0x4914b2b9, 
        0x172442d7, 
        unchecked((int)0xda8a0600), 
        unchecked((int)0xa96f30bc), 
        0x163138aa, 
        unchecked((int)0xe38dee4d), 
        unchecked((int)0xb0fb0e4e) 
    };

    private readonly int[] _v = new int[8];
    private readonly int[] _v_ = new int[8];

    private static readonly int[] _x0 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    private readonly int[] _x = new int[68];
    private int xOff;

    private readonly int _t_00_15 = 0x79cc4519;
    private readonly int _t_16_63 = 0x7a879d8a;

    public SM3Digest()
    {
        Reset();
    }

    public SM3Digest(SM3Digest t) : base(t)
    {
        Array.Copy(t._x, 0, _x, 0, t._x.Length);
        xOff = t.xOff;

        Array.Copy(t._v, 0, _v, 0, t._v.Length);
    }

    public override void Reset()
    {
        base.Reset();

        Array.Copy(_v0, 0, _v, 0, _v0.Length);

        xOff = 0;
        Array.Copy(_x0, 0, _x, 0, _x0.Length);
    }

    internal override void ProcessBlock()
    {
        int i;

        int[] ww = _x;
        int[] ww_ = new int[64];

        for (i = 16; i < 68; i++)
        {
            ww[i] = P1(ww[i - 16] ^ ww[i - 9] ^ (ROTATE(ww[i - 3], 15))) ^ (ROTATE(ww[i - 13], 7)) ^ ww[i - 6];
        }

        for (i = 0; i < 64; i++)
        {
            ww_[i] = ww[i] ^ ww[i + 4];
        }

        int[] vv = _v;
        int[] vv_ = _v_;

        Array.Copy(vv, 0, vv_, 0, _v0.Length);

        int SS1, SS2, TT1, TT2, aaa;
        for (i = 0; i < 16; i++)
        {
            aaa = ROTATE(vv_[0], 12);
            SS1 = aaa + vv_[4] + ROTATE(_t_00_15, i);
            SS1 = ROTATE(SS1, 7);
            SS2 = SS1 ^ aaa;

            TT1 = FF_00_15(vv_[0], vv_[1], vv_[2]) + vv_[3] + SS2 + ww_[i];
            TT2 = GG_00_15(vv_[4], vv_[5], vv_[6]) + vv_[7] + SS1 + ww[i];
            vv_[3] = vv_[2];
            vv_[2] = ROTATE(vv_[1], 9);
            vv_[1] = vv_[0];
            vv_[0] = TT1;
            vv_[7] = vv_[6];
            vv_[6] = ROTATE(vv_[5], 19);
            vv_[5] = vv_[4];
            vv_[4] = P0(TT2);
        }
        for (i = 16; i < 64; i++)
        {
            aaa = ROTATE(vv_[0], 12);
            SS1 = aaa + vv_[4] + ROTATE(_t_16_63, i);
            SS1 = ROTATE(SS1, 7);
            SS2 = SS1 ^ aaa;

            TT1 = FF_16_63(vv_[0], vv_[1], vv_[2]) + vv_[3] + SS2 + ww_[i];
            TT2 = GG_16_63(vv_[4], vv_[5], vv_[6]) + vv_[7] + SS1 + ww[i];
            vv_[3] = vv_[2];
            vv_[2] = ROTATE(vv_[1], 9);
            vv_[1] = vv_[0];
            vv_[0] = TT1;
            vv_[7] = vv_[6];
            vv_[6] = ROTATE(vv_[5], 19);
            vv_[5] = vv_[4];
            vv_[4] = P0(TT2);
        }
        for (i = 0; i < 8; i++)
        {
            vv[i] ^= vv_[i];
        }

        // Reset
        xOff = 0;
        Array.Copy(_x0, 0, _x, 0, _x0.Length);
    }

    internal override void ProcessWord(byte[] in_Renamed, int inOff)
    {
        int n = in_Renamed[inOff] << 24;
        n |= (in_Renamed[++inOff] & 0xff) << 16;
        n |= (in_Renamed[++inOff] & 0xff) << 8;
        n |= (in_Renamed[++inOff] & 0xff);
        _x[xOff] = n;

        if (++xOff == 16)
        {
            ProcessBlock();
        }
    }

    internal override void ProcessLength(long bitLength)
    {
        if (xOff > 14)
        {
            ProcessBlock();
        }

        _x[14] = (int)(SupportClass.URShift(bitLength, 32));
        _x[15] = (int)(bitLength & unchecked((int)0xffffffff));
    }

    public static void IntToBigEndian(int n, byte[] bs, int off)
    {
        bs[off] = (byte)(SupportClass.URShift(n, 24));
        bs[++off] = (byte)(SupportClass.URShift(n, 16));
        bs[++off] = (byte)(SupportClass.URShift(n, 8));
        bs[++off] = (byte)(n);
    }

    public override int DoFinal(byte[] out_Renamed, int outOff)
    {
        Finish();

        for (int i = 0; i < 8; i++)
        {
            IntToBigEndian(_v[i], out_Renamed, outOff + i * 4);
        }

        Reset();

        return DIGEST_LENGTH;
    }

    private static int ROTATE(int x, int n)
    {
        return (x << n) | (SupportClass.URShift(x, (32 - n)));
    }

    private static int P0(int X)
    {
        return ((X) ^ ROTATE((X), 9) ^ ROTATE((X), 17));
    }

    private static int P1(int X)
    {
        return ((X) ^ ROTATE((X), 15) ^ ROTATE((X), 23));
    }

    private static int FF_00_15(int X, int Y, int Z)
    {
        return (X ^ Y ^ Z);
    }

    private static int FF_16_63(int X, int Y, int Z)
    {
        return ((X & Y) | (X & Z) | (Y & Z));
    }

    private static int GG_00_15(int X, int Y, int Z)
    {
        return (X ^ Y ^ Z);
    }

    private static int GG_16_63(int X, int Y, int Z)
    {
        return ((X & Y) | (~X & Z));
    }

    public override void BlockUpdate(ReadOnlySpan<byte> input)
    {
    }

    public override int DoFinal(Span<byte> output)
    {
        return DIGEST_LENGTH;
    }

    //[STAThread]
    //public static void  Main()
    //{
    //    byte[] md = new byte[32];
    //    byte[] msg1 = Encoding.Default.GetBytes("ererfeiisgod");
    //    SM3Digest sm3 = new SM3Digest();
    //    sm3.BlockUpdate(msg1, 0, msg1.Length);
    //    sm3.DoFinal(md, 0);
    //    System.String s = new UTF8Encoding().GetString(Hex.Encode(md));
    //    System.Console.Out.WriteLine(s.ToUpper());

    //    Console.ReadLine();
    //}
}