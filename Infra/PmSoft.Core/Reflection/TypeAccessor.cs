﻿using System.Diagnostics.CodeAnalysis;

namespace PmSoft.Core.Reflection;

/// <summary>
/// 类型原信息(反射)访问器
/// </summary>
public static class TypeAccessor
{
	/// <summary>
	/// 获取类型的反射服务
	/// </summary>
	/// <typeparam name="T">类型</typeparam>
	/// <returns>反射服务</returns>
	public static ReflectService<T> Get<T>()
	{
		return Get<T>(typeof(T), default);
	}

	/// <summary>
	/// 获取类型的反射服务
	/// </summary>
	/// <typeparam name="T">类型</typeparam>
	/// <param name="obj">对象实例</param>
	/// <returns>反射服务</returns>
	public static ReflectService<T> Get<T>(T obj)
	{
		return Get<T>(typeof(T), obj);
	}

	/// <summary>
	/// 获取类型的反射服务
	/// </summary>
	/// <typeparam name="T">类型</typeparam>
	/// <param name="type">类型</param>
	/// <param name="obj">对象实例</param>
	/// <returns>反射服务</returns>
	public static ReflectService<T> Get<T>([NotNull] Type type, T? obj)
	{
		var ctx = ReflectionContextCache.GetOrAdd(type);
		return new ReflectService<T>(ctx, obj);
	}

	/// <summary>
	/// 获取类型的反射服务
	/// </summary>
	/// <param name="type">类型</param>
	/// <param name="obj">对象实例</param>
	/// <returns>反射服务</returns>
	public static ReflectService<object> Get([NotNull] Type type, object? obj)
	{
		var ctx = ReflectionContextCache.GetOrAdd(type);
		return new ReflectService<object>(ctx, obj);
	}
}
