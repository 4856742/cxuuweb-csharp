﻿using MessagePack.Resolvers;
using MessagePack;

namespace PmSoft.Core
{
    public static class Bson
    {
        private static MessagePackSerializerOptions _options = new MessagePackSerializerOptions(TypelessObjectResolver.Instance);

        public static byte[] Serialize<T>(T model)
        {
            return MessagePackSerializer.Serialize(model, _options);
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Parse<T>(byte[] value)
        {
            return MessagePackSerializer.Deserialize<T>(value, _options);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToJson(byte[] value)
        {
            return MessagePackSerializer.ConvertToJson(value, _options);
        }
    }
}