﻿using Microsoft.AspNetCore.Http.Extensions;
using PmSoft.Core.Domain;
using PmSoft.Core.Domain.Auth;

namespace PmSoft.Core.EventBus;

/// <summary>
/// 表示与用户相关的事件参数，包含当前用户信息、客户端信息和请求的显示 URL。
/// </summary>
public class UserEventArgs : CommonEventArgs
{
	/// <summary>
	/// 初始化 <see cref="UserEventArgs"/> 类的新实例。
	/// </summary>
	/// <param name="applicationContext">应用程序上下文，提供用户信息、客户端信息和 HTTP 上下文。可以为 null。</param>
	public UserEventArgs(IApplicationContext? applicationContext)
	{
		User = applicationContext?.CurrentUser;
		ClientInfo = applicationContext?.ClientInfo;
		DisplayUrl = applicationContext?.HttpContext?.Request.GetDisplayUrl();
	}

	/// <summary>
	/// 获取或设置当前已认证的用户信息。
	/// 如果没有认证用户，则为 null。
	/// </summary>
	public IAuthedUser? User { get; set; }

	/// <summary>
	/// 获取或设置客户端信息提供程序。
	/// 如果上下文未提供客户端信息，则为 null。
	/// </summary>
	public IClientInfoProvider? ClientInfo { get; set; }

	/// <summary>
	/// 获取或设置当前请求的显示 URL。
	/// 如果 HTTP 上下文不可用，则为 null。
	/// </summary>
	public string? DisplayUrl { get; set; }
}