﻿namespace PmSoft.Core.EventBus;

/// <summary>
/// 同步事件处理器接口，定义事件处理的同步方法。
/// </summary>
/// <typeparam name="TEvent">事件参数类型，必须继承自 <see cref="CommonEventArgs"/>。</typeparam>
public interface IEventHandler<TEvent> where TEvent : CommonEventArgs
{
	/// <summary>
	/// 同步处理事件。
	/// </summary>
	/// <param name="args">事件参数实例。</param>
	void Handle(TEvent args);
}