﻿namespace PmSoft.Core.EventBus;

public interface IMessageQueue
{
	Task PublishAsync<TEvent>(TEvent args) where TEvent : CommonEventArgs;
}