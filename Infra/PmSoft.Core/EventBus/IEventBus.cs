﻿namespace PmSoft.Core.EventBus;

/// <summary>
/// 事件总线
/// </summary>
public interface IEventBus
{
	/// <summary>
	///  异步发布（等待执行）
	/// </summary>
	/// <typeparam name="TEvent"></typeparam>
	/// <param name="args"></param>
	/// <returns></returns>
	Task PublishAsync<TEvent>(TEvent args) where TEvent : CommonEventArgs;
	/// <summary>
	/// 同步发布（阻塞执行）
	/// </summary>
	/// <typeparam name="TEvent"></typeparam>
	/// <param name="args"></param>
	void Publish<TEvent>(TEvent args) where TEvent : CommonEventArgs;
	/// <summary>
	/// 发布后无需等待（后台执行）
	/// </summary>
	/// <typeparam name="TEvent"></typeparam>
	/// <param name="args"></param>
	void FireAndForget<TEvent>(TEvent args) where TEvent : CommonEventArgs;
}
