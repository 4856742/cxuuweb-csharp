﻿namespace PmSoft.Core.EventBus;

/// <summary>
/// 异步事件处理器接口，定义事件处理的异步方法。
/// </summary>
/// <typeparam name="TEvent">事件参数类型，必须继承自 <see cref="CommonEventArgs"/>。</typeparam>
public interface IAsyncEventHandler<TEvent> where TEvent : CommonEventArgs
{
	/// <summary>
	/// 异步处理事件。
	/// </summary>
	/// <param name="args">事件参数实例。</param>
	/// <returns>表示异步操作的任务。</returns>
	Task HandleAsync(TEvent args);
}