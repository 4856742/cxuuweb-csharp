﻿using System.Diagnostics;
using PmSoft.Core.Extensions;

namespace PmSoft.Core;

/// <summary>
/// 提供静态方法用于参数验证，确保输入值符合要求。
/// </summary>
[DebuggerStepThrough]
public static class Guard
{
	/// <summary>
	/// 检查值是否为 null，如果是则抛出异常。
	/// </summary>
	/// <typeparam name="T">值的类型。</typeparam>
	/// <param name="value">要检查的值。</param>
	/// <param name="parameterName">参数的名称，用于异常消息。</param>
	/// <returns>非 null 的值。</returns>
	/// <exception cref="ArgumentNullException">如果 <paramref name="value"/> 为 null，则抛出。</exception>
	public static T NotNull<T>(T value, string parameterName)
	{
		if (value == null)
		{
			throw new ArgumentNullException(parameterName);
		}

		return value;
	}

	/// <summary>
	/// 检查字符串是否为 null 或空，如果是则抛出异常。
	/// </summary>
	/// <param name="value">要检查的字符串。</param>
	/// <param name="parameterName">参数的名称，用于异常消息。</param>
	/// <returns>非 null 且非空的字符串。</returns>
	/// <exception cref="ArgumentException">如果 <paramref name="value"/> 为 null 或空，则抛出。</exception>
	public static string NotNullOrEmpty(string value, string parameterName)
	{
		if (value.IsNullOrEmpty())
		{
			throw new ArgumentException($"{parameterName} 不能为 null 或空！", parameterName);
		}

		return value;
	}

	/// <summary>
	/// 检查字符串是否为 null、空或仅包含空白字符，如果是则抛出异常。
	/// </summary>
	/// <param name="value">要检查的字符串。</param>
	/// <param name="parameterName">参数的名称，用于异常消息。</param>
	/// <returns>非 null、非空且不全是空白字符的字符串。</returns>
	/// <exception cref="ArgumentException">如果 <paramref name="value"/> 为 null、空或仅包含空白字符，则抛出。</exception>
	public static string NotNullOrWhiteSpace(string value, string parameterName)
	{
		if (value.IsNullOrEmpty())
		{
			throw new ArgumentException($"{parameterName} 不能为 null、空或仅包含空白字符！", parameterName);
		}

		return value;
	}

	/// <summary>
	/// 检查集合是否为 null 或空，如果是则抛出异常。
	/// </summary>
	/// <typeparam name="T">集合中元素的类型。</typeparam>
	/// <param name="value">要检查的集合。</param>
	/// <param name="parameterName">参数的名称，用于异常消息。</param>
	/// <returns>非 null 且非空的集合。</returns>
	/// <exception cref="ArgumentException">如果 <paramref name="value"/> 为 null 或空，则抛出。</exception>
	public static ICollection<T> NotNullOrEmpty<T>(ICollection<T> value, string parameterName)
	{
		if (value.IsNullOrEmpty())
		{
			throw new ArgumentException($"{parameterName} 不能为 null 或空！", parameterName);
		}

		return value;
	}
}