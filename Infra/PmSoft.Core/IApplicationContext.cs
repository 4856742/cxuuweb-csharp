﻿using Microsoft.AspNetCore.Http;
using PmSoft.Core.Domain;
using PmSoft.Core.Domain.Auth;

namespace PmSoft.Core
{
	/// <summary>
	/// 定义应用程序上下文接口，用于访问服务提供者和当前认证用户信息。
	/// </summary>
	public interface IApplicationContext
	{
		/// <summary>
		/// 获取依赖注入的服务提供者实例。
		/// 通过该服务提供者，可以访问应用程序中注册的所有服务。
		/// </summary>
		IServiceProvider ServiceProvider { get; }

		/// <summary>
		/// 获取指定类型的服务实例。
		/// 如果该服务类型未注册，将抛出 <see cref="InvalidOperationException"/> 异常。
		/// </summary>
		/// <typeparam name="TService">服务类型</typeparam>
		/// <returns>返回所请求服务类型的实例</returns>
		TService GetRequiredService<TService>()
			where TService : notnull;

		/// <summary>
		/// 获取当前已认证的用户信息。
		/// 如果用户未认证，可能返回 null。
		/// </summary>
		IAuthedUser? CurrentUser { get; }
		/// <summary>
		/// 获取当前已认证的用户信息，保证非空。
		/// 如果当前用户未认证，将抛出异常。
		/// </summary>
		/// <exception cref="InvalidOperationException">当当前用户未认证时抛出。</exception>
		IAuthedUser RequiredCurrentUser { get; }
		/// <summary>
		/// 客户端信息
		/// </summary>
		public IClientInfoProvider ClientInfo { get; }

		/// <summary>
		/// HTTP上下文信息
		/// </summary>
		HttpContext HttpContext { get; }
	}
}
