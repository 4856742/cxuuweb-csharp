﻿namespace PmSoft.Core.FileStorage;

/// <summary>
/// 文件数据类，用于表示获取到的文件内容和元数据
/// </summary>
public class FileData
{
	/// <summary>
	/// 文件内容，以字节数组形式存储
	/// </summary>
	public byte[] Data { get; set; }

	/// <summary>
	/// 文件元数据，存储文件的附加信息
	/// </summary>
	public Dictionary<string, object> Metadata { get; set; } = new Dictionary<string, object>();
}
