﻿namespace PmSoft.Core.FileStorage;

/// <summary>
/// 文件存储提供商接口
/// </summary>
public interface IFileStorageProvider
{
	/// <summary>
	/// 异步保存文件
	/// </summary>
	/// <param name="args">保存文件参数</param>
	/// <returns>任务</returns>
	Task SaveFileAsync(SaveFileArgs args);

	/// <summary>
	/// 同步保存文件
	/// </summary>
	/// <param name="args">保存文件参数</param>
	void SaveFile(SaveFileArgs args);

	/// <summary>
	/// 异步删除文件
	/// </summary>
	/// <param name="args">删除文件参数</param>
	/// <returns>任务</returns>
	Task DeleteFileAsync(DeleteFileArgs args);

	/// <summary>
	/// 同步删除文件
	/// </summary>
	/// <param name="args">删除文件参数</param>
	void DeleteFile(DeleteFileArgs args);

	/// <summary>
	/// 异步判断文件是否存在
	/// </summary>
	/// <param name="args">判断文件是否存在参数</param>
	/// <returns>文件是否存在</returns>
	Task<bool> FileExistsAsync(FileExistsArgs args);

	/// <summary>
	/// 同步判断文件是否存在
	/// </summary>
	/// <param name="args">判断文件是否存在参数</param>
	/// <returns>文件是否存在</returns>
	bool FileExists(FileExistsArgs args);

	/// <summary>
	/// 异步获取文件
	/// </summary>
	/// <param name="args">获取文件参数</param>
	/// <returns>文件数据，如果文件不存在则返回 null</returns>
	Task<FileData?> GetFileAsync(GetFileArgs args);

	/// <summary>
	/// 同步获取文件
	/// </summary>
	/// <param name="args">获取文件参数</param>
	/// <returns>文件数据，如果文件不存在则返回 null</returns>
	FileData? GetFile(GetFileArgs args);

	/// <summary>
	/// 异步移动文件到新位置
	/// </summary>
	/// <param name="args">移动文件参数</param>
	/// <returns>表示异步操作的任务</returns>
	Task MoveFileAsync(MoveFileArgs args);

	/// <summary>
	/// 同步移动文件到新位置
	/// </summary>
	/// <param name="args">移动文件参数</param>
	void MoveFile(MoveFileArgs args);
}

 