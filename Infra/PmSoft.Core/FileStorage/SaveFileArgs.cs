﻿namespace PmSoft.Core.FileStorage;

/// <summary>
/// 保存文件参数
/// </summary>
public class SaveFileArgs
{
	/// <summary>
	/// 存储桶名称
	/// </summary>
	public string BucketName { get; set; }
	/// <summary>
	/// 文件路径（可选，支持多层目录，例如 "folder1/folder2"）
	/// </summary>
	public string? Path { get; set; }
	/// <summary>
	/// 文件名
	/// </summary>
	public string ObjectName { get; set; }
	/// <summary>
	/// 文件数据
	/// </summary>
	public byte[] Data { get; set; }
	/// <summary>
	/// 附加参数（可选，取决于具体实现）
	/// </summary>
	public Dictionary<string, object> AdditionalArgs { get; set; } = new();
}