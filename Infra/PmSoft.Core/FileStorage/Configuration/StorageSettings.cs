﻿namespace PmSoft.Core.FileStorage.Configuration;

/// <summary>
/// 存储相关的配置类
/// </summary>
public class StorageSettings
{
	/// <summary>
	/// 使用的存储提供商名称
	/// </summary>
	public string Provider { get; set; }

	/// <summary>
	/// MinIO 存储提供商的配置
	/// </summary>
	public MinioSettings MinIO { get; set; }
	/// <summary>
	/// AWS 存储提供商的配置
	/// </summary>
	public S3Settings S3 { get; set; }

	/// <summary>
	/// 本地文件系统存储提供商的配置
	/// </summary>
	public LocalFileSystemSettings LocalFileSystem { get; set; }
}
