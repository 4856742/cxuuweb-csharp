﻿namespace PmSoft.Core.FileStorage.Configuration;

/// <summary>
/// 本地文件系统配置类
/// </summary>
public class LocalFileSystemSettings
{
	/// <summary>
	/// 文件存储的根目录，例如 "/var/storage"
	/// </summary>
	public string RootPath { get; set; }
}
