﻿namespace PmSoft.Core.FileStorage.Configuration;

/// <summary>
/// MinIO 配置类
/// </summary>
public class MinioSettings
{
	/// <summary>
	/// MinIO 服务器地址，例如 "localhost:9000"
	/// </summary>
	public string Endpoint { get; set; }

	/// <summary>
	/// 访问密钥，用于身份验证
	/// </summary>
	public string AccessKey { get; set; }

	/// <summary>
	/// 秘密密钥，用于身份验证
	/// </summary>
	public string SecretKey { get; set; }
}
