﻿namespace PmSoft.Core.FileStorage.Configuration;

public class S3Settings
{
	public string AccessKey { get; set; }
	public string SecretKey { get; set; }
	public string Region { get; set; }
}
