﻿namespace PmSoft.Core.FileStorage;

/// <summary>
/// 删除文件所需的参数类
/// </summary>
public class DeleteFileArgs
{
	/// <summary>
	/// 存储桶名称，用于标识存储的容器
	/// </summary>
	public string BucketName { get; set; }

	/// <summary>
	/// 文件路径（可选），支持多层目录，例如 "folder1/folder2"
	/// </summary>
	public string Path { get; set; }

	/// <summary>
	/// 文件名，文件的具体名称
	/// </summary>
	public string ObjectName { get; set; }

	/// <summary>
	/// 附加参数（可选），用于特定实现的扩展需求
	/// </summary>
	public Dictionary<string, object> AdditionalArgs { get; set; } = new Dictionary<string, object>();
}