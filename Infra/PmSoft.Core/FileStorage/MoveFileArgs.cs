﻿namespace PmSoft.Core.FileStorage;

/// <summary>
/// 移动文件参数对象
/// </summary>
public class MoveFileArgs
{
	/// <summary>
	/// 源桶名称
	/// </summary>
	public string SourceBucketName { get; set; }

	/// <summary>
	/// 源对象名称，包含路径
	/// </summary>
	public string SourceObjectName { get; set; }

	/// <summary>
	/// 目标桶名称
	/// </summary>
	public string DestBucketName { get; set; }

	/// <summary>
	/// 目标对象名称，包含路径
	/// </summary>
	public string DestObjectName { get; set; }
}
