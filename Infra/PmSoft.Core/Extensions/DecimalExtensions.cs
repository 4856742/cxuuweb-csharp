﻿namespace PmSoft.Core.Extensions;

/// <summary>
/// Decimal 类型的扩展方法
/// </summary>
public static class DecimalExtensions
{
	/// <summary>
	/// 将 decimal 值转换为万元格式的字符串
	/// </summary>
	/// <param name="value">要转换的 decimal 值</param>
	/// <returns>转换后的字符串，格式为 "N2" 万元</returns>
	public static string ToTenThousandYuanFormat(this decimal value)
	{
		return string.Format("{0:N2}万元", value);
	}

	/// <summary>
	/// 将可空的 decimal 值转换为万元格式的字符串
	/// </summary>
	/// <param name="value">要转换的可空 decimal 值</param>
	/// <param name="defaultValue">如果值为 null，返回的默认字符串</param>
	/// <returns>转换后的字符串，格式为 "N2" 万元；如果值为 null，则返回默认字符串</returns>
	public static string ToTenThousandYuanFormat(this decimal? value, string defaultValue = "N/A")
	{
		if (!value.HasValue)
			return defaultValue;

		return value.Value.ToTenThousandYuanFormat();
	}
}
