namespace PmSoft.Core.Extensions;

/// <summary>
/// 为 <see cref="EventHandler"/> 提供扩展方法，用于安全地触发事件。
/// </summary>
public static class EventHandlerExtensions
{
	/// <summary>
	/// 安全地触发指定的事件，不带事件参数。
	/// </summary>
	/// <param name="eventHandler">要触发的事件处理程序。</param>
	/// <param name="sender">事件源对象。</param>
	public static void InvokeSafely(this EventHandler? eventHandler, object sender)
	{
		eventHandler?.InvokeSafely(sender, EventArgs.Empty);
	}

	/// <summary>
	/// 安全地触发指定的事件，带事件参数。
	/// </summary>
	/// <param name="eventHandler">要触发的事件处理程序。</param>
	/// <param name="sender">事件源对象。</param>
	/// <param name="e">事件参数，默认为 <see cref="EventArgs"/> 类型。</param>
	public static void InvokeSafely(this EventHandler? eventHandler, object sender, EventArgs e)
	{
		eventHandler?.Invoke(sender, e);
	}

	/// <summary>
	/// 安全地触发指定的事件，带泛型事件参数。
	/// </summary>
	/// <typeparam name="TEventArgs">事件参数的类型，必须继承自 <see cref="EventArgs"/>。</typeparam>
	/// <param name="eventHandler">要触发的事件处理程序。</param>
	/// <param name="sender">事件源对象。</param>
	/// <param name="e">泛型事件参数。</param>
	public static void InvokeSafely<TEventArgs>(this EventHandler<TEventArgs>? eventHandler, object sender, TEventArgs e)
		where TEventArgs : EventArgs
	{
		eventHandler?.Invoke(sender, e);
	}
}