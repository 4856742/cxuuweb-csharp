﻿namespace PmSoft.Core.Extensions;

/// <summary>
/// 数学相关的扩展方法
/// </summary>
public static class MathExtensions
{
	/// <summary>
	/// 返回不大于指定最大值的整数
	/// </summary>
	/// <param name="value">要比较的整数</param>
	/// <param name="maxValue">最大值</param>
	/// <returns>不大于最大值的整数</returns>
	public static int ClampMax(this int value, int maxValue)
	{
		return value > maxValue ? maxValue : value;
	}
}


