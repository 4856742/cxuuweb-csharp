﻿using System.Collections;

namespace PmSoft.Core.Extensions;

/// <summary>
/// ICollection 类型的扩展方法
/// </summary>
public static class ICollectionExtensions
{
	/// <summary>
	/// 根据条件将元素添加到集合中
	/// </summary>
	/// <typeparam name="T">集合中元素的类型</typeparam>
	/// <param name="collection">集合</param>
	/// <param name="predicate">条件谓词</param>
	/// <param name="value">要添加的元素</param>
	public static void AddIf<T>(this ICollection<T> collection, Func<bool> predicate, T? value) where T : class
	{
		if (predicate()) collection.Add(value!);
	}

	/// <summary>
	/// 检查集合是否为 null 或空。
	/// </summary>
	/// <typeparam name="T">集合中元素的类型。</typeparam>
	/// <param name="source">要检查的集合。</param>
	/// <returns>如果集合为 null 或不包含任何元素，则返回 true；否则返回 false。</returns>
	public static bool IsNullOrEmpty<T>(this ICollection<T> source)
	{
		return source == null || source.Count <= 0;
	}

	/// <summary>
	/// 检查可枚举对象是否为 null 或空。
	/// </summary>
	/// <param name="this">要检查的可枚举对象。</param>
	/// <returns>如果可枚举对象为 null 或不包含任何元素，则返回 true；否则返回 false。</returns>
	public static bool IsNullOrEmpty(this IEnumerable @this)
	{
		return @this == null || @this.GetEnumerator().MoveNext() == false;
	}
}

