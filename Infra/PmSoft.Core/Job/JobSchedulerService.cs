﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace PmSoft.Core.Job;

// <summary>
/// 作业调度服务，协调作业的启动和停止
/// </summary>
public class JobSchedulerService : IHostedService
{
	private readonly IJobSchedulerProvider _schedulerProvider;
	private readonly IJobTypeCollection _jobTypes;

	/// <summary>
	/// 构造函数，注入调度器提供者和服务提供者
	/// </summary>
	/// <param name="schedulerProvider">作业调度器提供者</param>
	/// <param name="jobTypes"></param>
	public JobSchedulerService(IJobSchedulerProvider schedulerProvider, IJobTypeCollection jobTypes)
	{
		_schedulerProvider = schedulerProvider;
		_jobTypes = jobTypes;
	}

	/// <summary>
	/// 启动服务，扫描并调度作业
	/// </summary>
	public async Task StartAsync(CancellationToken cancellationToken)
	{
		await _schedulerProvider.StartAsync(_jobTypes);
	}

	/// <summary>
	/// 停止服务，清理调度器
	/// </summary>
	public async Task StopAsync(CancellationToken cancellationToken)
	{
		await _schedulerProvider.StopAsync();
	}
}
