﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace PmSoft.Core.Job.Quartz;

/// <summary>
/// Quartz 作业执行包装器，继承自抽象类并实现 Quartz 的 IJob 接口
/// </summary>
[DisallowConcurrentExecution]
public class QuartzJobExecutor : JobExecutionWrapper, IJob
{
	/// <summary>
	/// 构造函数，初始化作业类型
	/// </summary>
	/// <param name="scopeFactory">服务作用域工厂</param>
	/// <param name="logger">日志记录器</param>
	public QuartzJobExecutor(IServiceScopeFactory scopeFactory, ILogger<QuartzJobExecutor> logger)
		: base(scopeFactory, logger)
	{
	}

	/// <summary>
	/// Quartz 执行入口，调用抽象类的作业执行方法
	/// </summary>
	/// <param name="context">作业执行上下文</param>
	public async Task Execute(IJobExecutionContext context)
	{
		var jobType = context.JobDetail.JobDataMap.Get("JobType") as Type;
		if (jobType == null)
		{
			throw new InvalidOperationException("无法从 JobDataMap 获取作业类型");
		}
		await ExecuteJob(jobType);
	}
}

