﻿namespace PmSoft.Core.Job;

/// <summary>
/// 作业调度器提供者接口，定义调度作业的行为
/// </summary>
public interface IJobSchedulerProvider
{
	/// <summary>
	/// 启动调度器并调度所有作业
	/// </summary>
	/// <param name="jobTypes">作业类型的集合</param>
	Task StartAsync(IEnumerable<Type> jobTypes);

	/// <summary>
	/// 停止调度器，清理资源
	/// </summary>
	Task StopAsync();
}
