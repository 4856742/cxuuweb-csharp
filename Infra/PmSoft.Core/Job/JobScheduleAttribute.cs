﻿namespace PmSoft.Core.Job;

/// <summary>
/// 作业调度属性，用于标记作业的名称和调度规则
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class JobScheduleAttribute : Attribute
{
	/// <summary>
	/// 作业名称
	/// </summary>
	public string Name { get; }

	/// <summary>
	/// 调度规则（Quartz 使用 Cron，Timer 使用秒数）
	/// </summary>
	public string Schedule { get; }

	/// <summary>
	/// 构造函数，初始化作业名称和调度规则
	/// </summary>
	/// <param name="name">作业名称</param>
	/// <param name="schedule">调度规则</param>
	public JobScheduleAttribute(string name, string schedule)
	{
		Name = name;
		Schedule = schedule;
	}
}
