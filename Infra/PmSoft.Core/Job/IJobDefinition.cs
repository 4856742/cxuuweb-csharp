﻿namespace PmSoft.Core.Job;

/// <summary>
/// 作业定义接口，所有具体作业需实现此接口
/// </summary>
public interface IJobDefinition
{
	/// <summary>
	/// 执行作业的具体逻辑
	/// </summary>
	Task Execute();
}
