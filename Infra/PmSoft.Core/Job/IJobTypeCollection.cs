﻿namespace PmSoft.Core.Job;

public interface IJobTypeCollection : IEnumerable<Type>
{
}

public class JobTypeCollection : List<Type>, IJobTypeCollection
{
	public JobTypeCollection(IEnumerable<Type> jobTypes) : base(jobTypes) { }
}
