﻿using System.Diagnostics.CodeAnalysis;

namespace PmSoft.Core;

/// <summary>
/// 表示 PmSoft 框架中的业务异常类。
/// 该异常用于封装业务逻辑中的错误情况，可携带错误码和详细信息。
/// </summary>
public class PmSoftException : Exception
{
	/// <summary>
	/// 初始化 <see cref="PmSoftException"/> 类的新实例。
	/// </summary>
	public PmSoftException() { }

	/// <summary>
	/// 使用指定的错误消息初始化 <see cref="PmSoftException"/> 类的新实例。
	/// </summary>
	/// <param name="message">描述异常原因的错误消息。</param>
	public PmSoftException(string message) : base(message) { }

	/// <summary>
	/// 使用指定的错误消息和内部异常初始化 <see cref="PmSoftException"/> 类的新实例。
	/// </summary>
	/// <param name="message">描述异常原因的错误消息，可以为 null。</param>
	/// <param name="innerException">导致当前异常的内部异常，可以为 null。</param>
	public PmSoftException(string? message, Exception? innerException) : base(message, innerException) { }

	/// <summary>
	/// 使用指定的错误消息和错误码初始化 <see cref="PmSoftException"/> 类的新实例。
	/// </summary>
	/// <param name="message">描述异常原因的错误消息。</param>
	/// <param name="code">与异常关联的错误码。</param>
	public PmSoftException(string message, string code) : base(message)
	{
		Code = code;
	}

	/// <summary>
	/// 获取与此异常关联的错误码。
	/// 默认值为 "BUSINESS_ERROR"。
	/// </summary>
	public string Code { get; } = "BUSINESS_ERROR";

	/// <summary>
	/// 抛出一个带有指定错误消息的 <see cref="PmSoftException"/> 异常。
	/// </summary>
	/// <param name="message">描述异常原因的错误消息。</param>
	/// <exception cref="PmSoftException">始终抛出，包含指定的错误消息。</exception>
	[DoesNotReturn]
	public static void Throw(string message) => throw new PmSoftException(message);

	/// <summary>
	/// 抛出一个带有指定错误消息和内部异常的 <see cref="PmSoftException"/> 异常。
	/// </summary>
	/// <param name="message">描述异常原因的错误消息，可以为 null。</param>
	/// <param name="innerException">导致当前异常的内部异常，可以为 null。</param>
	/// <exception cref="PmSoftException">始终抛出，包含指定的错误消息和内部异常。</exception>
	[DoesNotReturn]
	public static void Throw(string? message, Exception? innerException) => throw new PmSoftException(message, innerException);

	/// <summary>
	/// 抛出一个带有指定错误消息和错误码的 <see cref="PmSoftException"/> 异常。
	/// </summary>
	/// <param name="message">描述异常原因的错误消息。</param>
	/// <param name="code">与异常关联的错误码。</param>
	/// <exception cref="PmSoftException">始终抛出，包含指定的错误消息和错误码。</exception>
	[DoesNotReturn]
	public static void Throw(string message, string code) => throw new PmSoftException(message, code);
}