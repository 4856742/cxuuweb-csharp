﻿namespace PmSoft.Core.Domain.Auth;

/// <summary>
/// 定义获取用户标识的接口，用于表示用户的身份信息。
/// </summary>
public interface IUserIdentifier
{
	/// <summary>
	/// 租户 类型。
	/// 对于主机用户可以为 null。
	/// </summary>
	string? TenantType { get; }

	/// <summary>
	/// 用户的 ID。
	/// </summary>
	int UserId { get; }
}