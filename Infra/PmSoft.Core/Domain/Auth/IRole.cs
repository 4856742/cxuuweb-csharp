﻿namespace PmSoft.Core.Domain.Auth;

// 角色接口
public interface IRole
{
	/// <summary>
	/// 角色ID
	/// </summary>
	int Id { get; }
	/// <summary>
	/// 角色代码
	/// </summary>
	string Code { get; }

	/// <summary>
	/// 角色名称
	/// </summary>
	string Name { get; }
}