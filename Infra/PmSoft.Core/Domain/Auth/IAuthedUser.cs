﻿namespace PmSoft.Core.Domain.Auth;

// 已认证用户接口
public interface IAuthedUser: IUserIdentifier
{
	/// <summary>
	/// 用户名称
	/// </summary>
	string Name { get; }
	/// <summary>
	/// 用户类型 
	/// </summary>
	string UserType { get; }
	/// <summary>
	/// 是否为超级管理员
	/// </summary>
	/// <returns></returns>
	bool IsSuperAdmin();
	/// <summary>
	/// 用户角色集合
	/// </summary>
	IEnumerable<IRole> Roles { get; }
}
 