﻿namespace PmSoft.Core.Domain.Auth;

// 菜单接口
public interface IMenu
{
	/// <summary>
	/// 菜单ID
	/// </summary>
	public int MenuId { get; }
	/// <summary>
	/// 权限编码
	/// </summary>
	string PermCode { get; }
	/// <summary>
	/// 菜单名称
	/// </summary>
	string Title { get; }
	/// <summary>
	/// 父级菜单ID（可选）
	/// </summary>
	int? ParentId { get; }
}

