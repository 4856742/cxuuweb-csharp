﻿namespace PmSoft.Core.Domain.Entities;

public interface IDelEntity<TKey> : IEntity<TKey>
{
	/// <summary>
	/// 是否删除
	/// </summary>
	bool IsDeleted { get; set; }

	/// <summary>
	/// 删除签名
	/// </summary>
	TKey? DeleteToken { get; set; }
}
