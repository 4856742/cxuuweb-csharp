namespace PmSoft.Core.Domain.Entities;

public interface IEntity  
{
	object Id { get; }
}

/// <summary>
/// 定义基础实体类型的接口。系统中的所有实体都必须实现此接口。
/// </summary>
/// <typeparam name="TKey">实体主键的类型</typeparam>
public interface IEntity<TKey> : IEntity
{
	/// <summary>
	/// 此实体的唯一标识符
	/// </summary>
	new TKey Id { get; set; }
}


/// <summary>
/// 数据实体抽象类
/// </summary>
/// <typeparam name="TKey"></typeparam>
public abstract class Entity<TKey> : IEntity<TKey>
{
	public abstract TKey Id { get; set; } 
	object IEntity.Id => Id!;
}