﻿namespace PmSoft.Core.Domain.Entities.Caching;

/// <summary>
/// 实体缓存设置属性
/// </summary>

[AttributeUsage(AttributeTargets.Class)]
public class CacheSettingAttribute : Attribute
{
	private EntityCacheExpirationPolicies expirationPolicy = EntityCacheExpirationPolicies.Normal;

	public CacheSettingAttribute(bool enableCache)
	{
		this.EnableCache = enableCache;
	}

	/// <summary>
	/// 实体名称 为空则取Type.FullName
	/// </summary>
	public string Name { get; set; } = string.Empty;

	/// <summary>
	/// 是否启用缓存
	/// </summary>
	public bool EnableCache { get; private set; }

	/// <summary>
	/// 缓存过期策略
	/// </summary>
	public EntityCacheExpirationPolicies ExpirationPolicy
	{
		get
		{
			return expirationPolicy;
		}
		set
		{
			expirationPolicy = value;
		}
	}
	/// <summary>
	/// 缓存分区的属性名称（多个属性用“,”分割）
	/// </summary>
	public string PropertyNamesOfArea { get; set; } = string.Empty;

}
