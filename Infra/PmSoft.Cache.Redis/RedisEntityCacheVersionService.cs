﻿using PmSoft.Cache.Abstractions;
using PmSoft.Cache.Abstractions.EntityCache;
using PmSoft.Core.Domain.Entities;
using System.Linq.Expressions;

namespace PmSoft.Cache.Redis;

/// <summary>
/// Redis实体缓存服务类，提供实体级别缓存管理
/// </summary>
/// <typeparam name="TEntity">实体类型，必须实现 IEntity<TKey></typeparam>
/// <typeparam name="TKey">实体主键类型，不可为空</typeparam>
public class RedisEntityCacheVersionService<TEntity, TKey> : IDistributedEntityCacheVersionService<TEntity, TKey>
	where TEntity : IEntity<TKey> where TKey : notnull
{
	private readonly ICacheService _cacheService;
	private readonly IRedisCacheProvider _redisCache;

	private const string EntityCachePrefix = "entitys";           // 实体缓存键前缀
	private const string MetadataCachePrefix = "metadata_cache_version"; // 元数据缓存键前缀

	/// <summary>
	/// 构造函数，初始化缓存服务和Redis提供者
	/// </summary>
	/// <param name="cacheService">缓存服务接口</param>
	/// <param name="redisCache">Redis缓存提供者</param>
	public RedisEntityCacheVersionService(ICacheService cacheService, IRedisCacheProvider redisCache)
	{
		_cacheService = cacheService ?? throw new ArgumentNullException(nameof(cacheService));
		_redisCache = redisCache ?? throw new ArgumentNullException(nameof(redisCache));
		Metadata = CacheEntityMetadata.ForType(typeof(TEntity), _cacheService.EnableDistributedCache);
	}

	public IEntityMetadata Metadata { get; }

	public bool IsDistributed => true;

	/// <summary>
	/// 获取单个实体的缓存键（同步）
	/// </summary>
	/// <param name="entityId">实体ID</param>
	public string GetCacheKeyOfEntity(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		return $"{EntityCachePrefix}:{Metadata.Name}:{entityId}:{GetEntityVersion(entityId)}";
	}

	/// <summary>
	/// 获取单个实体的缓存键（异步）
	/// </summary>
	/// <param name="entityId">实体ID</param>
	public async ValueTask<string> GetCacheKeyOfEntityAsync(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		int entityVersion = await GetEntityVersionAsync(entityId);
		return $"{EntityCachePrefix}:{Metadata.Name}:{entityId}:{entityVersion}";
	}

	/// <summary>
	/// 获取多个实体的缓存键（同步）
	/// </summary>
	/// <param name="entityIds">实体ID集合</param>
	public Dictionary<string, string> GetCacheKeyOfEntitys(IEnumerable<TKey> entityIds)
	{
		if (entityIds == null) throw new ArgumentNullException(nameof(entityIds));
		string hashKey = GetEntityVersionHashKey();
		var versions = _redisCache.HashGet<int>(hashKey, entityIds.Select(id => id.ToString() ?? string.Empty));
		return entityIds.ToDictionary(
			id => id.ToString() ?? string.Empty,
			id => $"{EntityCachePrefix}:{Metadata.Name}:{id}:{versions.GetValueOrDefault(id.ToString() ?? string.Empty, 0)}"
		);
	}

	/// <summary>
	/// 获取多个实体的缓存键（异步）
	/// </summary>
	/// <param name="entityIds">实体ID集合</param>
	public async ValueTask<Dictionary<string, string>> GetCacheKeyOfEntitysAsync(IEnumerable<TKey> entityIds)
	{
		if (entityIds == null) throw new ArgumentNullException(nameof(entityIds));
		string hashKey = GetEntityVersionHashKey();
		var versions = await _redisCache.HashGetAsync<int>(hashKey, entityIds.Select(id => id.ToString() ?? string.Empty));
		return entityIds.ToDictionary(
			id => id.ToString() ?? string.Empty,
			id => $"{EntityCachePrefix}:{Metadata.Name}:{id}:{versions.GetValueOrDefault(id.ToString() ?? string.Empty, 0)}"
		);
	}

	/// <summary>
	/// 获取实体版本号（同步）
	/// </summary>
	/// <param name="entityId">实体ID</param>
	public int GetEntityVersion(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		return _redisCache.HashGet<int>(GetEntityVersionHashKey(), entityId.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 获取实体版本号（异步）
	/// </summary>
	/// <param name="entityId">实体ID</param>
	public async ValueTask<int> GetEntityVersionAsync(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		return await _redisCache.HashGetAsync<int>(GetEntityVersionHashKey(), entityId.ToString() ?? string.Empty);
	}

	//// <summary>
	/// 获取区域版本号（同步），基于实体属性选择器
	/// </summary>
	/// <typeparam name="TAreaKey">区域键类型</typeparam>
	/// <param name="areaKeySelector">实体属性选择表达式，用于指定区域键</param>
	/// <param name="propertyValue">属性值，若为空则返回 0</param>
	/// <returns>区域版本号，若未找到则返回 0</returns>
	public int GetAreaVersion<TAreaKey>(Expression<Func<TEntity, TAreaKey>> areaKeySelector, object? propertyValue)
	{
		if (areaKeySelector == null) throw new ArgumentNullException(nameof(areaKeySelector));
		if (propertyValue == null) return 0;

		string propertyName = ExtractPropertyName(areaKeySelector);
		if (string.IsNullOrWhiteSpace(propertyName)) return 0;

		return _redisCache.HashGet<int>(GetAreaVersionHashKey(propertyName), propertyValue.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 获取区域版本号（同步），基于实体属性选择器
	/// </summary>
	/// <typeparam name="TAreaKey">区域键类型</typeparam>
	/// <param name="areaKeySelector">实体属性选择表达式，用于指定区域键</param>
	/// <param name="propertyValue">属性值，若为空则返回 0</param>
	/// <returns>区域版本号，若未找到则返回 0</returns>
	public async ValueTask<int> GetAreaVersionAsync<TAreaKey>(Expression<Func<TEntity, TAreaKey>> areaKeySelector, object? propertyValue)
	{
		if (areaKeySelector == null) throw new ArgumentNullException(nameof(areaKeySelector));
		if (propertyValue == null) return 0;

		string propertyName = ExtractPropertyName(areaKeySelector);
		if (string.IsNullOrWhiteSpace(propertyName)) return 0;

		return await _redisCache.HashGetAsync<int>(GetAreaVersionHashKey(propertyName), propertyValue.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 从属性选择表达式中提取属性名称
	/// </summary>
	/// <typeparam name="TAreaKey">属性类型</typeparam>
	/// <param name="selector">属性选择表达式</param>
	/// <returns>属性名称</returns>
	private string ExtractPropertyName<TAreaKey>(Expression<Func<TEntity, TAreaKey>> selector)
	{
		var expr = selector.Body as MemberExpression;
		if (expr == null && selector.Body is UnaryExpression unaryExpr)
		{
			expr = unaryExpr.Operand as MemberExpression;
		}

		string propertyName = expr?.Member.Name ?? throw new ArgumentException("无法从选择表达式中提取属性名", nameof(selector));
		if (!Metadata.PropertiesOfArea!.Any(m => m.Name == propertyName))
			throw new InvalidOperationException($"{Metadata.Name}实体的缓存属性未配置属性名为“{propertyName}”的缓存区域");
		return propertyName;
	}

	/// <summary>
	/// 获取全局版本号（同步）
	/// </summary>
	public int GetGlobalVersion()
	{
		return _redisCache.HashGet<int>(GetGlobalVersionHashKey(), Metadata.Name);
	}

	/// <summary>
	/// 获取全局版本号（异步）
	/// </summary>
	public async ValueTask<int> GetGlobalVersionAsync()
	{
		return await _redisCache.HashGetAsync<int>(GetGlobalVersionHashKey(), Metadata.Name);
	}

	/// <summary>
	/// 增加全局版本号（同步）
	/// </summary>
	public void IncreaseGlobalVersion()
	{
		_redisCache.HashIncrement(GetGlobalVersionHashKey(), Metadata.Name);
	}

	/// <summary>
	/// 增加全局版本号（异步）
	/// </summary>
	public async Task IncreaseGlobalVersionAsync()
	{
		await _redisCache.HashIncrementAsync(GetGlobalVersionHashKey(), Metadata.Name);
	}

	/// <summary>
	/// 增加区域版本号（同步，多个值）
	/// </summary>
	/// <param name="propertyName">属性名</param>
	/// <param name="propertyValues">属性值集合</param>
	public void IncreaseAreaVersion(string propertyName, IEnumerable<object> propertyValues)
	{
		if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException(nameof(propertyName));
		if (propertyValues == null) throw new ArgumentNullException(nameof(propertyValues));
		string hashKey = GetAreaVersionHashKey(propertyName);
		foreach (var value in propertyValues)
		{
			_redisCache.HashIncrement(hashKey, value?.ToString() ?? string.Empty);
		}
	}

	/// <summary>
	/// 增加区域版本号（异步，多个值）
	/// </summary>
	/// <param name="propertyName">属性名</param>
	/// <param name="propertyValues">属性值集合</param>
	public async Task IncreaseAreaVersionAsync(string propertyName, IEnumerable<object> propertyValues)
	{
		if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException(nameof(propertyName));
		if (propertyValues == null) throw new ArgumentNullException(nameof(propertyValues));
		string hashKey = GetAreaVersionHashKey(propertyName);
		foreach (var value in propertyValues)
		{
			await _redisCache.HashIncrementAsync(hashKey, value?.ToString() ?? string.Empty);
		}
	}

	/// <summary>
	/// 增加区域版本号（同步，单个值）
	/// </summary>
	/// <param name="propertyName">属性名</param>
	/// <param name="propertyValue">属性值</param>
	public void IncreaseAreaVersion(string propertyName, object propertyValue)
	{
		if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException(nameof(propertyName));
		if (propertyValue == null) throw new ArgumentNullException(nameof(propertyValue));
		_redisCache.HashIncrement(GetAreaVersionHashKey(propertyName), propertyValue.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 增加区域版本号（异步，单个值）
	/// </summary>
	/// <param name="propertyName">属性名</param>
	/// <param name="propertyValue">属性值</param>
	public async Task IncreaseAreaVersionAsync(string propertyName, object propertyValue)
	{
		if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException(nameof(propertyName));
		if (propertyValue == null) throw new ArgumentNullException(nameof(propertyValue));
		await _redisCache.HashIncrementAsync(GetAreaVersionHashKey(propertyName), propertyValue.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 增加实体缓存版本号（同步）
	/// </summary>
	/// <param name="entityId">实体ID</param>
	public void IncreaseEntityCacheVersion(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		_redisCache.HashIncrement(GetEntityVersionHashKey(), entityId.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 增加实体缓存版本号（异步）
	/// </summary>
	/// <param name="entityId">实体ID</param>
	public async Task IncreaseEntityCacheVersionAsync(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		await _redisCache.HashIncrementAsync(GetEntityVersionHashKey(), entityId.ToString() ?? string.Empty);
	}

	/// <summary>
	/// 增加列表缓存版本号（同步）
	/// </summary>
	/// <param name="entity">实体对象</param>
	public void IncreaseListCacheVersion(IEntity<TKey> entity)
	{
		if (Metadata.PropertiesOfArea != null)
		{
			foreach (var info in Metadata.PropertiesOfArea)
			{
				if (info.GetValue(entity) is object propertyValue)
				{
					IncreaseAreaVersion(info.Name.ToLower(), propertyValue);
				}
			}
		}
		IncreaseGlobalVersion();
	}

	/// <summary>
	/// 增加列表缓存版本号（异步）
	/// </summary>
	/// <param name="entity">实体对象</param>
	public async Task IncreaseListCacheVersionAsync(IEntity<TKey> entity)
	{
		if (Metadata.PropertiesOfArea != null)
		{
			foreach (var info in Metadata.PropertiesOfArea)
			{
				if (info.GetValue(entity) is object propertyValue)
				{
					await IncreaseAreaVersionAsync(info.Name.ToLower(), propertyValue);
				}
			}
		}
		await IncreaseGlobalVersionAsync();
	}

	#region 私有方法

	/// <summary>
	/// 获取全局版本号的Hash键
	/// </summary>
	private string GetGlobalVersionHashKey() => $"{MetadataCachePrefix}:global_version".ToLower();

	/// <summary>
	/// 获取区域版本号的Hash键
	/// </summary>
	/// <param name="propertyName">属性名</param>
	private string GetAreaVersionHashKey(string propertyName) =>
		$"{MetadataCachePrefix}:area_version:{Metadata.Name}:{propertyName}".ToLower();

	/// <summary>
	/// 获取实体版本号的Hash键
	/// </summary>
	private string GetEntityVersionHashKey() => $"{MetadataCachePrefix}:entity_version:{Metadata.Name}".ToLower();

	#endregion
}