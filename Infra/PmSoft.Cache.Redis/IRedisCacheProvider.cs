﻿using Microsoft.Extensions.Caching.StackExchangeRedis;
using StackExchange.Redis;

namespace PmSoft.Cache.Redis;

/// <summary>
/// Redis 缓存提供者接口，用于操作 Redis 数据库。
/// </summary>
public interface IRedisCacheProvider : IDisposable
{
	/// <summary>
	/// 获取默认的 Redis 数据库实例。
	/// </summary>
	IDatabase Database { get; }

	/// <summary>
	/// 获取 Redis 缓存配置选项。
	/// </summary>
	RedisCacheOptions Options { get; }

	/// <summary>
	/// 获取指定数据库编号的 Redis 数据库实例。
	/// </summary>
	/// <param name="db">数据库编号，默认为 -1（使用默认数据库）。</param>
	/// <returns>Redis 数据库实例。</returns>
	IDatabase GetDatabase(int db = -1);
}