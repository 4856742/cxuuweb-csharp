﻿using Microsoft.Extensions.Logging;
using PmSoft.Web.Abstractions.RealTime;

namespace PmSoft.Web.Abstractions.SignalR.Hubs;

public class CommonHub : OnlineClientHubBase
{
	private readonly ILogger _logger;
	public CommonHub(ILogger<CommonHub> logger, IOnlineClientManager onlineClientManager, IOnlineClientInfoProvider onlineClientInfoProvider) : base(logger, onlineClientManager, onlineClientInfoProvider)
	{
		_logger = logger;
	}

	public void Register()
	{
		_logger.LogDebug("A client is registered: " + Context.ConnectionId);
	}
}
