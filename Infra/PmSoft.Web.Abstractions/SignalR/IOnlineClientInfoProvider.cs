﻿using Microsoft.AspNetCore.SignalR;
using PmSoft.Web.Abstractions.RealTime;

namespace PmSoft.Web.Abstractions.SignalR;

public interface IOnlineClientInfoProvider
{
	IOnlineClient CreateClientForCurrentConnection(HubCallerContext context);
}
