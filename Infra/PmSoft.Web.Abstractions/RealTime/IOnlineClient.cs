﻿namespace PmSoft.Web.Abstractions.RealTime;

/// <summary>
/// 表示连接到应用程序的在线客户端。
/// </summary>
public interface IOnlineClient
{
	/// <summary>
	/// 该客户端的唯一连接 ID。
	/// </summary>
	string ConnectionId { get; }

	/// <summary>
	/// 该客户端的 IP 地址。
	/// </summary>
	string IpAddress { get; }

	/// <summary>
	/// 租户类型。
	/// </summary>
	string? TenantType { get; }

	/// <summary>
	/// 用户 ID。
	/// </summary>
	int? UserId { get; }

	/// <summary>
	/// 该客户端建立连接的时间。
	/// </summary>
	DateTime ConnectTime { get; }

	/// <summary>
	/// 用于快速设置或获取 <see cref="Properties"/> 中的属性。
	/// </summary>
	/// <param name="key">属性键名。</param>
	/// <returns>对应键名的属性值。</returns>
	object this[string key] { get; set; }

	/// <summary>
	/// 可用于为此客户端添加自定义属性的字典。
	/// </summary>
	Dictionary<string, object> Properties { get; }
}