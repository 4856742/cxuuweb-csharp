﻿using System;

namespace PmSoft.Web.Abstractions.RealTime;

public class OnlineClientEventArgs : EventArgs
{
    public IOnlineClient Client { get; }

    public OnlineClientEventArgs(IOnlineClient client)
    {
        Client = client;
    }
}