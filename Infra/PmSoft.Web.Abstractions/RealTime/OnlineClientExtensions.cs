﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.RealTime;

/// <summary>
/// 为 <see cref="IOnlineClient"/> 提供扩展方法，增强在线客户端的功能。
/// </summary>
public static class OnlineClientExtensions
{
	/// <summary>
	/// 将在线客户端转换为 <see cref="UserIdentifier"/> 对象，如果用户 ID 不存在则返回 null。
	/// </summary>
	/// <param name="onlineClient">要转换的在线客户端对象。</param>
	/// <returns>如果 <see cref="IOnlineClient.UserId"/> 有值，则返回对应的 <see cref="UserIdentifier"/> 对象；否则返回 null。</returns>
	public static UserIdentifier? ToUserIdentifierOrNull(this IOnlineClient onlineClient)
	{
		return onlineClient.UserId.HasValue
			? new UserIdentifier(onlineClient.TenantType, onlineClient.UserId.Value)
			: null;
	}
}