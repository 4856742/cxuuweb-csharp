﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.RealTime;

/// <summary>
/// 定义在线客户端存储的接口，用于管理实时连接的客户端。
/// </summary>
public interface IOnlineClientStore
{
	/// <summary>
	/// 异步添加在线客户端。
	/// </summary>
	/// <param name="client">要添加的在线客户端对象。</param>
	/// <returns>表示异步操作的任务。</returns>
	Task AddAsync(IOnlineClient client);

	/// <summary>
	/// 异步移除指定连接 ID 的客户端。
	/// </summary>
	/// <param name="connectionId">客户端的连接 ID。</param>
	/// <returns>如果客户端成功移除则返回 true，否则返回 false。</returns>
	Task<bool> RemoveAsync(string connectionId);

	/// <summary>
	/// 尝试移除指定连接 ID 的客户端，并对客户端执行自定义操作。
	/// </summary>
	/// <param name="connectionId">客户端的连接 ID。</param>
	/// <param name="configureClient">对客户端执行的配置操作。</param>
	/// <returns>如果客户端存在并成功移除则返回 true，否则返回 false。</returns>
	Task<bool> TryRemoveAsync(string connectionId, Action<IOnlineClient> configureClient);

	/// <summary>
	/// 尝试获取指定连接 ID 的客户端，并对客户端执行自定义操作。
	/// </summary>
	/// <param name="connectionId">客户端的连接 ID。</param>
	/// <param name="configureClient">对客户端执行的配置操作。</param>
	/// <returns>如果客户端存在则返回 true，否则返回 false。</returns>
	Task<bool> TryGetAsync(string connectionId, Action<IOnlineClient> configureClient);

	/// <summary>
	/// 异步获取所有在线客户端的只读列表。
	/// </summary>
	/// <returns>包含所有在线客户端的只读列表。</returns>
	Task<IReadOnlyList<IOnlineClient>> GetAllAsync();

	/// <summary>
	/// 异步获取指定用户标识的所有在线客户端。
	/// </summary>
	/// <param name="userIdentifier">用户ID</param>
	/// <returns>与指定用户关联的所有在线客户端的只读列表。</returns>
	Task<IReadOnlyList<IOnlineClient>> GetAllByUserIdAsync(IUserIdentifier userIdentifier);
}