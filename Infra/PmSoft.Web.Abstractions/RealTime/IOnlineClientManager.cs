﻿using PmSoft.Core.Domain.Auth;
using System.Diagnostics.CodeAnalysis;

namespace PmSoft.Web.Abstractions.RealTime;

/// <summary>
/// 用于管理连接到应用程序的在线客户端。
/// </summary>
public interface IOnlineClientManager<T> : IOnlineClientManager
{
}

/// <summary>
/// 定义管理在线客户端的基本接口，提供客户端和用户连接状态的管理功能。
/// </summary>
public interface IOnlineClientManager
{
	/// <summary>
	/// 当客户端连接时触发的事件。
	/// </summary>
	event EventHandler<OnlineClientEventArgs> ClientConnected;

	/// <summary>
	/// 当客户端断开连接时触发的事件。
	/// </summary>
	event EventHandler<OnlineClientEventArgs> ClientDisconnected;

	/// <summary>
	/// 当用户连接时触发的事件。
	/// </summary>
	event EventHandler<OnlineUserEventArgs> UserConnected;

	/// <summary>
	/// 当用户断开连接时触发的事件。
	/// </summary>
	event EventHandler<OnlineUserEventArgs> UserDisconnected;

	/// <summary>
	/// 异步添加在线客户端。
	/// </summary>
	/// <param name="client">要添加的在线客户端对象。</param>
	/// <returns>表示异步操作的任务。</returns>
	Task AddAsync(IOnlineClient client);

	/// <summary>
	/// 异步移除指定连接 ID 的客户端。
	/// </summary>
	/// <param name="connectionId">客户端的连接 ID。</param>
	/// <returns>如果客户端成功移除则返回 true，否则返回 false。</returns>
	Task<bool> RemoveAsync(string connectionId);

	/// <summary>
	/// 尝试根据连接 ID 查找客户端，如果未找到则返回 null。
	/// </summary>
	/// <param name="connectionId">客户端的连接 ID。</param>
	/// <returns>找到的客户端对象，如果未找到则返回 null。</returns>
	Task<IOnlineClient?> GetByConnectionIdOrNullAsync(string connectionId);

	/// <summary>
	/// 异步获取所有在线客户端的只读列表。
	/// </summary>
	/// <returns>包含所有在线客户端的只读列表。</returns>
	Task<IReadOnlyList<IOnlineClient>> GetAllClientsAsync();

	/// <summary>
	/// 异步获取指定用户标识的所有在线客户端。
	/// </summary>
	/// <param name="user">用户标识，不能为 null。</param>
	/// <returns>与指定用户关联的所有在线客户端的只读列表。</returns>
	Task<IReadOnlyList<IOnlineClient>> GetAllByUserIdAsync([NotNull] IUserIdentifier user);
}