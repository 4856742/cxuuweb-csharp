﻿using PmSoft.Core;
using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.RealTime;

/// <summary>
/// 为 <see cref="IOnlineClientManager"/> 提供扩展方法，增强在线客户端管理的功能。
/// </summary>
public static class OnlineClientManagerExtensions
{
	/// <summary>
	/// 判断指定用户是否在线。
	/// </summary>
	/// <param name="onlineClientManager">在线客户端管理器，不能为 null。</param>
	/// <param name="user">用户标识，不能为 null。</param>
	/// <returns>如果用户在线则返回 true，否则返回 false。</returns>
	/// <exception cref="ArgumentNullException">如果 <paramref name="onlineClientManager"/> 或 <paramref name="user"/> 为 null，则抛出。</exception>
	public static async Task<bool> IsOnlineAsync(
		this IOnlineClientManager onlineClientManager,
		IUserIdentifier user)
	{
		Guard.NotNull(onlineClientManager, nameof(onlineClientManager));
		Guard.NotNull(user, nameof(user));

		return (await onlineClientManager.GetAllByUserIdAsync(user)).Any();
	}

	/// <summary>
	/// 移除指定的在线客户端。
	/// </summary>
	/// <param name="onlineClientManager">在线客户端管理器，不能为 null。</param>
	/// <param name="client">要移除的在线客户端，不能为 null。</param>
	/// <returns>如果客户端成功移除则返回 true，否则返回 false。</returns>
	/// <exception cref="ArgumentNullException">如果 <paramref name="onlineClientManager"/> 或 <paramref name="client"/> 为 null，则抛出。</exception>
	public static async Task<bool> RemoveAsync(
		this IOnlineClientManager onlineClientManager,
		IOnlineClient client)
	{
		Guard.NotNull(onlineClientManager, nameof(onlineClientManager));
		Guard.NotNull(client, nameof(client));

		return await onlineClientManager.RemoveAsync(client.ConnectionId);
	}
}