namespace PmSoft.Web.Abstractions.Attachment;

/// <summary>
/// 附件信息接口，用于定义附件元数据的属性
/// </summary>
public interface IAttachment
{
    /// <summary>
    /// 租户类型，例如 "Company" 或 "User"
    /// </summary>
    string TenantType { get; set; }

    /// <summary>
    /// 租户ID，与业务实体关联
    /// </summary>
    string? TenantId { get; set; }

    /// <summary>
    /// 附件ID，使用 GUID 唯一标识
    /// </summary>
    string AttachmentId { get; set; }

    /// <summary>
    /// 原始文件名，例如 "document.pdf"
    /// </summary>
    string FileName { get; set; }

    /// <summary>
    /// 存储桶名称，用于区分存储空间
    /// </summary>
    string BucketName { get; set; }

    /// <summary>
    /// 对象名称，包含文件路径，例如 "files/document.pdf"
    /// </summary>
    string ObjectName { get; set; }

    /// <summary>
    /// 用户自定义的友好名称，可选
    /// </summary>
    string FriendlyName { get; set; }

    /// <summary>
    /// 媒体类型，使用枚举定义，例如 Image, Document
    /// </summary>
    MediaType MediaType { get; set; }

    /// <summary>
    /// MIME 类型，例如 "application/pdf"
    /// </summary>
    string MimeType { get; set; }

    /// <summary>
    /// 文件大小，单位：字节
    /// </summary>
    long FileSize { get; set; }

    /// <summary>
    /// 上传者的 IP 地址
    /// </summary>
    string UploaderIp { get; set; }

    /// <summary>
    /// 附件描述，可选
    /// </summary>
    string? Description { get; set; }

    /// <summary>
    /// 是否已删除，默认为 false
    /// </summary>
    bool IsDeleted { get; set; }

    /// <summary>
    /// 是否为临时附件，默认为 true
    /// </summary>
    bool IsTemporary { get; set; }
}
