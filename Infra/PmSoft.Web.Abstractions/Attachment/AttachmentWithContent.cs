﻿namespace PmSoft.Web.Abstractions.Attachment;

/// <summary>
/// 附件及其内容，包含附件信息和文件字节流
/// </summary>
public class AttachmentWithContent
{
	/// <summary>
	/// 附件信息
	/// </summary>
	public IAttachment Attachment { get; set; }

	/// <summary>
	/// 文件内容字节流
	/// </summary>
	public byte[] Content { get; set; }
}
