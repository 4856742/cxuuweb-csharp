﻿namespace PmSoft.Web.Abstractions.Attachment;

public class TempAttachment : IAttachment
{
	/// <summary>
	/// 租户类型，例如 "Company" 或 "User"
	/// </summary>
	public string TenantType { get; set; }

	/// <summary>
	/// 租户ID，与业务实体关联
	/// </summary>
	public string? TenantId { get; set; }

	/// <summary>
	/// 附件ID，使用 GUID 唯一标识
	/// </summary>
	public string AttachmentId { get; set; } = Guid.NewGuid().ToString();

	/// <summary>
	/// 原始文件名，例如 "document.pdf"
	/// </summary>
	public string FileName { get; set; }

	/// <summary>
	/// 存储桶名称，用于区分存储空间
	/// </summary>
	public string BucketName { get; set; }

	/// <summary>
	/// 对象名称，包含文件路径，例如 "files/document.pdf"
	/// </summary>
	public string ObjectName { get; set; }

	/// <summary>
	/// 用户自定义的友好名称，可选
	/// </summary>
	public string FriendlyName { get; set; }

	/// <summary>
	/// 媒体类型，使用枚举定义，例如 Image, Document
	/// </summary>
	public MediaType MediaType { get; set; }

	/// <summary>
	/// MIME 类型，例如 "application/pdf"
	/// </summary>
	public string MimeType { get; set; }

	/// <summary>
	/// 文件大小，单位：字节
	/// </summary>
	public long FileSize { get; set; }

	/// <summary>
	/// 上传者的 IP 地址
	/// </summary>
	public string UploaderIp { get; set; }

	/// <summary>
	/// 附件描述，可选
	/// </summary>
	public string Description { get; set; }

	/// <summary>
	/// 是否已删除，默认为 false
	/// </summary>
	public bool IsDeleted { get; set; } = false;

	/// <summary>
	/// 是否为临时附件，默认为 true
	/// </summary>
	public bool IsTemporary { get; set; } = true;
}
