﻿
namespace PmSoft.Web.Abstractions.Attachment;

/// <summary>
/// 附件服务接口，定义了附件的创建、获取和发布方法。
/// </summary>
public interface IAttachmentService
{
	/// <summary>
	/// 异步创建附件。
	/// </summary>
	/// <param name="attachment">附件信息。</param>
	/// <returns>任务。</returns>
	Task CreateAsync(IAttachment attachment);

	/// <summary>
	/// 异步获取附件信息。
	/// </summary>
	/// <param name="attachmentId">附件ID。</param>
	/// <returns>附件信息。</returns>
	Task<IAttachment> GetAsync(string attachmentId);

	/// <summary>
	/// 批量获取附件信息。
	/// </summary>
	/// <param name="attachmentIds">附加ID集合。</param>
	/// <returns>附件信息集合</returns>
	Task<IEnumerable<IAttachment>> GetBatchAsync(List<string> attachmentIds);

	/// <summary>
	/// 异步发布附件。
	/// </summary>
	/// <param name="attachment">附件信息。</param>
	/// <returns>任务。</returns>
	Task PublishAsync(IAttachment attachment);
	/// <summary>
	/// 批量发布附件。
	/// </summary>
	/// <param name="updatedAttachments">附件信息集合。</param>
	/// <returns></returns>
	Task PublishBatchAsync(List<IAttachment> updatedAttachments);
	/// <summary>
	/// 删除附件
	/// </summary>
	/// <param name="attachmentId"></param>
	/// <returns></returns>
	Task DeleteAsync(string attachmentId);
	/// <summary>
	/// 删除附件集合
	/// </summary>
	/// <param name="attachmentIds"></param>
	/// <returns></returns>
	Task DeleteBatchAsync(List<string> attachmentIds);
}

