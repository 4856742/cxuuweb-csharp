﻿namespace PmSoft.Web.Abstractions.Attachment;

/// <summary>
/// 媒体类型枚举，用于定义文件的类别
/// </summary>
public enum MediaType
{
	/// <summary>
	/// 图片文件，例如 .jpg, .png
	/// </summary>
	Image,

	/// <summary>
	/// 文档文件，例如 .pdf, .doc
	/// </summary>
	Document,

	/// <summary>
	/// 压缩文件，例如 .zip, .rar
	/// </summary>
	Archive,

	/// <summary>
	/// 视频文件，例如 .mp4, .avi
	/// </summary>
	Video,

	/// <summary>
	/// 未知文件类型
	/// </summary>
	Unknown
}
