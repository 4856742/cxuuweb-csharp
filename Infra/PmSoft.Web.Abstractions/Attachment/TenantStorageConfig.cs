﻿namespace PmSoft.Web.Abstractions.Attachment;

/// <summary>
/// 租户存储配置类，基于租户类型定义
/// </summary>
public class TenantStorageConfig
{
	/// <summary>
	/// 租户类型，例如 "Company", "User"
	/// </summary>
	public string TenantType { get; set; }

	/// <summary>
	/// 桶名称，用于存储该类型租户的文件
	/// </summary>
	public string BucketName { get; set; }

	/// <summary>
	/// 最大文件大小（字节），0表示无限制
	/// </summary>
	public long MaxFileSize { get; set; } = 0;

	/// <summary>
	/// 允许的扩展名列表，例如 [".pdf", ".jpg"]
	/// </summary>
	public List<string> AllowedExtensions { get; set; } = new List<string>();
}
