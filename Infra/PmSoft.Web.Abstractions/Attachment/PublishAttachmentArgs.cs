﻿namespace PmSoft.Web.Abstractions.Attachment;

/// <summary>
/// 发布附件参数
/// </summary>
public class PublishAttachmentArgs
{
	public string AttachmentId { get; set; }
	public string TenantId { get; set; }
	public string? FriendlyName { get; set; }
}
