﻿namespace PmSoft.Web.Abstractions.ErrorCode;

/// <summary>
/// 错误码定义类，包含单个错误码的详细信息
/// </summary>
public class ErrorCodeDefinition
{
	/// <summary>
	/// 错误码标识符
	/// </summary>
	public string Code { get; set; }

	/// <summary>
	/// 默认的错误消息
	/// </summary>
	public string Message { get; set; }

	/// <summary>
	/// 对应的HTTP状态码
	/// </summary>
	public int HttpStatus { get; set; }
}