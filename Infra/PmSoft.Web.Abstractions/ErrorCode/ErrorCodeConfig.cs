﻿namespace PmSoft.Web.Abstractions.ErrorCode;

/// <summary>
/// 错误码配置文件类，用于存储所有错误码的集合
/// </summary>
public class ErrorCodeConfig
{
	/// <summary>
	/// 错误码字典，以错误码字符串为键，错误定义为值
	/// </summary>
	public Dictionary<string, ErrorCodeDefinition> ErrorCodes { get; set; }
}