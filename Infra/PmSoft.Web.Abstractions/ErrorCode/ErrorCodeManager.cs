﻿using PmSoft.Core;

namespace PmSoft.Web.Abstractions.ErrorCode;

/// <summary>
/// 错误码管理器，负责加载和提供错误码定义
/// </summary>
public class ErrorCodeManager
{
	private static ErrorCodeConfig _config;     // 错误码配置
	private static readonly object _lock = new object();  // 线程锁

	/// <summary>
	/// 初始化错误码管理器，从JSON文件加载配置
	/// </summary>
	/// <param name="configFilePath">配置文件路径</param>
	public static void Initialize(string configFilePath)
	{
		lock (_lock)  // 确保线程安全
		{
			if (_config == null)
			{
				var json = File.ReadAllText(configFilePath);  // 读取JSON配置文件
				_config = Json.Parse<ErrorCodeConfig>(json) ?? new ErrorCodeConfig();  // 反序列化
			}
		}
	}

	/// <summary>
	/// 获取指定错误码的定义
	/// </summary>
	/// <param name="code">错误码</param>
	/// <returns>错误码定义，如果未找到返回默认未知错误</returns>
	public static ErrorCodeDefinition GetError(string code)
	{
		if (_config == null)
		{
			throw new InvalidOperationException("ErrorCodeManager尚未初始化");  // 未初始化异常
		}

		if (_config.ErrorCodes.TryGetValue(code, out var error))  // 尝试获取错误定义
		{
			return error;
		}

		// 返回默认未知错误
		return new ErrorCodeDefinition
		{
			Code = "UNKNOWN_ERROR",
			Message = "未知错误",
			HttpStatus = 500
		};
	}
}