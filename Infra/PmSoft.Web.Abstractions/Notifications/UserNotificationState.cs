﻿namespace PmSoft.Web.Abstractions.Notifications;

/// <summary>
/// 表示 <see cref="UserNotification"/> 的状态。
/// </summary>
public enum UserNotificationState
{
	/// <summary>
	/// 通知尚未被用户读取。
	/// </summary>
	Unread = 0,

	/// <summary>
	/// 通知已被用户读取。
	/// </summary>
	Read
}
