﻿namespace PmSoft.Web.Abstractions.Notifications;

/// <summary>
/// 定义向用户发送实时通知的接口。
/// </summary>
public interface IRealTimeNotifier
{
	/// <summary>
	/// 异步向指定用户发送实时通知。
	/// 如果用户不在线，则应忽略该用户。
	/// </summary>
	/// <param name="userNotifications">要发送的用户通知数组。</param>
	/// <returns>表示异步操作的任务。</returns>
	Task SendNotificationsAsync(IUserNotification[] userNotifications);

	/// <summary>
	/// 获取一个值，表示此实时通知器是否仅在被明确请求时使用。
	/// <para>
	/// 如果为 true，则此通知器仅在被指定为目标时用于发送实时通知；如果为 false，则不会发送任何通知。
	/// </para>
	/// </summary>
	bool UseOnlyIfRequestedAsTarget { get; }
}