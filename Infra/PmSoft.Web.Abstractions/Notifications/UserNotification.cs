﻿namespace PmSoft.Web.Abstractions.Notifications;

/// <summary>
/// 表示发送给用户的通知，实现 <see cref="IUserNotification"/> 接口
/// </summary>
[Serializable]
public class UserNotification : IUserNotification
{
	/// <summary>
	/// 租户 类型。
	/// 对于主机用户可以为 null。
	/// </summary>
	public string? TenantType { get; set; }

	/// <summary>
	/// 用户 ID。
	/// </summary>
	public int UserId { get; set; }

	/// <summary>
	/// 用户通知的当前状态。
	/// </summary>
	public UserNotificationState State { get; set; }

	/// <summary>
	/// 通知主体
	/// </summary>
	public INotificationBody Notification { get; set; }
}

