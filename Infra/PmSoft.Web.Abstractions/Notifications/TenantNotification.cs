﻿namespace PmSoft.Web.Abstractions.Notifications;

public class TenantNotification : INotificationBody
{
	/// <summary>
	/// Tenant Type.
	/// </summary>
	public string? TenantType { get; set; }

	/// <summary>
	/// Unique notification name.
	/// </summary>
	public string NotificationName { get; set; }

	/// <summary>
	/// Notification data.
	/// </summary>
	public object Data { get; set; }

	/// <summary>
	/// Name of the entity type (including namespaces).
	/// </summary>
	public string EntityTypeName { get; set; }

	/// <summary>
	/// Entity id.
	/// </summary>
	public object EntityId { get; set; }

	/// <summary>
	/// Severity.
	/// </summary>
	public NotificationSeverity Severity { get; set; }

	public DateTime CreateTime { get; set; }

	/// <summary>
	/// Initializes a new instance of the <see cref="TenantNotification"/> class.
	/// </summary>
	public TenantNotification()
	{
		CreateTime = DateTime.Now;
	}
}
