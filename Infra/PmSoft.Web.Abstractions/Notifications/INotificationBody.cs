﻿namespace PmSoft.Web.Abstractions.Notifications;

public interface INotificationBody
{
	public string? TenantType { get; set; }

	public string NotificationName { get; }

	public object Data { get; }

	public NotificationSeverity Severity { get; set; }
}
