﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.Notifications;

public static class UserIdentifierExtensions
{
	public static UserIdentifier ToUserIdentifier(this IUserIdentifier userIdentifier)
	{
		return new UserIdentifier(userIdentifier.TenantType, userIdentifier.UserId);
	}
}
