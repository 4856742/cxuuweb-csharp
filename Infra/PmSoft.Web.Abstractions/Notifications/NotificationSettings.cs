﻿namespace PmSoft.Web.Abstractions.Notifications;

public class NotificationSettings
{
	private static string _chatHubPath = "/chatHub"; // 默认值

	public static string ChatHubPath
	{
		get => _chatHubPath;
		set => _chatHubPath = value ?? "/chatHub"; // 确保不为空
	}
}
