﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.Notifications;

public interface IUserNotification : IUserIdentifier
{
	/// <summary>
	/// 用户通知的当前状态。
	/// </summary>
	public UserNotificationState State { get; }

	/// <summary>
	/// 通知主体
	/// </summary>
	public INotificationBody Notification { get; }
}
