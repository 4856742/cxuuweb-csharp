﻿namespace PmSoft.Web.Abstractions.Settings;

public class SiteOption
{
    public string CryptoKey { get; set; }
    public string CryptoIv { get; set; }

    /// <summary>
    /// 访问白名单，分号分割
    /// </summary>
    public string WhiteList { get; set; }
    public bool Debug { get; set; }
}