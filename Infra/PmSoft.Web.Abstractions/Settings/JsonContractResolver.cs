﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Reflection;

namespace PmSoft.Web.Abstractions.Settings;

internal class JsonContractResolver : DefaultContractResolver
{
    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        var property = base.CreateProperty(member, memberSerialization);

        var dateFormatAttribute = member.GetCustomAttribute<JsonDateFormatAttribute>();
        if (dateFormatAttribute != null)
        {
            property.Converter = new IsoDateTimeConverter { DateTimeFormat = dateFormatAttribute.Format };
        }

        return property;
    }
}