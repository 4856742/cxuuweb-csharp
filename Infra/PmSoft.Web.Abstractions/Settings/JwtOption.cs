﻿namespace PmSoft.Web.Abstractions.Settings;

public class JwtOption
{
    public string SecretKey { get; set; } = "CMSP3-u6u^Bdob@OJ&KF2RcAB%ybsoy&2S7jhP^SW!q!Z^FK7eB7F8CcxIHsIh4Ll3pL^#";
    public string Issuer    { get; set; } = "CMSP3";
    public string Audience  { get; set; } = "CMSP3";
}