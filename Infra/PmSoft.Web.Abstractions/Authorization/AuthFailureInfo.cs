﻿namespace PmSoft.Web.Abstractions.Authorization;

/// <summary>
/// 表示授权失败时的详细信息，包括状态码、错误代码和消息。
/// </summary>
public class AuthFailureInfo
{
	/// <summary>
	/// HTTP 状态码，例如 401 或 403。
	/// </summary>
	public int StatusCode { get; }

	/// <summary>
	/// 错误代码，用于标识失败原因。
	/// </summary>
	public string Code { get; }

	/// <summary>
	/// 错误消息，提供用户友好的说明。
	/// </summary>
	public string Message { get; }

	/// <summary>
	/// 初始化 <see cref="AuthFailureInfo"/> 的新实例。
	/// </summary>
	/// <param name="statusCode">HTTP 状态码。</param>
	/// <param name="code">错误代码。</param>
	/// <param name="message">错误消息。</param>
	public AuthFailureInfo(int statusCode, string code, string message)
	{
		StatusCode = statusCode;
		Code = code ?? throw new ArgumentNullException(nameof(code));
		Message = message ?? throw new ArgumentNullException(nameof(message));
	}
}