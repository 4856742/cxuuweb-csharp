﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.Authorization;

public interface IUserAuthenticationService
{
	/// <summary>
	/// 根据用户ID获取认证后的用户信息
	/// </summary>
	/// <param name="userId">用户ID</param>
	/// <param name="tenantType">租户类型</param>
	/// <returns>认证后的用户信息</returns>
	Task<IAuthedUser> GetAuthUserAsync(int userId, string? tenantType);
	/// <summary>
	/// 获取认证用户拥有的菜单集合
	/// </summary>
	/// <param name="user">用户</param>
	/// <returns></returns>
	Task<IEnumerable<IMenu>> GeUserMenusAsync(IAuthedUser user);
}