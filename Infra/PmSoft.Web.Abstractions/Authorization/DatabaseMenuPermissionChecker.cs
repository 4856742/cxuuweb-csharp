﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.Authorization;

/// <summary>
/// 基于数据库的权限检查器
/// </summary>
public class DatabaseMenuPermissionChecker : IMenuPermissionChecker
{
	private readonly IUserAuthenticationService userAuthenticationService;
	public DatabaseMenuPermissionChecker(IUserAuthenticationService userAuthenticationService)
	{
		this.userAuthenticationService = userAuthenticationService;
	}

	public async Task<bool> HasPermissionAsync(IAuthedUser authUser, IEnumerable<string> requiredPermCodes)
	{
		var menus = await userAuthenticationService.GeUserMenusAsync(authUser);
		if (requiredPermCodes.All(m => menus.Any(menu => menu.PermCode.Equals(m, StringComparison.OrdinalIgnoreCase))))
			return true;
		return false;
	}
}
