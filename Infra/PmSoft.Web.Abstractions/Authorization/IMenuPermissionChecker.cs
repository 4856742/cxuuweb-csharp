﻿using PmSoft.Core.Domain.Auth;

namespace PmSoft.Web.Abstractions.Authorization;

/// <summary>
/// 菜单权限检查接口
/// </summary>
public interface IMenuPermissionChecker
{
	Task<bool> HasPermissionAsync(IAuthedUser authUser, IEnumerable<string> requiredPermCodes);
}
