﻿using Microsoft.AspNetCore.Http;

namespace PmSoft.Web.Abstractions.Services;

/// <summary>
/// 文件上传服务接口，定义了文件上传相关的方法。
/// </summary>
public interface IFileUploadService
{
	/// <summary>
	/// 移动文件
	/// </summary>
	/// <param name="sourceBucketName">源桶名称</param>
	/// <param name="sourceObjectName">源对象名称</param>
	/// <param name="destBucketName">目标桶名称</param>
	/// <param name="destObjectName">目标对象名称</param>
	/// <returns>表示异步操作的任务</returns>
	Task MoveFileAsync(string sourceBucketName, string sourceObjectName, string destBucketName, string destObjectName);

	/// <summary>
	/// 上传文件块。
	/// </summary>
	/// <param name="file">文件块。</param>
	/// <param name="fileName">文件名。</param>
	/// <param name="chunkNumber">当前块编号。</param>
	/// <param name="totalChunks">总块数。</param>
	/// <param name="bucketName">存储桶名称。</param>
	/// <param name="tempPath">临时存储路径。</param>
	/// <param name="finalPath">最终存储路径。</param>
	/// <returns>上传是否成功。</returns>
	(bool IsCompleted, long TotalSize) UploadChunk(IFormFile file, string fileName, int chunkNumber, int totalChunks, string bucketName, string tempPath = "chunks/temp", string finalPath = "files");

	/// <summary>
	/// 异步上传文件块。
	/// </summary>
	/// <param name="data">文件块数据。</param>
	/// <param name="fileName">文件名。</param>
	/// <param name="chunkNumber">当前块编号。</param>
	/// <param name="totalChunks">总块数。</param>
	/// <param name="bucketName">存储桶名称。</param>
	/// <param name="tempPath">临时存储路径。</param>
	/// <param name="finalPath">最终存储路径。</param>
	/// <returns>上传是否成功的任务。</returns>
	Task<(bool IsCompleted, long TotalSize)> UploadChunkAsync(byte[] data, string fileName, int chunkNumber, int totalChunks, string bucketName, string tempPath = "chunks/temp", string finalPath = "files");

	/// <summary>
	/// 异步上传文件块。
	/// </summary>
	/// <param name="file">文件块。</param>
	/// <param name="fileName">文件名。</param>
	/// <param name="chunkNumber">当前块编号。</param>
	/// <param name="totalChunks">总块数。</param>
	/// <param name="bucketName">存储桶名称。</param>
	/// <param name="tempPath">临时存储路径。</param>
	/// <param name="finalPath">最终存储路径。</param>
	/// <returns>上传是否成功的任务。</returns>
	Task<(bool IsCompleted, long TotalSize)> UploadChunkAsync(IFormFile file, string fileName, int chunkNumber, int totalChunks, string bucketName, string tempPath = "chunks/temp", string finalPath = "files");

	/// <summary>
	/// 上传文件。
	/// </summary>
	/// <param name="file">文件。</param>
	/// <param name="bucketName">存储桶名称。</param>
	/// <param name="fileName">文件名称</param>
	/// <param name="path">存储路径。</param>
	/// <returns>上传后的文件路径。</returns>
	string UploadFile(IFormFile file, string bucketName, string fileName, string? path = null);

	/// <summary>
	/// 异步上传文件。
	/// </summary>
	/// <param name="data">文件数据。</param>
	/// <param name="fileName">文件名。</param>
	/// <param name="bucketName">存储桶名称。</param>
	/// <param name="path">存储路径。</param>
	/// <returns>上传后的文件路径的任务。</returns>
	Task<string> UploadFileAsync(byte[] data, string fileName, string bucketName, string? path = null);

	// <summary>
	/// 上传单个文件（异步）
	/// </summary>
	/// <param name="file">上传的文件</param>
	/// <param name="bucketName">存储桶名称</param>
	/// <param name="fileName">文件名称</param>
	/// <param name="path">存储路径（可选）</param>
	/// <returns>上传成功的文件名</returns>
	Task<string> UploadFileAsync(IFormFile file, string bucketName, string fileName, string? path = null);
}
