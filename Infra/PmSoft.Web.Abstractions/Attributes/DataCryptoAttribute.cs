﻿namespace PmSoft.Web.Abstractions;

public class DataCryptoAttribute : Attribute
{
    public bool Enable { get; }

    public DataCryptoAttribute(bool enable)
    {
        Enable =
#if DEBUG //调试下不开启加密
            false
#else
            enable
#endif
            ;
    }
}