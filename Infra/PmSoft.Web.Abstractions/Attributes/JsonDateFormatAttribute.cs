﻿using System.ComponentModel.DataAnnotations;

namespace PmSoft.Web.Abstractions;

[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
public class JsonDateFormatAttribute : ValidationAttribute
{
    private readonly string _format;

    public JsonDateFormatAttribute(string format)
    {
        _format = format;
    }

    public string Format => _format;
}