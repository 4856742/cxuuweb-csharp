﻿namespace PmSoft.Web.Abstractions.Captcha;

/// <summary>
/// 验证码配置选项类
/// </summary>
public class CaptchaOptions
{
	/// <summary>
	/// 验证码加密密钥
	/// </summary>
	public string SeKey { get; set; } = "PmSoft";

	/// <summary>
	/// 验证码字符集合
	/// </summary>
	public string CodeSet { get; set; } = "2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY";

	/// <summary>
	/// 验证码过期时间（秒）
	/// </summary>
	public int ExpireSeconds { get; set; } = 600;

	/// <summary>
	/// 是否使用中文验证码
	/// </summary>
	public bool UseZh { get; set; } = false;

	/// <summary>
	/// 验证码字体大小（像素）
	/// </summary>
	public int FontSize { get; set; } = 25;

	/// <summary>
	/// 是否绘制干扰曲线
	/// </summary>
	public bool UseCurve { get; set; } = true;

	/// <summary>
	/// 是否添加干扰点
	/// </summary>
	public bool UseNoise { get; set; } = true;
	/// <summary>
	/// 是否使用波浪线
	/// </summary>
	public bool UseWavy { get; set; } = true;
	/// <summary>
	/// 是否使用渐变
	/// </summary>
	public bool UseGradient { get; set; } = true;

	/// <summary>
	/// 验证码图片高度，0表示自动计算
	/// </summary>
	public int ImageHeight { get; set; } = 60;

	/// <summary>
	/// 验证码图片宽度，0表示自动计算
	/// </summary>
	public int ImageWidth { get; set; } = 150;

	/// <summary>
	/// 验证码字符长度
	/// </summary>
	public int Length { get; set; } = 4;

	/// <summary>
	/// 背景颜色RGB值数组
	/// </summary>
	public int[] BackgroundColor { get; set; } = new[] { 243, 251, 254 };

	/// <summary>
	/// 验证成功后是否重置验证码
	/// </summary>
	public bool Reset { get; set; } = true;
}
