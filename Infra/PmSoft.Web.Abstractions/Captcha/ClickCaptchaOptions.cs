﻿namespace PmSoft.Web.Abstractions.Captcha;

/// <summary>
/// 点击验证码的配置选项
/// </summary>
public class ClickCaptchaOptions
{
	/// <summary>
	/// 验证码过期时间（单位：秒，默认 600 秒 = 10 分钟）
	/// </summary>
	public int Expire { get; set; } = 600;

	/// <summary>
	/// 背景图片目录
	/// </summary>
	public string BackgroundDir { get; set; }

	/// <summary>
	/// 图标图片目录
	/// </summary>
	public string IconDir { get; set; }

	/// <summary>
	/// 字体文件目录
	/// </summary>
	public string FontDir { get; set; }

	/// <summary>
	/// 透明度（0-100）
	/// </summary>
	public int Alpha { get; set; } = 36;

	/// <summary>
	/// 中文字符集
	/// </summary>
	public string ZhSet { get; set; }

	/// <summary>
	/// 验证点数量
	/// </summary>
	public int Length { get; set; } = 4;

	/// <summary>
	/// 混淆点数量
	/// </summary>
	public int ConfuseLength { get; set; } = 2;

	/// <summary>
	/// 支持的模式（text 和/或 icon）
	/// </summary>
	public string[] Mode { get; set; } = new[] { "text", "icon" };
}
