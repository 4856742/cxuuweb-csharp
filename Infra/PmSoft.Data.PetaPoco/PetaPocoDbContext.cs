﻿using PetaPoco;
using PmSoft.Data.Abstractions;

namespace PmSoft.Data.PetaPoco;

public abstract class PetaPocoDbContext : Database, IDbContext
{
	public PetaPocoDbContext(string connectionString, string providerName)
		: base(connectionString, providerName, new PetaPocoEntityMapper())
	{

	}
}
