﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PmSoft.Data.Abstractions;
using PmSoft.Data.PetaPoco.Repositories;

namespace PmSoft.Data.PetaPoco;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddPetaPoco(this IServiceCollection services)
	{
		services.TryAddScoped(typeof(PetaPocoUnitOfWork<>));
		services.TryAddScoped(typeof(ICacheRepository<,,>), typeof(CacheRepository<,,>));
		services.TryAddScoped(typeof(ICachedEntityLoader<>), typeof(CachedEntityLoader<>));
		return services;
	}
}
