﻿namespace PmSoft.Data.Abstractions;

/// <summary>
/// 定义支持缓存的仓储接口，用于结合数据库和缓存高效获取实体数据。
/// </summary>
/// <typeparam name="TDbContext">数据库上下文类型，需实现 <see cref="IDbContext"/>。</typeparam>
/// <typeparam name="TEntity">实体类型。</typeparam>
/// <typeparam name="TKey">实体主键类型。</typeparam>
/// <remarks>
/// 此接口提供同步和异步方法，优先从缓存读取数据，若缓存未命中则从数据库加载并填充缓存。
/// </remarks>
public interface ICacheRepository<TDbContext, TEntity, TKey>
	where TDbContext : IDbContext
{
	/// <summary>
	/// 根据主键同步获取单个实体（优先从缓存读取）。
	/// </summary>
	/// <param name="entityId">实体的主键值。</param>
	/// <returns>
	/// 若找到则返回实体对象
	/// </returns>
	TEntity? Get(TKey entityId);

	/// <summary>
	/// 根据主键集合同步获取多个实体（优先从缓存读取）。
	/// </summary>
	/// <param name="entityIds">主键集合。</param>
	/// <returns>
	/// 匹配的实体集合
	/// </returns>
	IEnumerable<TEntity> GetEntitiesByIds(IEnumerable<TKey> entityIds);

	/// <summary>
	/// 根据主键异步获取单个实体（优先从缓存读取）。
	/// </summary>
	/// <param name="entityId">实体的主键值。</param>
	/// <returns>
	/// 表示异步操作的任务，结果为找到的实体或 <see langword="null"/>。
	/// </returns>
	Task<TEntity?> GetAsync(TKey entityId);

	/// <summary>
	/// 根据主键集合异步获取多个实体（优先从缓存读取）。
	/// </summary>
	/// <param name="entityIds">主键集合。</param>
	/// <returns>
	/// 表示异步操作的任务，结果为匹配的实体集合。
	/// </returns>
	Task<IEnumerable<TEntity>> GetEntitiesByIdsAsync(IEnumerable<TKey> entityIds);
}