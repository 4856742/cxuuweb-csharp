﻿using PmSoft.Core.Domain.Entities;

namespace PmSoft.Data.Abstractions;


/// <summary>
/// 实体数据加载器
/// </summary>
public interface ICachedEntityLoader<TDbContext> : ICachedEntityLoader { }

/// <summary>
/// 实体数据加载器
/// </summary>
public interface ICachedEntityLoader
{
	/// <summary>
	/// 根据 ID 集合批量获取实体
	/// </summary>
	/// <typeparam name="TEntity">实体类型</typeparam>
	/// <typeparam name="TKey">实体主键类型</typeparam>
	/// <param name="entityIds">实体 ID 集合</param>
	/// <returns>匹配的实体集合</returns>
	Task<IEnumerable<TEntity>> GetEntitiesByIdsAsync<TEntity, TKey>(IEnumerable<TKey> entityIds)
		where TEntity : class, IEntity<TKey>, new()
		where TKey : notnull;
	/// <summary>
	/// 根据提供的键值从仓储中获取关联实体集合
	/// </summary>
	/// <typeparam name="TSource"></typeparam>
	/// <typeparam name="TAssociatedEntity"></typeparam>
	/// <typeparam name="TAssociatedKey"></typeparam>
	/// <param name="source"></param>
	/// <param name="foreignKeySelector"></param>
	/// <returns></returns>
	Task<IEnumerable<TAssociatedEntity>> LoadAssociatedEntitiesAsync<TSource, TAssociatedEntity, TAssociatedKey>(
		IEnumerable<TSource> source,
		Func<TSource, TAssociatedKey> foreignKeySelector)
		where TAssociatedEntity : class, IEntity<TAssociatedKey>, new()
		where TAssociatedKey : notnull;

	/// <summary>
	/// 根据主键加载关联实体
	/// </summary>
	/// <typeparam name="TEntity">关联实体类型</typeparam>
	/// <typeparam name="TKey">关联实体主键类型</typeparam>
	/// <param name="entityKey">关联实体的主键值</param>
	/// <returns>映射后的关联实体</returns>
	Task<TEntity?> GetEntityAsync<TEntity, TKey>(
	   TKey entityKey)
	   where TEntity : class, IEntity<TKey>, new()
	   where TKey : notnull;

}
