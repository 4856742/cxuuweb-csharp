﻿using PmSoft.Core;

namespace PmSoft.Data.Abstractions;

public class PagedList<T> : IPagedList<T>
{
	public PagedList(IEnumerable<T> datas)
	{
		Items = datas;
	}

	public PagedList(int total, int pageSize, IEnumerable<T> datas) : this(datas)
	{
		Total = total;
		PageSize = pageSize;
	}
	/// <summary>
	/// 数据集合
	/// </summary>
	public IEnumerable<T> Items { get; private set; }
	/// <summary>
	/// 总条目数
	/// </summary>
	public int? Total { get; set; }
	/// <summary>
	/// 每页数量
	/// </summary>
	public int? PageSize { get; set; }
	/// <summary>
	/// 当前页码
	/// </summary>
	public int PageIndex { get; set; }
	/// <summary>
	/// 查询耗时(毫秒数)
	/// </summary>
	public double QueryDuration { get; set; }
}
