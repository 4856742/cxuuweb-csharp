﻿namespace PmSoft.Data.Abstractions;

public interface IPagedList<T>
{
	/// <summary>
	/// 数据集合
	/// </summary>
	IEnumerable<T> Items { get; }
	/// <summary>
	/// 总条目数
	/// </summary>
	int? Total { get; }
	/// <summary>
	/// 每页数量
	/// </summary>
	int? PageSize { get; }
	/// <summary>
	/// 当前页码
	/// </summary>
	int PageIndex { get; }
	/// <summary>
	/// 查询耗时(毫秒数)
	/// </summary>
	double QueryDuration { get;  }
}
