﻿using PmSoft.Core;
using PmSoft.Core.Domain.Entities;
using PmSoft.Core.EventBus;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace PmSoft.Data.Abstractions.Events;

/// <summary>
/// 表示与实体相关的基本事件参数，包含实体的标识、表名和描述信息。
/// </summary>
public class EntityEventArgs : UserEventArgs
{
	public EntityEventArgs(IApplicationContext applicationContext, IEntity entity) : base(applicationContext)
	{
		if (entity == null)
			throw new ArgumentNullException(nameof(entity));
		EntityId = entity.Id;
		Type type = entity.GetType();
		var metadata = EntityMetadataCache.GetMetadata(type);
		TableName = metadata.TableAttribute?.Name ?? type.Name;
		EntityDescription = metadata.DescriptionAttribute?.Description ?? type.Name;
	}
	/// <summary>
	/// 获取实体的唯一标识符。
	/// </summary>
	public object EntityId { get; }

	/// <summary>
	/// 获取或设置实体对应的数据库表名。
	/// 如果实体类标记了 <see cref="TableAttribute"/>，则使用其值；否则使用实体类型的名称。
	/// </summary>
	public string TableName { get; set; }

	/// <summary>
	/// 获取或设置实体的描述信息。
	/// 如果实体类标记了 <see cref="DescriptionAttribute"/>，则使用其值；否则使用实体类型的名称。
	/// </summary>
	public string EntityDescription { get; set; }
}
