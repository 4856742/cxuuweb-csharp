﻿namespace PmSoft.Data.Abstractions.Events;

/// <summary>
/// 实体属性变更记录
/// </summary>
public class EntityPropertyChangeInfo
{
	/// <summary>
	/// 属性名称
	/// </summary>
	public string PropertyName { get; set; }

	/// <summary>
	/// 旧值
	/// </summary>
	public object? OldValue { get; set; }

	/// <summary>
	/// 新值
	/// </summary>
	public object? NewValue { get; set; }

	public EntityPropertyChangeInfo(string propertyName, object? oldValue, object? newValue)
	{
		PropertyName = propertyName;
		OldValue = oldValue;
		NewValue = newValue;
	}
}
