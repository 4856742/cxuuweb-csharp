﻿using PmSoft.Core;
using PmSoft.Core.Domain.Entities;
using PmSoft.Data.Abstractions.Attributes;

namespace PmSoft.Data.Abstractions.Events;

/// <summary>
/// 实体添加事件参数
/// </summary>
public class EntityAddedEventArgs : EntityEventArgs
{
	public EntityAddedEventArgs(IApplicationContext applicationContext, IEntity entity, AuditableAttribute auditableAttribute)
		: base(applicationContext, entity)
	{
		Entity = entity ?? throw new ArgumentNullException(nameof(entity));
		AuditableAttribute = auditableAttribute ?? throw new ArgumentNullException(nameof(auditableAttribute));
	}
	/// <summary>
	/// 添加的实体
	/// </summary>
	public IEntity Entity { get; }
	/// <summary>
	/// 审计属性
	/// </summary>
	public AuditableAttribute AuditableAttribute { get; set; }


}
