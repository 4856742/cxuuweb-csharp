﻿using PmSoft.Data.Abstractions.Attributes;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace PmSoft.Data.Abstractions;

/// <summary>
/// 实体元数据缓存，提供类型相关的特性信息以优化反射性能。
/// </summary>
public static class EntityMetadataCache
{
	private static readonly ConcurrentDictionary<Type, EntityMetadata> _metadataCache = new();

	/// <summary>
	/// 获取指定类型的元数据，包括审计特性、表名和描述。
	/// </summary>
	/// <param name="entityType">实体类型。</param>
	/// <returns>实体元数据。</returns>
	public static EntityMetadata GetMetadata(Type entityType)
	{
		return _metadataCache.GetOrAdd(entityType, type => new EntityMetadata
		{
			AuditableAttribute = type.GetCustomAttribute<AuditableAttribute>(false),
			TableAttribute = type.GetCustomAttribute<TableAttribute>(false),
			DescriptionAttribute = type.GetCustomAttribute<DescriptionAttribute>(false)
		});
	}
}

/// <summary>
/// 实体元数据，包含审计特性、表名和描述信息。
/// </summary>
public class EntityMetadata
{
	public AuditableAttribute? AuditableAttribute { get; init; }
	public TableAttribute? TableAttribute { get; init; }
	public DescriptionAttribute? DescriptionAttribute { get; init; }
}
