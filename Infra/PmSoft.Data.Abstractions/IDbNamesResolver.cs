﻿using PmSoft.Core.Extensions;

namespace PmSoft.Data.Abstractions;

/// <summary>
/// 数据库名/字段命名规则解析服务
/// </summary>
public interface IDbNamesResolver
{
    string ResolveName(string name);
}

/// <summary>
/// 写成什么样就用什么样
/// </summary>
public class DefaultNamesResolver : IDbNamesResolver
{
    public string ResolveName(string name) => name;
}

/// <summary>
/// 驼峰命名
/// </summary>
public class CamelNamesResolver : IDbNamesResolver
{
    public string ResolveName(string name)
    {
        return name.ToCamelCase();
    }
}

/// <summary>
/// Pascal命名
/// </summary>
public class PascalNamesResolver : IDbNamesResolver
{
    public string ResolveName(string name)
    {
        return name.ToPascalCase();
    }
}

/// <summary>
/// Snake命名
/// </summary>
public class SnakeNamesResolver : IDbNamesResolver
{
    public string ResolveName(string name)
    {
        return name.ToSnakeCase();
    }
}