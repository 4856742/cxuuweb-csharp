﻿namespace PmSoft.Data.Abstractions.Attributes;

/// <summary>
/// 表示需要记录审计日志的属性标记。
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public class AuditableAttribute : Attribute
{
	public AuditableAttribute() { }

	public AuditableAttribute(object groupName)
	{
		GroupName = groupName;
	}
	/// <summary>
	/// 审计日志分组名称
	/// </summary>
	public object? GroupName { get; set; }
}
