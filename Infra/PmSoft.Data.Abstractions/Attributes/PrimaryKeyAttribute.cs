﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PmSoft.Data.Abstractions.Attributes;

/// <summary>
/// 主键特性，用于标记实体类中的主键属性。
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class PrimaryKeyAttribute : ColumnAttribute
{
	/// <summary>
	/// 获取或设置一个值，指示该主键是否为自增列。
	/// </summary>
	public bool IsIdentity { get; set; } = true;

	/// <summary>
	/// 初始化 <see cref="PrimaryKeyAttribute"/> 类的新实例。
	/// </summary>
	public PrimaryKeyAttribute() { }

	/// <summary>
	/// 使用指定的列名初始化 <see cref="PrimaryKeyAttribute"/> 类的新实例。
	/// </summary>
	/// <param name="name">主键列的名称。</param>
	public PrimaryKeyAttribute(string name) : base(name) { }
}