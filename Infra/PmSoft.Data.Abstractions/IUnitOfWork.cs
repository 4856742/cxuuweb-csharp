﻿using System.Data;

namespace PmSoft.Data.Abstractions;

/// <summary>
/// 定义工作单元接口，用于统一管理数据库事务的生命周期。
/// </summary>
/// <remarks>
/// 此接口提供同步和异步方法，支持事务的开启、提交、回滚及资源释放，
/// 适用于需要跨多个数据库操作的事务控制场景。
/// </remarks>
public interface IUnitOfWork : IDisposable
{
	/// <summary>
	/// 同步开启数据库事务。
	/// </summary>
	/// <param name="isolationLevel">事务隔离级别（默认: <see cref="IsolationLevel.ReadCommitted"/>）。</param>
	/// <exception cref="InvalidOperationException">当重复开启事务时可能抛出。</exception>
	void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

	/// <summary>
	/// 异步开启数据库事务。
	/// </summary>
	/// <param name="isolationLevel">事务隔离级别（默认: <see cref="IsolationLevel.ReadCommitted"/>）。</param>
	/// <param name="cancellationToken">取消令牌，用于取消异步操作。</param>
	/// <exception cref="InvalidOperationException">当重复开启事务时可能抛出。</exception>
	Task BeginTransactionAsync(
		IsolationLevel isolationLevel = IsolationLevel.ReadCommitted,
		CancellationToken cancellationToken = default
	);

	/// <summary>
	/// 同步提交当前事务。
	/// </summary>
	/// <exception cref="InvalidOperationException">当未开启事务时调用此方法可能抛出。</exception>
	/// <remarks>
	/// 提交后事务将不可回滚。若提交过程中发生异常，事务会自动回滚。
	/// </remarks>
	void CommitTransaction();

	/// <summary>
	/// 异步提交当前事务。
	/// </summary>
	/// <param name="cancellationToken">取消令牌，用于取消异步操作。</param>
	/// <exception cref="InvalidOperationException">当未开启事务时调用此方法可能抛出。</exception>
	/// <remarks>
	/// 提交后事务将不可回滚。若提交过程中发生异常，事务会自动回滚。
	/// </remarks>
	Task CommitTransactionAsync(CancellationToken cancellationToken = default);

	/// <summary>
	/// 同步回滚当前事务。
	/// </summary>
	/// <remarks>
	/// 若事务已提交或未开启，调用此方法无任何效果。
	/// </remarks>
	void RollbackTransaction();

	/// <summary>
	/// 异步回滚当前事务。
	/// </summary>
	/// <param name="cancellationToken">取消令牌，用于取消异步操作。</param>
	/// <remarks>
	/// 若事务已提交或未开启，调用此方法无任何效果。
	/// </remarks>
	Task RollbackTransactionAsync(CancellationToken cancellationToken = default);
}