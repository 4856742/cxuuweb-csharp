﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PmSoft.Cache.Abstractions.EntityCache;

namespace PmSoft.Cache.Abstractions;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddDefaultCache(this IServiceCollection services, IConfiguration configuration)
	{
		if (services == null) throw new ArgumentNullException(nameof(services));

		services.Configure<CacheServiceOptions>(configuration.GetSection("CacheService"));
 
		services.AddMemoryCache();
		services.TryAddSingleton<ICacheService, DefaultCacheService>();
		services.TryAddSingleton(typeof(IEntityCacheVersionService<,>), typeof(MemoryEntityCacheVersionService<,>));
		return services;
	}


}