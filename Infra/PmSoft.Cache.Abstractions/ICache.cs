﻿namespace PmSoft.Cache.Abstractions;

/// <summary>
/// 通用缓存接口，定义实体缓存的基本操作
/// </summary>
public interface ICache
{
	/// <summary>
	/// 从缓存获取实体（同步）
	/// </summary>
	/// <typeparam name="T">实体类型</typeparam>
	/// <param name="cacheKey">缓存键</param>
	/// <returns>缓存的实体，若不存在则返回 null</returns>
	T? Get<T>(string cacheKey);

	/// <summary>
	/// 从缓存获取实体（异步）
	/// </summary>
	/// <typeparam name="T">实体类型</typeparam>
	/// <param name="cacheKey">缓存键</param>
	/// <returns>缓存的实体，若不存在则返回 null</returns>
	Task<T?> GetAsync<T>(string cacheKey);

	/// <summary>
	/// 移除缓存中的实体（同步）
	/// </summary>
	/// <param name="cacheKey">缓存键</param>
	void Remove(string cacheKey);

	/// <summary>
	/// 移除缓存中的实体（异步）
	/// </summary>
	/// <param name="cacheKey">缓存键</param>
	/// <returns>异步任务</returns>
	Task RemoveAsync(string cacheKey);

	/// <summary>
	/// 设置缓存中的实体（同步）
	/// </summary>
	/// <typeparam name="T">实体类型</typeparam>
	/// <param name="key">缓存键</param>
	/// <param name="value">实体值</param>
	/// <param name="timeSpan">过期时间</param>
	void Set<T>(string key, T value, TimeSpan timeSpan);

	/// <summary>
	/// 设置缓存中的实体（异步）
	/// </summary>
	/// <typeparam name="T">实体类型</typeparam>
	/// <param name="key">缓存键</param>
	/// <param name="value">实体值</param>
	/// <param name="timeSpan">过期时间</param>
	/// <returns>异步任务</returns>
	Task SetAsync<T>(string key, T value, TimeSpan timeSpan);
}