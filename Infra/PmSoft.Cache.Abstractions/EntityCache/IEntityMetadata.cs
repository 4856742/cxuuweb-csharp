﻿namespace PmSoft.Cache.Abstractions.EntityCache;

/// <summary>
/// 实体元数据接口，定义实体缓存相关的属性
/// </summary>
public interface IEntityMetadata
{
	/// <summary>
	/// 实体类型
	/// </summary>
	Type Type { get; }

	/// <summary>
	/// 实体名称，可由特性指定，默认使用类型名
	/// </summary>
	string Name { get; }

	/// <summary>
	/// 是否启用缓存
	/// </summary>
	bool EnableCache { get; }

	/// <summary>
	/// 是否启用分布式缓存
	/// </summary>
	bool EnableDistributedCache { get; }

	/// <summary>
	/// 缓存过期类型，决定缓存的过期策略
	/// </summary>
	CachingExpirationType CachingExpirationType { get; }

	/// <summary>
	/// 分区属性列表，用于区域版本控制
	/// </summary>
	List<EntityPropertyInfo>? PropertiesOfArea { get; }
}