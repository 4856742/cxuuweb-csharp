﻿using PmSoft.Core.Domain.Entities;

namespace PmSoft.Cache.Abstractions.EntityCache;

/// <summary>
/// 分布式实体缓存版本管理服务接口
/// </summary>
public interface IDistributedEntityCacheVersionService<TEntity, TKey> 
	: IEntityCacheVersionService<TEntity, TKey>
	where TEntity : IEntity<TKey> where TKey : notnull
{
}
