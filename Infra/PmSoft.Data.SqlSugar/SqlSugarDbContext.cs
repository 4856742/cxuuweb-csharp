﻿using PmSoft.Data.Abstractions;
using SqlSugar;

namespace PmSoft.Data.SqlSugar;
public abstract class SqlSugarDbContext : IDbContext
{
	/// <summary>
	/// 构造函数
	/// </summary>
	/// <param name="configId"></param>
	/// <param name="unitOfWork"></param>
	public SqlSugarDbContext(object configId, SqlSugarUnitOfWork unitOfWork)
	{
		this.Provider = unitOfWork.GetConnection(configId);
	}

	public SqlSugarProvider Provider { get; private set; }

	#region IDispose
	public void Dispose()
	{
		this.Provider?.Dispose();
	}
	#endregion
}
