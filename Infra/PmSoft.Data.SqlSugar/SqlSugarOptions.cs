﻿using SqlSugar;

namespace PmSoft.Data.SqlSugar;

public class SqlSugarOptions
{
	public SqlSugarOptions()
	{
		Connections = new();
	}
	public List<SqlSugarConnection> Connections { get; set; }
}
public record SqlSugarConnection(string ConnectionString, DbType DbType, bool IsAutoCloseConnection, object? ConfigId = null, List<SlaveConnection>? Slaves = null);

public record SlaveConnection(string ConnectionString);