﻿using Microsoft.Extensions.DependencyInjection;
using PmSoft.Core;
using System.Diagnostics;
using PmSoft.Cache.Abstractions;
using PmSoft.Cache.Abstractions.EntityCache;
using PmSoft.Core.Domain.Entities;
using PmSoft.Data.Abstractions;
using SqlSugar;
using ICacheService = PmSoft.Cache.Abstractions.ICacheService;

namespace PmSoft.Data.SqlSugar.Repositories;

/// <summary>
/// 带缓存的实体仓储类，基于 SqlSugar 提供 CRUD 和查询操作，支持同步和异步方法
/// </summary>
/// <typeparam name="TDbContext">SqlSugar 数据库上下文类型</typeparam>
/// <typeparam name="TEntity">实体类型，必须实现 IEntity<TKey></typeparam>
/// <typeparam name="TKey">主键类型，不可为空</typeparam>
public class CacheRepository<TDbContext, TEntity, TKey> : Repository<TEntity, TKey>, ICacheRepository<TDbContext, TEntity, TKey>
	where TDbContext : SqlSugarDbContext
	where TEntity : class, IEntity<TKey>, new()
	where TKey : notnull
{
	/// <summary>
	/// 构造函数，初始化仓储和缓存服务
	/// </summary>
	/// <param name="dbContext">SqlSugar 数据库上下文</param>
	/// <param name="applicationContext">应用程序上下文</param>
	public CacheRepository(TDbContext dbContext, IApplicationContext applicationContext)
		: base(dbContext, applicationContext)
	{
	}

	/// <summary>
	/// 缓存服务
	/// </summary>
	protected ICacheService CacheService => ApplicationContext.ServiceProvider.GetRequiredService<ICacheService>();

	/// <summary>
	/// 实体缓存版本服务，根据分布式缓存配置选择对应实现
	/// </summary>
	protected IEntityCacheVersionService<TEntity, TKey> CacheVersionService
	{
		get
		{
			if (CacheService.EnableDistributedCache)
				return ApplicationContext.GetRequiredService<IDistributedEntityCacheVersionService<TEntity, TKey>>();
			return ApplicationContext.GetRequiredService<IEntityCacheVersionService<TEntity, TKey>>();
		}
	}

	#region Insert

	/// <summary>
	/// 插入实体到数据库（同步）
	/// </summary>
	/// <param name="entity">要插入的实体</param>
	/// <returns>受影响的行数</returns>
	public override int Insert(TEntity entity)
	{
		int result = base.Insert(entity);
		if (result > 0) OnInserted(entity);
		return result;
	}

	/// <summary>
	/// 插入实体后触发（同步），更新缓存
	/// </summary>
	/// <param name="entity">已插入的实体</param>
	protected virtual void OnInserted(TEntity entity)
	{
		if (CacheVersionService.Metadata.EnableCache)
		{
			CacheVersionService.IncreaseListCacheVersion(entity);
			CacheService.Set(CacheVersionService.GetCacheKeyOfEntity(entity.Id), entity, CacheVersionService.Metadata.CachingExpirationType);
		}
	}

	/// <summary>
	/// 插入实体到数据库（异步）
	/// </summary>
	/// <param name="entity">要插入的实体</param>
	/// <returns>受影响的行数</returns>
	public override async Task<int> InsertAsync(TEntity entity)
	{
		int result = await base.InsertAsync(entity);
		if (result > 0) await OnInsertedAsync(entity);
		return result;
	}

	/// <summary>
	/// 插入实体后触发（异步），更新缓存
	/// </summary>
	/// <param name="entity">已插入的实体</param>
	protected virtual async Task OnInsertedAsync(TEntity entity)
	{
		if (CacheVersionService.Metadata.EnableCache)
		{
			await CacheVersionService.IncreaseListCacheVersionAsync(entity);
			await CacheService.SetAsync(CacheVersionService.GetCacheKeyOfEntity(entity.Id), entity, CacheVersionService.Metadata.CachingExpirationType);
		}
	}

	#endregion

	#region Update

	/// <summary>
	/// 更新数据库中的实体（同步）
	/// </summary>
	/// <param name="entity">要更新的实体</param>
	/// <returns>受影响的行数</returns>
	public override int Update(TEntity entity)
	{
		int result = base.Update(entity);
		if (result > 0) OnUpdated(entity);
		return result;
	}

	/// <summary>
	/// 更新实体后触发（同步），更新缓存
	/// </summary>
	/// <param name="entity">已更新的实体</param>
	protected virtual void OnUpdated(TEntity entity)
	{
		if (CacheVersionService.Metadata.EnableCache)
		{
			CacheVersionService.IncreaseEntityCacheVersion(entity.Id);
			CacheVersionService.IncreaseListCacheVersion(entity);
			CacheService.Set(CacheVersionService.GetCacheKeyOfEntity(entity.Id), entity, CacheVersionService.Metadata.CachingExpirationType);
		}
	}

	/// <summary>
	/// 更新数据库中的实体（异步）
	/// </summary>
	/// <param name="entity">要更新的实体</param>
	/// <returns>受影响的行数</returns>
	public override async Task<int> UpdateAsync(TEntity entity)
	{
		int result = await base.UpdateAsync(entity);
		if (result > 0) await OnUpdatedAsync(entity);
		return result;
	}

	/// <summary>
	/// 更新实体后触发（异步），更新缓存
	/// </summary>
	/// <param name="entity">已更新的实体</param>
	protected virtual async Task OnUpdatedAsync(TEntity entity)
	{
		if (CacheVersionService.Metadata.EnableCache)
		{
			await CacheVersionService.IncreaseEntityCacheVersionAsync(entity.Id);
			await CacheVersionService.IncreaseListCacheVersionAsync(entity);
			await CacheService.SetAsync(CacheVersionService.GetCacheKeyOfEntity(entity.Id), entity, CacheVersionService.Metadata.CachingExpirationType);
		}
	}

	#endregion

	#region Delete

	/// <summary>
	/// 删除数据库中的实体（同步），支持逻辑删除
	/// </summary>
	/// <param name="entity">要删除的实体</param>
	/// <returns>受影响的行数</returns>
	public override int Delete(TEntity entity)
	{
		if (entity == null) return 0;
		int result = entity is IDelEntity<TKey> delEntity
			? PerformLogicalDelete(delEntity)
			: base.Delete(entity);
		if (result > 0) OnDeleted(entity);
		return result;
	}

	/// <summary>
	/// 执行逻辑删除（同步）
	/// </summary>
	/// <param name="delEntity">支持逻辑删除的实体</param>
	/// <returns>受影响的行数</returns>
	private int PerformLogicalDelete(IDelEntity<TKey> delEntity)
	{
		delEntity.IsDeleted = true;
		delEntity.DeleteToken = delEntity.Id;
		return base.Update((TEntity)delEntity);
	}

	/// <summary>
	/// 删除实体后触发（同步），更新缓存
	/// </summary>
	/// <param name="entity">已删除的实体</param>
	protected virtual void OnDeleted(TEntity entity)
	{
		if (CacheVersionService.Metadata.EnableCache)
		{
			CacheVersionService.IncreaseEntityCacheVersion(entity.Id);
			CacheVersionService.IncreaseListCacheVersion(entity);
			CacheService.Remove(CacheVersionService.GetCacheKeyOfEntity(entity.Id));
		}
	}

	/// <summary>
	/// 根据实体 ID 删除（同步）
	/// </summary>
	/// <param name="entityId">实体 ID</param>
	/// <returns>受影响的行数</returns>
	public virtual int DeleteByEntityId(TKey entityId)
	{
		var entity = Get(entityId);
		return entity == null ? 0 : Delete(entity);
	}

	/// <summary>
	/// 删除数据库中的实体（异步），支持逻辑删除
	/// </summary>
	/// <param name="entity">要删除的实体</param>
	/// <returns>受影响的行数</returns>
	public override async Task<int> DeleteAsync(TEntity entity)
	{
		if (entity == null) return 0;
		int result = entity is IDelEntity<TKey> delEntity
			? await PerformLogicalDeleteAsync(delEntity)
			: await base.DeleteAsync(entity);
		if (result > 0) await OnDeletedAsync(entity);
		return result;
	}

	/// <summary>
	/// 执行逻辑删除（异步）
	/// </summary>
	/// <param name="delEntity">支持逻辑删除的实体</param>
	/// <returns>受影响的行数</returns>
	private async Task<int> PerformLogicalDeleteAsync(IDelEntity<TKey> delEntity)
	{
		delEntity.IsDeleted = true;
		delEntity.DeleteToken = delEntity.Id;
		return await base.UpdateAsync((TEntity)delEntity);
	}

	/// <summary>
	/// 删除实体后触发（异步），更新缓存
	/// </summary>
	/// <param name="entity">已删除的实体</param>
	protected virtual async Task OnDeletedAsync(TEntity entity)
	{
		if (CacheVersionService.Metadata.EnableCache)
		{
			await CacheVersionService.IncreaseEntityCacheVersionAsync(entity.Id);
			await CacheVersionService.IncreaseListCacheVersionAsync(entity);
			await CacheService.RemoveAsync(CacheVersionService.GetCacheKeyOfEntity(entity.Id));
		}
	}

	/// <summary>
	/// 根据实体 ID 删除（异步）
	/// </summary>
	/// <param name="entityId">实体 ID</param>
	/// <returns>受影响的行数</returns>
	public virtual async Task<int> DeleteByEntityIdAsync(TKey entityId)
	{
		var entity = await GetAsync(entityId);
		return entity == null ? 0 : await DeleteAsync(entity);
	}

	#endregion

	#region Query

	/// <summary>
	/// 检查实体是否存在（同步）
	/// </summary>
	/// <param name="entityId">实体 ID</param>
	/// <returns>是否存在</returns>
	public bool Exists(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		return base.Exists(m => m.Id.Equals(entityId));
	}

	/// <summary>
	/// 检查实体是否存在（异步）
	/// </summary>
	/// <param name="entityId">实体 ID</param>
	/// <returns>是否存在</returns>
	public async Task<bool> ExistsAsync(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		return await base.ExistsAsync(m => m.Id.Equals(entityId));
	}

	/// <summary>
	/// 获取单个实体（同步），支持缓存
	/// </summary>
	/// <param name="entityId">实体 ID</param>
	/// <returns>实体对象，若不存在或已删除则返回 null</returns>
	public override TEntity? Get(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		TEntity? entity = CacheVersionService.Metadata.EnableCache
			? CacheService.Get<TEntity>(CacheVersionService.GetCacheKeyOfEntity(entityId))
			: null;

		if (entity == null)
		{
			entity = base.Get(entityId);
			if (entity != null && CacheVersionService.Metadata.EnableCache)
			{
				CacheService.Set(CacheVersionService.GetCacheKeyOfEntity(entity.Id), entity, CacheVersionService.Metadata.CachingExpirationType);
			}
		}

		return FilterDeleted(entity);
	}

	/// <summary>
	/// 获取单个实体（异步），支持缓存
	/// </summary>
	/// <param name="entityId">实体 ID</param>
	/// <returns>实体对象，若不存在或已删除则返回 null</returns>
	public override async Task<TEntity?> GetAsync(TKey entityId)
	{
		if (entityId == null) throw new ArgumentNullException(nameof(entityId));
		TEntity? entity = CacheVersionService.Metadata.EnableCache
			? await CacheService.GetAsync<TEntity>(await CacheVersionService.GetCacheKeyOfEntityAsync(entityId))
			: null;

		if (entity == null)
		{
			entity = await base.GetAsync(entityId);
			if (entity != null && CacheVersionService.Metadata.EnableCache)
			{
				string cacheKey = await CacheVersionService.GetCacheKeyOfEntityAsync(entity.Id);
				await CacheService.SetAsync(cacheKey, entity, CacheVersionService.Metadata.CachingExpirationType);
			}
		}

		return FilterDeleted(entity);
	}

	/// <summary>
	/// 过滤已删除的实体
	/// </summary>
	/// <param name="entity">待检查的实体</param>
	/// <returns>未删除的实体或 null</returns>
	private TEntity? FilterDeleted(TEntity? entity)
	{
		if (entity == null) return null;
		return entity is IDelEntity<TKey> delEntity && delEntity.IsDeleted ? null : entity;
	}

	/// <summary>
	/// 获取前 topCount 条实体（同步，带缓存）
	/// </summary>
	/// <param name="topCount">获取的实体数量</param>
	/// <param name="cacheExpiration">缓存过期类型</param>
	/// <param name="generateCacheKey">生成缓存键的函数</param>
	/// <param name="applyQuery">应用查询的函数</param>
	/// <returns>实体集合</returns>
	public virtual IEnumerable<TEntity> GetTopEntitiesWithCache(
		int topCount,
		CachingExpirationType cacheExpiration,
		Func<string> generateCacheKey,
		Func<ISugarQueryable<TEntity>, ISugarQueryable<TEntity>> applyQuery)
	{
		string cacheKey = $"AreaCollection:{generateCacheKey()}";
		var cachedEntityIds = CacheService.Get<PagedEntityIdsCollection<TKey>>(cacheKey)
			?? CacheTopEntities(applyQuery, cacheExpiration, cacheKey);
		return GetEntitiesByIds(cachedEntityIds.GetTopIds(topCount));
	}

	/// <summary>
	/// 缓存前 topCount 条实体的 ID（同步）
	/// </summary>
	private PagedEntityIdsCollection<TKey> CacheTopEntities(
		Func<ISugarQueryable<TEntity>, ISugarQueryable<TEntity>> applyQuery,
		CachingExpirationType cacheExpiration,
		string cacheKey)
	{
		var queryable = applyQuery(Query());
		var topEntityIds = queryable.Select(m => m.Id).ToArray();
		var cachedEntityIds = new PagedEntityIdsCollection<TKey>(topEntityIds);
		CacheService.Set(cacheKey, cachedEntityIds, cacheExpiration);
		return cachedEntityIds;
	}

	/// <summary>
	/// 获取前 topCount 条实体（异步，带缓存）
	/// </summary>
	/// <param name="topCount">获取的实体数量</param>
	/// <param name="cacheExpiration">缓存过期类型</param>
	/// <param name="generateCacheKey">生成缓存键的函数</param>
	/// <param name="applyQuery">应用查询的函数</param>
	/// <returns>实体集合</returns>
	public virtual async Task<IEnumerable<TEntity>> GetTopEntitiesWithCacheAsync(
		int topCount,
		CachingExpirationType cacheExpiration,
		Func<string> generateCacheKey,
		Func<ISugarQueryable<TEntity>, ISugarQueryable<TEntity>> applyQuery)
	{
		string cacheKey = $"AreaCollection:{generateCacheKey()}";
		var cachedEntityIds = await CacheService.GetAsync<PagedEntityIdsCollection<TKey>>(cacheKey)
			?? await CacheTopEntitiesAsync(applyQuery, cacheExpiration, cacheKey);
		return await GetEntitiesByIdsAsync(cachedEntityIds.GetTopIds(topCount));
	}

	/// <summary>
	/// 缓存前 topCount 条实体的 ID（异步）
	/// </summary>
	private async Task<PagedEntityIdsCollection<TKey>> CacheTopEntitiesAsync(
		Func<ISugarQueryable<TEntity>, ISugarQueryable<TEntity>> applyQuery,
		CachingExpirationType cacheExpiration,
		string cacheKey)
	{
		var queryable = applyQuery(Query());
		var topEntityIds = await queryable.Select(m => m.Id).ToArrayAsync();
		var cachedEntityIds = new PagedEntityIdsCollection<TKey>(topEntityIds);
		await CacheService.SetAsync(cacheKey, cachedEntityIds, cacheExpiration);
		return cachedEntityIds;
	}

	/// <summary>
	/// 根据实体 ID 集合获取实体（同步，自动缓存）
	/// </summary>
	/// <param name="entityIds">实体 ID 集合</param>
	/// <returns>实体集合</returns>
	public virtual IEnumerable<TEntity> GetEntitiesByIds(IEnumerable<TKey> entityIds)
	{
		var entityIdList = ValidateEntityIds(entityIds);
		if (!entityIdList.Any()) return Enumerable.Empty<TEntity>();

		var entities = FetchEntitiesWithCache(entityIdList);
		return FilterValidEntities(entities);
	}

	/// <summary>
	/// 根据实体 ID 集合获取实体（异步，自动缓存）
	/// </summary>
	/// <param name="entityIds">实体 ID 集合</param>
	/// <returns>实体集合</returns>
	public virtual async Task<IEnumerable<TEntity>> GetEntitiesByIdsAsync(IEnumerable<TKey> entityIds)
	{
		var entityIdList = ValidateEntityIds(entityIds);
		if (!entityIdList.Any()) return Enumerable.Empty<TEntity>();

		var entities = await FetchEntitiesWithCacheAsync(entityIdList);
		return FilterValidEntities(entities);
	}

	/// <summary>
	/// 验证实体 ID 集合
	/// </summary>
	private List<TKey> ValidateEntityIds(IEnumerable<TKey> entityIds)
	{
		if (entityIds == null) throw new ArgumentNullException(nameof(entityIds));
		var entityIdList = entityIds.Where(x => x != null).Distinct().ToList();
		if (entityIdList.Count > 2000) throw new ArgumentOutOfRangeException(nameof(entityIds), "实体 ID 集合不能超过 2000");
		return entityIdList;
	}

	/// <summary>
	/// 从缓存或数据库获取实体（同步）
	/// </summary>
	private TEntity?[] FetchEntitiesWithCache(List<TKey> entityIdList)
	{
		var entitiesInMemory = new TEntity?[entityIdList.Count];
		var missingEntityIds = new Dictionary<TKey, int>();
		var entityCacheKeys = CacheVersionService.GetCacheKeyOfEntitys(entityIdList);

		for (int i = 0; i < entityIdList.Count; i++)
		{
			TKey entityId = entityIdList[i];
			string cacheKey = entityCacheKeys[entityId.ToString() ?? throw new ArgumentNullException(nameof(entityId))];
			TEntity? cachedEntity = CacheService.Get<TEntity>(cacheKey);
			entitiesInMemory[i] = cachedEntity ?? null;
			if (cachedEntity == null) missingEntityIds[entityId] = i;
		}

		if (missingEntityIds.Any())
		{
			var entitiesFromDb = base.Query(x => missingEntityIds.Keys.Contains(x.Id)).ToArray();
			foreach (var entity in entitiesFromDb)
			{
				int index = missingEntityIds[entity.Id];
				entitiesInMemory[index] = entity;
				if (CacheVersionService.Metadata.EnableCache)
				{
					CacheService.Set(CacheVersionService.GetCacheKeyOfEntity(entity.Id), entity, CacheVersionService.Metadata.CachingExpirationType);
				}
			}
		}

		return entitiesInMemory;
	}

	/// <summary>
	/// 从缓存或数据库获取实体（异步）
	/// </summary>
	private async Task<TEntity?[]> FetchEntitiesWithCacheAsync(List<TKey> entityIdList)
	{
		var entitiesInMemory = new TEntity?[entityIdList.Count];
		var missingEntityIds = new Dictionary<TKey, int>();
		var entityCacheKeys = await CacheVersionService.GetCacheKeyOfEntitysAsync(entityIdList);

		for (int i = 0; i < entityIdList.Count; i++)
		{
			TKey entityId = entityIdList[i];
			string cacheKey = entityCacheKeys[entityId.ToString() ?? throw new ArgumentNullException(nameof(entityId))];
			TEntity? cachedEntity = await CacheService.GetAsync<TEntity>(cacheKey);
			entitiesInMemory[i] = cachedEntity ?? null;
			if (cachedEntity == null) missingEntityIds[entityId] = i;
		}

		if (missingEntityIds.Any())
		{
			var entitiesFromDb = await base.Query(x => missingEntityIds.Keys.Contains(x.Id)).ToArrayAsync();
			foreach (var entity in entitiesFromDb)
			{
				int index = missingEntityIds[entity.Id];
				entitiesInMemory[index] = entity;
				if (CacheVersionService.Metadata.EnableCache)
				{
					string cacheKey = await CacheVersionService.GetCacheKeyOfEntityAsync(entity.Id);
					await CacheService.SetAsync(cacheKey, entity, CacheVersionService.Metadata.CachingExpirationType);
				}
			}
		}

		return entitiesInMemory;
	}

	/// <summary>
	/// 过滤有效实体
	/// </summary>
	private IEnumerable<TEntity> FilterValidEntities(TEntity?[] entities)
	{
		var validEntities = entities
			.Where(e => e != null && (!(e is IDelEntity<TKey> del) || !del.IsDeleted))
			.Cast<TEntity>()
			.ToList();
		return validEntities.Any() ? validEntities : Enumerable.Empty<TEntity>();
	}

	/// <summary>
	/// 获取分页实体（同步，主键不缓存，实体缓存）
	/// </summary>
	/// <param name="applyQuery">应用查询的函数</param>
	/// <param name="pageIndex">页码，默认 1</param>
	/// <param name="pageSize">每页大小，默认 20</param>
	/// <returns>分页结果</returns>
	protected virtual IPagedList<TEntity> GetPagedEntities(
		Func<ISugarQueryable<TEntity>, ISugarQueryable<TEntity>> applyQuery,
		int pageIndex = 1,
		int pageSize = 20)
	{
		var stopwatch = Stopwatch.StartNew();
		var pagedEntityIds = PagingEntityIds(applyQuery, pageIndex, pageSize);
		var entities = GetEntitiesByIds(pagedEntityIds.Items);
		stopwatch.Stop();

		return new PagedList<TEntity>(entities)
		{
			Total = pagedEntityIds.Total,
			PageIndex = pageIndex,
			PageSize = pageSize,
			QueryDuration = stopwatch.ElapsedMilliseconds
		};
	}

	/// <summary>
	/// 获取分页实体（异步，主键不缓存，实体缓存）
	/// </summary>
	/// <param name="applyQuery">应用查询的函数</param>
	/// <param name="pageIndex">页码，默认 1</param>
	/// <param name="pageSize">每页大小，默认 20</param>
	/// <returns>分页结果</returns>
	protected virtual async Task<IPagedList<TEntity>> GetPagedEntitiesAsync(
		Func<ISugarQueryable<TEntity>, ISugarQueryable<TEntity>> applyQuery,
		int pageIndex = 1,
		int pageSize = 20)
	{
		var stopwatch = Stopwatch.StartNew();
		var pagedEntityIds = await PagingEntityIdsAsync(applyQuery, pageIndex, pageSize);
		var entities = await GetEntitiesByIdsAsync(pagedEntityIds.Items);
		stopwatch.Stop();

		return new PagedList<TEntity>(entities)
		{
			Total = pagedEntityIds.Total,
			PageIndex = pageIndex,
			PageSize = pageSize,
			QueryDuration = stopwatch.ElapsedMilliseconds
		};
	}

	#endregion
}