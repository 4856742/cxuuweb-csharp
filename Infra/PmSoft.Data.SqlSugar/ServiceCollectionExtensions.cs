﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PmSoft.Data.Abstractions;
using PmSoft.Data.SqlSugar.Repositories;

namespace PmSoft.Data.SqlSugar;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddSqlSugar(this IServiceCollection services, Action<SqlSugarConfigBuilder>? builder = null)
	{
		var configBuilder = new SqlSugarConfigBuilder(services);
		builder?.Invoke(configBuilder);
		services.TryAddScoped<IUnitOfWork, SqlSugarUnitOfWork>();
		services.TryAddScoped(typeof(ICacheRepository<,,>), typeof(CacheRepository<,,>));
		services.TryAddScoped(typeof(ICachedEntityLoader<>), typeof(CachedEntityLoader<>));
		return services;
	}
}
